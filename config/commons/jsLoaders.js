/**
 * Generate JavaScript loaders configuration
 * @param {String} appDirectory
 * @param {Object} envConfig
 * @returns {Array}
 */
function getJSLoaders(appDirectory, envConfig) {
  const { IS_PRODUCTION } = envConfig;

  const babelLoader = {
    options: {
      cacheDirectory: !IS_PRODUCTION
    },
    loader: 'babel-loader'
  };

  let JS_LOADERS = [babelLoader];
  if (IS_PRODUCTION) {
    JS_LOADERS.push('strip-loader?strip[]=console.debug,strip[]=console.log');
  }

  return JS_LOADERS;
}

module.exports = getJSLoaders;
