const getHtmlConfig = require('./htmlPluginConfig');
const getCopyConfig = require('./copyPluginConfig');
const getJSLoaders = require('./jsLoaders');

module.exports = {
  getHtmlConfig,
  getCopyConfig,
  getJSLoaders
};
