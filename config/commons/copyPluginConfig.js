const path = require('path');
//Use copy webpack plugin only if it's needed to preserve static assets paths
const CopyWebpackPlugin = require('copy-webpack-plugin');

/**
 * Generate webpack copy plugin config
 * @param {String} appDirectory
 * @param {String} outputPath
 * @param {Object} envConfig
 * @param {Array} favicons
 * @returns {Array} copy plugin config
 */
function getCopyConfig(appDirectory, outputPath, envConfig, favicons) {
  const { IS_CORDOVA, IS_PRODUCTION, AVATAR_FILE_NAMES } = envConfig;

  let MAPBOX_LIBRARY = ['mapbox-gl.css' /* , 'svg' */];
  MAPBOX_LIBRARY = [...MAPBOX_LIBRARY, IS_PRODUCTION ? 'mapbox-gl.js' : 'mapbox-gl-dev.js'];
  if (!IS_CORDOVA && IS_PRODUCTION) {
    MAPBOX_LIBRARY = [...MAPBOX_LIBRARY, 'mapbox-gl.js.map'];
  }

  const AppInsightsLibrary = new CopyWebpackPlugin([
    {
      context: appDirectory,
      from: 'node_modules/applicationinsights-js/dist/ai.js',
      to: `${outputPath}/ai.js`
    }
  ]);

  const MapboxLibrary = new CopyWebpackPlugin(
    MAPBOX_LIBRARY.map(mapboxFileOrFolder => {
      return {
        context: appDirectory,
        from: `node_modules/mapbox-gl/dist/${mapboxFileOrFolder}`,
        to: `${outputPath}/${mapboxFileOrFolder}`
      };
    })
  );

  const NumeralLibrary = new CopyWebpackPlugin([
    {
      context: appDirectory,
      from: envConfig.IS_PRODUCTION
        ? 'node_modules/numeral/min/numeral.min.js'
        : 'node_modules/numeral/numeral.js',
      to: `${outputPath}/numeral.js`
    }
  ]);

  const MapStylePictures = new CopyWebpackPlugin([
    {
      context: appDirectory,
      from: 'src/assets/mapSettings',
      to: `${outputPath}/assets/mapSettings`
    }
  ]);

  const UserAvatars = new CopyWebpackPlugin(
    AVATAR_FILE_NAMES.map(fname => {
      return {
        context: appDirectory,
        from: `src/assets${fname}`,
        to: `${outputPath}/${fname}`
      };
    })
  );

  const SvgIcons = new CopyWebpackPlugin([
    {
      context: appDirectory,
      from: 'src/assets/home/',
      to: `${outputPath}/assets/home/`
    },
    {
      context: appDirectory,
      from: 'src/assets/hazard_badges/',
      to: `${outputPath}/assets/hazard_badges/`
    },
    {
      context: appDirectory,
      from: 'src/assets/monthly_awards/',
      to: `${outputPath}/assets/monthly_awards/`
    },
    {
      context: appDirectory,
      from: 'src/assets/unlocked_contents/',
      to: `${outputPath}/assets/unlocked_contents/`
    },
    {
      context: appDirectory,
      from: 'src/assets/levels/',
      to: `${outputPath}/assets/levels/`
    },
    {
      context: appDirectory,
      from: 'src/assets/medals/',
      to: `${outputPath}/assets/medals/`
    },
    {
      context: appDirectory,
      from: 'src/assets/badges/',
      to: `${outputPath}/assets/badges/`
    },
    {
      context: appDirectory,
      from: 'src/assets/misc/',
      to: `${outputPath}/assets/misc/`
    }
  ]);

  const ICON_ASSETS = new CopyWebpackPlugin(
    favicons.map(assetFileName => {
      return {
        context: appDirectory,
        from: `src/assets/icons/${assetFileName}`,
        to: `${outputPath}/${assetFileName}`
      };
    })
  );

  const supportedLanguages = require(path.join(appDirectory, 'src', 'locale', 'languages.json'));

  const TRANSLATIONS = new CopyWebpackPlugin([
    {
      context: appDirectory,
      from: 'src/locale',
      to: `${outputPath}/locales`
    }
  ]);

  function getNumeralLocaleName(langCode) {
    let lc = langCode;
    switch (langCode) {
      case 'da':
        lc = 'da-dk';
        break;
      case 'en':
        lc = 'en-gb';
        break;
      default:
        break;
    }

    return lc;
  }

  const NUMERAL_TRANSLATIONS = new CopyWebpackPlugin(
    Object.keys(supportedLanguages).map(langCode => {
      const numloc = getNumeralLocaleName(langCode);
      return {
        context: appDirectory,
        from: envConfig.IS_PRODUCTION
          ? `node_modules/numeral/min/locales/${numloc}.min.js`
          : `node_modules/numeral/locales/${numloc}.js`,
        to: `${outputPath}/numeralLocales/${numloc}.js`
      };
    })
  );

  return [
    MapStylePictures,
    UserAvatars,
    AppInsightsLibrary,
    MapboxLibrary,
    NumeralLibrary,
    ICON_ASSETS,
    TRANSLATIONS,
    NUMERAL_TRANSLATIONS,
    SvgIcons
  ];
}

module.exports = getCopyConfig;
