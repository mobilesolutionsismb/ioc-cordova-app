const webpackTemplate = require('html-webpack-template');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

/**
 * Generate html plugin (template) config and other assets
 * @param {String} appDirectory
 * @param {Object} envConfig
 * @param {String} contentSecurityPolicy
 * @param {String} appMountId
 * @returns {Object} html plugin config and variables
 */
function getHtmlConfig(appDirectory, envConfig, contentSecurityPolicy, appMountId) {
  const {
    IS_PRODUCTION,
    PUBLIC_PATH,
    IS_CORDOVA,
    IREACT_BACKEND,
    SOCIAL_BACKEND,
    CLIENT_TYPE,
    title
  } = envConfig;

  const CHROME_MOBILE_THEME_COLOR = '#000000';

  const FAVICONS = [
    'apple-touch-icon.png',
    'favicon-32x32.png',
    'favicon-16x16.png',
    'manifest.json',
    //See manifest.json for these
    'android-chrome-36x36.png',
    'android-chrome-48x48.png',
    'android-chrome-72x72.png',
    'android-chrome-96x96.png',
    'android-chrome-144x144.png',
    'android-chrome-192x192.png',
    'android-chrome-256x256.png'
  ];

  //See http://realfavicongenerator.net/blog/new-favicon-package-less-is-more/
  const FAVICONS_LINKS = [
    {
      rel: 'apple-touch-icon',
      sizes: '180x180',
      href: `${PUBLIC_PATH}apple-touch-icon.png`
    },
    {
      rel: 'icon',
      sizes: '32x32',
      href: `${PUBLIC_PATH}favicon-32x32.png`
    },
    {
      rel: 'icon',
      sizes: '16x16',
      href: `${PUBLIC_PATH}favicon-16x16.png`
    },
    {
      rel: 'manifest',
      href: `${PUBLIC_PATH}manifest.json`
    }
  ];

  const mapboxGLScript = {
    src: `${PUBLIC_PATH}${IS_PRODUCTION ? 'mapbox-gl.js' : 'mapbox-gl-dev.js'}`,
    type: 'text/javascript'
  };

  const numeralJSScript = {
    src: `${PUBLIC_PATH}numeral.js`,
    type: 'text/javascript'
  };

  let SCRIPTS = IS_CORDOVA
    ? ['cordova.js', mapboxGLScript, numeralJSScript]
    : [mapboxGLScript, numeralJSScript];

  const ApplicationInsightsScript = {
    src: `${PUBLIC_PATH}ai.js`,
    type: 'text/javascript'
  };
  // TEMPORARY - use in production only
  SCRIPTS = [ApplicationInsightsScript, ...SCRIPTS];

  if (!IS_PRODUCTION && process.env.REACT_DEVTOOLS_URL) {
    console.log('Using REACT_DEVTOOLS_URL', process.env.REACT_DEVTOOLS_URL);
    // use with https://github.com/facebook/react-devtools/tree/master/packages/react-devtools
    SCRIPTS = [
      {
        src: process.env.REACT_DEVTOOLS_URL
        // type: 'text/javascript'
      },
      ...SCRIPTS
    ];
  }

  const htmlPluginConfig = {
    inject: false,
    template: webpackTemplate,
    appMountId: appMountId,
    mobile: true,
    links: IS_CORDOVA
      ? [`${PUBLIC_PATH}mapbox-gl.css`]
      : [`${PUBLIC_PATH}mapbox-gl.css`, ...FAVICONS_LINKS],
    meta: [
      {
        name: 'theme-color',
        content: CHROME_MOBILE_THEME_COLOR
      },
      {
        'http-equiv': 'Content-Security-Policy',
        content: contentSecurityPolicy
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
      },
      {
        name: 'apple-mobile-web-app-capable',
        content: 'yes'
      },
      {
        name: 'apple-mobile-web-app-status-bar-style',
        content: 'black'
      }
    ],
    lang: 'en',
    filename: 'index.html',
    title,
    chunks: ['vendor', 'app'],
    scripts: SCRIPTS,
    window: {
      CLIENT_TYPE: CLIENT_TYPE,
      IREACT_BACKEND: IREACT_BACKEND,
      SOCIAL_BACKEND: SOCIAL_BACKEND
    },
    headHtmlSnippet:
      '<style>html,body{background-color:color: rgb(48, 48, 48);color: rgb(255, 255, 255);}</style >',
    minify: IS_PRODUCTION
  };

  if (!IS_CORDOVA) {
    htmlPluginConfig['favicon'] = path.join(appDirectory, 'src', 'assets', 'icons', 'favicon.ico');
  }

  const HTML = new HtmlWebpackPlugin(htmlPluginConfig);

  return {
    CHROME_MOBILE_THEME_COLOR,
    FAVICONS,
    FAVICONS_LINKS,
    SCRIPTS,
    plugin: HTML
  };
}

module.exports = getHtmlConfig;
