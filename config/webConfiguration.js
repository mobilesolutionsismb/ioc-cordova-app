const CopyWebpackPlugin = require('copy-webpack-plugin');
const commons = require('./commons');
const path = require('path');
const Mustache = require('mustache');

async function getConfiguration(appDirectory, envConfig, contentSecurityPolicy, appMountId) {
  const outputPath = path.join(appDirectory, envConfig.DEST);
  const htmlPlugin = commons.getHtmlConfig(
    appDirectory,
    envConfig,
    contentSecurityPolicy,
    appMountId
  );
  const favicons = htmlPlugin.FAVICONS;
  const copyPlugins = commons.getCopyConfig(appDirectory, outputPath, envConfig, favicons);

  // For new report on Web UI when WebRTC is unsupported
  const FAKE_IMAGES = new CopyWebpackPlugin(
    favicons.map(assetFileName => {
      return {
        context: appDirectory,
        from: 'src/assets/fakeImages',
        to: `${outputPath}/fakeImages`
      };
    })
  );
  // For azure deploy
  const WEB_CONFIG = new CopyWebpackPlugin([
    {
      context: appDirectory,
      from: './web.config',
      to: `${outputPath}/web.config`
    },
    {
      context: appDirectory, // For apache deploy
      from: './htaccess.mustache',
      toType: 'file',
      to: '.htaccess',
      transform(content, path) {
        return Mustache.render(content.toString('utf8'), { publicPath: envConfig.PUBLIC_PATH });
      }
    }
  ]);
  const _copyPlugins = [WEB_CONFIG, FAKE_IMAGES, ...copyPlugins];

  return Promise.resolve({
    outputPath,
    htmlPlugin: htmlPlugin.plugin,
    copyPlugins: _copyPlugins
  });
}

module.exports = { getConfiguration };
