const path = require('path');
const fs = require('fs-extra');
const xmlJS = require('xml-js');
const webpack = require('webpack');

async function getCordovaAttributes(appDirectory, isCordova) {
  const cordovaAttributes = {};
  const cfgName = 'default';

  if (isCordova) {
    try {
      const cfgPathName = path.resolve(appDirectory, 'mobileAppConfigs', cfgName, 'config.xml');
      const file = await fs.readFile(cfgPathName, 'utf8');
      const cordovaConfigObject = xmlJS.xml2js(file, { ignoreDeclaration: true });
      const attributes = cordovaConfigObject.elements[0].attributes;
      cordovaAttributes['bundleIdentifier'] = attributes['id'];
      cordovaAttributes['androidApkVersion'] = attributes['android-versionCode'];
      cordovaAttributes['iosBundleVersion'] = attributes['ios-CFBundleVersion'];
    } catch (err) {
      console.error(`Cannot parse Cordova config.xml for ${cfgName}`, err);
      throw err;
    }
  } else {
    cordovaAttributes['bundleIdentifier'] = 'web interface';
  }
  return cordovaAttributes;
}

function getGeoserverURL(url) {
  return typeof url === 'string'
    ? /geoserver\/?$/.test(url)
      ? /\/$/.test(url)
        ? url
        : url + '/'
      : `${url.replace(/\/$/, '')}/geoserver/`
    : null;
}

const buildDate = new Date();

function padInt(v, amount = 2, str = '0') {
  return `${v}`.padStart(amount, str);
}

async function getEnvConfiguration(appDirectory, pkg, env, mountId, supportedLanguages) {
  const ENVIRONMENT = env.NODE_ENV || 'development';
  const IS_PRODUCTION = ENVIRONMENT === 'production';
  const IS_CORDOVA = env.IS_CORDOVA === 'true';

  const DEPLOY_PATH =
    process.env.DEPLOY_PATH && ENVIRONMENT !== 'debug_app' ? process.env.DEPLOY_PATH : 'default';

  const IREACT_BACKEND = process.env.IREACT_BACKEND;

  const SOCIAL_BACKEND = process.env.SOCIAL_BACKEND;

  const DEST = IS_CORDOVA
    ? 'www'
    : DEPLOY_PATH === 'default'
    ? 'dist'
    : `build_${buildDate.getFullYear()}_${padInt(buildDate.getMonth() + 1)}_${padInt(
        buildDate.getDate()
      )}_${padInt(buildDate.getHours())}_${padInt(buildDate.getMinutes())}`;

  const PUBLIC_PATH = IS_CORDOVA ? '' : DEPLOY_PATH === 'default' ? '/' : `${DEPLOY_PATH}${DEST}/`;

  const CLIENT_TYPE = 'mobile-app'; // for ireact api presets

  const title = pkg.title; // app display title

  // User Avatars
  const AVATAR_NUMBER = 15;
  const AVATAR_FILE_NAMES = [...Array(AVATAR_NUMBER).keys()].map(
    n => `/avatars/avatar-${('' + n).padStart(2, '0')}.svg`
  );

  const cordovaAttributes = await getCordovaAttributes(appDirectory, IS_CORDOVA);

  const DEFINITIONS = new webpack.DefinePlugin({
    TITLE: JSON.stringify(title),
    VERSION: JSON.stringify(pkg.version),
    PKG_NAME: JSON.stringify(pkg.name),
    DESCRIPTION: JSON.stringify(pkg.description),
    ENVIRONMENT: JSON.stringify(ENVIRONMENT),
    IS_PRODUCTION: JSON.stringify(IS_PRODUCTION),
    BUILD_DATE: JSON.stringify(buildDate),
    APP_MOUNT_ID: JSON.stringify(mountId),
    IS_CORDOVA: JSON.stringify(IS_CORDOVA),
    CORDOVA_APP_ATTRIBUTES: JSON.stringify(cordovaAttributes),
    // Number of Avatars
    AVATAR_FILE_NAMES: JSON.stringify(
      AVATAR_FILE_NAMES
      // AVATAR_FILE_NAMES.map(avatarFname => `${PUBLIC_PATH.replace(/\/$/, '')}${avatarFname}`)
    ),
    //This tells which languages are supported by this app
    SUPPORTED_LANGUAGES: JSON.stringify(supportedLanguages),
    //Mapbox - OSM Tiles
    OSM_TILES_KEY: JSON.stringify(process.env.OSM_TILES_KEY),
    //Used by ReactJS to turn optimization on/off
    'process.env': {
      NODE_ENV: JSON.stringify(ENVIRONMENT)
    },
    IREACT_BACKEND: JSON.stringify(IREACT_BACKEND),
    SOCIAL_BACKEND: JSON.stringify(SOCIAL_BACKEND),
    WKT_BACKEND: JSON.stringify(process.env.WKT_BACKEND !== 'false'),
    RECAPTCHA_SITEKEY: JSON.stringify(pkg.config.RECAPTCHA_SITEKEY),
    MAP_SERVER_APIKEY: JSON.stringify(
      process.env.MAP_SERVER_APIKEY || pkg.config.MAP_SERVER_APIKEY
    ),
    MAP_SERVER_URL: JSON.stringify(process.env.MAP_SERVER_URL || pkg.config.MAP_SERVER_URL),
    MAP_STYLES_URL: JSON.stringify(process.env.MAP_STYLES_URL || pkg.config.MAP_STYLES_URL),
    GEOSERVER_URL: JSON.stringify(
      getGeoserverURL(process.env.GEOSERVER_URL || pkg.config.GEOSERVER_URL)
    ), // this is now optional and IF SET will override the ones coming from server
    USE_WMTS: JSON.stringify(process.env.USE_WMTS === 'true'), // enable WMTS mode for raster layer - experimental
    GEONAMES_API_CREDENTIALS: JSON.stringify({
      username: process.env.GEONAMES_API_USERNAME,
      password: process.env.GEONAMES_API_PASSWORD
    }),
    APP_INSIGHTS_INSTR_KEY: JSON.stringify(process.env.APP_INSIGHTS_INSTR_KEY),
    PUBLIC_PATH: JSON.stringify(PUBLIC_PATH),
    TOS_URL: JSON.stringify(process.env.TOS_URL),
    PRIVACY_URL: JSON.stringify(process.env.PRIVACY_URL)
  });

  return {
    title,
    ENVIRONMENT,
    IS_PRODUCTION,
    IS_CORDOVA,
    IREACT_BACKEND,
    SOCIAL_BACKEND,
    AVATAR_FILE_NAMES,
    DEFINITIONS,
    CLIENT_TYPE,
    PUBLIC_PATH,
    DEST
  };
}

module.exports = {
  getEnvConfiguration
};
