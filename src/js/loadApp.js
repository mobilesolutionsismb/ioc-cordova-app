// import 'react-hot-loader/patch';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
//import 'mapbox-gl/dist/mapbox-gl.css';
import 'style/style.scss';
import { API } from 'ioc-api-interface';
import nop from 'nop';
import { AppLoading } from 'js/startup/AppLoading';
import App from 'js/startup/App';
import Root from 'js/startup/Root';
import { globalErrorHandler, globalPromiseErrorHandler } from 'js/startup/errorHandlingAndTracking';
import { pushError, pushMessage, pushModalMessage, popModalMessage } from 'js/modules/ui';
import { pause, resume, ready } from 'js/modules/appStatus';
import { watchNetworkEvents, checkConnection } from 'js/modules/networkInformation';
import { checkPushNotificationsAvailable } from 'js/modules/pushNotifications';
import { updateRemoteUserDataIfNeeded } from 'js/utils/updateRemotePosition';
import { loadDictionary } from 'js/utils/getAssets';
import { newReportActions } from 'js/modules/newReport2';
import { newReportTransform, localeTransform } from 'js/startup/persistTransforms';
import { appInfo } from 'js/utils/getAppInfo';
import {
  checkBTEnabled,
  checkBTIsConnected
} from 'js/modules/bluetooth/dataSources/ActionCreators';
import { logMain, logError, logSevereWarning } from 'js/utils/log';

logMain('APP INFO', appInfo, 'WKT_BACKEND', WKT_BACKEND, IREACT_BACKEND);
/**
 * init the app
 * @param {App} app
 * @param {Function} done
 * @param {Function} fail
 */
function onInit(app, done, fail) {
  try {
    // await checkStoreAndAppVersion(app);
    // await registerReducers(app);
    logMain(`Application '${appInfo.title}' is starting...`);

    done();
  } catch (err) {
    fail(err);
  }
}

// Now integrated in redux persist
// See https://github.com/rt2zz/redux-persist/issues/517#issuecomment-348632092
async function onReady(app) {
  const getState = app.store.getState;
  // const state = getState();
  const dispatch = app.store.dispatch;
  try {
    // const localeState = state.locale;
    //  dispatch(checkStartupState());

    // const dictIsEmpty = localeState.dictionary.isEmpty;
    // const beforeDone = () => {
    //WARN - should be moved BEFORE this could need a polyfill on non-chrome browswers
    window.addEventListener('unhandledrejection', globalPromiseErrorHandler.bind(null, dispatch));
    window.onerror = globalErrorHandler.bind(null, dispatch);
    const api = API.getInstance();
    api.reduxStore = app.store;

    await loadInitialDictionary(app.store, api);
    // if (!IS_CORDOVA) {
    //   window.addEventListener('resize', () => {
    //     console.log('Porcazzazozza');
    //     window.location.reload();
    //   });
    // }

    dispatch(checkPushNotificationsAvailable());
    dispatch(checkConnection());
    dispatch(watchNetworkEvents(true));
    checkWereableStatus(dispatch, getState);

    if (ENVIRONMENT.startsWith('debug')) {
      app.pushError = (...params) => {
        dispatch(pushError(...params));
      };
      app.pushMessage = (...params) => {
        dispatch(pushMessage(...params));
      };
      app.openModal = (...params) => {
        return dispatch(pushModalMessage(...params));
      };
      app.closeModal = () => {
        dispatch(popModalMessage());
      };

      if (!IS_CORDOVA) {
        const pnModule = await import('js/modules/pushNotifications/pnMock');
        app.testPushNotification = pnModule.pnMock(app.store.dispatch);
      }
      app.api = api;
      if (!app.api) {
        const errMsg = 'I-REACT api not initialized!';
        console.error(errMsg);
      }
      window.app = app;
    }

    newReportActions.initializeReportQueueAtStartup()(dispatch);

    updateRemoteUserDataIfNeeded(app.store.dispatch, app.store.getState);
  } catch (err) {
    logError(err);
  }

  if (navigator.splashscreen) {
    navigator.splashscreen.hide();
  }

  dispatch(ready());

  return true;
}

// const onHotReload = function(root, renderCb) {
//   if (module.hot) {
//     module.hot.accept('js/startup/Root', () => {
//       // const Next = require('js/startup').Root;
//       console.debug('♨️ Hot reload');
//       renderCb(root, Root);
//     });
//   }
// };

// const transforms = [newReportTransform, apiTransform, localeTransform/* , preferenceTransform */];

// const whitelist = ['locale', 'api', 'newReport', 'geolocation', 'map', 'preferences'];

const transforms = [
  /* appVersionCheckTransform, */ newReportTransform,
  localeTransform /* , preferenceTransform */
];

const persistingAPIModules = [
  //   'api_agentLocations',
  'api_authentication',
  'api_mobileApp',
  'api_categories',
  'api_gamification',
  //   'api_emergencyCommunications',
  //   'api_emergencyEvents',
  'api_enums',
  //   'api_ireactFeatures',
  'api_liveParams',
  'api_layers',
  //   'api_mapRequests',
  //   'api_missions',
  'api_organizationUnits',
  //   'api_reports',
  // 'api_roles',
  'api_mobileApp_tweets'
  // 'api_settings'
]; // for now everything, then we'll remove unnecessary things

// store whitelist - what is to be stored
const whitelist = [
  'locale',
  'geolocation',
  'map',
  'bluetooth',
  'devicePermissions',
  'partialRegistration',
  'preferences',
  'layersAndSettings',
  'gamificationAwards',
  ...persistingAPIModules
];

async function checkWereableStatus(dispatch, getState) {
  const state = getState();
  let connected = false;
  if (state.api_authentication.logged_in && state.api_authentication.user.userType !== 'citizen') {
    const btState = state.bluetooth;
    if (!btState.isBtEnabled) {
      await checkBTEnabled()(dispatch, getState);
    }
    connected = await checkBTIsConnected()(dispatch, getState);
  }
  return connected;
}

export async function loadInitialDictionary(store, api) {
  const state = store.getState();
  const localeState = state.locale;
  const locale = state.locale.locale;
  if (api.culture !== locale) {
    api.culture = locale;
  }

  const dictIsEmpty = localeState.dictionary.isEmpty;
  if (dictIsEmpty) {
    const params = await loadDictionary(localeState.locale);
    const { locale, translationLoader, loading } = params;
    if (loading !== true) {
      return new Promise(success => {
        translationLoader(translations => {
          store.dispatch({
            type: 'LOCALE@LOCALE_CHANGED',
            locale: locale,
            translations
          });
        });
        success();
      });
    } else {
      return Promise.resolve();
    }
  } else {
    return Promise.resolve();
  }
}

// async function start() {
//   app.start(`#${APP_MOUNT_ID}`, Root, Loading);
// }
function addCordovaHandlers(app) {
  if (IS_CORDOVA) {
    let btWasConnected = false;
    let btDevice = null;

    document.addEventListener(
      'pause',
      async () => {
        logMain('Pause');
        app.store.dispatch(pause());
        app.store.dispatch(watchNetworkEvents(false));
        // BLE DISCONNECT ? there is no way to distinguish between app terminated or background
        try {
          const isConnected = await checkWereableStatus(app.store.dispatch, app.store.getState);
          if (isConnected) {
            btWasConnected = true;
            btDevice = app.store.getState().bluetooth.selectedDevice;
            //   disconnectDevice()(app.store.dispatch, app.store.getState);
          }
        } catch (err) {
          logError(err);
        }
      },
      false
    );
    document.addEventListener(
      'resume',
      async () => {
        logMain('Resume');
        updateRemoteUserDataIfNeeded(app.store.dispatch, app.store.getState);
        app.store.dispatch(watchNetworkEvents(true));
        app.store.dispatch(resume());
        // BLE RECONNECT ?
        try {
          if (btWasConnected && btDevice) {
            const isConnected = await checkWereableStatus(app.store.dispatch, app.store.getState);
            // connectDevice(btDevice)(app.store.dispatch, app.store.getState).catch(err => {
            if (btWasConnected && !isConnected) {
              logSevereWarning('Cannot reconnect wereable device');
            }
            // });
          }
        } catch (err) {
          logError(err);
        }
      },
      false
    );
  }
}

export default function launch() {
  const app = new App({
    appInfo,
    onInit,
    onReady,
    persistConfig: { whitelist, transforms },
    nop
  });
  if (IS_CORDOVA) {
    addCordovaHandlers(app);
  }
  app.start(`#${APP_MOUNT_ID}`, Root, AppLoading);
}
