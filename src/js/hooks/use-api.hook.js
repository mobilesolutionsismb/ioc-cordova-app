import { useState, useEffect } from 'react';

export function useAPI(apiFunction, params, initialData = null) {
  const [cancel, setCancel] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [data, setData] = useState(initialData);

  function onLoad(isLoading) {
    if (cancel !== null) {
      setCancel(null);
    }
    setLoading(isLoading);
  }

  useEffect(() => {
    if (loading === false) {
      apiFunction(params, setData, setError, onLoad, setCancel);
    } else {
      if (cancel !== null) {
        cancel();
        apiFunction(params, setData, setError, onLoad, setCancel);
      }
    }
  }, [params]);

  return [data, error, loading];
}
