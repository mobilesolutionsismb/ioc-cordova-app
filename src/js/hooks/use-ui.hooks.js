import { useRedux } from 'js/hooks/use-redux.hook';
import { messagesActionCreators, messagesSelector } from 'js/modules/ui/dataProviders/withMessages';
import { loadingActionCreators, loadingSelector } from 'js/modules/ui/dataProviders/withLoader';
import { drawerActionCreators, drawerSelector } from 'js/modules/ui/dataProviders/withDrawer';

export function useUiMessages() {
  /* eslint-disable */
  const [selector, actions, dispatch] = useRedux(messagesSelector, messagesActionCreators);
  /* eslint-enable */
  const { pushMessage, pushError, pushModalMessage } = actions;
  return [/* selector, */ { pushMessage, pushError, pushModalMessage }, dispatch];
}

export function useLoader() {
  const [{ loading }, actions, dispatch] = useRedux(loadingSelector, loadingActionCreators);

  return [{ loading: loading.status }, actions, dispatch];
}

export function useDrawer() {
  const [selector, actions, dispatch] = useRedux(drawerSelector, drawerActionCreators);

  return [selector, actions, dispatch];
}
