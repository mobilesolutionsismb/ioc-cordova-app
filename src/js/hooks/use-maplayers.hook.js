import { useRedux } from 'js/hooks/use-redux.hook';
import { mapLayersSelector, mapLayersActionCreators } from 'ioc-api-interface';

export function useMapLayers() {
  const [selector, actions, dispatch] = useRedux(mapLayersSelector, mapLayersActionCreators);

  return [selector, actions, dispatch];
}
