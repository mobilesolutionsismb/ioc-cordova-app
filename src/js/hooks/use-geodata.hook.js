import { useAPI } from 'js/hooks/use-api.hook';
import { getGeodata } from 'js/utils/geodata-utils/geodata-api';

export function useGeodata(params, initialData = null) {
  const [data, error, loading] = useAPI(getGeodata, params, initialData);
  return [data, error, loading];
}
