import { useRedux } from 'js/hooks/use-redux.hook';
import { loginSelector, loginActionCreators } from 'ioc-api-interface';
export function useLogin() {
  const [selector, actions, dispatch] = useRedux(loginSelector, loginActionCreators);

  return [selector, actions, dispatch];
}
