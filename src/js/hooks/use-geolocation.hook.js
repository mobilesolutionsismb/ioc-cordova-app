import { useRedux } from 'js/hooks/use-redux.hook';
import { geolocationSelector, geolocationActionCreators } from 'ioc-geolocation';
export function useGeolocation() {
  const [selector, actions, dispatch] = useRedux(geolocationSelector, geolocationActionCreators);
  return [selector, actions, dispatch];
}
