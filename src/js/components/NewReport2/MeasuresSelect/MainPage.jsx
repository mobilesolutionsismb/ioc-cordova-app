import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import numeral from 'numeral';
import { List, ListItem, RadioButton, RadioButtonGroup } from 'material-ui';
import QuantitativeValueItem from './QuantitativeValueItem';
import { getNumeralLocaleName } from 'js/utils/getAssets';

const INNER_ITEM_HEIGHT = 90;
const MAX_RADIO_BUTTONS_PER_ROW = 4;

const STYLES = {
  radio: {
    height: INNER_ITEM_HEIGHT, //'100%',
    width: `${Math.floor(100 / MAX_RADIO_BUTTONS_PER_ROW)}%`
  },
  radioIcon: {
    float: 'none',
    marginRight: 'calc(50% - 12px)',
    marginLeft: 'calc(50% - 12px)'
  },
  radioLabel: {
    position: 'absolute',
    float: 'none',
    display: 'block',
    top: 32,
    left: 0,
    width: '100%',
    textAlign: 'center',
    overflowX: 'hidden',
    textOverflow: 'ellipsis',
    // whiteSpace: 'nowrap',
    fontSize: 14
  },
  innerList: { margin: 0, padding: 4 }, //, height: INNER_ITEM_HEIGHT },
  nested: { margin: 0 }
};
// const NUMBER_FORMAT = '0.[000]';

const OptionsRadioButtonGroup = ({
  options,
  dictionary,
  name,
  measureId,
  onChange,
  selected,
  selectedOption,
  hasOverFlowingHeight,
  selectedColor
}) => (
  <RadioButtonGroup
    className="radio-group"
    name={name}
    valueSelected={selectedOption}
    style={hasOverFlowingHeight ? { justifyContent: 'flex-start' } : {}}
    //onChange={(e, selectedId) => onChange({ id: measureId, measureType: 'qualitative', selectedOption: selectedId })}
  >
    {options.map((opt, i) => (
      <RadioButton
        key={i}
        className="radio-group__item"
        iconStyle={STYLES.radioIcon}
        style={STYLES.radio}
        // style={{ ...STYLES.radio, width: `${options.length ? 100 / options.length : 100}%` }}
        labelStyle={
          selected && selectedOption === opt.id
            ? { ...STYLES.radioLabel, color: selectedColor }
            : STYLES.radioLabel
        }
        //                    labelStyle={{ ...STYLES.radioLabel, fontSize: getFontSize(options.length) }}
        value={opt.id}
        label={dictionary(opt.label)}
        onClick={() =>
          onChange({
            id: measureId,
            measureType: 'qualitative',
            selectedOption: opt.id,
            selected: selectedOption === opt.id
          })
        }
      />
    ))}
  </RadioButtonGroup>
);

class MeasureListItem extends Component {
  // _autoref = null;

  // _setAutoRef = e => (this._autoref = e);

  _setVisible = listItem => {
    const node = ReactDOM.findDOMNode(listItem);
    console.log('listItem', listItem);
    if (node) {
      if (IS_CORDOVA && cordova.platformId === 'ios') {
        node.scrollIntoViewIfNeeded();
      } else {
        node.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'center' });
      }
    }
  };

  render() {
    const { measure, dictionary, palette, onSelect, totalMeasures } = this.props;
    const type = measure.measureType;
    const title = dictionary(measure.title);
    // console.log('MeasureListItem', title, type, measure);
    const innerHeight =
      type === 'qualitative'
        ? INNER_ITEM_HEIGHT * Math.ceil(measure.options.length / MAX_RADIO_BUTTONS_PER_ROW)
        : INNER_ITEM_HEIGHT;
    return (
      <ListItem
        // ref={this._setAutoRef}
        className="measure-list-item"
        initiallyOpen={measure.selected || totalMeasures <= 3}
        style={measure.selected ? { color: palette.primary1Color } : {}}
        nestedListStyle={STYLES.nested}
        primaryText={title}
        primaryTogglesNestedList={true}
        onNestedListToggle={this._setVisible}
        nestedItems={[
          <ListItem
            key={measure.id}
            className="inner-item"
            innerDivStyle={{ ...STYLES.innerList, height: innerHeight }}
          >
            {type === 'qualitative' && (
              <OptionsRadioButtonGroup
                measureId={measure.id}
                hasOverFlowingHeight={innerHeight > INNER_ITEM_HEIGHT}
                options={measure.options}
                name={measure.name}
                dictionary={dictionary}
                onChange={onSelect}
                selectedColor={palette.primary1Color}
                selected={measure.selected}
                selectedOption={
                  typeof measure.selectedOption === 'undefined' ? null : measure.selectedOption
                }
              />
            )}
            {type === 'quantitative' && (
              <QuantitativeValueItem
                dictionary={dictionary}
                measure={measure}
                onSelect={onSelect}
                numeral={numeral}
              />
            )}
          </ListItem>
        ]}
      />
    );
  }
}

class MeasuresSelect extends Component {
  static defaultProps = {
    measures: []
  };

  onMeasureItemSelect = selectedMeasure => {
    console.log('onMeasureItemSelect', selectedMeasure);
    const { onSelect, draft } = this.props;
    let newMeasures;
    if (selectedMeasure.measureType === 'qualitative') {
      if (selectedMeasure.selected) {
        // Deselect
        newMeasures = draft.measures.filter(m => m.id !== selectedMeasure.id);
      } else {
        // Select or update
        const newMeasure = { id: selectedMeasure.id, optionId: selectedMeasure.selectedOption };
        newMeasures = draft.measures.filter(m => m.id !== selectedMeasure.id).concat(newMeasure);
      }
    } else if (selectedMeasure.measureType === 'quantitative') {
      if (!selectedMeasure.hasValue === true) {
        // Deselect
        newMeasures = draft.measures.filter(m => m.id !== selectedMeasure.id);
      } else {
        // Select or update
        const newMeasure = {
          id: selectedMeasure.id,
          value: selectedMeasure.selectedValue,
          numericValue: selectedMeasure.numericValue
        };
        newMeasures = draft.measures.filter(m => m.id !== selectedMeasure.id).concat(newMeasure);
      }
    }
    onSelect(newMeasures);
  };

  componentDidMount() {
    const numeralLocale = getNumeralLocaleName(this.props.locale);
    numeral.locale(numeralLocale);
  }

  render() {
    const { dictionary, palette, /* userType, onSelect, */ measures } = this.props;
    return (
      <List className="measures-content-list listview" style={{ padding: 0 }}>
        {measures.items.length > 0 &&
          measures.items.map((m, i) => (
            <MeasureListItem
              key={i}
              totalMeasures={measures.items.length}
              measure={m}
              palette={palette}
              dictionary={dictionary}
              onSelect={this.onMeasureItemSelect}
            />
          ))}
        {measures.items.length === 0 && <div>No Measures</div>}
      </List>
    );
  }
}

export default MeasuresSelect;
