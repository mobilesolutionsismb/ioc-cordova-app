import React, { Component } from 'react';
import { TextField } from 'material-ui';
import { MEASURE_UNITS } from 'js/components/ReportsCommons';

const STYLES = {
    numberImput: {
        marginLeft: 32
    },
    floatingLabel: {
        fontSize: 14
    }
};

class QuantitativeValueItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errorText: '',
            valueString: props.measure.selected ? props.measure.selectedValue : ''
        };
    }

    _validateInput = (valueString) => {
        try {
            const { measure, numeral, dictionary, onSelect } = this.props;
            // validate does not work with negative numbers https://github.com/adamwdraper/Numeral-js/issues/213
            if (numeral.validate(valueString.replace(/^-/, ''))) {
                const num = numeral(valueString);
                const value = num.value();
                const nextVString = num.format('0.[000]');
                if (value >= measure.min && value <= measure.max) {
                    this.setState({ valueString: nextVString, errorText: '' });
                    // select
                    const newMeasure = {
                        id: measure.id,
                        measureType: 'quantitative',
                        selectedValue: nextVString,
                        numericValue: value,
                        hasValue: true
                    };
                    onSelect(newMeasure);
                }
                else {
                    this.setState({ valueString: nextVString, errorText: dictionary('_value_out_of_bounds', value, measure.min, measure.max) });
                }
            }
            else {
                if (valueString === '') {
                    const newMeasure = {
                        id: measure.id,
                        measureType: 'quantitative',
                        selectedValue: valueString,
                        hasValue: false
                    };
                    // deselect
                    this.setState({ valueString: valueString, errorText: '' });
                    onSelect(newMeasure);
                }
                else {
                    this.setState({ valueString: valueString, errorText: dictionary('_invalid_number', valueString) });
                }
            }
        }
        catch (e) {
            console.warn(e);
        }
    }

    onChange = (evt, valueString) => {
        this._validateInput(valueString);
    }

    render() {
        const { measure, dictionary } = this.props;
        const { min, max, measureUnit } = measure;
        const mu = MEASURE_UNITS.hasOwnProperty(measureUnit) ? MEASURE_UNITS[measureUnit] : measureUnit;
        return (
            <TextField
                type="text"
                style={STYLES.numberImput}
                id={`meas-${measure.id}`}
                name={measure.name}
                errorText={this.state.errorText}
                floatingLabelStyle={STYLES.floatingLabel}
                floatingLabelText={dictionary('_enter_value', mu, min, max)}
                value={this.state.valueString}
                onChange={this.onChange}
            />
        );
    }
}


export { QuantitativeValueItem };
export default QuantitativeValueItem;