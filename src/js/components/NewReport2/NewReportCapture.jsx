import React, { Component } from 'react';
import { withEmergencyEvents, withEmergencyCommunicationsApp } from 'ioc-api-interface';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import { withLoader, withMessages } from 'js/modules/ui';
import { withNewReport } from 'js/modules/newReport2';
import { withGeolocation } from 'ioc-geolocation';
import { withCamera } from 'js/modules/camera';
import { logMain, logWarning } from 'js/utils/log';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';

const enhance = compose(
  withGeolocation,
  withCamera,
  withNewReport,
  withLoader,
  withMessages,
  withEmergencyEvents,
  withEmergencyCommunicationsApp
);

const IS_CANCEL_RX = /no image selected|camera cancelled/i;

class NewReportCapture extends Component {
  // _deselectAnyContent = () => {
  //   // TODO add a proper action creator in API
  //   this.props.deselectReport();
  //   this.props.deselectEvent();
  //   this.props.deselectEmergencyCommunication();
  // };

  _capture = async () => {
    // this._deselectAnyContent();
    if (IS_CORDOVA) {
      this.props.loadingStart();
      try {
        await this.props.updatePosition();
        const picture = await this.props.takePicture(true);
        logMain('NewReportCapture:_capture', picture);
      } catch (err) {
        logWarning('NewReportCapture:_capture, Camera capture error', err);
        this._cameraError(err);
      } finally {
        this.props.loadingStop();
      }
    } else {
      this.props.history.replace(`/webcamCapture${this.props.location.search}`);
    }
  };

  _next = async props => {
    try {
      this.props.loadingStart();
      const picture = props.cameraImage;
      const { coords } = props.geolocationPosition;
      const { latitude, longitude } = coords;
      const position = {
        latitude,
        longitude
      };
      await props.createNewReportDraft(picture, position);
      props.history.replace(`/newreport${this.props.location.search}`);
      props.clearCamera();
    } catch (err) {
      logWarning('NewReportCapture:_next, cannot create draft', err);
      this._cameraError(err);
    } finally {
      props.loadingStop();
    }
  };

  _cameraError(err) {
    if (IS_CANCEL_RX.test(err.message)) {
      this.props.pushMessage('_capture_cancelled');
    } else {
      appInsights.trackException(err, 'NewReportCapture:_cameraError', {}, {}, SeverityLevel.Error);
      this.props.pushError(err);
    }
    this.props.history.replace('/');
    this.props.clearCamera();
    if (this.props.loading.status) {
      this.props.loadingStop();
    }
  }

  _doAction(props) {
    switch (props.cameraStatus) {
      case 'ready':
        if (props.cameraImage) {
          this._next(props);
        } else {
          this._capture();
        }
        break;
      case 'error':
        this._cameraError(props.cameraError);
        break;
      case 'busy':
        logWarning('Camera is busy');
        break;
      default:
        break;
    }
  }

  componentDidMount() {
    this._doAction(this.props);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.cameraStatus !== this.props.cameraStatus) {
      this._doAction(this.props);
    }
  }

  render() {
    return <div />;
  }
}

export default withRouter(enhance(NewReportCapture));
