import React, { Component } from 'react';
import Webcam from 'react-webcam';
import { withRouter } from 'react-router';
import { compose } from 'redux';
import { withCamera } from 'js/modules/camera';
import { withLoader, withMessages } from 'js/modules/ui';
import { withGeolocation } from 'ioc-geolocation';
import { withNewReport } from 'js/modules/newReport2';
import { ColumnPage } from 'js/components/app/commons';
import styled from 'styled-components';
import { iReactTheme } from 'js/startup/iReactTheme';
import { FloatingActionButton, FontIcon } from 'material-ui';
import { prepareImageForUpload } from 'js/modules/camera/prepareImage';

const enhance = compose(
  withGeolocation,
  withNewReport,
  withLoader,
  withMessages,
  withCamera
);

const FAB = ({ onClick, mini, secondary, backgroundColor, disabled, icon }) => (
  <FloatingActionButton
    mini={mini || undefined}
    backgroundColor={backgroundColor}
    secondary={secondary}
    disabled={disabled}
    iconStyle={{ color: iReactTheme.palette.textColor }}
    onClick={onClick}
  >
    <FontIcon className="material-icons">{icon}</FontIcon>
  </FloatingActionButton>
);

const Preview = styled.div`
  position: fixed;
  top: 0;
  width: 100%;
  height: 100%;
  background-size: contain;
  background-color: black;
  background-position: center;
  background-repeat: no-repeat;
  background-image: ${props => (props.src ? `url(${props.src})` : '')};
  opacity: ${props => (props.src ? 1 : 0)};
  transition: opacity 0.5s ease-in;
`;

const Actions = styled.div`
  position: fixed;
  bottom: 0;
  z-index: 10;
  width: 100%;
  height: 64px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  background-color: ${iReactTheme.palette.canvasColor};
  color: ${iReactTheme.palette.textColor};
`;

/**
 * A wrapper is necessary because of https://github.com/mozmorris/react-webcam/issues/47
 */
class WebcamWrapper extends Component {
  static defaultProps = {
    selectedSource: null
  };

  constructor(props) {
    super(props);
    this.state = {
      changing: false
    };
    this.component = React.createRef();
  }

  setRef = webcam => {
    this.webcam = webcam;
  };

  getWebcam = () => {
    return this.webcam;
  };

  render() {
    const { changing } = this.state;
    const { selectedSource } = this.props;
    return changing ? (
      <div />
    ) : (
      <Webcam
        audio={false}
        height={768}
        ref={this.setRef}
        screenshotFormat="image/jpeg"
        width={1024}
        videoSource={selectedSource}
        audioSource={'any'}
      />
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.component.current &&
      prevProps.selectedSource !== null &&
      prevProps.selectedSource !== this.props.selectedSource
    ) {
      this.setState({
        changing: true
      });
    }
  }
}

class WebcamCapture extends Component {
  state = {
    previewSrc: null,
    capturePosition: null,
    videoSource: -1,
    videoDevices: []
  };

  setRef = webcamWrapper => {
    this.webcamWrapper = webcamWrapper;
  };

  componentDidMount() {
    this._enumerateDevices();
  }

  _showFallBackMessage = () => {
    this.props.pushMessage('No video devices found, falling back with fake pictures', 'warning');
    this._fallBack();
  };

  // Use when media not available
  _fallBack = async () => {
    this.props.loadingStart();
    try {
      await this.props.updatePosition();
      const picture = await this.props.takePicture(true);
      console.log('PICTURE', picture);
      // this.props.createReport();
      const { coords } = this.props.geolocationPosition;
      const { latitude, longitude } = coords;
      const position = {
        latitude,
        longitude
      };
      await this.props.createNewReportDraft(picture, position);

      this.props.loadingStop();
      this.props.history.push(`/newreport${this.props.location.search}`);
    } catch (err) {
      console.warn('Capture failed', JSON.stringify(err), err);
      this.props.loadingStop();
      if (err.message && err.message.toLowerCase().startsWith('camera cancelled')) {
        this.props.pushMessage('_capture_cancelled');
      } else {
        this.props.pushError(err);
      }
    }
  };

  _enumerateDevices = async () => {
    try {
      const devices = await navigator.mediaDevices.enumerateDevices();
      const videoDevices = devices.filter(d => d.kind === 'videoinput').map(d => d.deviceId);
      if (videoDevices.length === 0) {
        this._showFallBackMessage();
      }
      this.setState({ videoDevices, videoSource: videoDevices.length - 1 });
    } catch (err) {
      console.error('Cannot enumerate devices', err);
      this._showFallBackMessage();
    }
  };

  capture = async imageSrc => {
    if (!this.webcamWrapper || !this.webcamWrapper.webcam) {
      return;
    }
    try {
      const imageSrc = this.webcamWrapper.webcam.getScreenshot();
      this.props.loadingStart();
      const geolocationPos = await this.props.updatePosition();
      const { coords } = geolocationPos;
      const { latitude, longitude } = coords;
      const position = {
        latitude,
        longitude
      };
      this.props.loadingStop();
      this.setState({ previewSrc: imageSrc, capturePosition: position });
    } catch (err) {
      console.warn('Capture failed', JSON.stringify(err), err);
      this.props.loadingStop();
      this.props.pushError(err);
    }
  };

  _switchCamera = () => {
    console.log('Switch camera...');
    this.setState({ videoSource: (this.state.videoSource + 1) % this.state.videoDevices.length });
  };

  _makeNewReport = async (picture, position) => {
    try {
      const imageBlob = await prepareImageForUpload(picture, true);
      await this.props.createNewReportDraft(imageBlob, position);
      this.props.history.push(`/newreport${this.props.location.search}`);
    } catch (err) {
      console.warn('Capture failed', JSON.stringify(err), err);
      this.props.loadingStop();
      if (err.message && err.message.toLowerCase().startsWith('camera cancelled')) {
        this.props.pushMessage('_capture_cancelled');
      } else {
        this.props.pushError(err);
      }
    }
  };

  accept = () => {
    this._makeNewReport(this.state.previewSrc, this.state.capturePosition);
  };

  retake = () => {
    this.setState({ previewSrc: null, capturePosition: null });
  };

  cancel = () => {
    this.setState({ previewSrc: null, capturePosition: null });
    this.props.pushMessage('_capture_cancelled');
    this.props.history.goBack();
  };

  render() {
    const selectedSource =
      this.state.videoSource > -1 ? this.state.videoDevices[this.state.videoSource] : null;
    return (
      <ColumnPage ref={this.component}>
        {selectedSource && <WebcamWrapper ref={this.setRef} selectedSource={selectedSource} />}
        <Preview src={this.state.previewSrc} />
        {selectedSource && (
          <Actions className="actions">
            {this.state.previewSrc === null && (
              <FAB
                onClick={this._switchCamera}
                icon="switch_camera"
                mini
                disabled={this.state.videoDevices.length < 2}
              />
            )}
            {this.state.previewSrc === null && (
              <FAB
                onClick={this.capture}
                icon="camera"
                backgroundColor={iReactTheme.palette.reportRequestEmCommColor}
              />
            )}
            {this.state.previewSrc !== null && <FAB onClick={this.retake} icon="undo" mini />}
            {this.state.previewSrc !== null && (
              <FAB
                onClick={this.accept}
                icon="done"
                backgroundColor={iReactTheme.palette.validatedColor}
              />
            )}
            <FAB onClick={this.cancel} icon="cancel" mini secondary />
          </Actions>
        )}
      </ColumnPage>
    );
  }
}

export default withRouter(enhance(WebcamCapture));
