import React from 'react';
import { Redirect } from 'react-router-dom';
import { withNewReport } from 'js/modules/newReport2';

/**
 * Wraps the specified component into a connected Lock component that renders the original
 * component only if the `user` property is truthy, redirecting the user to the `lockScreenPath`
 * otherwise.
 * 
 * @param {any} Component the component to lock
 * @param {any} authenticatedLandingPage the path to the authenticated landing page
 * @returns 
 */
const NewReportRedirectLock = (Component) => {

    const Lock = ({ newReportDraft, hasDraft, outgoingReportsQueueLength, ...rest }) => {
        if (!hasDraft) {
            return <div><Redirect to="/" /></div>;
        }
        else {
            return <Component {...rest} />;
        }
    };

    return withNewReport(Lock);
};

export default NewReportRedirectLock;