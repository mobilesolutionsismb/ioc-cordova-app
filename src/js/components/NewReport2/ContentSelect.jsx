import './commons/styles.scss';
import React, { Component } from 'react';
import qs from 'qs';

import nop from 'nop';
import { Header } from 'js/components/app';
import { Link } from 'react-router-dom';
import muiThemeable from 'material-ui/styles/muiThemeable';

import { compose } from 'redux';
import { Route, Redirect } from 'react-router-dom';
import { withDictionary } from 'ioc-localization';
import { withNewReport } from 'js/modules/newReport2';
import { withMessages } from 'js/modules/ui';
import { withMapPreferences } from 'js/modules/preferences';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';

import { List, ListItem, Divider, Avatar } from 'material-ui';

import { DeleteReportButton, ContentTypeIcon, HazardAvatar } from 'js/components/ReportsCommons';

const REPORT_CONTENT_TYPES = [
  {
    label: '_measurement',
    type: 'measure',
    // icon: 'M',
    description: '_measurement_d'
  },
  {
    label: '_resources',
    type: 'resource',
    // icon: 'R',
    description: '_resources_d'
  },
  {
    label: '_damage',
    type: 'damage',
    // icon: 'D',
    description: '_damage_d'
  },
  {
    label: '_people',
    type: 'people',
    // icon: 'P',
    description: '_people_d'
  }
];

const enhance = compose(
  withNewReport,
  withDictionary,
  withMessages,
  withMapPreferences
);

class ContentSelectComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contents: []
    };
    // this.state = this._initState(props);
  }

  _initState(props) {
    const search = props.location.search.replace(/^\?/, '');
    const searchParams = qs.parse(search);
    const contents =
      Object.keys(searchParams).length > 0
        ? REPORT_CONTENT_TYPES.filter(c => this._filterByQs(c, searchParams))
        : REPORT_CONTENT_TYPES;
    this.setState(
      {
        contents
      },
      () => {
        if (this.state.contents.length === 1) {
          // no hassle, go straight to this content
          const link = this._getLink(this.state.contents[0].type);
          this.props.history.replace(link);
        }
      }
    );
  }

  _filterByQs(contentTypeCfg, searchParams = null) {
    return contentTypeCfg === null || searchParams === null
      ? true
      : searchParams[contentTypeCfg.type] === 'true';
  }

  _onConfirmExit = () => {
    this.props.deleteReportDraft();
  };

  _onCancelButtonClick = () => {
    const title = '_confirm';
    const message = '_confirm_delete_report_draft';
    this.props.pushModalMessage(title, message, {
      _close: nop,
      _confirm: this._onConfirmExit
    });
  };

  _getLink = type => {
    const draft = this.props.newReportDraft;
    if (draft && draft.hazard === null && type === 'measure') {
      return `/newreport/categorySelect/${type}${this.props.location.search}`;
    } else {
      return `/newreport/contentValue/${type}${this.props.location.search}`;
    }
  };

  componentDidMount() {
    /* const state = */ this._initState(this.props);
    // this.setState(state, () => {
    //   if (this.state.contents.length === 1) {
    //     // no hassle, go straight to this content
    //     const link = this._getLink(this.state.contents[0].type);
    //     this.props.history.replace(link);
    //   }
    // });
  }

  render() {
    if (!this.props.hasDraft) return <Redirect to="/" push={false} />;

    const dict = this.props.dictionary;
    const palette = this.props.muiTheme.palette;
    const hazard = this.props.newReportDraft.hazard;
    return (
      <div className="new-report content-selection page">
        <Header
          title={'_select_content_type'}
          leftButtonType="back"
          rightButton={<DeleteReportButton onClick={this._onCancelButtonClick} />}
        />
        <div className="new-report__body_full">
          <div
            className="line"
            style={{
              padding: '8px 16px',
              boxSizing: 'border-box',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            {hazard && (
              <HazardAvatar
                hazardName={hazard}
                size={30}
                backgroundColor={'transparent'}
                textColor={palette.textColor}
              />
            )}
            <span>{hazard === null ? dict._no_hazard : dict(`_${hazard}`)}</span>
          </div>
          <Divider />
          <List className="line" style={{ padding: 0 }}>
            {this.state.contents.map((item, i) => [
              <ListItem
                key={`${i}-ci`}
                primaryText={dict(item.label)}
                secondaryText={dict(item.description)}
                leftAvatar={
                  <Avatar
                    size={40}
                    color={palette.textColor}
                    icon={<ContentTypeIcon iconSize={40} contentType={item.type} />}
                  />
                }
                rightIcon={<ChevronRight />}
                containerElement={<Link to={this._getLink(item.type)} />}
              />,
              <Divider key={`${i}-div`} />
            ])}
          </List>
        </div>
      </div>
    );
  }
}

const ContentSelect = enhance(muiThemeable()(ContentSelectComponent));
const ContentSelectRoute = () => (
  <Route
    render={({ history, location, match }) => (
      <ContentSelect history={history} location={location} match={match} />
    )}
  />
);

export default ContentSelectRoute;
