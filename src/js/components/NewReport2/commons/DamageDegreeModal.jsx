import React, { Component } from 'react';
import { Dialog, FlatButton, Avatar, FontIcon } from 'material-ui';
import Slider from 'material-ui-slider-label/Slider';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { DAMAGE_ICONS } from 'js/components/ReportsCommons';
import { colorMap } from 'js/utils/arrayUtils';

const ICON_SIZE = 48;
const TOOLTIP_SIZE = 120;
const DOT_SIZE = 16;
const styles = {
  titleIcon: {
    position: 'absolute',
    top: -(ICON_SIZE / 2),
    left: `calc(50% - ${ICON_SIZE / 2}px)`
  },
  subheader: {
    textTransform: 'capitalize'
  },
  extremeLabels: {
    position: 'absolute',
    top: 50,
    fontSize: 12
  },
  labelTicks: {
    position: 'absolute',
    top: 0,
    width: DOT_SIZE,
    height: DOT_SIZE,
    borderRadius: '100%'
    // top: 12,
    // width: 2,
    // height: 20,
  },
  labelStyleOuter: {
    minWidth: TOOLTIP_SIZE + 'px',
    height: 'auto',
    padding: '8px 2px',
    borderRadius: '5% 5% 5% 0',
    // background: 'cyan',
    position: 'absolute',
    // transform: 'rotate(-45deg)',
    top: '-70px'
    //left: '-4vw',
  },
  labelStyleInner: {
    // transform: 'rotate(45deg)',
    textShadow: '2px 0 0 #000, 0 -1px 0 #000, 0 1px 0 #000, -2px 0 0 #000',
    letterSpacing: '2px',
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
    position: 'relative' /* ,
        top: '3px',
        right: '0px', */
    //fontSize: '10px',
  },
  dialogBody: {
    width: '100%',
    height: '40vh',
    maxHeight: 300,
    position: 'relative',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'column'
  },
  dialogBodyInner: {
    display: 'block',
    position: 'relative',
    width: '100%',
    height: 64
  }
};

class DamageDegreeModal extends Component {
  _getFontIcon(icon, iconType, iconSize, color) {
    let iconElement = null;
    switch (iconType) {
      case 'emoji':
        iconElement = (
          <FontIcon
            style={{ lineHeight: iconSize * 0.6 + 'px', fontSize: iconSize * 0.6 }}
            className="emoji"
          >
            {icon}
          </FontIcon>
        );
        break;
      case 'ireact':
        iconElement = (
          <FontIcon
            style={{ color: color, lineHeight: iconSize * 0.6 + 'px', fontSize: iconSize * 0.6 }}
            className="ireact-pinpoint-icons"
          >
            {icon}
          </FontIcon>
        );
        break;
      case 'ireact-icons':
        iconElement = <FontIcon className="ireact-icons">{icon}</FontIcon>;
        break;
      case 'material':
      default:
        iconElement = <FontIcon className="material-icons">{icon}</FontIcon>;
        break;
    }
    return iconElement;
  }

  onOkButtonClick = e => {
    this.props.closeDialog(e, 'ok');
  };

  _getValueLabel = () => {
    if (this.props.damageOption && this.props.selectedDamageDegreeIndex >= 0) {
      const dict = this.props.dictionary;
      return dict(`_${this.props.damageOption.degrees[this.props.selectedDamageDegreeIndex]}`);
    } else {
      return '';
    }
  };

  _getTicks = (size, colors) => {
    if (this.props.damageOption) {
      // return [...Array(size + 1)].map((u, i) => <div key={i} style={{ ...styles.labelTicks, left: `calc(${i * 100 / size}% - 1px)`, backgroundColor: colors[i] }} />);
      return [...Array(size + 1)].map((u, i) => (
        <div
          key={i}
          style={{
            ...styles.labelTicks,
            left: `calc(${(i * 100) / size}% - ${DOT_SIZE / 2}px)`,
            backgroundColor: colors[i]
          }}
          onClick={e => {
            this.props.onSliderChange(e, i);
          }}
        />
      ));
    } else {
      return null;
    }
  };

  _getExtremeLabels = size => {
    const dict = this.props.dictionary;
    if (this.props.damageOption) {
      const degrees = this.props.damageOption.degrees;
      return [degrees[0], degrees[degrees.length - 1]].map((e, i) => {
        const style = i === 0 ? { left: 0 } : { right: 0 };
        return (
          <div key={i} style={{ ...styles.extremeLabels, ...style }}>
            {dict(`_${e}`)}
          </div>
        );
      });
    } else {
      return null;
    }
  };

  _getLabelLeft(value, max) {
    let left;
    switch (value) {
      case 0:
        left = `-${TOOLTIP_SIZE * 0.1}px`;
        break;
      case max:
        left = `-${TOOLTIP_SIZE * 0.9}px`;
        break;
      default:
        left = `-${TOOLTIP_SIZE / 2}px`;
        break;
    }
    return left;
  }

  render() {
    //console.log('DamageDegreeModal', this.props.damageOption, this.props.selectedDamageDegreeIndex);
    const {
      damageOption,
      selectedDamageDegreeIndex,
      closeDialog,
      onSliderChange,
      dictionary,
      muiTheme
    } = this.props;
    const damageName = damageOption ? dictionary(damageOption.label) : '';
    const degrees = damageOption ? damageOption.degrees : [''];
    const size = degrees.length;
    const max = size <= 1 ? 1 : size - 1;
    const colors = colorMap(size);
    const value = Math.max(selectedDamageDegreeIndex, 0);
    const palette = muiTheme.palette;
    let icon = '',
      title = '';
    if (damageName && damageOption) {
      icon =
        DAMAGE_ICONS[
          damageOption.value && damageOption.value.length ? damageOption.value : damageName
        ];
      title =
        damageOption.label && damageOption.label.length
          ? dictionary(damageOption.label)
          : dictionary(`_${damageName}`);
    }
    return (
      <Dialog
        className="damage-degree-dialog"
        title={
          <div>
            <Avatar
              style={styles.titleIcon}
              size={ICON_SIZE}
              color={palette.textColor}
              icon={this._getFontIcon(icon, 'ireact-icons', 18, palette.textColor)}
            />
            <span>{dictionary(title)}</span>
          </div>
        }
        modal={false}
        onRequestClose={closeDialog}
        open={damageOption !== null}
        actions={[
          <FlatButton label={dictionary('_cancel')} onClick={closeDialog} />,
          <FlatButton label={dictionary('_ok')} onClick={this.onOkButtonClick} />
        ]}
      >
        <div className="damage-degree-dialog__body" style={styles.dialogBody}>
          <div>{dictionary('_damage_degree')}</div>
          <div className="damage-degree-dialog__body--inner" style={styles.dialogBodyInner}>
            {this._getTicks(max, colors)}
            {this.props.damageOption && <div />}
            <Slider
              className="damage-degree-dialog__body--slider"
              disableFocusRipple={true}
              min={0}
              max={max}
              step={1}
              value={value}
              onChange={onSliderChange}
              label={
                <div
                  style={{
                    ...styles.labelStyleOuter,
                    backgroundColor: colors[value],
                    left: this._getLabelLeft(value, max)
                  }}
                >
                  <div style={styles.labelStyleInner}>{this._getValueLabel()}</div>
                </div>
              }
            />
            {this._getExtremeLabels(max)}
          </div>
        </div>
      </Dialog>
    );
  }
}

export default muiThemeable()(DamageDegreeModal);
