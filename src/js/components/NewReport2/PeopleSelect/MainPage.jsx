import React, { Component } from 'react';
import { List, ListItem, FontIcon } from 'material-ui';
import { HeaderButton } from 'js/components/app/HeaderButton';
import { PEOPLE_ICONS } from 'js/components/ReportsCommons';

const STYLES = {
  quantityLabel: {
    fontSize: 24,
    height: 48,
    lineHeight: '48px',
    WebkitAppearance: 'none',
    background: 'inherit',
    color: 'inherit',
    border: 'none',
    textAlign: 'center',
    outline: 'none',
    width: '3em'
  },
  mediumIcon: {
    fontSize: 36
    // width: 48,
    // height: 48,
    // fontSize: 48
  },
  medium: {
    // width: 96,
    // height: 96,
    padding: 4
  },
  peopleListItemIcon: {
    maxWidth: 150,
    width: '30%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '0 12px',
    height: 96
  },
  innerListItem: {
    padding: '16px 0',
    width: '100%'
  },
  listItem: {
    height: 96,
    lineHeight: '64px',
    fontSize: 24,
    width: '100%',
    padding: '0 12px'
  }
};

// const AddButton = ({ onClick }) => (
//   <IconButton iconStyle={STYLES.mediumIcon} style={STYLES.medium} tooltip="Add" onClick={onClick}>
//     <FontIcon className="material-icons" style={STYLES.mediumIcon}>
//       add_circle_outline
//     </FontIcon>
//   </IconButton>
// );

// const RemoveButton = ({ onClick }) => (
//   <IconButton
//     iconStyle={STYLES.mediumIcon}
//     style={STYLES.medium}
//     tooltip="Remove"
//     onClick={onClick}
//   >
//     <FontIcon className="material-icons" style={STYLES.mediumIcon}>
//       remove_circle_outline
//     </FontIcon>
//   </IconButton>
// );

const AddButton = HeaderButton('add_circle_outline', 'Add');
const RemoveButton = HeaderButton('remove_circle_outline', 'Remove');

const QuantityInput = ({ name, value, onChange }) => (
  <form
    onSubmit={e => {
      e.preventDefault();
    }}
  >
    <input
      name={name}
      className="quantity-input-number"
      type="number"
      step={1}
      min={0}
      max={100}
      onChange={onChange}
      value={value}
      style={{ ...STYLES.quantityLabel, width: '3em' }}
    />
  </form>
);

const PeopleListItem = ({ dictionary, config, addPeople, removePeople, setPeople }) => (
  <ListItem
    innerDivStyle={STYLES.innerListItem}
    className="people-list-item"
    style={STYLES.listItem}
    containerElement="div"
    primaryText={
      <span>
        <FontIcon style={{ marginRight: 8 }} className="ireact-icons">
          {PEOPLE_ICONS[config.label]}
        </FontIcon>
        {dictionary(config.label)}
      </span>
    }
    rightIcon={
      <div className="quantity-input" style={STYLES.peopleListItemIcon}>
        <AddButton onClick={addPeople} style={STYLES.medium} iconStyle={STYLES.mediumIcon} />
        <QuantityInput
          name={config.label}
          value={config.quantity}
          onChange={e => setPeople(parseInt(e.target.value, 10))}
        />
        {/* <span className="quantity-input-number" style={STYLES.quantityLabel}>
          {config.quantity}
        </span> */}
        <RemoveButton onClick={removePeople} style={STYLES.medium} iconStyle={STYLES.mediumIcon} />
      </div>
    }
  />
);

class PeopleSelect extends Component {
  static defaultProps = {
    people: []
  };

  updateCategory = (opType, peopleCategory, value = null) => {
    const { onSelect, draft } = this.props;
    let newPeople = draft.people.filter(p => p.id !== peopleCategory.id);
    let currentValue = draft.people.find(p => p.id === peopleCategory.id);
    if (opType === 'add') {
      if (!currentValue) {
        newPeople = newPeople.concat({ ...peopleCategory, quantity: 1 });
      } else {
        newPeople = newPeople.concat({ ...peopleCategory, quantity: currentValue.quantity + 1 });
      }
    } else if (opType === 'remove') {
      if (currentValue) {
        let nextQ = currentValue.quantity > 0 ? currentValue.quantity - 1 : 0;
        if (nextQ > 0) {
          newPeople = newPeople.concat({ ...peopleCategory, quantity: nextQ });
        }
      }
    } else if (opType === 'set' && value) {
      newPeople = newPeople.concat({ ...peopleCategory, quantity: value });
    }
    onSelect(newPeople);
  };

  render() {
    const { dictionary, /* draft, userType, onSelect, */ people } = this.props;
    console.log('PEOPLE', people);
    return (
      <List className="people-content-list listview">
        {people.items.length > 0 &&
          people.items.map((peopleConfigItem, i) => (
            <PeopleListItem
              key={i}
              dictionary={dictionary}
              config={peopleConfigItem}
              addPeople={() => {
                this.updateCategory('add', peopleConfigItem);
              }}
              removePeople={() => {
                this.updateCategory('remove', peopleConfigItem);
              }}
              setPeople={value => {
                this.updateCategory('set', peopleConfigItem, value);
              }}
            />
          ))}
        {people.items.length === 0 && <div>People don't have the power :(</div>}
      </List>
    );
  }
}

export default PeopleSelect;
