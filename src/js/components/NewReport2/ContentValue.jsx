import './commons/styles.scss';
import React, { Component } from 'react';
import nop from 'nop';
import { ScrollableHeaderPage } from 'js/components';
import { Route, Redirect } from 'react-router-dom';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withNewReport } from 'js/modules/newReport2';
import { withMessages } from 'js/modules/ui';
import { withLogin, withAppNewReportUIConfig } from 'ioc-api-interface';
import qs from 'qs';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { Avatar, FlatButton, RaisedButton, FontIcon } from 'material-ui';
import { logMain } from 'js/utils/log';
import {
  DeleteReportButton,
  ContentTypeIcon,
  HazardAvatar,
  HazardLabel,
  MeasureCategoryIcon
} from 'js/components/ReportsCommons';

import { MeasuresSelect } from './MeasuresSelect';
import { ResourcesSelect } from './ResourcesSelect';
import { DamagesSelect } from './DamagesSelect';
import { PeopleSelect } from './PeopleSelect';

const enhance = compose(
  withLogin,
  withNewReport,
  withDictionary,
  withMessages,
  withAppNewReportUIConfig
);

const LANDING_PAGE = '/tabs/map';

class Call112 extends Component {
  render() {
    const { children, ...rest } = this.props;
    return (
      <button {...rest}>
        <a href="tel:112">{children}</a>
      </button>
    );
  }
}

//const iconSize = (window.innerHeight * 0.33 - 64) * 0.4;

function getHeaderIconSize() {
  return (window.innerHeight * 0.33 - 64) * 0.4;
}

/**
 * Determine input measure type
 * @param {Object} measure
 * @return String ('unknown'|'qualitative', 'quantitative')
 */
function determineInputMeasureType(measure) {
  let type = 'unknown';
  if (measure.hasOwnProperty('options')) {
    type = 'qualitative';
  } else if (measure.hasOwnProperty('min') && measure.hasOwnProperty('max')) {
    type = 'quantitative';
  }
  return type;
}

const ContentValueHeader = ({
  iconSize,
  palette,
  title,
  dictionary,
  contentType,
  selectionCount,
  cancelReportButton,
  hazard,
  measureCategory
}) => (
  <div className="content-value-header" style={{ backgroundColor: palette.primary1Color }}>
    {/* <div className="content-value-header-title" >{dictionary(title)}</div> */}
    <Avatar
      size={iconSize}
      style={{ minWidth: iconSize, minHeight: iconSize }}
      backgroundColor={palette.textColor}
      icon={
        <ContentTypeIcon
          iconSize={iconSize}
          contentType={contentType}
          color={palette.canvasColor}
        />
      }
    />
    <div className="content-info">
      <div className="content-type-resume">
        {hazard !== null && (
          <HazardAvatar
            hazardName={hazard}
            size={30}
            backgroundColor={'transparent'}
            textColor={palette.textColor}
          />
        )}
        {hazard !== null && <HazardLabel style={{ marginLeft: 2 }} hazardName={hazard} />}
        {contentType === 'measure' && (
          <MeasureCategoryIcon color={palette.textColor} category={measureCategory} />
        )}
        {contentType === 'measure' &&
          hazard == null &&
          measureCategory && (
            <span style={{ marginLeft: 2 }}>{dictionary(`_cat_${measureCategory}`)}</span>
          )}
      </div>
      {/* <div className="content-selection">{dictionary(contentType=== 'people' ? '_new_report_people_count' : '_new_report_selection_count', selectionCount)}</div> */}
    </div>
    {/*         <div style={{ position: 'absolute', top: 0, right: 0 }}>
            {cancelReportButton}
        </div> */}
  </div>
);

class ContentValueComponent extends Component {
  constructor(props) {
    super(props);
    const search = this.props.location.search.replace(/^\?/, '');
    const searchParams = qs.parse(search);
    const filters = Object.keys(searchParams);
    const hasFilters = filters.indexOf('measure') > -1;
    const allowedMeasureIds = hasFilters
      ? Array.isArray(searchParams.measureId)
        ? searchParams.measureId.map(id => parseInt(id, 10))
        : typeof searchParams.measureId === 'string'
          ? [parseInt(searchParams.measureId, 10)]
          : 'any'
      : 'any';
    this.state = {
      allowedMeasureIds,
      iconSize: getHeaderIconSize()
    };
  }

  _onConfirmExit = () => {
    this.props.deleteReportDraft();
  };

  _onCancelButtonClick = () => {
    const title = '_confirm';
    const message = '_confirm_delete_report_draft';
    this.props.pushModalMessage(title, message, {
      _close: nop,
      _confirm: this._onConfirmExit
    });
  };

  _showCallEmergencyNumberPopup = () => {
    const { muiTheme, dictionary, pushModalMessage } = this.props;
    const palette = muiTheme.palette;

    pushModalMessage(dictionary('_new_report_call_112_title'), closeDialog => (
      <div className="call-emergency-popup">
        {dictionary('_new_report_call_112_text')
          .split(/\\n/g)
          .map((line, i) => (
            <p key={i}>{line}</p>
          ))}
        <div
          className="call-emergency-popup__actions"
          style={{
            display: 'flex',
            flexDirection: 'column',
            height: 90,
            justifyContent: 'space-between',
            width: '100%'
          }}
        >
          <RaisedButton
            containerElement={<Call112 />}
            label={dictionary('_new_report_call_112_btn')}
            primary={true}
            labelStyle={{ color: palette.textColor }}
            onClick={closeDialog}
            icon={<FontIcon className="material-icons">call</FontIcon>}
          />
          <FlatButton label={dictionary('_close')} onClick={closeDialog} />
        </div>
      </div>
    ));
  };

  _onSendBtnClick = async () => {
    // SEND OR ENQUEUE REPORT
    try {
      // Show 112 only if citizen & ( event declared || people content)
      const show112 =
        this.props.user.userType === 'citizen'
          ? this.props.newReportDraft.hazard !== null || this.props.newReportDraft.people.length > 0
          : false;
      logMain(`Sending report to queue... [show 112 = ${show112}]`);
      await this.props.enqueueReportDraft();
      this.props.startSendingEnqueuedReports();
      if (show112) {
        this._showCallEmergencyNumberPopup();
      }
      this.props.history.replace(LANDING_PAGE);
    } catch (err) {
      this.props.pushError(err);
    }
  };

  _onSelect = contentValues => {
    const contentType = this.props.match.params.contentType;
    logMain('_onSelect', contentType, contentValues);
    if (contentType === 'measure') {
      this.props.updateReportDraft({
        measures: contentValues
      });
    } else if (contentType === 'damage') {
      this.props.updateReportDraft({
        damages: contentValues
      });
    } else if (contentType === 'resource') {
      this.props.updateReportDraft({
        resources: contentValues
      });
    }
    if (contentType === 'people') {
      this.props.updateReportDraft({
        people: contentValues
      });
    }
  };

  _updateIconSize = () => {
    this.setState({ iconSize: getHeaderIconSize() });
  };

  _checkDraftContents = draft => {
    // We enforce homogeneous contents
    // so if the report has already damages and by navigating back and forth we
    // for some reason changed the contenType, we clear everything
    const contentType = this.props.match.params.contentType;
    const draftContent = draft.getContentType();
    if (draftContent !== 'none' && draftContent !== contentType) {
      this.props.updateReportDraft({
        measures: [],
        damages: [],
        resources: [],
        people: []
      });
    }
  };

  componentDidUpdate(prevProps) {
    if (!prevProps.hasDraft && this.props.hasDraft) {
      this._checkDraftContents(this.props.newReportDraft);
    }
  }

  componentDidMount() {
    if (this.props.hasDraft) {
      this._checkDraftContents(this.props.newReportDraft);
    }
    window.addEventListener('resize', this._updateIconSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this._updateIconSize);
  }

  _getSelectedMeasures(draft, measures, draftHazard, draftCategory) {
    let filteredMeasures = [];
    if (draftHazard !== null && draftCategory === null) {
      filteredMeasures = measures.filter(
        m =>
          m.hazard
            .replace(/\s/g, '')
            .split(',')
            .indexOf(draftHazard) > -1
      );
    } else if (draftHazard == null && draftCategory !== null) {
      filteredMeasures = measures.filter(m => m.category === draftCategory);
    }

    let selectionCount = 0;
    const items = filteredMeasures.map(m => {
      const selectedIndex = draft.measures.findIndex(dm => dm.id === m.id);
      const selected = selectedIndex > -1;
      const measureType = determineInputMeasureType(m);
      const outputMeasure = { ...m, selected, measureType };
      if (selected) {
        selectionCount++;
        const dm = draft.measures[selectedIndex];
        if (measureType === 'qualitative') {
          outputMeasure['selectedOption'] = dm.optionId;
        } else if (measureType === 'quantitative') {
          outputMeasure['selectedValue'] = dm.value;
          outputMeasure['numericValue'] = dm.numericValue;
        }
      }
      return outputMeasure;
    });
    return {
      selectionCount,
      items
    };
  }

  _getSelectedDamages(draft, damages) {
    // Get alredy selected damages
    const damageCategories = Object.keys(damages);
    let selectionCount = 0;
    const items = damageCategories.reduce((enhancedDamage, current) => {
      enhancedDamage[current] = {
        options: damages[current].options.map(d => {
          const index = draft.damages.findIndex(dd => dd.id === d.id);
          const selected = index > -1;
          if (selected) {
            selectionCount++;
          }
          return {
            ...d,
            selected,
            selectedDegree: selected ? draft.damages[index].degree : null
          };
        })
      };
      return enhancedDamage;
    }, {});
    return {
      selectionCount,
      items
    };
  }

  _getSelectedResources(draft, resources) {
    // Get alredy selected resources
    const resourceCategories = Object.keys(resources);
    let selectionCount = 0;
    const items = resourceCategories.reduce((enhancedResource, current) => {
      enhancedResource[current] = {
        options: resources[current].options.map(res => {
          //TODO USE .id
          const selected = draft.resources.findIndex(r => res.id === r.id) > -1;
          if (selected) {
            selectionCount++;
          }
          return {
            ...res,
            selected
          };
        })
      };
      return enhancedResource;
    }, {});
    return {
      selectionCount,
      items
    };
  }

  _getSelectedPeople(draft, people) {
    const items = people.map(p => {
      const { id, label } = p;
      const peopleItem = draft.people.find(pc => pc.id === p.id);
      const quantity = peopleItem ? peopleItem.quantity : 0;
      return {
        id,
        label,
        quantity
      };
    });
    // each people in draft is {id, quantity}
    const selectionCount = draft.people.reduce((tot, peopleItem) => {
      if (typeof peopleItem.quantity === 'number' && peopleItem.quantity > 0) {
        tot += peopleItem.quantity;
      }
      return tot;
    }, 0);

    return {
      selectionCount,
      items
    };
  }

  _filterMeasures = measures => {
    const { allowedMeasureIds } = this.state;

    return measures.filter(
      m => (allowedMeasureIds === 'any' ? true : allowedMeasureIds.indexOf(m.id) > -1)
    );
  };

  getSelectedData(contentType) {
    const draft = this.props.newReportDraft;
    const { measures, damages, resources, people } = this.props.newReportUiConfig;
    let data = null;
    switch (contentType) {
      case 'measure':
        data = this._getSelectedMeasures(
          draft,
          this._filterMeasures(measures),
          draft.hazard,
          draft.category
        );
        break;
      case 'damage':
        data = this._getSelectedDamages(draft, damages);
        break;
      case 'resource':
        data = this._getSelectedResources(draft, resources);
        break;
      case 'people':
        data = this._getSelectedPeople(draft, people.options);
        break;
      default:
        break;
    }
    return data;
  }

  _getContentTypeComponent = (contentType, data, palette) => {
    const dictionary = this.props.dictionary;
    const locale = this.props.locale;
    const draft = this.props.newReportDraft;
    const { appUserType } = this.props.user;

    let component = null;
    switch (contentType) {
      case 'measure':
        component = (
          <MeasuresSelect
            dictionary={dictionary}
            locale={locale}
            measures={data}
            palette={palette}
            draft={draft}
            userType={appUserType}
            onSelect={this._onSelect}
          />
        );
        break;
      case 'resource':
        component = (
          <ResourcesSelect
            dictionary={dictionary}
            resources={data}
            palette={palette}
            draft={draft}
            userType={appUserType}
            onSelect={this._onSelect}
          />
        );
        break;
      case 'damage':
        component = (
          <DamagesSelect
            dictionary={dictionary}
            damages={data}
            palette={palette}
            draft={draft}
            userType={appUserType}
            onSelect={this._onSelect}
          />
        );
        break;
      case 'people':
        component = (
          <PeopleSelect
            dictionary={dictionary}
            people={data}
            palette={palette}
            draft={draft}
            userType={appUserType}
            onSelect={this._onSelect}
          />
        );
        break;
      default:
        break;
    }
    return component;
  };

  render() {
    if (!this.props.hasDraft) return <Redirect to="/" push={false} />;

    const draft = this.props.newReportDraft;
    const dictionary = this.props.dictionary;
    const palette = this.props.muiTheme.palette;
    const contentType = this.props.match.params.contentType;
    const data = this.getSelectedData(contentType);
    // logMain('this.props.newReportUiConfig', this.props.newReportUiConfig)
    const cancelReportButton = <DeleteReportButton onClick={this._onCancelButtonClick} />;
    return (
      <div className="new-report value-selection page">
        <ScrollableHeaderPage
          className="new-report-value page-inner"
          containerID="new-report-value-page"
          pageTitle={{
            title:
              data.selectionCount > 0
                ? dictionary('_new_report_selection_count', data.selectionCount)
                : '_select_content_value',
            leftButtonType: 'back',
            rightButton: cancelReportButton
          }}
          headerTextColor={palette.textColor}
          headerBackgroundColor={palette.primary1Color}
          headerContent={
            <ContentValueHeader
              iconSize={this.state.iconSize}
              title={'_select_content_value_' + contentType}
              dictionary={this.props.dictionary}
              palette={palette}
              contentType={contentType}
              selectionCount={data.selectionCount}
              cancelReportButton={cancelReportButton}
              hazard={draft.hazard}
              measureCategory={draft.category}
            />
          }
          content={this._getContentTypeComponent(contentType, data, palette)}
        />
        <div
          className="new-report-value bottom-toolbar"
          style={{ backgroundColor: palette.tabsColor, color: palette.textColor }}
        >
          <FlatButton label={dictionary._cancel} onClick={this._onCancelButtonClick} />
          <RaisedButton
            label={dictionary._send}
            onClick={this._onSendBtnClick}
            primary={true}
            labelStyle={{ color: palette.textColor }}
            disabled={!draft.isValid()}
          />
        </div>
      </div>
    );
  }
}

const ContentValue = enhance(muiThemeable()(ContentValueComponent));

const ContentValueRoute = () => (
  <Route
    render={({ history, location, match }) => (
      <ContentValue history={history} location={location} match={match} />
    )}
  />
);

export default ContentValueRoute;
