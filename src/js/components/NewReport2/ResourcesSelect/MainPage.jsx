import React, { Component } from 'react';
import { List, Avatar, FontIcon, /* ListItem, */ Subheader } from 'material-ui';
import { RESOURCE_ICONS } from 'js/components/ReportsCommons';

const ResourceListItem = ({ id, label, value, palette, selected, onClick }) => {
  const selectedColor = selected ? palette.primary1Color : undefined;

  return (
    <div className="resource-options-list__item" onClick={onClick}>
      <Avatar
        size={40}
        className="resource-options-list-item__avatar"
        style={{ minWidth: 40, minHeight: 40 }}
        backgroundColor={selectedColor}
      >
        <FontIcon className="ireact-icons" style={{ fontSize: 25 }}>
          {RESOURCE_ICONS[value]}
        </FontIcon>
      </Avatar>
      <div className="item-label">
        <div className="name-label" style={{ color: selectedColor }}>
          {label}
        </div>
      </div>
    </div>
  );
};

const ResourceList = ({ dictionary, palette, resourceName, resourceOptions, onItemClick }) => (
  <List className={`${resourceName} resource-options-list`}>
    <Subheader className="resource-options-list__subheader">
      {dictionary(`_${resourceName}`)}
    </Subheader>
    {resourceOptions.filter(opt => opt.value !== '').map((opt, i) => (
      <ResourceListItem
        key={i}
        {...opt}
        label={dictionary(opt.label)}
        palette={palette}
        onClick={() => onItemClick(opt)}
      />
    ))}
  </List>
);

class ResourcesSelect extends Component {
  static defaultProps = {
    resources: []
  };

  onResourceItemClick = selectedResourceOption => {
    console.log('onResourceItemClick', selectedResourceOption);
    const { onSelect, draft } = this.props;
    let newResources = [];
    if (selectedResourceOption.selected) {
      // Deselect
      newResources = draft.resources.filter(r => r.id !== selectedResourceOption.id);
    } else {
      // Select
      newResources = draft.resources
        .filter(r => r.id !== selectedResourceOption.id)
        .concat({ id: selectedResourceOption.id });
    }
    onSelect(newResources);
  };

  render() {
    const { dictionary, palette, /* draft, userType, onSelect,  */ resources } = this.props;
    const resourceCategories = Object.keys(resources.items);
    return (
      <div className="resources-content-list listview">
        {resourceCategories.length > 0 &&
          resourceCategories.map((resourceName, i) => (
            <ResourceList
              key={i}
              dictionary={dictionary}
              palette={palette}
              resourceName={resourceName}
              resourceOptions={resources.items[resourceName].options}
              onItemClick={this.onResourceItemClick}
            />
          ))}
        {resourceCategories.length === 0 && <div>No resources</div>}
      </div>
    );
  }
}

export default ResourcesSelect;
