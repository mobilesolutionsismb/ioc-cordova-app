import './commons/styles.scss';
import React, { Component } from 'react';
import qs from 'qs';

import nop from 'nop';
import { Header } from 'js/components/app';
import { RaisedButton } from 'material-ui';
// import { Link } from 'react-router-dom';
import { iReactTheme } from 'js/startup/iReactTheme';

import { compose } from 'redux';
import { Route, Redirect } from 'react-router-dom';
import { withDictionary } from 'ioc-localization';
import { withNewReport } from 'js/modules/newReport2';
import { withMessages } from 'js/modules/ui';
import { withAppNewReportUIConfig } from 'ioc-api-interface';
import { withLogin } from 'ioc-api-interface';

import { localizeDate } from 'js/utils/localizeDate';

import {
  DeleteReportButton,
  HazardSelectionButton,
  HAZARD_VALUES_BLACKLIST
} from 'js/components/ReportsCommons';

const enhance = compose(
  withNewReport,
  withDictionary,
  withMessages,
  withAppNewReportUIConfig,
  withLogin
);

const NEXT_STEP = '/newreport/hazardSelect/contentSelect';

class HazardSelectComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      filteringByReportRequest: false,
      objectURL: null,
      hazards: []
    };
  }

  _initState(props, objectURL = null) {
    if (props.hasDraft) {
      const { newReportUiConfig, location } = props;
      const search = location.search.replace(/^\?/, '');
      const searchParams = qs.parse(search);
      const filters = Object.keys(searchParams);
      const hasFilters = filters.indexOf('measure') > -1;
      const allowedMeasureIds = hasFilters
        ? Array.isArray(searchParams.measureId)
          ? searchParams.measureId.map(id => parseInt(id, 10))
          : typeof searchParams.measureId === 'string'
            ? [parseInt(searchParams.measureId, 10)]
            : 'any'
        : 'any';

      const allowedMeasureHazards = Array.from(
        new Set(
          newReportUiConfig.measures
            .filter(
              m => (allowedMeasureIds === 'any' ? true : allowedMeasureIds.indexOf(m.id) > -1)
            )
            .map(m => m.hazard.split(','))
            .reduce((prev, next) => {
              prev = [...next, ...prev];
              return prev;
            }, [])
        )
      );
      const filteringByReportRequest =
        allowedMeasureIds === 'any' ? false : allowedMeasureHazards.length > 0;

      const hazards = newReportUiConfig.hazards
        .filter(hName => this._filterHazards(hName))
        .map(hName => ({
          name: hName,
          disabled: filteringByReportRequest ? allowedMeasureHazards.indexOf(hName) === -1 : false
        }));

      this.setState({
        filteringByReportRequest: false,
        objectURL,
        hazards
      });
    }
  }

  componentWillUnmount() {
    if (this.state.objectURL) {
      URL.revokeObjectURL(this.state.objectURL);
    }
  }

  componentDidMount() {
    if (this.props.newReportUiConfig) {
      let url = null;
      if (this.props.newReportDraft && this.props.newReportDraft.pictureFile) {
        if (this.state.objectURL !== null) {
          URL.revokeObjectURL(this.state.objectURL);
        }
        url = URL.createObjectURL(this.props.newReportDraft.pictureFile);
      }
      this._initState(this.props, url);
    } else {
      this.props.history.replace('/');
    }
  }

  componendDidUpdate(prevProps) {
    if (this.props.newReportDraft) {
      if (
        !prevProps.newReportDraft ||
        (this.props.newReportDraft.pictureFile &&
          prevProps.newReportDraft.usercreationtime !== this.props.newReportDraft.usercreationtime)
      ) {
        console.warn('componendDidUpdate', this.props);
        const url = URL.createObjectURL(this.props.newReportDraft.pictureFile);
        this.setState(prevState => {
          if (prevState.objectURL !== null) {
            URL.revokeObjectURL(prevState.objectURL);
          }
          return { objectURL: url };
        });
      }
    }
  }

  _onConfirmExit = () => {
    this.props.deleteReportDraft();
  };

  _onCancelButtonClick = () => {
    const title = '_confirm';
    const message = '_confirm_delete_report_draft';
    this.props.pushModalMessage(title, message, {
      _close: nop,
      _confirm: this._onConfirmExit
    });
  };

  _onHazardSelection = async hazardName => {
    const draft = this.props.newReportDraft;
    const updatedReport = await this.props.updateReportDraft({
      category: null,
      hazard: draft.hazard === hazardName ? null : hazardName
    });
    if (updatedReport.hazard !== null) {
      this.props.history.push(`${NEXT_STEP}${this.props.location.search}`);
    }
  };

  _noHazardButtonClick = async () => {
    const draft = this.props.newReportDraft;
    if (draft.hazard !== null) {
      await this.props.updateReportDraft({
        hazard: null
      });
    }
    this.props.history.push(`${NEXT_STEP}${this.props.location.search}`);
  };

  _filterHazards = hazardName => HAZARD_VALUES_BLACKLIST.indexOf(hazardName) === -1;

  render() {
    if (!this.props.hasDraft) return <Redirect to="/" push={false} />;

    const dict = this.props.dictionary;
    const draft = this.props.newReportDraft;
    const userType = this.props.user ? this.props.user.userType : '';

    // console.log('this.props.newReportUiConfig', this.props.newReportUiConfig)
    return (
      <div className="new-report hazard-selection page">
        <Header
          title={'_select_hazard_type'}
          leftButtonType="back"
          rightButton={<DeleteReportButton onClick={this._onCancelButtonClick} />}
        />
        <div
          className="new-report__picture-preview"
          style={{ backgroundImage: `url(${this.state.objectURL})` }}
        >
          <div className="new-report__date">{localizeDate(draft.usercreationtime)}</div>
          {userType !== 'citizen' && (
            <div
              className="new-report__date"
              style={{ bottom: 0, fontSize: 12, height: 14, lineHeight: '14px' }}
            >
              <span>{dict._new_report_visibility_label}:</span>
              <b style={{ marginLeft: '1em' }}>
                {draft.visibility === 'public'
                  ? dict._new_report_visibility_public
                  : dict._new_report_visibility_private}
              </b>
            </div>
          )}
        </div>
        <div className="new-report__actions">
          <div className="line">
            <span className="info">{dict._question_hazard}</span>
          </div>
          <div className="line horizontal-list hazards-list">
            {this.state.hazards.map((hazard, i) => (
              <HazardSelectionButton
                key={i}
                disabled={hazard.disabled}
                hazardName={hazard.name}
                selected={draft.hazard === hazard.name}
                onClick={this._onHazardSelection.bind(this, hazard.name)}
              />
            ))}
          </div>
          <div className="line">
            <RaisedButton
              className="ui-button"
              fullWidth={true}
              primary={true}
              label={dict._no_hazard}
              labelStyle={{ color: iReactTheme.palette.textColor }}
              onClick={this._noHazardButtonClick}
            />
          </div>
        </div>
      </div>
    );
  }
}

const HazardSelect = enhance(HazardSelectComponent);

const HazardSelectRoute = () => (
  <Route
    render={({ history, location, match }) => (
      <HazardSelect history={history} location={location} match={match} />
    )}
  />
);
export default HazardSelectRoute;
