import React, { Component } from 'react';
import { List, Avatar, FontIcon, /* ListItem, */ Subheader } from 'material-ui';
import DamageDegreeModal from '../commons/DamageDegreeModal';
import { colorMap } from 'js/utils/arrayUtils';
import { DAMAGE_ICONS } from 'js/components/ReportsCommons';

const DamageListItem = ({
  id,
  label,
  degrees,
  value,
  selected,
  selectedDegree,
  selectedDegreeLabel,
  onClick
}) => {
  const colors = selected ? colorMap(degrees.length) : null;
  const selectedDegreeIndex = selected ? degrees.indexOf(selectedDegree) : null;
  const selectedColor = selected ? colors[selectedDegreeIndex] : undefined;
  return (
    <div className="damage-options-list__item" onClick={onClick}>
      <Avatar
        size={40}
        className="damage-options-list-item__avatar"
        style={{ minWidth: 40, minHeight: 40 }}
        backgroundColor={selectedColor}
      >
        <FontIcon className="ireact-icons" style={{ fontSize: 25 }}>
          {DAMAGE_ICONS[value]}
        </FontIcon>
      </Avatar>
      <div className="item-label">
        <div className="name-label">{label}</div>
        <div className="degree-label" style={{ color: selectedColor }}>
          {selectedDegreeLabel}
        </div>
      </div>
    </div>
  );
};

const DamageList = ({ dictionary, damageName, damageOptions, onItemClick }) => (
  <List className={`${damageName} damage-options-list`}>
    <Subheader className="damage-options-list__subheader">{dictionary(`_${damageName}`)}</Subheader>
    {damageOptions.filter(opt => opt.value !== '').map((opt, i) => (
      <DamageListItem
        key={i}
        {...opt}
        label={dictionary(opt.label)}
        selectedDegreeLabel={opt.selected ? dictionary(`_${opt.selectedDegree}`) : ' '}
        onClick={() => onItemClick(opt)}
      />
    ))}
  </List>
);

class DamagesSelect extends Component {
  static defaultProps = {
    damages: {
      items: {},
      selectionCount: 0
    }
  };

  state = {
    currentDamageOption: null,
    selectedDamageDegreeIndex: 0
  };

  // openDamageDegreeDialog = (damageOption, damageName) => {
  //     // console.log('openDamageDegreeDialog', damageOption, damageName);
  //     // this.props.clearReportMeasure();
  //     // this.setState({
  //     //     currentDamageName: damageName,
  //     //     currentDamageOption: damageOption,
  //     //     selectedDamageDegreeIndex: 0
  //     // });
  // };
  _resetState = () => {
    this.setState({ currentDamageOption: null, selectedDamageDegreeIndex: 0 });
  };

  closeDamageDegreeDialog = (evt, result = null) => {
    console.log('closeDamageDegreeDialog', result);
    // //if result === 'ok' get values from state and set selected
    const { onSelect, draft } = this.props;
    if (result === 'ok') {
      // PUSH to draft.damages
      const { currentDamageOption, selectedDamageDegreeIndex } = this.state;
      const selectedDamage = {
        id: currentDamageOption.id,
        degree: currentDamageOption.degrees[selectedDamageDegreeIndex]
      };
      const newDamages = draft.damages
        .filter(d => d.id !== selectedDamage.id)
        .concat(selectedDamage);
      onSelect(newDamages);
      //     this.props.setReportDamage(currentDamageOption.id, currentDamageOption.degrees[selectedDamageDegreeIndex]);
    }
    // else {
    //     // Remove from draft.damages
    //     //     this.props.clearReportMeasure();
    // }
    this._resetState();
  };

  onDamageDialogSliderChange = (event, value) => {
    console.log('onDamageDialogSliderChange', /*  event, */ value);
    this.setState({ selectedDamageDegreeIndex: value });
  };

  onDamageItemClick = selectedDamageOption => {
    console.log('onDamageItemClick', selectedDamageOption, this.state.currentDamageOption);
    if (this.state.currentDamageOption) {
      // Clear
      this._resetState();
    } else {
      // Deselect
      if (selectedDamageOption.selected) {
        const { onSelect, draft } = this.props;
        const newDamages = draft.damages.filter(d => d.id !== selectedDamageOption.id);
        onSelect(newDamages);
      } else {
        // Select
        this.setState({ currentDamageOption: selectedDamageOption });
      }
    }
  };

  render() {
    const { dictionary, /* draft, userType, onSelect, */ damages } = this.props;
    const damageCategories = Object.keys(damages.items);
    return (
      <div className="damages-content-list listview">
        {damageCategories.length > 0 &&
          damageCategories.map((damageName, i) => (
            <DamageList
              key={i}
              dictionary={dictionary}
              damageName={damageName}
              damageOptions={damages.items[damageName].options}
              onItemClick={this.onDamageItemClick}
            />
          ))}
        {damageCategories.length === 0 && <div>No Damages</div>}
        <DamageDegreeModal
          dictionary={dictionary}
          damageOption={this.state.currentDamageOption}
          selectedDamageDegreeIndex={this.state.selectedDamageDegreeIndex}
          closeDialog={this.closeDamageDegreeDialog}
          onSliderChange={this.onDamageDialogSliderChange}
        />
      </div>
    );
  }
}

export default DamagesSelect;
