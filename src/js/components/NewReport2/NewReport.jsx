import './commons/styles.scss';
import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { withDictionary } from 'ioc-localization';
import { withNewReport } from 'js/modules/newReport2';
import { withMessages } from 'js/modules/ui';
import { compose } from 'redux';
import nop from 'nop';
import { localizeDate } from 'js/utils/localizeDate';
import { withLogin } from 'ioc-api-interface';

import { RaisedButton, IconButton, Toggle } from 'material-ui';
import { Link } from 'react-router-dom';

import { Header } from 'js/components/app';
import { white, red900 } from 'material-ui/styles/colors';

import { PlaceIcon, InfoIcon } from 'js/components/ReportsCommons';
import { logMain } from 'js/utils/log';

const enhance = compose(
  withNewReport,
  withDictionary,
  withMessages,
  withLogin
);

const SUGGESTION_STYLE = {
  display: 'flex',
  justifyContent: 'flex-start',
  alignItems: 'center',
  flexDirection: 'row'
};

class NewReportComponent extends Component {
  static defaultProps = {
    newReportDraft: null
  };

  state = {
    objectURL: null
  };

  componentWillUnmount() {
    if (this.state.objectURL) {
      URL.revokeObjectURL(this.state.objectURL);
    }
  }

  componentDidMount() {
    console.warn('componentDidMount', this.props);
    if (this.props.newReportDraft) {
      if (this.props.newReportDraft.pictureFile) {
        if (this.state.objectURL !== null) {
          URL.revokeObjectURL(this.state.objectURL);
        }
        const url = URL.createObjectURL(this.props.newReportDraft.pictureFile);
        this.setState({ objectURL: url });
      }
      if (this.props.user && typeof this.props.newReportDraft.visibility === 'undefined') {
        this._toggleReportVisibility(null, this.props.userType === 'citizen');
      }
    }
  }

  _toggleReportVisibility = async (evt, isInputChecked) => {
    const visibility = isInputChecked ? 'public' : 'private';
    if (this.props.newReportDraft.visibility !== visibility) {
      await this.props.updateReportDraft({ visibility });
      logMain('Report visibility updated');
    }
  };

  componendDidUpdate(prevProps) {
    if (this.props.newReportDraft) {
      if (
        !prevProps.newReportDraft ||
        (this.props.newReportDraft.pictureFile &&
          prevProps.newReportDraft.usercreationtime !== this.props.newReportDraft.usercreationtime)
      ) {
        console.warn('componendDidUpdate', this.props);
        const url = URL.createObjectURL(this.props.newReportDraft.pictureFile);
        this.setState(prevState => {
          if (prevState.objectURL !== null) {
            URL.revokeObjectURL(prevState.objectURL);
          }
          return { objectURL: url };
        });
      }
    }
  }

  _showExplanation = () => {
    const title = this.props.dictionary('_new_rep_loc_mode_title'); //'Report Localization Mode';
    const message = this.props.dictionary('_new_rep_loc_mode_content'); //      'This popup will show some meaningful explanation about report localization mode';
    this.props.pushModalMessage(title, message);
  };

  _onConfirmExit = () => {
    this.props.deleteReportDraft();
  };

  _onBackButtonClick = () => {
    const title = '_confirm';
    const message = '_confirm_delete_report_draft';
    this.props.pushModalMessage(title, message, {
      _close: nop,
      _confirm: this._onConfirmExit
    });
  };

  render() {
    if (!this.props.hasDraft) return <Redirect to="/" push={false} />;
    // TODO picture-preview
    const dict = this.props.dictionary;
    const draft = this.props.newReportDraft;
    const userType = this.props.user ? this.props.user.userType : '';

    return (
      <div className="new-report page">
        <Header
          title={'_new_report_wizard'}
          leftButtonType="back"
          onLeftHeaderButtonClick={this._onBackButtonClick}
        />
        <div
          className="new-report__picture-preview"
          style={{ backgroundImage: `url(${this.state.objectURL})` }}
        >
          <div className="new-report__date">{localizeDate(draft.usercreationtime)}</div>
        </div>
        <div className="new-report__actions">
          <div className="line" style={SUGGESTION_STYLE}>
            <span className="info">{dict._please_locate_new_report}</span>
            <IconButton
              style={{ padding: 0, width: 20, height: 20 }}
              iconStyle={{ padding: 0, fontSize: 20, width: 20, height: 20 }}
              onClick={this._showExplanation}
            >
              <InfoIcon />
            </IconButton>
          </div>
          {userType === 'citizen' && (
            <div className="line center">
              <PlaceIcon />
            </div>
          )}
          {userType !== 'citizen' && (
            <div
              className="line"
              style={{
                fontSize: 14,
                display: 'flex',
                flexWrap: 'wrap',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: '0 8px',
                boxSizing: 'border-box'
              }}
            >
              <span style={window.innerWidth > 320 ? {} : { width: '100%', display: 'block' }}>
                {dict._new_report_visibility_label}:
              </span>
              <div style={{ display: 'inline-block', flexGrow: 1, marginLeft: '1em' }}>
                <Toggle
                  toggled={draft.visibility === 'public'}
                  label={
                    draft.visibility === 'public'
                      ? dict._new_report_visibility_public
                      : dict._new_report_visibility_private
                  }
                  // labelStyle={{

                  // }}
                  labelPosition="right"
                  onToggle={this._toggleReportVisibility}
                />
              </div>
            </div>
          )}
          <div className="line">
            <Link
              className="action-button"
              to={`/newreport/pickReportPosition${this.props.location.search}`}
            >
              <RaisedButton
                className="ui-button"
                fullWidth={true}
                primary={true}
                label={dict._pick_location}
                labelStyle={{ color: white }}
              />
            </Link>
            <Link
              className="action-button"
              to={`/newreport/hazardSelect${this.props.location.search}`}
            >
              <RaisedButton
                className="ui-button"
                fullWidth={true}
                primary={true}
                label={dict._self_locate}
                labelStyle={{ color: white }}
              />
            </Link>
            <div className="action-button">
              <RaisedButton
                className="ui-button"
                fullWidth={true}
                backgroundColor={red900}
                label={dict._cancel}
                onClick={this._onBackButtonClick}
                labelStyle={{ color: white }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const NewReport = enhance(NewReportComponent);
const NewReportStartRoute = () => (
  <Route
    render={({ history, location, match }) => (
      <NewReport history={history} location={location} match={match} />
    )}
  />
);
export default NewReportStartRoute;
