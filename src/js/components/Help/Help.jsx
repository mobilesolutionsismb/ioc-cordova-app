import React, { Component } from 'react';
import { Header, Content, TopCenteredPage } from 'js/components/app';
import { logMain } from 'js/utils/log';
import { withMessages } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import { ColumnPage } from 'js/components/app/commons';
import { RefreshIndicator } from 'material-ui';
import styled from 'styled-components';

function helpPage(lang = 'en') {
  return `https://ireactfe.azurewebsites.net/help/${lang}/index.html`;
}

const IFRAME_STYLE = {
  width: '100%',
  height: '100%',
  border: 'none'
};

const OverlayBackground = styled(ColumnPage)`
  background-color: rgba(0, 0, 0, 0.4);
  position: absolute;
  top: 0;
  left: 0;
  z-index: 999;
  display: ${props => (props.loading === true ? 'flex' : 'none')};
`;

class Help extends Component {
  _iframe = null;

  state = {
    loading: false
  };

  loadingStart = () => {
    this.setState({ loading: true });
  };

  loadingStop = () => {
    this.setState({ loading: false });
  };

  _setIframeRef = el => (this._iframe = el);

  _onIframeLoadEnd = () => {
    logMain('IFRAME Load END');
    this.loadingStop();
  };

  _onIFrameError = err => {
    this.props.pushError(err);
  };

  componentDidMount() {
    this.loadingStart();
    if (this._iframe) {
      this._iframe.addEventListener('load', this._onIframeLoadEnd);
      this._iframe.addEventListener('error', this._onIFrameError);
    }
  }

  componentWillUnmount() {
    this.loadingStop();
    if (this._iframe) {
      this._iframe.removeEventListener('load', this._onIframeLoadEnd);
      this._iframe.removeEventListener('error', this._onIFrameError);
    }
  }

  render() {
    return (
      <TopCenteredPage className="help page">
        <Header title="_help" leftButtonType="back" />
        <Content>
          <iframe
            ref={this._setIframeRef}
            title="help"
            src={helpPage(this.props.locale)}
            style={IFRAME_STYLE}
          />
          <OverlayBackground className="loading-overlay" loading={this.state.loading}>
            <RefreshIndicator
              size={40}
              left={10}
              top={0}
              status={'loading'}
              style={{
                display: 'inline-block',
                position: 'relative'
              }}
            />
          </OverlayBackground>
        </Content>
      </TopCenteredPage>
    );
  }
}

export default withDictionary(withMessages(Help));
