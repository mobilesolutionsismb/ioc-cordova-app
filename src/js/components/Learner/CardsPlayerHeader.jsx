import React, { PureComponent } from 'react';
import { Header } from 'js/components/app';
import { withDictionary } from 'ioc-localization';
import styled from 'styled-components';
import { iReactTheme } from 'js/startup/iReactTheme';
import AnimatedNumber from 'react-animated-number';
import moment from 'moment';

const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: calc(100% - 128px);
  margin-right: auto;
`;

const SessionPointsElement = styled.span`
  padding: 0 16px;
  width: 180px;
  text-align: right;
`;

const SessionPoints = ({ points }) => (
  <SessionPointsElement>
    {points !== 0 ? `${points > 0 ? '+' : '-'}${points} pts` : ''}
  </SessionPointsElement>
);

const enhance = withDictionary;

function formatTime(seconds) {
  const duration = moment.duration(seconds, 'seconds');
  const mins = duration.minutes() + '';
  const secs = duration.seconds() + '';
  return `${mins.padStart(2, 0)}:${secs.padStart(2, 0)}`;
}

class TipsPlayerHeaderComponent extends PureComponent {
  static defaultProperties = {
    initialValue: 5,
    displayCounter: false,
    counting: false,
    tipsReadingProgress: 0,
    sessionPoints: 0
  };

  render() {
    const {
      dictionary,
      initialValue,
      displayCounter,
      counting,
      tipsReadingProgress,
      onBackButtonClick,
      sessionPoints
    } = this.props;
    return (
      <Header
        primary={true}
        leftButtonType="back"
        onLeftHeaderButtonClick={onBackButtonClick}
        title={
          <TitleContainer>
            <span>{dictionary('_tips')}</span>
            {displayCounter && (
              <AnimatedNumber
                initialValue={initialValue}
                value={tipsReadingProgress}
                duration={300}
                style={{
                  transition: '0.3s ease-out',
                  fontSize: 24,
                  transitionProperty: 'background-color, color, opacity',
                  color: counting ? iReactTheme.palette.textColor : 'transparent'
                }}
                precision={1}
                frameStyle={perc =>
                  !counting ? {} : perc === 100 ? {} : { color: iReactTheme.palette.accent1Color }
                }
                formatValue={formatTime}
              />
            )}
          </TitleContainer>
        }
        rightButton={<SessionPoints points={sessionPoints} />}
      />
    );
  }
}

class QuizzesPlayerHeaderComponent extends PureComponent {
  render() {
    const { dictionary, onBackButtonClick, sessionPoints } = this.props;
    return (
      <Header
        primary={true}
        leftButtonType="back"
        onLeftHeaderButtonClick={onBackButtonClick}
        title={
          <TitleContainer>
            <span>{dictionary('_quizzes')}</span>
          </TitleContainer>
        }
        rightButton={<SessionPoints points={sessionPoints} />}
      />
    );
  }
}

const TipsPlayerHeader = enhance(TipsPlayerHeaderComponent);

const QuizzesPlayerHeader = enhance(QuizzesPlayerHeaderComponent);

export function CardsPlayerHeader({ type, onBackButtonClick, sessionPoints = 0, ...rest }) {
  return type === 'tips' ? (
    <TipsPlayerHeader
      onBackButtonClick={onBackButtonClick}
      sessionPoints={sessionPoints}
      {...rest}
    />
  ) : type === 'quizzes' ? (
    <QuizzesPlayerHeader onBackButtonClick={onBackButtonClick} sessionPoints={sessionPoints} />
  ) : null;
}
