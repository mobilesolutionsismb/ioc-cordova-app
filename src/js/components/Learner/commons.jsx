import React from 'react';
import styled from 'styled-components';
import { CircularProgress, RaisedButton, FontIcon, Avatar } from 'material-ui';
import { Link } from 'react-router-dom';
import { Content } from 'js/components/app/header2';
import { hazardNameToIcon } from 'js/components/ReportsCommons';
import { withLogin, withGamification } from 'ioc-api-interface';
import { lighten, fade } from 'material-ui/utils/colorManipulator';
import { getAsset } from 'js/utils/getAssets';
import { LinearProgress } from 'material-ui';
import { iReactTheme } from 'js/startup/iReactTheme';

export const TYPES = ['quizzes', 'tips'];

export const QTProgressListContainer = styled.div`
  width: 100%;
  /* height: 100%; */
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  flex-wrap: wrap;
  box-sizing: border-box;
`;

const QTProgressContainer = styled(Link).attrs(props => ({
  className: 'qt-progress'
}))`
  width: 33vw;
  height: calc(33vw + 40px);
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  position: relative;
  margin: 8px 0;

  & .qt-progress-circle::before {
    position: absolute;
    content: '';
    width: 100%;
    height: 100%;
    box-sizing: border-box;
    border: 7px solid rgba(255, 255, 255, 0.2);
    border-radius: 100%;
    top: 0;
    left: 0;
  }
`;

const QTHazardName = styled.span.attrs(props => ({ className: 'qt-progress-hazard-name' }))`
  width: 100%;
  height: 32px;
  line-height: 32px;
  text-align: center;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

const HazardIconWrapper = styled.div.attrs(props => ({ className: 'hazard-icon' }))`
  position: absolute;
  width: ${props => props.size || 20}px;
  height: ${props => props.size || 20}px;
  top: ${props => props.size * 0.5 || 10}px;
  border-radius: 100%;
  line-height: 57px;
  text-align: center;
  background-color: ${props => props.theme.palette.canvasColor};
`;

const HazardIcon = ({ hazard, size }) => (
  <HazardIconWrapper size={size}>
    <FontIcon
      className={hazard === 'other' ? 'mdi mdi-cards' : 'ireact-pinpoint-icons'}
      style={{ fontSize: size, lineHeight: `${size}px` }}
    >
      {hazard === 'other' || hazard === 'general' ? '' : hazardNameToIcon(hazard)}
    </FontIcon>
  </HazardIconWrapper>
);

const ProgressLabel = styled.span`
  bottom: calc(50% - ${props => props.size || 10}px);
  position: absolute;
  width: 100%;
  height: 14px;
  line-height: 14px;
  font-size: 12px;
  text-align: center;
`;

export const QTProgressButton = ({ dictionary, hazard, completed, total, type }) => {
  const size = Math.round((window.innerWidth / 3) * 0.9);
  return (
    <QTProgressContainer to={`/learner/${type}/cards?hazard=${hazard}`}>
      <CircularProgress
        className="qt-progress-circle"
        mode="determinate"
        value={Math.round((completed / total) * 100)}
        size={size}
        thickness={7}
      />
      <HazardIcon size={size * 0.4} hazard={hazard} />
      <ProgressLabel>{`${completed}/${total}`}</ProgressLabel>
      <QTHazardName>{dictionary(`_${hazard}`)}</QTHazardName>
    </QTProgressContainer>
  );
};

export const LearnerFooter = styled.div.attrs(props => ({ className: 'learner-footer' }))`
  width: 100%;
  height: 56px;
`;

const LearnerHeaderInner = styled.div.attrs(props => ({ className: 'learner-header-inner' }))`
  width: 100%;
  height: 50%;
  background-color: ${props => props.theme.palette.primary2Color};
  color: ${props => props.theme.palette.textColor};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
`;

const LearnerIcon = styled.span.attrs(props => ({
  className: 'qt-header-image'
}))`
  background-image: url(${getAsset('/assets/home/button-dashboard-tips.svg')});
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  width: 128px;
  height: 128px;
  text-align: center;
  border-radius: 100%;
  padding: 16px;
`;

const Text = styled.div.attrs(props => ({
  className: 'qt-header-text'
}))`
  max-width: 80%;
  text-align: center;
  padding: 8px;
  box-sizing: border-box;
`;

const LearnerBottomLinkLabel = styled.span.attrs(props => ({ className: 'bottom-link-label' }))`
  color: ${props => props.theme.palette.textColor};
  font-size: 18px;
  text-transform: uppercase;
`;

export const LearnerBottomLink = ({ dictionary, label }) => (
  <LearnerFooter>
    <RaisedButton
      label={<LearnerBottomLinkLabel>{dictionary(label)}</LearnerBottomLinkLabel>}
      primary={true}
      fullWidth={true}
      style={{ height: 56 }}
      containerElement={<Link to="/achievements" />}
    />
  </LearnerFooter>
);

export const LearnerHeader = ({ dictionary, text }) => (
  <LearnerHeaderInner>
    <LearnerIcon />
    <Text>{dictionary(text)}</Text>
  </LearnerHeaderInner>
);

export const LearnerContent = styled(Content)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;

  .disclaimer {
    width: 100%;
    height: 40px;
    font-size: 12px;
    line-height: 16px;
    box-sizing: border-box;
    padding: 0 16px;
    font-style: italic;
  }
`;

export const UserLineInner = styled.div.attrs(props => ({ className: 'user-line' }))`
  width: 100%;
  height: 56px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0 32px;
  box-sizing: border-box;

  .user-name-and-scores {
    text-transform: capitalize;
  }
`;

function is(x) {
  return !!x;
}

export const UserAvatar = styled(Avatar)`
  border-color: ${props => props.theme.palette.citizensColor};
  background-color: ${props => props.theme.palette.primary2Color} !important;
  border-width: 2px;
  border-style: solid;
  box-sizing: border-box;
  margin-right: 8px;
`;

export const UserLine = withGamification(
  withLogin(({ user, gamificationProfile }) => {
    if (!user || user.userType !== 'citizen') {
      return null;
    } else {
      const { userPicture, userName } = user;
      const { currentLevel, score } = gamificationProfile;
      const text = [userName, `${currentLevel || 'Novice'}`].filter(is).join(', ');
      const src = getAsset(AVATAR_FILE_NAMES[userPicture]);
      return (
        <UserLineInner>
          <UserAvatar className="user-avatar" size={30} src={src} />
          <span className="user-name-and-scores">{`${text}, ${score}`}</span>
          <span>&nbsp;pts</span>
        </UserLineInner>
      );
    }
  })
);

export const CardContainerOLD = styled.div.attrs(props => ({ className: 'tq-card' }))`
  width: ${window.innerWidth * 0.66}px;
  height: ${window.innerHeight * 0.6 - 15}px;
  margin: auto;
  background-color: ${props => lighten(props.theme.palette.canvasColor, 0.2)};
  position: relative;
  /* -webkit-box-reflect: below 0px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(50%, transparent), to(rgba(255, 255, 255, 0.2))); */

  &::before,
  &::after {
    position: absolute;
    content: '';
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    transform-origin: center top;
  }

  &::before {
    background-color: ${props => lighten(props.theme.palette.canvasColor, 0.15)};
    transform: translate(0, -10px) scale(0.95);
    z-index: -1;
  }

  &::after {
    background-color: ${props => lighten(props.theme.palette.canvasColor, 0.1)};
    transform: translate(0, -17.5px) scale(0.9);
    z-index: -2;
  }
`;

export const CardContainer = styled.div.attrs(props => ({ className: 'tq-card' }))`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  margin: auto;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  background-color: ${props => lighten(props.theme.palette.canvasColor, 0.2)};
  position: relative;
`;

export const CardHeaderElement = styled.div`
  width: 100%;
  display: flex;
  height: 48px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 8px 16px;
  box-sizing: border-box;

  span:nth-child(2) {
    text-transform: uppercase;
    width: calc(100% - 64px);
    text-align: center;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }

  .hazard-or-category {
    @media screen and (min-width: 321px) and (max-width: 360px) {
      font-size: 14px;
    }
    @media screen and (max-width: 320px) {
      font-size: 12px;
    }
  }
`;

export const CardBodyElement = styled.div`
  width: 100%;
  display: flex;
  height: calc(100% - 96px);
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  padding: 8px 16px;
  box-sizing: border-box;
  overflow-x: hidden;
  overflow-y: auto;

  p {
    line-height: 32px;
    text-align: center;
    margin-bottom:0;
  }

  p.title {
    font-weight: bold;
  }
  a, span.link {
    text-decoration: underline;
    color: ${props => props.theme.palette.primary1Color};
  }

  .user-answer {
    width: 80%;
    /* border-bottom: 1px solid; */
    /* box-sizing: border-box; */
    min-height: 24px;
    line-height: 24px;

    &.completed {
      /* font-weight: bold; */
      font-style: italic;
      /* border-bottom-color: transparent; */
    }

    &.right {
      font-style: italic;
      /* text-decoration: underline;
      text-decoration-color: ${props => props.theme.palette.primary1Color}; */
    }

    &.wrong {
      text-align: center;
      font-style: italic;
      color: ${props => props.theme.palette.accent1Color};
      /* text-decoration: underline;
      text-decoration-color: ${props => props.theme.palette.accent1Color}; */
    }

    &.missing {
      margin-top: 1em;
      position: relative;
      text-align: center;
      font-style: italic;
      opacity: 0.6;
    }
  }
`;

export const CardFooterElement = styled.div`
  width: 100%;
  display: flex;
  height: 48px;
  justify-content: space-between;
  align-items: center;
  background-color: ${props => fade(props.theme.palette.backgroundColor, 0.25)};
  font-size: 0.8em;
  font-weight: 100;
  padding: 8px 16px;
  box-sizing: border-box;

  span {
    text-transform: capitalize;
  }
`;

export const MDIIcon = styled(FontIcon).attrs(props => ({ className: 'mdi' }))`
  font-size: 20px;
  line-height: 20px;
  background-color: ${props =>
    props.background ? props.background : props.theme.palette.validatedColor};
  border-radius: 100%;
  padding: 4px;
  box-sizing: border-box;
  transform: rotate(${props => props.rotate || 0}deg);
`;

export const PhaseIcon = ({ phase }) => {
  let className = 'mdi-circle-slice-2',
    rot = 0;
  switch (phase) {
    case 'environment':
      className = 'mdi-pine-tree';
      break;
    case 'postEvent':
      rot = 270;
      break;
    case 'preparedness':
      rot = 90;
      break;
    case 'prevention':
      break;
    case 'response':
      rot = 180;
      break;
    default:
      break;
  }
  return (
    <MDIIcon background="transparent" style={{ fontSize: 20 }} className={className} rotate={rot} />
  );
};

export const HazardIconSmall = styled(FontIcon)`
  line-height: 24px;
  background: ${props => fade(props.theme.palette.backgroundColor, 0.25)};
  border-radius: 100%;
  padding: 4px;
  box-sizing: border-box;
`;

export const UserAnswer = React.memo(function UserAnswerFn({
  userAnswer,
  allAnswers = [],
  completed,
  onClick,
  dictionary
}) {
  if (userAnswer && completed) {
    const isAllOptions = userAnswer.text.toLowerCase() === 'all options'; // Case when all answers are valid
    return (
      <div className="user-answer completed">
        {!isAllOptions && <span>{userAnswer.text}</span>}
        {isAllOptions && (
          <ul style={{ margin: 0, padding: 0 }}>
            {allAnswers
              .filter(a => a.text.toLowerCase() !== 'all options')
              .map((a, i) => (
                <li key={i}>{a.text}</li>
              ))}
          </ul>
        )}
      </div>
    );
  } else if (userAnswer && !completed) {
    return (
      <div
        className={`user-answer ${userAnswer.isTheRightAnswer ? 'right' : 'wrong'}`}
        onClick={onClick}
      >
        {userAnswer.isTheRightAnswer ? userAnswer.text : 'Wrong Answer!'}
      </div>
    );
  } else {
    return (
      <div className="user-answer missing" onClick={onClick}>
        {dictionary('_q_ans_tap_here')}
      </div>
    );
  }
});

export const CounterContainer = styled.div.attrs(props => ({ className: 'cards-counter' }))`
  height: 32px;
  width: 100%;
  font-size: 16px;
  line-height: 32px;
  text-align: center;
`;

export const CardsPlayerContent = styled(Content)`
  flex-direction: column;
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

export const ProgressContainer = styled.div.attrs(props => ({
  className: 'tips-reading-progress'
}))`
  top: 0;
  left: 0;
  position: absolute;
  height: 12px;
  width: 100%;
  transform: rotateY(180deg);

  .progress {
    position: relative;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 12px;
  }
`;

export const TipsProgress = React.memo(function TipsProgressFn({
  type,
  displayProgress = false,
  progress = 0,
  maxValue = 5
}) {
  return type === 'quizzes' || displayProgress === false ? (
    <ProgressContainer>
      <div className="progress" />
    </ProgressContainer>
  ) : (
    <ProgressContainer>
      <LinearProgress
        mode="determinate"
        color={iReactTheme.palette.homeButtonsColor}
        style={{
          position: 'relative',
          top: 0,
          left: 0,
          width: '100%',
          height: 12,
          transition: 'background-color 0.3s linear 0ms',
          backgroundColor: displayProgress ? iReactTheme.palette.canvasColor : 'transparent'
        }}
        value={progress}
        max={maxValue}
        min={0}
      />
    </ProgressContainer>
  );
});
