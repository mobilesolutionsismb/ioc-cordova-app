import React, { memo } from 'react';
import { openExternalLink } from 'js/utils/getAssets';
import { hazardNameToIcon } from 'js/components/ReportsCommons';
import { Paper } from 'material-ui';
import {
  CardContainer,
  CardHeaderElement,
  CardBodyElement,
  CardFooterElement,
  HazardIconSmall,
  MDIIcon,
  UserAnswer,
  PhaseIcon
} from './commons';

function CardInnerFn({ type, card, openAnswerPanel, userAnswer, dictionary, currentCardAnswers }) {
  const { hazard, text, crisisPhase, eventContext, completed, title, textToUrl, url } = card;
  const _userAnswer =
    type === 'quizzes' && completed
      ? card.answers.find(a => a.isTheRightAnswer === true) || null
      : userAnswer;
  return (
    <CardContainer onClick={type === 'quizzes' && !completed ? openAnswerPanel : undefined}>
      <CardHeaderElement>
        <HazardIconSmall
          className={hazard === 'other' ? 'mdi mdi-cards' : 'ireact-pinpoint-icons'}
          style={{ fontSize: 24 }}
        >
          {hazard === 'other' ? '' : hazardNameToIcon(hazard)}
        </HazardIconSmall>
        <span className="hazard-or-category">
          {dictionary(`_${hazard === 'other' ? 'qtl_' : ''}${hazard}`)}
        </span>
        {completed ? (
          <MDIIcon
            className={type === 'tips' ? 'mdi-read' : 'mdi-check-decagram'}
            style={{ fontSize: 20 }}
          />
        ) : (
          <span style={{ width: 32 }} />
        )}
      </CardHeaderElement>
      <CardBodyElement>
        {title && <p className="title">{title}</p>}
        <p>{text}</p>
        {url && (
          <p>
            <span
              className="link"
              onClick={e => {
                e.stopPropagation();
                e.preventDefault();
                openExternalLink(url);
              }}
            >
              {textToUrl}
            </span>
          </p>
        )}
        {type === 'quizzes' && (
          <UserAnswer
            userAnswer={_userAnswer}
            allAnswers={currentCardAnswers}
            completed={completed}
            dictionary={dictionary}
          />
        )}
      </CardBodyElement>
      <CardFooterElement>
        {crisisPhase && <PhaseIcon phase={crisisPhase} />}
        {crisisPhase && (
          <span style={{ marginLeft: 0, marginRight: 'auto' }}>
            {dictionary(`_qtl_cp_${crisisPhase.toLowerCase()}`)}
          </span>
        )}
        {eventContext && <span>{dictionary(`_qtl_ec_${eventContext.toLowerCase()}`)}</span>}
      </CardFooterElement>
    </CardContainer>
  );
}

const CardInner = memo(CardInnerFn);

function CardFn({ index, style, isCurrent, card, ...rest }) {
  return (
    <Paper
      className={isCurrent ? 'card-container current' : 'card-container'}
      style={style}
      zDepth={isCurrent ? 4 : 2}
    >
      <CardInner className={isCurrent ? 'card current' : 'card'} card={card} {...rest} />
    </Paper>
  );
}

export const Card = memo(CardFn);
