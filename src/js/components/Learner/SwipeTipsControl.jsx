import React, { PureComponent } from 'react';
import { Swipeable } from 'react-swipeable';
import styled from 'styled-components';
import nop from 'nop';
import delay from 'lodash.delay';
import { iReactTheme } from 'js/startup/iReactTheme';

const swipingEdgeColor = iReactTheme.palette.accent1Color;

const SwipeControlContainer = styled.div.attrs(props => ({ className: 'swipe-tips-control' }))`
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  height: 100%;
  background-color: transparent;
  pointer-events: none;

  & .swipeable-control {
    position: absolute;
    top: 0;
    pointer-events: all;
    transition: background 0.25s linear;
    background-color: transparent;

    &.right {
      right: 0px;
      &.s-right {
        background: radial-gradient(
          ellipse at right,
          ${swipingEdgeColor} 0%,
          rgba(79, 0, 0, 0) 69%,
          rgba(0, 0, 0, 0) 100%
        );
      }
    }
    &.left {
      left: 0px;
      &.s-left {
        background: radial-gradient(
          ellipse at left,
          ${swipingEdgeColor} 0%,
          rgba(79, 0, 0, 0) 69%,
          rgba(0, 0, 0, 0) 100%
        );
      }
    }
  }
`;

const styles = {
  swipeable: {
    width: '25%',
    height: '100%'
  }
};

export class SwipeTipsControl extends PureComponent {
  static defaultProps = {
    onSwipedLeft: nop,
    onSwipedRight: nop
  };

  state = {
    leftswiping: false,
    rightswiping: false
  };

  _onSwipingLeft = () => {
    if (!this.props.canSwipeRight) {
      this.setState({ leftswiping: true }, this._clearSwiping);
    }
  };

  _onSwipingRight = () => {
    if (!this.props.canSwipeLeft) {
      this.setState({ rightswiping: true }, this._clearSwiping);
    }
  };

  _clearSwiping = () =>
    delay(() => {
      this.setState({ leftswiping: false, rightswiping: false });
    }, 500);

  render() {
    const { onSwipedLeft, onSwipedRight } = this.props;
    const { leftswiping, rightswiping } = this.state;
    return (
      <SwipeControlContainer>
        <Swipeable
          className={`swipeable-control left${rightswiping ? ' s-left' : ''}`}
          style={styles.swipeable}
          // onSwipedLeft={onSwipedLeft}
          onSwipedRight={onSwipedRight}
          onSwipingRight={this._onSwipingRight}
        />
        <Swipeable
          className={`swipeable-control right${leftswiping ? ' s-right' : ''}`}
          style={styles.swipeable}
          onSwipedLeft={onSwipedLeft}
          onSwipingLeft={this._onSwipingLeft}
          // onSwipedRight={onSwipedRight}
        />
      </SwipeControlContainer>
    );
  }
}
