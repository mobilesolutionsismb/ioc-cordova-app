import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import qs from 'qs';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withTipsAndQuizzes } from 'ioc-api-interface';
import { withMessages, withLoader } from 'js/modules/ui';
import { CardsPlayerInner } from './CardsPlayerInner';
import { TYPES } from './commons';
import { logMain } from 'js/utils/log';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';

const PAGE_SIZE = 1000; // don't care about paging for now

class CardsPlayerContentManagerComponent extends Component {
  state = {
    cards: [],
    cardsUpdating: false,
    lastCardUpdate: new Date(0),
    sessionPoints: 0
  };

  _autoref = null;

  _setAutorRef = e => (this._autoref = e);

  _answerQuiz = async (candidateAnswer, quizId, quizIndex) => {
    try {
      if (candidateAnswer.isTheRightAnswer === true) {
        const result = await this.props.setQuizAsAnswered(
          quizId,
          this.props.loadingStart,
          this.props.loadingStop
        );
        const { points } = result;
        this._updateSessionPoints(points, 'quizzes');
        logMain(`Answered quiz with id: ${quizId} [${quizIndex}] - ${points} pts`);
        this._fetchCards();
      }
    } catch (err) {
      this.props.pushError(err);
      appInsights.trackException(err, 'CardsPlayer::_answerToQuiz', {}, {}, SeverityLevel.Error);
    }
  };

  _markTipAsRead = async (id, index) => {
    try {
      const result = await this.props.setTipAsRead(id);
      const { points } = result;
      logMain(`Read tip with id: ${id} [${index}]`);
      this._updateSessionPoints(points, 'tips');
      this._fetchCards();
    } catch (err) {
      this.props.pushError(err);
      appInsights.trackException(err, 'CardsPlayer::_markTipAsRead', {}, {}, SeverityLevel.Error);
    }
  };

  _updateSessionPoints = (points, type) => {
    if (this._autoref) {
      this.setState(
        prevState => {
          return {
            sessionPoints: prevState.sessionPoints + points
          };
        },
        () => {
          if (points === 0) {
            return;
          }
          if (type === 'tips') {
            this.props.pushMessage(this.props.dictionary('_learner_tip_read', points), 'success');
          } else if (type === 'quizzes') {
            this.props.pushMessage(this.props.dictionary('_learner_quiz_done', points), 'success');
          }
        }
      );
    }
  };

  _computeSession = e => {
    e.preventDefault();
    const { sessionPoints } = this.state;
    logMain('Session points accumulated', sessionPoints);
    // this.props.history.replace('')
    sessionPoints > 0
      ? this.props.history.replace(`/learner/${this.props.type}?sessionPoints=${sessionPoints}`)
      : this.props.history.goBack();
  };

  _fetchCardsInner = async (completed = null) => {
    let cards = [];
    const { type, hazard, getTipsByHazard, getQuizzesByHazard, pushError } = this.props;
    let lastCardUpdate = this.state.lastCardUpdate;
    try {
      const updateFn = type === 'tips' ? getTipsByHazard : getQuizzesByHazard;
      const cardResult = await updateFn(0, PAGE_SIZE, hazard, completed);
      cards = cardResult.items;
      lastCardUpdate = new Date();
    } catch (ex) {
      pushError(ex);
    } finally {
      this.setState({ cardsUpdating: false, cards, lastCardUpdate });
    }
  };

  _fetchCards = () => {
    this.setState({ cardsUpdating: true }, this._fetchCardsInner);
  };

  componentDidMount() {
    document.addEventListener('backbutton', this._computeSession);
    logMain('CardsPlayerContentManagerComponent Mounted');
    this._fetchCards();
  }

  componentWillUnmount() {
    document.removeEventListener('backbutton', this._computeSession);
  }

  render() {
    const { type, hazard, history } = this.props;
    const { cardsUpdating, lastCardUpdate, cards, sessionPoints } = this.state;
    return (
      <CardsPlayerInner
        ref={this._setAutorRef}
        type={type}
        hazard={hazard}
        history={history}
        cards={cards}
        cardsUpdating={cardsUpdating}
        lastCardUpdate={lastCardUpdate}
        sessionPoints={sessionPoints}
        updateSessionPoints={this._updateSessionPoints}
        onBackButtonClick={this._computeSession}
        answerQuiz={this._answerQuiz}
        markTipAsRead={this._markTipAsRead}
      />
    );
  }
}

const enhance = compose(
  withMessages,
  withTipsAndQuizzes,
  withLoader,
  withDictionary
);

const CardsPlayerContentManager = enhance(CardsPlayerContentManagerComponent);

const CardsPlayerRoute = () => (
  <Route
    render={({ match, location, history }) => {
      const { type } = match.params;
      const searchParams = qs.parse(location.search.replace(/^\?/, ''));
      if (!TYPES.includes(type) || !searchParams.hazard) {
        return <Redirect to="/learner" push={false} />;
      } else {
        return (
          <CardsPlayerContentManager type={type} hazard={searchParams.hazard} history={history} />
        );
      }
    }}
  />
);

export default CardsPlayerRoute;
