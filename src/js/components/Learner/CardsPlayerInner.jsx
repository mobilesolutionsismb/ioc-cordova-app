import React, { Component } from 'react';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withLoader } from 'js/modules/ui';
import { TopCenteredPage } from 'js/components/app/Pages';
import nop from 'nop';
import { CardsPlayerHeader } from './CardsPlayerHeader';
import { TYPES, CardsPlayerContent, CounterContainer, UserLine, TipsProgress } from './commons';
import { QuizAnswers } from './QuizAnswers';
import { RefreshIndicator } from 'material-ui';
import SwipeableViews from 'react-swipeable-views';
import { virtualize } from 'react-swipeable-views-utils';
import { mod } from 'react-swipeable-views-core';
import { Card } from './Card';
import { SwipeTipsControl } from './SwipeTipsControl';
import debounce from 'lodash.debounce';
import { logMain } from 'js/utils/log';

const VirtualizeSwipeableViews = virtualize(SwipeableViews);

const enhance = compose(
  withDictionary,
  withLoader
);
const SECOND = 1000; // 1000ms
const READING_TIMEOUT = 5; //SECONDS
// const CARD_WIDTH = window.innerWidth - 80;
// const CARD_HEIGHT = window.innerHeight * 0.6;
const VH_CAROUSEL_HEIGHT_START = 70;

const styles = {
  debugInfo: {
    zIndex: 100,
    position: 'fixed',
    top: 80,
    left: 16,
    color: 'lime',
    fontSize: 10,
    lineHeight: '12px'
  },
  container: {
    backgroundColor: 'rgba(255,255,255,0.045)'
  },
  slide: {
    minHeight: `calc(${VH_CAROUSEL_HEIGHT_START}vh - 25px)`,
    height: `calc(${VH_CAROUSEL_HEIGHT_START}vh - 25px)`,
    marginTop: 5,
    overflow: 'hidden'
    // border: '1px solid'
  },
  root: {
    width: '100%',
    height: `${VH_CAROUSEL_HEIGHT_START}vh`
  },
  loadingOverlay: {
    position: 'absolute',
    backgroundColor: 'rgba(0,0,0,0.45)',
    zIndex: 300,
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  slideContainer: {
    width: 'calc(100% - 20px)',
    padding: '0 10px',
    height: `${VH_CAROUSEL_HEIGHT_START}vh`
  },
  carousel: {
    width: 'calc(100% - 40px)',
    padding: '0 20px',
    height: '100%'
  }
};

const DebugInfo = IS_PRODUCTION
  ? null
  : React.memo(function DebugInfoFn({ counting, type, tipsReadingProgress }) {
      return (
        <div style={styles.debugInfo}>
          <div>
            <small>{counting ? 'Counting' : 'Stopped'}</small>
          </div>
          <div>
            <small>{type === 'tips' && <span>Count: {tipsReadingProgress}</span>}</small>
          </div>
        </div>
      );
    });

function cardRenderer(cards, currentCardIndex, otherCardProps) {
  return ({ key, index }) => {
    const cardIndex = mod(index, cards.length);
    const isCurrent = currentCardIndex === cardIndex;

    return (
      <Card
        key={key}
        style={styles.slide}
        index={cardIndex}
        card={cards[cardIndex]}
        isCurrent={isCurrent}
        {...otherCardProps}
      />
    );
  };
}

class CardsPlayerInnerComponent extends Component {
  static defaultProps = {
    type: TYPES[0],
    history: null,
    hazard: 'other',
    cards: [],
    cardsUpdating: false,
    sessionPoints: 0,
    updateSessionPoints: nop,
    onBackButtonClick: nop,
    answerQuiz: nop,
    markTipAsRead: nop
  };

  state = {
    counting: false,
    // offset: 0,
    // cards: [],
    slideIndex: 0,
    tipsReadingProgress: READING_TIMEOUT,
    // sessionPoints: 0,
    userAnswerParams: null,
    showAnswersPanel: false
  };

  _toggleAnswerPanelVisibility = () => {
    this.setState(prevState => ({
      showAnswersPanel: !prevState.showAnswersPanel
    }));
  };

  _answerToQuiz = (candidateAnswer, quizId, quizIndex) => {
    this.setState({
      userAnswerParams: {
        candidateAnswer,
        quizId,
        quizIndex
      }
    });
  };

  _openAnswersPanel = () => {
    this.setState({
      showAnswersPanel: true
    });
  };

  _closeAnswersPanel = () => {
    this.setState({
      showAnswersPanel: false
    });
  };

  _goToCard = slideIndex => {
    this.setState({
      slideIndex: typeof slideIndex === 'number' ? slideIndex : slideIndex.slideIndex,
      userAnswerParams: null
    });
  };

  _next = () => {
    logMain('Swipe left/next');
    const index = mod(this.state.slideIndex + 1, this.props.cards.length);
    const card = this.props.cards[this.state.slideIndex];
    // const nextCard = this.props.cards[index];
    if (this.props.type !== 'tips' || card.completed === true) {
      this._goToCard(index);
    }
  };

  _previous = () => {
    logMain('Swipe right/prev');
    const index = mod(this.state.slideIndex - 1, this.props.cards.length);
    // const card = this.props.cards[this.state.slideIndex];
    const prevCard = this.props.cards[index];
    if (this.props.type !== 'tips' || prevCard.completed === true) {
      this._goToCard(index);
    }
  };

  _showFirstUnansweredQuiz = debounce(() => {
    const firstUncompleteAnswerIndex = this.props.cards.findIndex(c => c.completed !== true);
    this._goToCard(
      firstUncompleteAnswerIndex > -1 ? firstUncompleteAnswerIndex : this.state.slideIndex
    );
  }, 300);

  _showFirstUnansweredTip = debounce(() => {
    const firstUncompleteAnswerIndex = this.props.cards.findIndex(c => c.completed !== true);
    const slideIndex =
      firstUncompleteAnswerIndex > -1 ? firstUncompleteAnswerIndex : this.state.slideIndex;
    this.setState(
      {
        slideIndex: typeof slideIndex === 'number' ? slideIndex : slideIndex.slideIndex
      },
      this._startInterval
    );
  }, 300);

  _autoref = null;

  _setAutorRef = e => (this._autoref = e);

  /**
   * The interval handle
   *
   * @memberof CardsPlayerComponent
   */
  _tipsProgressInterval = null;

  /**
   * stop an set state.counting to false
   *
   * @memberof CardsPlayerComponent
   */
  _stopInterval = (callback = nop) => {
    console.log('_stopInterval', this, this._tipsProgressInterval);
    if (this._tipsProgressInterval !== null) {
      clearInterval(this._tipsProgressInterval);
      this._tipsProgressInterval = null;
    }
    if (this._autoref !== null) {
      this.state.counting ? this.setState({ counting: false }, callback) : callback();
    }
  };

  /**
   * start an interval and set state.counting to true
   *
   * @memberof CardsPlayerComponent
   */
  _startInterval = debounce(() => {
    if (this.props.type === 'tips') {
      const onStop = () => {
        if (this._autoref !== null) {
          this.setState({ counting: true, tipsReadingProgress: READING_TIMEOUT }, () => {
            console.log('_startInterval', this);
            this._tipsProgressInterval = setInterval(this._onIntervalThick, SECOND);
          });
        }
      };
      this._stopInterval(onStop);
    }
  }, 1000);

  _markAsReadIfNeeded = () => {
    const cards = this.props.cards;
    const slideIndex = this.state.slideIndex;
    const cardIndex = cards.length === 0 ? -1 : mod(slideIndex, cards.length);
    const currentCard = cards.length === 0 ? null : cards[cardIndex];
    if (currentCard && !currentCard.completed) {
      this.props.markTipAsRead(currentCard.id, cardIndex + 1);
    }
  };

  /**
   * Update (decrease) state.tipsReadingProgress
   *
   * @memberof CardsPlayerComponent
   */
  _onIntervalThick = () => {
    if (this._autoref !== null) {
      const tipsReadingProgress = Math.max(this.state.tipsReadingProgress - 1, 0);
      logMain('tipsReadingProgress', tipsReadingProgress);
      this.setState({ tipsReadingProgress });
    }
  };

  componentWillUnmount() {
    this._stopInterval();
  }

  componentDidMount() {
    if (this.props.type === 'quizzes') {
      this._showFirstUnansweredQuiz();
    } else {
      this._showFirstUnansweredTip();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.type === 'quizzes') {
      if (
        prevState.userAnswerParams !== this.state.userAnswerParams &&
        this.state.userAnswerParams !== null
      ) {
        const { candidateAnswer, quizId, quizIndex } = this.state.userAnswerParams;
        this.props.answerQuiz(candidateAnswer, quizId, quizIndex);
      }
      if (prevProps.lastCardUpdate !== this.props.lastCardUpdate) {
        if (prevProps.cards.length === 0 && this.props.cards.length > 0) {
          this._showFirstUnansweredQuiz();
        } else {
          this._closeAnswersPanel();
        }
      }
    } else if (this.props.type === 'tips') {
      if (prevProps.lastCardUpdate !== this.props.lastCardUpdate) {
        if (prevProps.cards.length === 0 && this.props.cards.length > 0) {
          this._showFirstUnansweredTip();
        }
      }
      if (prevState.slideIndex !== this.state.slideIndex) {
        const index = mod(this.state.slideIndex, this.props.cards.length);
        if (!this.props.cards[index].completed) {
          this._startInterval();
        }
      }

      if (prevState.tipsReadingProgress !== 0 && this.state.tipsReadingProgress === 0) {
        this._stopInterval();
        this._markAsReadIfNeeded();
      }
    }
  }

  render() {
    const {
      type,
      onBackButtonClick,
      sessionPoints,
      cards,
      // hazard,
      cardsUpdating,
      dictionary
    } = this.props;
    const {
      tipsReadingProgress,
      counting,
      slideIndex,
      showAnswersPanel,
      userAnswerParams
    } = this.state;

    const cardIndex = cards.length === 0 ? -1 : mod(slideIndex, cards.length);
    const currentCard = cards.length === 0 ? null : cards[cardIndex];
    const currentCardAnswers = type === 'quizzes' ? currentCard && currentCard.answers.filter(c => typeof c.text === 'string' && c.text.length > 0) : null;
    const showAnswers =
      currentCard &&
      !currentCard.completed &&
      Array.isArray(currentCardAnswers) &&
      showAnswersPanel;
    const userAnswer = userAnswerParams ? userAnswerParams.candidateAnswer : null;

    const canSwipeRight =
      cards.length > 0 ? (type === 'quizzes' ? true : currentCard.completed) : false;
    const canSwipeLeft =
      cards.length > 0
        ? type === 'quizzes'
          ? true
          : cards[mod(slideIndex - 1, cards.length)].completed
        : false;

    const otherCardProps = {
      openAnswerPanel: this._openAnswersPanel,
      type,
      userAnswer,
      currentCardAnswers,
      dictionary
    };

    const debugInfo = { counting, type, tipsReadingProgress };

    return (
      <TopCenteredPage ref={this._setAutorRef}>
        <CardsPlayerHeader
          type={type}
          initialValue={READING_TIMEOUT}
          tipsReadingProgress={tipsReadingProgress}
          displayCounter={type === 'tips' && counting && cards.length > 0}
          counting={counting}
          onBackButtonClick={onBackButtonClick}
          sessionPoints={sessionPoints}
        />
        <CardsPlayerContent className="content" style={styles.container}>
          <TipsProgress
            displayProgress={currentCard && !currentCard.completed}
            type={type}
            maxValue={READING_TIMEOUT}
            progress={tipsReadingProgress}
          />
          <UserLine />
          {!IS_PRODUCTION && <DebugInfo {...debugInfo} />}
          <div style={styles.root} className="content-wrapper">
            {cardsUpdating && (
              <div style={styles.loadingOverlay} className="overlay">
                <RefreshIndicator
                  size={40}
                  left={10}
                  top={0}
                  status={'loading'}
                  style={{
                    display: 'inline-block',
                    position: 'relative'
                  }}
                />
                <h3>{`${dictionary('_loading')}...`}</h3>
              </div>
            )}
            {cards.length > 0 && (
              <VirtualizeSwipeableViews
                index={slideIndex}
                disabled={type === 'tips'}
                enableMouseEvents={!IS_CORDOVA}
                onChangeIndex={this._goToCard}
                slideRenderer={cardRenderer(cards, cardIndex, otherCardProps)}
                style={styles.carousel}
                slideStyle={styles.slideContainer}
              />
            )}
            {cards.length > 0 && type === 'tips' && (
              <SwipeTipsControl
                onSwipedLeft={this._next}
                onSwipedRight={this._previous}
                canSwipeLeft={canSwipeLeft}
                canSwipeRight={canSwipeRight}
              />
            )}
            {cards.length === 0 && !cardsUpdating && <h2>{dictionary._no_contents_title}</h2>}
          </div>
          <CounterContainer>
            {cardIndex + 1}/{cards.length}
          </CounterContainer>
          {type === 'quizzes' && (
            <QuizAnswers
              isLoading={cardsUpdating}
              show={showAnswers}
              cardIndex={slideIndex}
              cardId={currentCard ? currentCard.id : null}
              answers={currentCardAnswers}
              onSelect={this._answerToQuiz}
              selectedAnswer={userAnswer}
              toggleVisibility={this._toggleAnswerPanelVisibility}
            />
          )}
        </CardsPlayerContent>
      </TopCenteredPage>
    );
  }
}

export const CardsPlayerInner = enhance(CardsPlayerInnerComponent);
