import React, { Component } from 'react';
import { withDictionary } from 'ioc-localization';
import { Header } from 'js/components/app';
import { withTipsAndQuizzes } from 'ioc-api-interface';
import { withMessages } from 'js/modules/ui';
import { compose } from 'redux';
import { TopCenteredPage } from 'js/components/app/Pages';
import {
  QTProgressListContainer,
  QTProgressButton,
  LearnerContent,
  LearnerBottomLink,
  TYPES
} from './commons';
import { ConratulationsModalWrapper } from './ConratulationsModalWrapper';
import { Route, Redirect } from 'react-router-dom';
import { logMain } from 'js/utils/log';
import { Congratulations } from 'js/components/Achievements';
import { CircularProgress } from 'material-ui';
import { ColumnPage } from 'js/components/app/commons';

import qs from 'qs';

const enhance = compose(
  withTipsAndQuizzes,
  withDictionary,
  withMessages
);

class ProgressPageComponent extends Component {
  state = {
    loading: false
  };

  _updateProgress = () => {
    this.setState({ loading: true }, this._updateProgressInner);
  };

  _updateProgressInner = async () => {
    const type = this.props.type;
    const updateFn = type === 'tips' ? this.props.getTipsProgress : this.props.getQuizzesProgress;
    try {
      await updateFn();
      logMain(`${type} Progress updated`);
      // this.props.pushMessage(`${type} Progress updated`);
    } catch (err) {
      this.props.pushError(err);
    } finally {
      this.setState({ loading: false });
    }
  };

  _showCongratulations = (scoreDifference = 0) => {
    const smallScreen = window.innerWidth < 360;
    const midScreen = window.innerWidth < 400;
    const title = {
      text: '_keep_going',
      titleStyle: { textAlign: 'center', fontSize: smallScreen ? 20 : midScreen ? 23 : 28 }
    };
    const sf = 1; // midScreen ? 0.55 : 0.65;
    const message = (
      <ConratulationsModalWrapper scale={sf}>
        {baseWidth =>
          baseWidth ? (
            <Congratulations
              scaleFactor={0.65}
              isLearner={true}
              scoreDifference={scoreDifference}
              baseWidth={baseWidth}
              // style={{ transform: `scale3d(${sf}, ${sf}, 1)` }}
            />
          ) : null
        }
      </ConratulationsModalWrapper>
    );
    this.props.pushModalMessage(title, message, {
      _close: () => {
        logMain('Click modal');
        setTimeout(() => this.props.history.replace(`/learner/${this.props.type}`), 0);
      }
    });
  };

  componentDidMount() {
    if (this.props.sessionPoints) {
      this._showCongratulations(this.props.sessionPoints);
    }
    this._updateProgress();
  }

  render() {
    const { type, quizzesProgress, tipsProgress, dictionary } = this.props;
    const { loading } = this.state;
    const progresses = type === 'tips' ? tipsProgress : quizzesProgress;
    return (
      <TopCenteredPage className={`${type} page`}>
        <Header
          title={`_${type}`}
          leftButtonType="back"
          primary={true}
          rightButton={
            IS_PRODUCTION ? null : (
              <span
                style={{ padding: '0 16px' }}
                onClick={() => this._showCongratulations(Math.floor(Math.random() * 100) + 1)}
              >
                test
              </span>
            )
          }
          onLeftHeaderButtonClick={() => {
            this.props.history.replace('/learner');
          }}
        />
        <LearnerContent>
          {loading === true && (
            <ColumnPage>
              <CircularProgress size={80} thickness={5} />
            </ColumnPage>
          )}
          {loading === false && (
            <QTProgressListContainer>
              {progresses.map((prog, i) => (
                <QTProgressButton key={i} dictionary={dictionary} type={type} {...prog} />
              ))}
            </QTProgressListContainer>
          )}
          <LearnerBottomLink dictionary={dictionary} label="_my_scores" />
        </LearnerContent>
      </TopCenteredPage>
    );
  }
}

const ProgressPage = enhance(ProgressPageComponent);

const ProgressPageRoute = () => (
  <Route
    render={({ match, history, location }) => {
      const { type } = match.params;
      if (!TYPES.includes(type)) {
        return <Redirect to="/learner" push={false} />;
      } else {
        const searchParams = qs.parse(location.search.replace(/^\?/, ''));
        return <ProgressPage type={type} history={history} {...searchParams} />;
      }
    }}
  />
);
export default ProgressPageRoute;
