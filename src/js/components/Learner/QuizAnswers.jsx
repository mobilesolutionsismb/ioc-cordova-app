import React, { memo } from 'react';
import styled from 'styled-components';
import { FlatButton, RadioButton } from 'material-ui';
import { fade } from 'material-ui/utils/colorManipulator';
import { IS_IOS } from 'js/utils/getAppInfo';
import { iReactTheme } from 'js/startup';
const QUIZ_ANSWER_HEIGHT = 48;

export const QuizAnswerElement = styled.div.attrs(props => ({ className: 'quiz-answer' }))`
  width: 100%;
  height: auto;
  min-height: ${QUIZ_ANSWER_HEIGHT}px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-around;
  box-sizing: border-box;
  border: 1px solid;
  color: ${props =>
    props.selectedAnswer
      ? props.selectedAnswer.id === props.answerId
        ? props.selectedAnswer.isTheRightAnswer
          ? props.theme.palette.primary1Color
          : props.theme.palette.accent1Color
        : props.theme.palette.textColor
      : props.theme.palette.textColor};

  &.not-selected:not(:last-child) {
    border-bottom-color: transparent;
  }
`;

export const QuizAnswersElement = styled.div.attrs(props => ({
  className: 'quiz-answers-overlay'
}))`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100vw;
  height: ${props => (props.isVisible ? 'calc(100vh - 64px)' : '0vh')};
  transition: height 0.3s ease-in-out;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
  background-color: rgba(0, 0, 0, 0.41);
  z-index: 1000;

  .quiz-answers {
    width: 100%;
    transition: min-height 0.3s ease-in-out, opacity 2s linear;
    opacity: ${props => (props.isVisible ? 1 : 0)};
    min-height: ${props => props.height}px;
    height: auto;
    background-color: ${props =>
      IS_IOS ? 'transparent' : fade(props.theme.palette.canvasColor, 0.95)};
    backdrop-filter: blur(14px);
  }
`;

function QuizAnswersFn({
  isLoading,
  answers,
  onSelect,
  selectedAnswer,
  toggleVisibility,
  cardId,
  cardIndex,
  show = true
}) {
  const canDisplay = Array.isArray(answers) && show;
  return (
    <QuizAnswersElement
      onClick={() => (!isLoading ? toggleVisibility() : null)}
      isVisible={canDisplay}
      height={canDisplay ? answers.length * QUIZ_ANSWER_HEIGHT : 0}
    >
      <div className="quiz-answers">
        {canDisplay
          ? answers.map(ans => {
              const selectItem = e => {
                e.stopPropagation();
                if (!isLoading) {
                  onSelect(ans, cardId, cardIndex);
                }
              };
              const isSelected = selectedAnswer && ans.id === selectedAnswer.id;
              const isRight = isSelected && selectedAnswer.isTheRightAnswer;
              return (
                <QuizAnswerElement
                  key={ans.id}
                  answerId={ans.id}
                  selectedAnswer={selectedAnswer}
                  className={isSelected ? 'selected' : 'not-selected'}
                >
                  <div
                    style={{
                      position: 'absolute',
                      left: 0,
                      width: 40,
                      height: 40,
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}
                  >
                    <RadioButton
                      key="radio"
                      rippleStyle={{
                        left: 0,
                        color: isSelected
                          ? isRight
                            ? iReactTheme.palette.primary1Color
                            : iReactTheme.palette.accent1Color
                          : iReactTheme.palette.textColor
                      }}
                      iconStyle={{
                        fill: isSelected
                          ? isRight
                            ? iReactTheme.palette.primary1Color
                            : iReactTheme.palette.accent1Color
                          : iReactTheme.palette.textColor
                      }}
                      style={{
                        textAlign: 'center'
                      }}
                      onClick={selectItem}
                      checked={isSelected || false}
                    />
                  </div>
                  <FlatButton
                    style={{
                      height: '100%',
                      color: 'inherit'
                      // width: 'calc(100% - 40px)'
                    }}
                    fullWidth={true}
                    label={ans.text}
                    labelStyle={{
                      textTransform: 'none',
                      display: 'flex',
                      paddingLeft: 56,
                      textAlign: 'left'
                    }}
                    onClick={selectItem}
                  />
                </QuizAnswerElement>
              );
            })
          : ''}
      </div>
    </QuizAnswersElement>
  );
}

export const QuizAnswers = memo(QuizAnswersFn);
