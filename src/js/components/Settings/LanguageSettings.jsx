import React, { Component } from 'react';
import { iReactTheme } from 'js/startup/iReactTheme';
import { Header } from 'js/components/app';
import { withLogin } from 'ioc-api-interface';
import { withDictionary } from 'ioc-localization';
import { LanguageSelect } from './components';
import { Redirect } from 'react-router-dom';

const STYLES = {
  header: {
    color: iReactTheme.palette.textColor
  }
};
class LanguageSettings extends Component {
  render() {
    const { dictionary, loggedIn } = this.props;
    if (!loggedIn) {
      return <Redirect to="/" />;
    }
    return (
      <div className="language-settings page">
        <Header title={'_language_settings'} leftButtonType="back" />
        <h2 style={STYLES.header}>{dictionary._language}</h2>
        <LanguageSelect mini={false} />
      </div>
    );
  }
}

export default withLogin(withDictionary(LanguageSettings));
