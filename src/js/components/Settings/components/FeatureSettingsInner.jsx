import React, { Component } from 'react';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withMapPreferences } from 'js/modules/preferences';
import SelectField from 'material-ui-superselectfield/es';
import { iReactTheme } from 'js/startup/iReactTheme';
// import Place from 'material-ui/svg-icons/maps/place';
import { MenuIcon } from 'js/components/app/drawer/DrawerBody';

import { Subheader, Chip, Avatar, ListItem } from 'material-ui';
import Visibility from 'material-ui/svg-icons/action/visibility';
import VisibilityOff from 'material-ui/svg-icons/action/visibility-off';

import { STYLES } from './styles';

const enhance = compose(
  withMapPreferences,
  withDictionary
);

// Provisional dictionary
const PROV_DICT = {
  report: '_drawer_label_reports',
  reportRequest: '_filter_report_requests_label',
  alertAndWarning: '_home_btn_comms',
  mission: '_drawer_label_mission',
  agentLocation: '_infield_agents',
  tweet: 'Tweets'
};

const ICONS_CFG = {
  report: { type: 'ireact-icons', text: '\ue919' }, //missing icon!
  reportRequest: { type: 'ireact-pinpoint-icons', text: '\ue909' }, // '\ue91b',
  alertAndWarning: { type: 'mdi', text: 'alert' }, //'AW', //missing icon!
  mission: { type: 'ireact-pinpoint-icons', text: '\ue908' }, // '\ue919',
  agentLocation: { type: 'ireact-pinpoint-icons', text: '\ue901' }, // '\ue91a',
  tweet: { type: 'ireact-icons', text: '\ue92c' } // 'T' //missing icon!
};

const ICON_COLORS = {
  report: iReactTheme.palette.reportColor,
  reportRequest: iReactTheme.palette.reportRequestEmCommColor,
  alertAndWarning: iReactTheme.palette.alertOrWarning,
  mission: iReactTheme.palette.missionColor,
  agentLocation: iReactTheme.palette.agentLocation,
  tweet: iReactTheme.palette.tweet
};

class FeatureSettingsInner extends Component {
  handleSelection = values => {
    this.props.setIreactFeaturesOnMap(values.map(v => v.value));
  };

  onRequestDelete = itemType => event => {
    console.log('onRequestDelete', itemType);
    const { ireactFeaturesOnMap, setIreactFeaturesOnMap } = this.props;
    setIreactFeaturesOnMap(ireactFeaturesOnMap.filter(name => name !== itemType));
  };

  selectionsRenderer = values =>
    values.length ? (
      <div style={STYLES.superSelectSelection}>
        {values.map(({ value: name }, index) => {
          const cfg = ICONS_CFG[name];
          const Icon = MenuIcon(cfg.text, cfg.type);

          return (
            <Chip key={index} style={{ margin: 5 }} onRequestDelete={this.onRequestDelete(name)}>
              <Avatar
                color="#FFF"
                icon={
                  <Icon
                    style={{ ...STYLES.chipAvatarStyle, backgroundColor: ICON_COLORS[name] }}
                    color="#FFF"
                  />
                }
              />
              {this.props.dictionary(PROV_DICT[name])}
            </Chip>
          );
        })}
      </div>
    ) : (
      <div style={STYLES.noneSelected}>None Selected</div>
    );

  menuItems(allValues /* , selectedValues */) {
    return allValues.map(name => (
      <div
        key={name}
        className="list-item"
        value={name}
        label={this.props.dictionary(PROV_DICT[name])}
      >
        {this.props.dictionary(PROV_DICT[name])}
      </div>
    ));
  }

  render() {
    const {
      allSelectableItemTypes,
      ireactFeaturesOnMap,
      dictionary,
      itemIndex,
      openItemIndex,
      setOpen
    } = this.props;
    const values = ireactFeaturesOnMap.map(itemType => ({ value: itemType }));
    const LeftIcon = MenuIcon('map-marker-multiple', 'mdi');

    return (
      <ListItem
        primaryText={dictionary('_ls_features')}
        open={openItemIndex === itemIndex}
        onNestedListToggle={() => setOpen(itemIndex)}
        leftIcon={<LeftIcon />}
        primaryTogglesNestedList={true}
        className="features-select map-setting-section"
        nestedItems={[
          <ListItem
            className="nested"
            containerElement="div"
            key="fs-1"
            innerDivStyle={STYLES.nestedItemInner}
          >
            <Subheader style={STYLES.subheader}>
              <span>{dictionary('_ls_ireact_features')}</span>
            </Subheader>
            <SelectField
              className="ireact-feature-superselect"
              checkPosition="right"
              multiple={true}
              // hintText={dictionary('_ls_features_hint')}
              value={values}
              onChange={this.handleSelection}
              selectionsRenderer={this.selectionsRenderer}
              checkedIcon={<Visibility style={STYLES.checkedIcon} />}
              unCheckedIcon={<VisibilityOff style={STYLES.checkedIcon} />}
              canAutoPosition={false}
              useLayerForClickAway={true}
              menuStyle={{ width: '100%' }}
              elementHeight={62}
              selectedMenuItemStyle={{ width: '100%' }}
              style={{ width: '100%' }}
              innerDivStyle={{
                width: 'calc(100% - 72px)',
                height: 42,
                lineHeight: '42px'
              }}
              popoverClassName="ireact-feature-superselect-popover"
              popoverWidth={window.innerWidth}
            >
              {this.menuItems(allSelectableItemTypes)}
            </SelectField>
          </ListItem>
        ]}
      />
    );
  }
}

export default enhance(FeatureSettingsInner);
