import React, { Component } from 'react';
import { withMapPreferences, MAP_STYLES_NAMES } from 'js/modules/preferences';
import { withDictionary } from 'ioc-localization';
import { getAsset } from 'js/utils/getAssets';
import MapIcon from 'material-ui/svg-icons/maps/map';

import { STYLES, AVATAR_SIZE } from './styles';
import { SelectField, MenuItem, Avatar, Subheader, ListItem } from 'material-ui';

const SUFFIX_RX = /-(gl|raster)$/i;

// const prefix = IS_CORDOVA ? `${cordova.file.applicationDirectory}www/` : '';

const MAP_PICS = MAP_STYLES_NAMES.reduce((pics, style) => {
  const imageName = style.replace(SUFFIX_RX, '');
  pics[style] = `assets/mapSettings/${imageName}.png`;
  return pics;
}, {});

class BaseMapSettingsInner extends Component {
  _changeMapStyle = (evt, index, mapStyleName) => {
    this.props.changeMapStyle(mapStyleName);
    // this.props.history.push('/tabs/map');
  };

  _selectionRenderer = mapStyleName => {
    const selected = true;
    const preview = getAsset(MAP_PICS[mapStyleName]);
    // const preview = MAP_PICS[mapStyleName];
    return (
      <MenuItem
        key={'selected-mi'}
        className="menu-item"
        style={STYLES.menuItem}
        value={mapStyleName}
        primaryText={mapStyleName}
        leftIcon={
          <Avatar
            size={AVATAR_SIZE}
            style={selected ? STYLES.selectedAvatar : STYLES.avatar}
            src={preview}
          />
        }
      />
    );
  };

  render() {
    const { dictionary, preferences, itemIndex, openItemIndex, setOpen } = this.props;
    // const dict = this.props.dictionary;
    const selectedStyleName = preferences.mapStyleName;
    return (
      <ListItem
        primaryText={dictionary('_ls_base_map')}
        open={openItemIndex === itemIndex}
        onNestedListToggle={() => setOpen(itemIndex)}
        leftIcon={<MapIcon />}
        primaryTogglesNestedList={true}
        className="base-layer map-setting-section"
        nestedItems={[
          <ListItem
            className="nested"
            containerElement="div"
            key="ms-1"
            innerDivStyle={STYLES.nestedItemInner}
          >
            <Subheader style={STYLES.subheader}>
              <span>{dictionary('_map_style_list_subheader')}</span>
            </Subheader>
            <SelectField
              style={STYLES.select}
              underlineStyle={{ display: 'none' }}
              value={selectedStyleName}
              onChange={this._changeMapStyle}
              selectionRenderer={this._selectionRenderer}
              dropDownMenuProps={{ className: 'base-layer-select' }}
            >
              {MAP_STYLES_NAMES.map((mapStyleName, i) => {
                const selected = selectedStyleName === mapStyleName;
                const preview = getAsset(MAP_PICS[mapStyleName]);
                // const preview = MAP_PICS[mapStyleName];
                return (
                  <MenuItem
                    key={`ms-${i}`}
                    style={STYLES.menuItem}
                    className="menu-item"
                    value={mapStyleName}
                    innerDivStyle={STYLES.menuItemInner}
                    primaryText={mapStyleName}
                    leftIcon={
                      <Avatar
                        size={AVATAR_SIZE}
                        style={selected ? STYLES.selectedAvatar : STYLES.avatar}
                        src={preview}
                      />
                    }
                  />
                );
              })}
            </SelectField>
          </ListItem>
        ]}
      />
    );
  }
}

export default withDictionary(withMapPreferences(BaseMapSettingsInner));

// export default withRouter(withDictionary(withMapPreferences(MapSettingsInner)));
