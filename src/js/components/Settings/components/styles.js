import { iReactTheme } from 'js/startup/iReactTheme';
import { fade } from 'material-ui/utils/colorManipulator';

export const AVATAR_SIZE = 48;

export const STYLES = {
  select: { width: 'calc(100% - 32px)', height: '100%', padding: 16 },
  avatar: {
    borderColor: fade(iReactTheme.palette.textColor, 0.5),
    borderWidth: '2px',
    borderStyle: 'solid',
    boxSizing: 'border-box',
    margin: 0,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE
  },
  selectedAvatar: {
    borderColor: iReactTheme.menuItem.selectedTextColor,
    borderWidth: '2px',
    borderStyle: 'solid',
    boxSizing: 'border-box',
    margin: 0,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE
  },
  menuItem: { height: AVATAR_SIZE + 4, fontSize: 16, display: 'flex' },
  menuItemInner: {
    display: 'flex',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '100%'
  },
  selectedText: { fontWeight: 'bold' },
  subheader: { display: 'flex', justifyContent: 'space-between' },
  legendLink: { marginRight: 12 },
  checkedIcon: { top: 'calc(50% - 12px)' },
  superSelectSelection: { display: 'flex', flexWrap: 'wrap', width: '100%' },
  noneSelected: { minHeight: 42, lineHeight: '42px', width: '100%' },
  chipAvatarStyle: {
    width: 32, //'100%',
    height: 32, //'100%',
    margin: 0,
    left: 0,
    borderRadius: '50%',
    backgroundSize: 'cover',
    textAlign: 'center',
    color: iReactTheme.palette.textColor,
    fontSize: '19px',
    lineHeight: '32px'
  },
  selectedOption: { color: iReactTheme.palette.primary1Color },
  nestedItemInner: { width: '100%', margin: 0, padding: 0 }
};
