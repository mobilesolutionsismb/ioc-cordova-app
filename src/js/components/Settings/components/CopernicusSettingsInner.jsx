import React, { Component } from 'react';
import { withDictionary } from 'ioc-localization';
import { Link } from 'react-router-dom';
import Satellite from 'material-ui/svg-icons/maps/satellite';
import { Subheader, IconButton, FontIcon, ListItem } from 'material-ui';
import { STYLES } from './styles';

class CopernicusSettingsInner extends Component {
  render() {
    const { dictionary, itemIndex, openItemIndex, setOpen } = this.props;

    return (
      <ListItem
        disabled={true}
        primaryText="Copernicus"
        open={openItemIndex === itemIndex}
        onNestedListToggle={() => setOpen(itemIndex)}
        leftIcon={<Satellite />}
        primaryTogglesNestedList={true}
        className="layer-select map-setting-section"
        nestedItems={[
          <ListItem
            className="nested"
            containerElement="div"
            key="cs-1"
            innerDivStyle={STYLES.nestedItemInner}
          >
            <Subheader style={STYLES.subheader}>
              <span>{dictionary('_copernicus_layers_subheader')}</span>
              <Link to="/layerslegend" style={STYLES.legendLink}>
                <IconButton tooltip={dictionary('_layers_legend')} tooltipPosition="bottom-left">
                  <FontIcon className="material-icons">list</FontIcon>
                </IconButton>
              </Link>
            </Subheader>
          </ListItem>
        ]}
      />
    );
  }
}

export default withDictionary(CopernicusSettingsInner);
