import React, { Component } from 'react';
import { withDictionary } from 'ioc-localization';
// import { Link } from 'react-router-dom';
import Layers from 'material-ui/svg-icons/maps/layers';
import { findInSettingsTree } from 'js/modules/layersAndSettings';
import Cancel from 'material-ui/svg-icons/navigation/cancel';
import styled from 'styled-components';
import {
  Subheader,
  IconButton,
  // FontIcon,
  ListItem,
  SelectField,
  MenuItem,
  FlatButton
} from 'material-ui';
import { STYLES } from './styles';
import { MenuIcon } from 'js/components/app/drawer/DrawerBody';
import { LinkedButton } from 'js/components/app/LinkedButton';

const MDI_ICON_NAMES = {
  // features: 'map-marker-multiple',
  general: 'bank',
  atmospheric: 'weather-partlycloudy',
  hazard_and_risk: 'alert'
};

const SelectionLine = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ButtonPlaceHolder = styled.div`
  width: 48px;
  height: 48px;
`;

const NoLayers = styled.span`
  height: 100%;
  line-height: 48px;
  font-size: 16px;
  opacity: 0.7;
`;

class LayerSettingsInner extends Component {
  constructor(props) {
    super(props);
    // // const selection = this._initSelection(props);
    // const result = this._checkShouldOpen();
    // this.state = { ...result.state };
    this.state = {
      selection: [],
      legendEnabled: false // true if selection is not empty and activeSettingName is in current settingsGroup
    };
  }

  // /**
  //  * Compute initial selection state
  //  * @param {React.Props} props
  //  */
  // _initSelection(props) {
  //   const selection = [];
  //   const categorySettings = props.settingsGroup.children;
  //   const singleFirstChild = categorySettings.length === 1;
  //   if (singleFirstChild) {
  //     selection.push(categorySettings[0]);
  //   }
  //   return selection;
  // }

  _getActiveSettingsGroup = (settingsGroup, activeSettingName) =>
    findInSettingsTree(settingsGroup, sg => sg.name === activeSettingName, true, false);

  /**
   * Check if this component should be open
   * @param {React.Props} props
   */
  ___checkShouldOpen = () => {
    const { setOpen, openItemIndex, itemIndex, settingsGroup, activeSettingName } = this.props;
    const activeSettingGroup = this._getActiveSettingsGroup(settingsGroup, activeSettingName);
    let selection = [];
    if (activeSettingGroup !== null) {
      const { branch, ...rest } = activeSettingGroup;
      selection = [...branch, rest.group];
      // }
      // if (activeSettingGroup !== null) {
      if (selection.length) {
        this.setState({ selection, legendEnabled: true }, () => {
          if (itemIndex !== openItemIndex) {
            setOpen(itemIndex);
          }
        });
      } else {
        if (itemIndex !== openItemIndex) {
          setOpen(itemIndex);
        }
      }
    } else {
      if (this.state.legendEnabled) {
        this.setState({ legendEnabled: false });
      }
    }
    return { activeSettingGroup, selection };
  };

  _checkShouldOpen = () => {
    const {
      /* setOpen, openItemIndex, itemIndex, */ settingsGroup,
      activeSettingName
    } = this.props;
    const activeSettingGroup = this._getActiveSettingsGroup(settingsGroup, activeSettingName);
    let selection = [];
    let legendEnabled = false;
    if (activeSettingGroup !== null) {
      const { branch, ...rest } = activeSettingGroup;
      selection = [...branch, rest.group];
      legendEnabled = true;
      /*       if (selection.length) {
        // this.setState({ selection, legendEnabled: true }, () => {
        //   if (itemIndex !== openItemIndex) {
        //     setOpen(itemIndex);
        //   }
        // });
      } else {
        if (itemIndex !== openItemIndex) {
          setOpen(itemIndex);
        }
      } */
    } else {
      // if (this.state.legendEnabled) {
      //   this.setState({ legendEnabled: false });
      // }
    }
    return { activeSettingGroup, state: { selection, legendEnabled } };
  };

  _computeSelectionState = () => {
    const result = this._checkShouldOpen();
    const { /* activeSettingGroup, */ state } = result;
    if (this._autoref) {
      this.setState(state);
      // this.setState(state, () => {
      //   const { setOpen, openItemIndex, itemIndex } = this.props;
      //   if (
      //     activeSettingGroup !== null &&
      //     this.state.selection.length > 0 &&
      //     itemIndex !== openItemIndex
      //   ) {
      //     setOpen(itemIndex);
      //   }
      // });
    }
  };

  componentDidMount() {
    // this._checkShouldOpen();
    this._computeSelectionState();
    // setTimeout(this._computeSelectionState, 1000);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.activeSettingName !== this.props.activeSettingName) {
      if (this.props.activeSettingName !== null) {
        this._computeSelectionState();
        //this._checkShouldOpen();
      } else {
        // const selection = this._initSelection(this.props);
        this.setState({ selection: [], legendEnabled: false });
      }
    }
    // openItemIndex === itemIndex
    if (prevProps.openItemIndex !== this.props.openItemIndex) {
      if (prevProps.openItemIndex === prevProps.itemIndex) {
        // item was closed
        this._resetState(this.props);
      } else if (this.props.openItemIndex === this.props.itemIndex) {
        //this._checkShouldOpen();
        this._computeSelectionState();
      }
    }
    if (
      this.state.selection.length > 0 &&
      prevState.selection.length !== this.state.selection.length
    ) {
      const { setOpen, openItemIndex, itemIndex } = this.props;
      if (itemIndex !== openItemIndex) {
        // BUG? it is not clear but apparently without delay the selection list
        // does not render properly
        setTimeout(() => setOpen(itemIndex), 500);
      }
    }
  }

  /**
   * Return true if at leas 1 settingTask is in remoteTaskAvailable
   * @param {*} setting
   */
  _hasValidTaskIds = setting => {
    return Array.isArray(setting.iReactTasks)
      ? setting.iReactTasks.some(taskId => this.props.remoteTaskIDsAvailable.indexOf(taskId) > -1)
      : true;
  };

  _updateSelection = (depthLevel, setting) => {
    this.setState(prevState => {
      const hasChildren = setting ? this._hasChildren(setting) : false;
      const hasSingleChild = hasChildren && setting.children.length === 1;

      const prevSelection = [...prevState.selection];

      let selection = [];

      if (setting) {
        const isTerminalNode = Array.isArray(setting.iReactTasks);

        if (prevSelection[depthLevel]) {
          // must update existing selection
          prevSelection.splice(depthLevel, prevSelection.length - depthLevel);
        }
        const _children = hasChildren ? setting.children.filter(this._hasValidTaskIds) : [];

        if (hasSingleChild) {
          const singleChild = _children.length === 0 ? false : _children[0];
          // add a setting
          selection =
            !isTerminalNode && _children.length === 0
              ? [...prevSelection, setting, false]
              : [...prevSelection, setting, singleChild];
        } else {
          selection =
            !isTerminalNode && _children.length === 0
              ? [...prevSelection, setting, false]
              : [...prevSelection, setting];
        }
      } else {
        // remove
        selection = depthLevel === 0 ? [] : prevState.selection.slice(0, depthLevel);
      }
      return { selection };
    }, this._activateLayer);
  };

  // Render selected menu item
  _selectionRenderer = setting => {
    // console.log('_selRenderer', setting ? setting.displayName : 'No Layers Available');
    return setting ? setting.displayName : this.props.dictionary('_ls_no_layers');
    // return (
    //   <MenuItem
    //     key={`${setting.name}-sel`}
    //     primaryText={setting ? setting.displayName : 'No Layers Available'}
    //     value={setting}
    //   />
    // );
  };

  // Render options list
  _renderOptions = setting => {
    // console.log('_renderOptions', setting);
    return <MenuItem key={setting.name} primaryText={setting.displayName} value={setting} />;
  };

  _hasChildren = sg => sg && Array.isArray(sg.children) && sg.children.length > 0;

  /**
   * Render the whole setting three
   */
  _renderSettingsGroup = (settings, depth) => {
    // console.log('_renderSettingsGroup', depth);
    return new Array(depth).fill(null).map((e, i) => this._renderSelect(settings, i));
  };

  /**
   * Render a SelectionLine for each setting level of the setting group
   */
  _renderSelect = (settings, depthLevel) => {
    const name = this.props.settingsGroup.name; //Abp.someCrap.that.identified.theAbpSetting
    const dictionary = this.props.dictionary; // for transation

    const parentSelection = depthLevel > 0 ? this.state.selection[depthLevel - 1] : null; // parent element in the selection
    const values = (depthLevel === 0
      ? settings
      : this._hasChildren(parentSelection)
      ? parentSelection.children
      : []
    ).filter(this._hasValidTaskIds);
    const onChange = (evt, index, setting) => this._updateSelection(depthLevel, setting);
    const clearLevel = () => this._updateSelection(depthLevel, null);

    // const hasSingleChild = values.length === 1;
    const selection = this.state.selection[depthLevel] || null; //hasSingleChild ? values[0] : this.state.selection[depthLevel];
    // const disabled = false;
    // const disabled = (depthLevel > 0 && parentSelection === null) || hasSingleChild;
    // console.log(
    //   `_renderSelect depth=${depthLevel}`,
    //   selection ? selection.displayName : 'NO SELECTION'
    // );
    return [
      <SelectionLine key={`${name}-sel-${depthLevel}`}>
        {values.length > 0 && (
          <SelectField
            id={`${name}-sel-${depthLevel}`}
            className={`${name}-sel-${depthLevel}`}
            onChange={onChange}
            selectionRenderer={this._selectionRenderer}
            value={selection}
            hintText={dictionary('_ls_select_value')}
            // disabled={disabled}
          >
            {values.map(this._renderOptions)}
          </SelectField>
        )}
        {values.length === 0 && <NoLayers>{dictionary('_ls_no_layers')}</NoLayers>}
        {selection !== null && values.length > 0 && (
          <IconButton tooltip="Clear" onClick={clearLevel}>
            <Cancel />
          </IconButton>
        )}
        {selection === null && <ButtonPlaceHolder />}
      </SelectionLine>
    ];
  };

  _activateLayer = () => {
    const selection = this.state.selection;
    if (selection.length === 0) {
      this.props.removeAllLayers();
    } else {
      const activeSettingsGroup = selection[selection.length - 1];
      if (activeSettingsGroup && Array.isArray(activeSettingsGroup.iReactTasks)) {
        this.props.activateLayer(activeSettingsGroup.name);
      }
    }
  };

  _removeAllLayers = () => {
    // const selection = this._initSelection(this.props);
    this.setState({ selection: [] }, this.props.removeAllLayers);
  };

  _resetState = props => {
    const { activeSettingName } = props;
    const { selection } = this.state;
    const selectedOption = selection.length > 0 ? selection[selection.length - 1] : { name: null };

    const isActiveOption = activeSettingName !== null && selectedOption.name === activeSettingName;

    if (!isActiveOption) {
      this.setState({
        selection: [], //this._initSelection(props),
        legendEnabled: false // true if selection is not empty and activeSettingName is in current settingsGroup
      });
    }
  };

  _onNestedListToggle = () => {
    const { itemIndex, setOpen } = this.props;
    setOpen(itemIndex);
  };

  _getIconFromDisplayName = displayName => {
    const displayNameKey = displayName.toLowerCase().replace(/\s/g, '_');
    const iconName = MDI_ICON_NAMES[displayNameKey];
    return iconName ? MenuIcon(iconName, 'mdi') : Layers;
  };

  _autoref = null;

  _setRef = e => (this._autoref = e);

  render() {
    const { dictionary, itemIndex, openItemIndex, settingsGroup, activeSettingName } = this.props;
    const { legendEnabled, selection } = this.state;
    const hasChildren =
      selection.length > 0 && Array.isArray(selection[selection.length - 1].children);
    const settingsTreeDepth =
      selection.length === 0 ? 1 : hasChildren ? selection.length + 1 : selection.length;

    const selectedOption = selection.length > 0 ? selection[selection.length - 1] : { name: null };

    // console.log(selectedOption.name === activeSettingName, activeSettingName, selectedOption);
    const isActiveOption = activeSettingName !== null && selectedOption.name === activeSettingName;

    const displayActions = this.state.selection[this.state.selection.length - 1] !== false;
    // const activateEnabled = isActiveOption
    //   ? false
    //   : selection.length > 0
    //     ? !hasChildren && !legendEnabled
    //     : false;

    // const activateEnabled =
    //   selection.length > 0 ? (!hasChildren && !legendEnabled) || isActiveOption : false;
    const clearEnabled = !hasChildren && legendEnabled;
    const style = isActiveOption ? STYLES.selectedOption : {};
    const LeftIcon = this._getIconFromDisplayName(settingsGroup.displayName);
    return (
      <ListItem
        ref={this._setRef}
        style={style}
        primaryText={settingsGroup.displayName}
        open={openItemIndex === itemIndex}
        onNestedListToggle={this._onNestedListToggle}
        leftIcon={<LeftIcon {...style} />}
        primaryTogglesNestedList={true}
        className="layer-select map-setting-section"
        nestedItems={[
          <ListItem
            className="nested"
            containerElement="div"
            key="ls-1"
            innerDivStyle={STYLES.nestedItemInner}
          >
            <Subheader style={STYLES.subheader}>
              <span>{dictionary('_ls_select_layers')}</span>
            </Subheader>
          </ListItem>,
          <ListItem key="ls-2" className="list-item2">
            {this._renderSettingsGroup(settingsGroup.children, settingsTreeDepth)}
            {displayActions && (
              <SelectionLine>
                <FlatButton
                  disabled={!legendEnabled}
                  label={dictionary._ls_show_layer}
                  primary={true}
                  containerElement={<LinkedButton to="/tabs/map" disabled={!legendEnabled} />}
                />
                <FlatButton
                  disabled={!legendEnabled}
                  label={dictionary._layers_legend}
                  containerElement={
                    <LinkedButton to="/layerslegend" replace={false} disabled={!legendEnabled} />
                  }
                />
                <FlatButton
                  disabled={!clearEnabled}
                  label={dictionary._reset}
                  secondary={true}
                  onClick={this._removeAllLayers}
                />
              </SelectionLine>
            )}
          </ListItem>
        ]}
      />
    );
  }
}

export default withDictionary(LayerSettingsInner);
