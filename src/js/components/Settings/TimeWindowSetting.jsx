import React, { Component } from 'react';
import { iReactTheme } from 'js/startup/iReactTheme';
import { withAPISettings, withLogin } from 'ioc-api-interface';
import { SelectField, MenuItem } from 'material-ui';
import { Header } from 'js/components/app';
import { withDictionary } from 'ioc-localization';
import { withMapPreferences } from 'js/modules/preferences';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';

const enhance = compose(
  withDictionary,
  withMapPreferences,
  withLogin,
  withAPISettings
);

const UNITS = [
  { label: '_tw_unit_d', value: 'days' },
  { label: '_tw_unit_w', value: 'weeks' },
  { label: '_tw_unit_m', value: 'months' } /* , 'years' */
];

const CENTERED = { textAlign: 'center' };

const distances = [50, 100, 200, 300, 500, 600, 700, 800, 900, 1000];

class TimeWindowSetting extends Component {
  _getUpperBoundForTwPreferences(tw) {
    let max = 4;
    switch (tw.unit) {
      case 'days':
        max = 7;
        break;
      case 'months':
        max = 6;
        break;
      case 'weeks':
      default:
        break;
    }
    // 1, ..., max
    let arr = [...Array(max + 1).keys()];
    arr.shift();
    console.log(tw.unit, arr);
    return arr;
  }

  handleTimeQuantityChange = (event, index, value) => {
    const quantity = value;
    const unit = this.props.liveTWPreferences.unit;
    this.props.setLiveTimeWindowExtent(
      quantity,
      unit /* Add padding in the future if needed: default 2 weeks*/
    );
  };

  handleTimeUnitChange = (evt, index, value) => {
    const quantity = this.props.liveTWPreferences.quantity;
    const unit = value;
    this.props.setLiveTimeWindowExtent(
      quantity,
      unit /* Add padding in the future if needed: default 2 weeks*/
    );
  };

  updateContentsRadiusValue = (event, index, value) => {
    this.props.updateContentsRadius(value);
  };

  render() {
    const { liveTWPreferences, contentsRadius, dictionary, loggedIn } = this.props;
    if (!loggedIn) {
      return <Redirect to="/" />;
    }
    const values = this._getUpperBoundForTwPreferences(liveTWPreferences);
    return (
      <div className="tw-settings page" style={{ color: iReactTheme.palette.textColor }}>
        <Header title={'_app_settings'} leftButtonType="back" />
        <h2>{dictionary('_app_settings_tw')}</h2>
        <div
          style={{
            width: '100%',
            height: 64,
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'flex-end'
          }}
        >
          <SelectField
            style={{ maxWidth: 100 }}
            floatingLabelText={dictionary('_app_settings_tw_quantity')}
            menuStyle={CENTERED}
            selectedMenuItemStyle={CENTERED}
            menuItemStyle={CENTERED}
            value={liveTWPreferences.quantity}
            onChange={this.handleTimeQuantityChange}
          >
            {values.map(u => (
              <MenuItem key={`q-${u}`} value={u} primaryText={u} />
            ))}
          </SelectField>
          <SelectField
            style={{ maxWidth: 200 }}
            floatingLabelText={dictionary('_app_settings_tw_unit')}
            value={liveTWPreferences.unit}
            onChange={this.handleTimeUnitChange}
          >
            {UNITS.map(u => (
              <MenuItem key={`u-${u.value}`} value={u.value} primaryText={dictionary(u.label)} />
            ))}
          </SelectField>
          {/* ADD a select with units and add SET button */}
        </div>
        <h2>{dictionary('_app_settings_contents')}</h2>
        <div
          style={{
            width: '100%',
            height: 64,
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'baseline'
          }}
        >
          <SelectField
            style={{ maxWidth: 150, marginLeft: 32 }}
            floatingLabelText={dictionary('_app_settings_radius')}
            menuStyle={CENTERED}
            selectedMenuItemStyle={CENTERED}
            value={contentsRadius}
            onChange={this.updateContentsRadiusValue}
          >
            {distances.map(u => (
              <MenuItem key={`cr-${u}`} value={u} primaryText={u} />
            ))}
          </SelectField>
          <span>km</span>
        </div>
      </div>
    );
  }
}

export default enhance(TimeWindowSetting);
