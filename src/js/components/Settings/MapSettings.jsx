import './index.scss';
import React, { Component } from 'react';
import {
  BaseMapSettingsInner,
  LayerSettingsInner,
  // CopernicusSettingsInner,
  FeatureSettingsInner
} from './components';
import { Header } from 'js/components/app';
import { List, Divider, ListItem } from 'material-ui';
import { withLayersAndSettings, findInSettingsTree } from 'js/modules/layersAndSettings';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { logError } from 'js/utils/log';
import { withDictionary } from 'ioc-localization';
import { withLogin } from 'ioc-api-interface';
import { Redirect } from 'react-router-dom';

const MAIN_SG_NAME = 'App.UserSettingsGroups.Layers';
const COPERNINICUS_SG_NAME = 'App.UserSettingsGroups.Layers.ExternalServices';

const searchMainSGCallback = f => f.name === MAIN_SG_NAME;

class MapSettings extends Component {
  state = {
    openItemIndex: -1
  };

  _extractLayersConfig = () => {
    let layersConfig = [];
    try {
      if (this.props.remoteLayers && Array.isArray(this.props.remoteLayers.children)) {
        const mainSettingsGroup = findInSettingsTree(
          this.props.remoteLayers,
          searchMainSGCallback,
          true
        );
        if (mainSettingsGroup) {
          return mainSettingsGroup.group.children.filter(
            child => child.name !== COPERNINICUS_SG_NAME
          );
        }
      }
    } catch (err) {
      logError('Cannot extract layer settings', err, this.props.remoteLayers);
      appInsights.trackException(
        err,
        'MapSettings::_extractLayersConfig',
        { remoteLayers: this.props.remoteLayers },
        {},
        SeverityLevel.Error
      );
    }
    return layersConfig;
  };

  setOpen = index => {
    this.setState({ openItemIndex: index === this.state.openItemIndex ? -1 : index });
  };

  _updateSettings = async () => {
    try {
      await this.props.loadSettings();
    } catch (err) {
      logError(err);
    }
  };

  componentDidMount() {
    this._updateSettings();
  }

  render() {
    const {
      activeSettingName,
      removeAllLayers,
      activateLayer,
      remoteTaskIDsAvailable,
      remoteSettingsUpdating,
      dictionary,
      loggedIn
    } = this.props;
    if (!loggedIn) {
      return <Redirect to="/" />;
    }
    const { openItemIndex } = this.state;
    const layersConfig = this._extractLayersConfig();
    // const copernicusItemIndex = layersConfig.length + 2;
    return (
      <div className="map-settings page">
        <Header title={'_map_settings'} leftButtonType="back" />
        <List className="map-settings list">
          <BaseMapSettingsInner
            itemIndex={0}
            openItemIndex={openItemIndex}
            setOpen={this.setOpen}
          />
          <Divider />
          <FeatureSettingsInner
            itemIndex={1}
            openItemIndex={openItemIndex}
            setOpen={this.setOpen}
          />
          <Divider />
          {remoteSettingsUpdating && <ListItem primaryText={`${dictionary('_loading')}...`} />}
          {!remoteSettingsUpdating &&
            layersConfig.map((child, index) => [
              <LayerSettingsInner
                key={child.name + '-ls'}
                itemIndex={2 + index}
                openItemIndex={openItemIndex}
                setOpen={this.setOpen}
                settingsGroup={child}
                activeSettingName={activeSettingName}
                removeAllLayers={removeAllLayers}
                activateLayer={activateLayer}
                remoteTaskIDsAvailable={remoteTaskIDsAvailable}
                // selectLayer={activateIReactSettingByName}
              />,
              <Divider key={child.name + '-div'} />
            ])}
          {/* <CopernicusSettingsInner
            itemIndex={copernicusItemIndex}
            openItemIndex={openItemIndex}
            setOpen={this.setOpen}
            activeSettingName={activeSettingName}
          /> */}
        </List>
      </div>
    );
  }
}
export default withLogin(withDictionary(withLayersAndSettings(MapSettings)));
