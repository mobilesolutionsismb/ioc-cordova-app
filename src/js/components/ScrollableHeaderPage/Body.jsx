import React, { Component } from 'react';

import { Element } from 'react-scroll';


const DEFAULT_STYLE = {
    width: '100%',
    position: 'relative',
    transition: 'height 0.35s ease-in-out'
};

class Body extends Component {
    static defaultProps = {
        height: '70%'
    }

    _getHeightStyle = () => {
        return {
            height: this.props.height,
            minHeight: this.props.height
        };
    }

    render() {
        const Content = this.props.content;
        return (
            <Element name='scrollable-body'
                className="scrollable-body"
                style={{ ...DEFAULT_STYLE, ...this._getHeightStyle() }}
            >
                {Content}
            </Element>
        );
    }
}

export default Body;
