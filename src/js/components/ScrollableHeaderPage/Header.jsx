import React, { Component } from 'react';
import { IconButton, FontIcon, Paper } from 'material-ui';
import { withRouter } from 'react-router';
import { Header as AppHeader } from 'js/components/app';

const BackButton = ({ onClick }) => (
  <IconButton
    className="back-button"
    onClick={onClick}
    style={{ position: 'absolute', left: 0, top: 0, zIndex: 100 }}
  >
    <FontIcon className="material-icons">arrow_back</FontIcon>
  </IconButton>
);

const DEFAULT_STYLE = {
  width: '100%',
  position: 'relative',
  opacity: 1
};

const INNER_FULL = {
  boxShadow: 'none',
  maxHeight: 'calc(100% - 64px)',
  minHeight: 'calc(100% - 64px)',
  height: 'calc(100% - 64px)',
  opacity: 1
};

const INNER_EXPANDED = {
  boxShadow: 'none',
  maxHeight: 0,
  minHeight: 0,
  height: 0,
  opacity: 0
};

class Header extends Component {
  static defaultProps = {
    height: '33%',
    onBackButtonClick: null,
    headerMinifiedStyle: {},
    pageTitle: ''
  };

  _getHeightStyle = () => {
    return {
      maxHeight: this.props.height,
      height: this.props.height,
      minHeight: this.props.height
    };
  };

  goBack = () => {
    if (this.props.history.length > 0) {
      this.props.history.goBack();
    } else {
      this.props.history.replace('/tabs');
    }
  };

  render() {
    const Content = this.props.content;
    let Title = null;
    if (typeof this.props.pageTitle === 'string') {
      Title = (
        <AppHeader
          title={this.props.pageTitle}
          leftButtonType={this.props.history.length > 0 ? 'back' : 'null'}
          onLeftHeaderButtonClick={
            typeof this.props.onBackButtonClick === 'function'
              ? this.props.onBackButtonClick
              : this.goBack
          }
          style={this.props.headerStyle}
        />
      );
    } else if (typeof this.props.pageTitle === 'object') {
      Title = <AppHeader {...this.props.pageTitle} style={this.props.headerStyle} />;
    } else {
      Title = (
        <BackButton
          onClick={
            typeof this.props.onBackButtonClick === 'function'
              ? this.props.onBackButtonClick
              : this.goBack
          }
        />
      );
    }

    return (
      <Paper
        zDepth={1}
        className="scrollable-header"
        style={{ ...DEFAULT_STYLE, ...this.props.headerStyle, ...this._getHeightStyle() }}
      >
        {Title}
        <Paper
          zDepth={0}
          className="scrollable-header-inner"
          style={this.props.expanded ? INNER_EXPANDED : INNER_FULL}
        >
          {Content}
        </Paper>
      </Paper>
    );
  }
}

export default withRouter(Header);
