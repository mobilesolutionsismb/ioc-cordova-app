import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import Header from './Header';
import Body from './Body';

import { Swipeable } from 'react-swipeable';
import Scroll, { scroller as scroll } from 'react-scroll';

const SCROLL_OPTS = {
  smooth: 'easeInOut',
  duration: 350
};

const DEFAULT_STYLE = {
  position: 'absolute',
  overflow: 'hidden'
};

/**
 * This component mimicks the scrollable behavior of material design
 */
class ScrollableHeaderPage extends Component {
  scrollContainer = null;

  static defaultProps = {
    onBackButtonClick: null
  };

  state = {
    expanded: false
  };

  setExpanded = () => {
    this.setState({ expanded: true });
  };

  setReduced = () => {
    this.setState({ expanded: false });
  };

  scrollBodyUp = () => {
    this.scrollTo('scrollable-body');
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.expanded === true && prevState.expanded === false) {
      //expanding
      this.scrollToBottom();
    }
    if (this.state.expanded === false && prevState.expanded === true) {
      //reducing
      this.scrollToTop();
    }
  }

  scrollToTop = () => {
    Scroll.animateScroll.scrollToTop({ ...SCROLL_OPTS, containerId: this.props.containerID });
  };

  scrollToBottom = () => {
    Scroll.animateScroll.scrollToBottom({ ...SCROLL_OPTS, containerId: this.props.containerID });
  };

  scrollTo = to => {
    scroll.scrollTo(to, { ...SCROLL_OPTS, container: findDOMNode(this.scrollContainer) });
  };

  _setScrollContainerRef = ref => {
    this.scrollContainer = ref;
  };

  render() {
    return (
      <Swipeable
        style={{ ...DEFAULT_STYLE, ...this.props.style }}
        className={this.props.className}
        onSwipedUp={this.setExpanded}
        onSwipedDown={this.setReduced}
        id={this.props.containerID}
        ref={this._setScrollContainerRef}
      >
        <Header
          className={`${this.props.className}-scrollable-header`}
          pageTitle={this.props.pageTitle}
          content={this.props.headerContent}
          expanded={this.state.expanded}
          headerStyle={{
            backgroundColor: this.props.headerBackgroundColor,
            color: this.props.headerTextColor
          }}
          height={this.state.expanded ? 64 : '33%'}
          onBackButtonClick={this.props.onBackButtonClick}
        />
        <Body
          className={`${this.props.className}-scrollable-body`}
          content={this.props.content}
          height={this.state.expanded ? 'calc(100% - 64px)' : '67%'}
        />
      </Swipeable>
    );
  }
}

export default ScrollableHeaderPage;
