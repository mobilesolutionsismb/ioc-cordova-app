import React, { Component } from 'react';
import { connect } from 'react-redux';
import { appInsights } from 'js/startup/errorHandlingAndTracking';
import { withLogin } from 'ioc-api-interface';
import { RESET_ACTIONS as resetActions } from 'js/modules/resetActions';
import { getNewReportStore } from 'js/modules/newReport2/commons';
import { getFeatureStore } from 'js/utils/ireactFeatureStorage';
import { API } from 'ioc-api-interface';
import { Redirect } from 'react-router-dom';

class Logout extends Component {
  _clearAll = async props => {
    console.warn('Clearing everything in storage');
    localStorage.removeItem('gamification_started'); //gamification access flag
    await props.logout();
    appInsights.clearAuthenticatedUserContext();
    for (const resetAction of resetActions) {
      props.dispatch({ type: resetAction });
    }
    const newReportStore = getNewReportStore();
    await newReportStore.clear();
    const featureStore = getFeatureStore();
    await featureStore.clear();
    const api = API.getInstance();
    await api.clearInnerStorage();
  };

  componentDidMount() {
    this._clearAll(this.props);
  }

  render() {
    return this.props.loggedIn ? <div /> : <Redirect to="/" from="/logout" />;
  }
}
// Adding connect() injects this.props.dispatch()
export default connect()(withLogin(Logout));
