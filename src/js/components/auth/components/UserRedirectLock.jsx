import React from 'react';
import { Redirect } from 'react-router-dom';
import { withLogin } from 'ioc-api-interface';
// import PushNotificationsRedirectLock from './PushNotificationsRedirectLock';

function checkHasAccess(userType, allowedUserTypes) {
    if (!Array.isArray(allowedUserTypes)) {
        return true;
    }
    else {
        return allowedUserTypes.indexOf(userType) > -1;
    }
}

//OLD, no problems
const UserRedirectLock = (Component, unauthenticatedLandingPage = null, authenticatedLandingPage = null, allowedUserTypes = null) => {

    const Lock = ({
        history,
        // Login stuff
        loggedIn, user,
        rememberCredentials,
        login,
        logout,
        passwordForgot,
        register,
        storeCredentials,
        ...rest }) => {
        const userType = user ? user.userType : null;
        const hasAccessGranted = checkHasAccess(userType, allowedUserTypes);

        //may be a more in-depth check such has token is valid
        if (loggedIn) {
            if (hasAccessGranted) {
                if (authenticatedLandingPage === null) {
                    // const PNLockedComponent = PushNotificationsRedirectLock(Component);
                    // return <PNLockedComponent {...rest} />;
                    return <Component {...rest}/>;
                }
                else {
                    if (rest.pathname !== authenticatedLandingPage) {
                        return <Redirect to={authenticatedLandingPage} />;
                    }
                    else {
                        return <Component {...rest} />;
                    }
                }
            }
            else {
                history.goBack();
                return null;
            }
        } else {
            if (unauthenticatedLandingPage === null) {
                return <Component {...rest} />;
            }
            else {
                if (rest.pathname !== unauthenticatedLandingPage) {
                    return <Redirect to={unauthenticatedLandingPage} />;
                }
                else {
                    return <Component {...rest} />;
                }
            }
        }
    };

    return withLogin(Lock);
};

export default UserRedirectLock;
