import './login.scss';
import React, { Component } from 'react';
import { withLayersAndSettings } from 'js/modules/layersAndSettings';
import { withPartialRegistration } from 'js/modules/partialRegistration';
import { withLogin, withEnumsAndRoles, withGamification } from 'ioc-api-interface';
// import { withRouter } from 'react-router-dom';
import { withMessages, withLoader } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import { compose } from 'redux';
import LoginForm from './LoginForm';
import { Checkbox } from 'material-ui';
// import { IS_ANDROID } from 'js/utils/getAppInfo';
// import HardwareBackButtonHandler from 'js/components/app/HardwareBackButtonHandler';
import {
  loginFormContainerStyle,
  logoBGStyle,
  logoStyle,
  checkboxWrapperStyle,
  formStyle
} from './styles';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { LanguageSelect } from 'js/components/Settings/components';
import { PageContainer } from 'js/components/app/commons';
import { Redirect } from 'react-router-dom';
import { loadDictionary } from 'js/utils/getAssets';

const languageSelectStyle = {
  position: 'absolute',
  right: 0,
  top: 0
};

const enhance = compose(
  withDictionary,
  withLogin,
  withGamification,
  withEnumsAndRoles,
  withLayersAndSettings,
  withMessages,
  withLoader,
  withPartialRegistration
);

class Login extends Component {
  onLoginFormSubmit = async formData => {
    try {
      this.props.loadingStart();
      const user = await this.props.login(formData.user, formData.password, null, null);
      appInsights.setAuthenticatedUserContext(`${user.id}`, user.userName, true);
      this.props.getEnums();
      // this.props.loadSettings();
      if (user.userType === 'citizen') {
        this.props.updateScores();
        this.props.updateSkillsAndAchievements();
      }
      console.log('%cLOGIN Success', 'color: white; background: lime', user);
      this.props.loadingStop();
    } catch (e) {
      this.props.loadingStop();
      if (e) {
        console.log('%cLOGIN Error', 'color: white; background: darkRed', e);
        appInsights.trackException(
          e,
          'Login::onLoginFormSubmit',
          { api: 'login' },
          {},
          SeverityLevel.Error
        );
        this.props.pushError(e);
      } else {
        appInsights.trackException(
          e,
          'Login::onLoginFormSubmit',
          { api: 'login' },
          {},
          SeverityLevel.Critical
        );
        this.props.pushError('_unknown_error');
      }
    }
  };

  storePassword = (e, isChecked) => {
    this.props.storeCredentials(isChecked);
  };

  _checkPendingRegistration = () => {
    let destination = null;
    if (this.props.hasPendingRegistration) {
      destination =
        this.props.partialUser.isPhoneConfirmed === false ? '/smsvalidation' : '/emailvalidation';
      // this.props.history.replace(destination);
    }
    return destination;
  };

  _checkLanguageIsLoaded = async () => {
    if (this.props.dictIsEmpty) {
      const params = await loadDictionary(this.props.locale);
      const { locale, translationLoader, loading } = params;
      if (loading !== true) {
        this.props.loadDictionary(locale, translationLoader);
      }
    }
  };

  componentDidUpdate(prevProps) {
    if (this.props.dictIsEmpty && !prevProps.dictIsEmpty) {
      this._checkLanguageIsLoaded();
    }
  }

  // componentDidUpdate(prevProps) {
  //   if (this.props.hasPendingRegistration && !prevProps.hasPendingRegistration) {
  //     this._checkPendingRegistration();
  //   }
  // }

  componentDidMount() {
    this._checkLanguageIsLoaded();
    this._checkPendingRegistration();
  }

  render() {
    if (this.props.loggedIn === false) {
      if (this.props.hasPendingRegistration) {
        const destination = this._checkPendingRegistration();
        return <Redirect to={destination} from="/" />;
      } else {
        return (
          <PageContainer className="login page">
            <LanguageSelect mini={true} style={languageSelectStyle} />
            <div className="login-logo-bg" style={logoBGStyle}>
              <div className="login-logo" style={logoStyle} />
            </div>
            <div className="login-form-container" style={loginFormContainerStyle}>
              <LoginForm
                style={formStyle}
                onSubmit={this.onLoginFormSubmit}
                dictionaryIsEmpty={this.props.dictionary.isEmpty}
                dictionary={this.props.dictionary}
                locale={this.props.locale}
              />
              <div style={checkboxWrapperStyle}>
                <Checkbox
                  onCheck={this.storePassword}
                  label={this.props.dictionary('_remember_password')}
                />
              </div>
            </div>
          </PageContainer>
        );
      }
    } else {
      return <Redirect to="/home" from="/" />;
    }
  }
}

export default enhance(Login);
