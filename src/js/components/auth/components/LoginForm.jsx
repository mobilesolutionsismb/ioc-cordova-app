import React, { Component } from 'react';
import { FormComponent } from 'js/components/app/FormComponent';
import { RaisedButton, FlatButton } from 'material-ui';
import { emailRule, passwordRule } from 'js/utils/fieldValidation';
import { LinkedButton } from 'js/components/app/LinkedButton';
import styled from 'styled-components';

const StyledForm = styled.form`
  width: 100%;
  height: auto;
  /* height: calc(100% - 64px); */
`;

const FormContentWrapper = styled.div`
  width: 100%;
  height: 172px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  overflow-y: auto;
  overflow-x: hidden;
`;

const StyledFormAction = styled.div`
  width: 100%;
  height: auto;
  height: 142px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 8px 0;
`;

const loginFormConfig = [
  {
    name: 'user',
    trim: 'left',
    inputType: 'text',
    label: '_reg_form_email',
    rules: [emailRule],
    defaultValue: ''
  },
  {
    name: 'password',
    trim: true,
    inputType: 'password',
    label: '_reg_form_password',
    rules: [passwordRule],
    defaultValue: ''
  }
];

const style = {
  button: {
    width: 304
  }
};

export class LoginForm extends Component {
  render() {
    const { dictionary } = this.props;
    return (
      <FormComponent
        className="login-form"
        config={loginFormConfig}
        dictionary={dictionary}
        onSubmit={this.props.onSubmit}
        formComponent={StyledForm}
        innerComponent={FormContentWrapper}
        actionComponent={StyledFormAction}
        actions={() => [
          <RaisedButton
            key="login"
            type="submit"
            className="ui-button"
            primary={true}
            style={style.button}
            label={dictionary._signin}
          />,
          <FlatButton
            key="password-forgot"
            className="ui-button"
            disableTouchRipple={true}
            containerElement={<LinkedButton to="/passwordForgot" replace={false} />}
            label={dictionary._password_forgot}
          />,
          <FlatButton
            key="register"
            className="ui-button"
            disableTouchRipple={true}
            containerElement={<LinkedButton to="/terms" replace={false} />}
            label={dictionary._signup}
          />
        ]}
      />
    );
  }
}

export default LoginForm;
