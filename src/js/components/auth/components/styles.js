import { getAsset } from 'js/utils/getAssets';

export const logoStyle = {
  width: '80%',
  maxWidth: 300,
  height: 100,
  position: 'relative',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'contain',
  backgroundImage: getAsset('logo', true)
};

export const logoBGStyle = {
  width: '100%',
  height: '33%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  backgroundImage: getAsset('logBg', true)
};

export const loginFormContainerStyle = {
  width: '100%',
  height: '67%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start',
  alignItems: 'center'
};

export const selectedFlagStyle = {
  borderBottomWidth: '1px',
  borderBottomStyle: 'solid'
};

export const fullWidthStyle = {
  width: '100%',
  marginTop: 8,
  marginBottom: 8
};

export const checkboxWrapperStyle = {
  width: '70%',
  maxWidth: 250,
  height: 32
};

export const formStyle = {
  width: '70%',
  maxWidth: 250
};
