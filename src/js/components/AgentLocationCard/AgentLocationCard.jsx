import React /* , { Component } */ from 'react';
import styled from 'styled-components';
import MoreVert from 'material-ui/svg-icons/navigation/more-vert';
import PowerSettings from 'material-ui/svg-icons/action/power-settings-new';
import Accessibility from 'material-ui/svg-icons/action/accessibility';
import Toll from 'material-ui/svg-icons/action/toll';

import { localizeDate } from 'js/utils/localizeDate';
import { IconButton, FontIcon } from 'material-ui';
import { iReactTheme } from 'js/startup/iReactTheme';
import { withMessages } from 'js/modules/ui';
import { lightGreenA400, redA700, white, grey400 } from 'material-ui/styles/colors';

const agentIconStyle = {
  backgroundColor: iReactTheme.palette.agentLocation,
  borderRadius: '100%',
  width: '28px',
  height: '28px',
  lineHeight: '28px',
  textAlign: 'center',
  marginTop: '8px'
};

const padding = 8;

const InnerCard = styled.div`
  padding: ${padding}px;
  width: calc(100% - ${2 * padding}px);
  height: calc(100% - ${2 * padding}px);
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  align-content: stretch;
  position: relative;
`;

const defaultLineSize = 14;

const Line = styled.div`
  height: ${props => (props.size ? props.size : defaultLineSize)}px;
  line-height: ${props => (props.size ? props.size : defaultLineSize)}px;
  font-size: ${props => Math.ceil((props.size ? props.size : defaultLineSize) * 0.8)}px;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  align-items: center;
  justify-content: space-between;
`;

const BaseColumn = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const AgentIconColumn = styled(BaseColumn)`
  align-items: center;
  width: 48px;
  justify-content: flex-start;
`;

const AgentDetailsColumn = styled(BaseColumn)`
  align-items: flex-start;
  width: calc(100% - 48px);
  justify-content: space-around;
`;

const Actions = styled.div`
  top: 0px;
  right: 0px;
  width: 48px;
  height: 48px;
  position: absolute;
`;

const Name = ({ name, surname }) => (
  <Line size={18}>
    {surname} {name}
  </Line>
);

const wearableIconStyle = {
  fontSize: 12
};

const WearableInfo = ({ oxygenLevel, deviceStatus, fallDetection }) => {
  const hasOxygen = oxygenLevel && oxygenLevel.type;
  const oxygenColor = hasOxygen ? (oxygenLevel.type === 'low' ? redA700 : lightGreenA400) : white;
  const fallDownColor = fallDetection
    ? grey400
    : fallDetection !== 'fallModerate'
    ? redA700
    : lightGreenA400;
  const connectedColor = deviceStatus === 'off' ? redA700 : lightGreenA400;
  return [
    hasOxygen ? <Toll key="oxygen" color={oxygenColor} style={wearableIconStyle} /> : null,
    <Accessibility key="fall" color={fallDownColor} style={wearableIconStyle} />,
    <PowerSettings key="connect" color={connectedColor} style={wearableIconStyle} />
  ];
};

const WearableDetails = ({ wearableInfo }) => (
  <Line className="wearable-info">{wearableInfo && <WearableInfo {...wearableInfo} />}</Line>
);

const MissionsAndUpdateRow = ({ ongoingMissions, lastModified, dictionary }) => (
  <Row>
    <Line>{ongoingMissions.length} ongoing missions</Line>
    <Line style={{ paddingRight: 24 }}>
      {localizeDate(lastModified, dictionary.locale.language, 'll LTS')}
    </Line>
  </Row>
);

export const AgentLocationCard = withMessages(function _AgentCard({
  agentLocationFeature,
  /* currentUserId, */ dictionary,
  pushModalMessage
}) {
  const { properties } = agentLocationFeature;
  const {
    // id,
    name,
    surname,
    // userName,
    phoneNumber,
    // emailAddress,
    lastModified,
    // organizationUnitName,
    ongoingMissions,
    wearableInfo
  } = properties;

  const actionClick = e => {
    e.preventDefault();
    e.stopPropagation();

    console.log(`Call ${phoneNumber}`);
    pushModalMessage(phoneNumber, () => (
      <a href={`tel:${phoneNumber}`} style={{ lineHeight: '32px' }}>
        <span className="material-icons">phone</span>
        <span
          style={{ fontSize: 24, marginLeft: 8 }}
        >{`Press here to call ${name} ${surname}`}</span>
      </a>
    ));
  };

  return (
    <InnerCard className="agentLocation-card">
      <AgentIconColumn>
        {/* EDIT ICON HERE */}
        <FontIcon className="ireact-pinpoint-icons" style={agentIconStyle}>
          {wearableInfo ? '\ue90c' : '\ue901'}
        </FontIcon>
      </AgentIconColumn>
      <AgentDetailsColumn>
        <Name name={name} surname={surname} />
        <WearableDetails wearableInfo={wearableInfo} />
        <MissionsAndUpdateRow
          ongoingMissions={ongoingMissions}
          lastModified={lastModified}
          dictionary={dictionary}
        />
      </AgentDetailsColumn>
      <Actions>
        <IconButton onClick={actionClick}>
          <MoreVert />
        </IconButton>
      </Actions>
    </InnerCard>
  );
});
