import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { compose } from 'redux';
import { withLogin, withAPISettings, withMapLayers } from 'ioc-api-interface';
import { withDictionary } from 'ioc-localization';
import { Header } from 'js/components/app';
import { UserPositionUpdater, Tabs } from './components';
import {
  HeaderControls,
  ReportsQueueIndicator,
  MissionsButton,
  BTDeviceButton
} from 'js/components/Home';
import { /* IS_ANDROID, */ IS_IOS } from 'js/utils/getAppInfo';
import { FeatureSelectionContext } from 'js/components/app/FeatureSelectionContext';
import { withLayersAndSettings } from 'js/modules/layersAndSettings';

import { TopCenteredPage } from 'js/components/app';
import { logError } from 'js/utils/log';

const enhance = compose(
  withLogin,
  withAPISettings,
  withMapLayers,
  withDictionary,
  withLayersAndSettings
);

const VALID_TABS = [/* 'events', */ 'map', 'emergencyCommunications', 'reports', 'social'];

function isValidTab(tabName) {
  return VALID_TABS.indexOf(tabName) > -1;
}

class Main extends Component {
  _updateSettings = async () => {
    try {
      if (
        !this.props.mapLayersUpdating &&
        Array.isArray(this.props.mapLayers.availableTaskIds) &&
        this.props.mapLayers.availableTaskIds.length === 0
      ) {
        // Try a getLayers and update settings
        await this.props.getLayers();
        await this.props.loadSettings();
        // await Promise.all[(this.props.getLayers(), this.props.loadSettings())];
      }
    } catch (err) {
      logError(err);
    }
  };

  componentDidUpdate(prevProps) {
    if (prevProps.liveAOI === null && this.props.liveAOI !== null) {
      this._updateSettings();
    }
  }

  render() {
    const {
      user,
      liveAOI,
      setLiveAreaOfInterestFromUserPosition,
      match,
      dictionary,
      loggedIn
    } = this.props;
    if (!loggedIn) {
      return <Redirect to="/" push={false} />;
    }
    const isValidUserData = user || liveAOI !== null;

    const userUpdateProps = {
      loggedIn,
      user,
      liveAOI,
      setLiveAreaOfInterestFromUserPosition
    };

    const tabname = match.params.tabname;

    const userType = user ? user.userType : null; //POSSIBLE BUG HERE?

    const isValid = isValidTab(tabname);

    return (
      <FeatureSelectionContext.Consumer>
        {FeatureSelectionState => (
          <UserPositionUpdater
            {...userUpdateProps}
            selectedFeature={FeatureSelectionState.selectedFeature}
            onLiveAOIUpdate={this.props.getLayers}
          >
            {(requestGeolocationUpdate, geolocationPosition, geolocationUpdating) =>
              isValidUserData ? (
                <TopCenteredPage className="main">
                  <Header
                    showLogo={true}
                    // leftButtonType="back"
                    leftButtonType="drawer"
                    rightButton={
                      <HeaderControls className="header-controls">
                        {userType !== 'citizen' && !IS_IOS && <BTDeviceButton />}
                        {userType !== 'citizen' && (
                          <MissionsButton label={dictionary._drawer_label_mission} />
                        )}
                        <ReportsQueueIndicator
                          selectedFeature={FeatureSelectionState.selectedFeature}
                          selectFeature={FeatureSelectionState.selectFeature}
                        />
                      </HeaderControls>
                    }
                  />
                  {isValid && (
                    <Tabs
                      activeTab={tabname}
                      liveAOI={liveAOI}
                      geolocationPosition={geolocationPosition}
                      geolocationUpdating={geolocationUpdating}
                      requestGeolocationUpdate={requestGeolocationUpdate}
                    />
                  )}
                  {!isValid && <h2>{`404 Not Found /tabs/${tabname}`}</h2>}
                </TopCenteredPage>
              ) : (
                <div />
              )
            }
          </UserPositionUpdater>
        )}
      </FeatureSelectionContext.Consumer>
    );
  }
}

const EnhancedMain = enhance(Main);

function MainRoute(props) {
  return <Route render={routeProps => <EnhancedMain {...routeProps} {...props} />} />;
}

export default MainRoute;
