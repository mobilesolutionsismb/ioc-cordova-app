import React, { Component } from 'react';
import { Page } from 'js/components/app/commons';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { isOldGeolocation } from 'js/utils/localizeDate';
import {
  updateRemotePosition,
  getTimeDistance,
  getDistanceInKm
} from 'js/utils/updateRemotePosition';
import { getUserType } from 'js/utils/userPermissions';
import { withMap } from 'js/modules/map';
import { compose } from 'redux';
import { withGeolocation } from 'ioc-geolocation';
import { withLoader, withMessages } from 'js/modules/ui';
import { logMain, logError, logWarning } from 'js/utils/log';
import { withMapPreferences } from 'js/modules/preferences';
import { withDevicePermissions } from 'js/modules/devicePermissions';
import { UserPermissionPrimer } from 'js/components/app/UserPermissionPrimer';
import debounce from 'lodash.debounce';
import nop from 'nop';

const enhance = compose(
  withGeolocation,
  withLoader,
  withMessages,
  withMapPreferences,
  withDevicePermissions,
  withMap /* , withIREACTFeatures */
);

const MAP_GEOLOCATION_ZOOM = 16;
const UPDATE_AOI_DISTANCE_THRESHOLD = 1; //km
const UPDATE_AOI_TIME_THRESHOLD = 15; //minutes

const POSITION_UPDATE_FREQ = IS_PRODUCTION ? 300000 : 60000; // 5 min : 1min

// Produces an initial update of AOI
const INITIAL_FAKE_POS = {
  timestamp: 0,
  coords: {
    longitude: 0,
    latitude: 0
  }
};

// const IS_WEB_PERMISSION_SUPPORTED = !IS_CORDOVA && !!navigator.permissions;

// function checkLocationPermissionsCordova() {
//   if (!IS_CORDOVA) {
//     return Promise.resolve(false);
//   }
//   return new Promise((success, fail) => {
//     cordova.plugins.diagnostic.isLocationAuthorized(success, fail);
//   });
// }

// function checkPermissionsWeb() {
//   return navigator.permissions
//     .query({ name: 'geolocation' })
//     .then(permissionStatus => permissionStatus.state === 'granted');
// }

//TODO MOVE AT LAYOUT LEVEL

class UserPositionUpdater extends Component {
  // See https://stackoverflow.com/questions/34544314/setstate-can-only-update-a-mounted-or-mounting-component-this-usually-mea/40969739#40969739
  _autoRef = null;

  _setAutoRef = e => {
    this._autoRef = e;
  };

  positionTimer = null;

  _initPositionCheckTimer = debounce((force = false) => {
    this.requestGeolocationUpdate(force);
    this.positionTimer = setInterval(() => {
      if (this._autoRef) {
        this.requestGeolocationUpdate();
      }
    }, POSITION_UPDATE_FREQ);
  }, 300);

  _clearPositionTimer = () => {
    clearInterval(this.positionTimer);
  };

  _updateMapCenterIfNeeded = (nextUserPosition, forceMapUpdate = false) => {
    if (!forceMapUpdate && this.props.selectedFeature !== null) {
      return;
    }
    const { center, zoom } = this.props;
    const _zoom = zoom >= MAP_GEOLOCATION_ZOOM ? zoom : MAP_GEOLOCATION_ZOOM;
    if (
      (center[0] === 0 && center[1] === 0) || // should check for NaN but must change the geolocation plugin first
      forceMapUpdate === true
    ) {
      this.props.updateMapSettings({ center: nextUserPosition, zoom: _zoom });
    } /* else {
      const bounds = this.props.bounds;
      if (!booleanContains(bboxPolygon(bounds), point(nextUserPosition))) {
        this.props.updateMapSettings({ center: nextUserPosition, zoom: _zoom });
      }
    } */
  };

  _updateLiveArea = async position => {
    try {
      if (this.props.loggedIn) {
        const { zoom } = this.props;
        const _zoom = zoom >= MAP_GEOLOCATION_ZOOM ? zoom : MAP_GEOLOCATION_ZOOM;
        const center = [position.coords.longitude, position.coords.latitude];
        const contentsRadius = this.props.contentsRadius;
        this.props.updateMapSettings({
          center,
          zoom: _zoom
        });
        const nextLiveAOI = await this.props.setLiveAreaOfInterestFromUserPosition(
          position,
          contentsRadius
        );
        // this.props.updateMapSettings({
        //   center,
        //   bounds: nextLiveAOI.bounds,
        //   maxBounds: nextLiveAOI.bounds,
        //   zoom: _zoom
        // });
        if (this.props.onLiveAOIUpdate) {
          await this.props.onLiveAOIUpdate();
        }
        return nextLiveAOI;
      } else {
        return null;
      }
    } catch (err) {
      logError('Cannot update live area', err);
      appInsights.trackException(
        err,
        'UserPositionUpdater::_updateAreaIfNeeded',
        {},
        {},
        SeverityLevel.Error
      );
    }
  };

  _updateAreaIfNeeded = async (currentPosition, nextPosition, forceMapUpdate = false) => {
    if (currentPosition && nextPosition) {
      const tooOld =
        currentPosition.timestamp === 0 ||
        getTimeDistance(currentPosition.timestamp, nextPosition.timestamp, 'minutes') >
          UPDATE_AOI_TIME_THRESHOLD;
      const tooFar =
        getDistanceInKm(
          [currentPosition.coords.longitude, currentPosition.coords.latitude],
          [nextPosition.coords.longitude, nextPosition.coords.latitude]
        ) > UPDATE_AOI_DISTANCE_THRESHOLD;
      if (tooOld || tooFar) {
        await this._updateLiveArea(nextPosition);
        const userType = getUserType(this.props.user);
        if (userType === 'citizen') {
          try {
            await updateRemotePosition(nextPosition);
          } catch (err) {
            logError('Cannot update remote position', err);
            appInsights.trackException(
              err,
              'UserPositionUpdater::_updateAreaIfNeeded',
              {},
              {},
              SeverityLevel.Error
            );
          }
        }
      } else if (forceMapUpdate) {
        this._updateMapCenterIfNeeded(
          [nextPosition.coords.longitude, nextPosition.coords.latitude],
          forceMapUpdate
        );
      }
    } else {
      this._updateMapCenterIfNeeded(
        [nextPosition.coords.longitude, nextPosition.coords.latitude],
        forceMapUpdate
      );
    }
  };

  _requestGeolocationPermission = async () => {
    try {
      await this.props.requestDevicePermission('location');
    } catch (err) {
      this.props.pushError(err);
    }
  };

  _checkGeolocationPermissions = async () => {
    try {
      const locationGranted = await this.props.checkDevicePermissionGranted('location');
      logMain('Location granted', locationGranted);
      if (locationGranted) {
        // Go with the program
        this._initPositionCheckTimer(true);
      } else {
        const locationDenied = this.props.locationPermission === 'DENIED';
        // Ask for permission
        this.props.pushModalMessage(
          '_geol_permission_primer_title',
          (onClose, dictionary) => (
            <UserPermissionPrimer type="location" denied={locationDenied} dictionary={dictionary} />
          ),
          locationDenied
            ? { _close: null }
            : {
                _close: nop,
                _next: this._requestGeolocationPermission
              }
        );
      }
    } catch (err) {
      this.props.pushError(err);
    }
  };

  componentDidMount() {
    if (this.props.loggedIn) {
      this._checkGeolocationPermissions();
    }
  }

  componentWillUnmount() {
    this._clearPositionTimer();
  }

  componentDidUpdate(prevProps) {
    if (this.props.geolocationUpdating !== prevProps.geolocationUpdating) {
      if (this.props.geolocationUpdating === true) {
        console.log(
          '%c 🛰️ Updating Geolocation: ' + Date.now(),
          'color: orange; background: #583c63;'
        );
      } else {
        console.log(
          '%c 🛰️ Updated Geolocation ' + Date.now(),
          'color: chartreuse; background: #583c63;'
        );
      }
    }
    if (
      (!prevProps.loggedIn && this.props.loggedIn) ||
      (this.props.loggedIn && !this.props.liveAOI)
    ) {
      this._updateAreaIfNeeded(INITIAL_FAKE_POS, this.props.geolocationPosition, false);
    }
    if (prevProps.locationIsGranted === false && this.props.locationIsGranted === true) {
      this._initPositionCheckTimer(true);
    }
    if (prevProps.loggedIn && !this.props.loggedIn) {
      this._clearPositionTimer();
    }
  }

  _updatePosition = async () => {
    let geolocationPos = null;
    try {
      geolocationPos = await this.props.updatePosition();
      console.log('%c geolocationPos', 'color: lightgreen;', geolocationPos);
    } catch (posError) {
      logWarning('Position acquire Error', posError);
      if (posError.code === 1) {
        appInsights.trackException(
          posError,
          'Login::_updatePosition',
          { geolocation: 'updatePosition', error: 'Geolocation permission denied' },
          {},
          SeverityLevel.Error
        );
        const dictionary = this.props.dictionary;
        const title = dictionary._geolocation_permission_denied;
        const message = dictionary._geolocation_permission_denied_message;
        this.props.pushModalMessage(title, message, {
          _close: null
        });
      }
    }
    return geolocationPos;
  };

  requestGeolocationUpdate = async (force = false) => {
    if (
      force === true ||
      isOldGeolocation(this.props.geolocationPosition, null, { amount: 5, unit: 'minutes' }) ||
      this.props.liveAOI === null
    ) {
      try {
        const geolocation = this.props.geolocationPosition;
        const newGeolocation = await this._updatePosition();
        if (newGeolocation) {
          // console.log('HOME::updatePosition', newGeolocation);
          this._updateAreaIfNeeded(geolocation, newGeolocation, force);
        }
        return newGeolocation;
      } catch (posError) {
        logError('Cannot update position', posError);
        if (posError.code === 1) {
          appInsights.trackException(
            posError,
            'UserPositionUpdater::requestGeolocationUpdate',
            { geolocation: 'updatePosition', error: 'Geolocation permission denied' },
            {},
            SeverityLevel.Error
          );
          const dictionary = this.props.dictionary;
          const title = dictionary._geolocation_permission_denied;
          const message = dictionary._geolocation_permission_denied_message;
          this.props.pushModalMessage(title, message, {
            _close: null
          });
        }
        return posError;
      }
    } else {
      return this.props.geolocationPosition;
    }
  };

  render() {
    const {
      geolocationPosition,
      geolocationUpdating,
      locationIsGranted,
      children /* ,
      ...rest */
    } = this.props;
    const requestGeolocationUpdate = this.requestGeolocationUpdate;
    const requestPositionPermission = locationIsGranted ? nop : this._checkGeolocationPermissions;
    return (
      <Page ref={this._setAutoRef} /*  {...rest} */>
        {children(
          requestGeolocationUpdate,
          geolocationPosition,
          geolocationUpdating,
          requestPositionPermission
        )}
      </Page>
    );
  }
}

export default enhance(UserPositionUpdater);
