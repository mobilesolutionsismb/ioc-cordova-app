export const STYLES = {
  tab: {
    position: 'relative',
    width: '100%',
    height: '100%',
    overflow: 'hidden'
  },
  wrapper: {
    position: 'absolute',
    width: '100vw',
    height: 'calc(100vh - 112px)'
  },
  controls: {
    position: 'absolute',
    display: 'block',
    zIndex: 99
  },
  legendButton: {
    bottom: 96,
    left: 8
  },
  positionButton: {
    bottom: 96,
    right: 8
  },
  newReportButton: {
    bottom: 40,
    right: 8
  },
  compass: {
    top: 48,
    right: 8
  },
  notificationsButton: {
    top: 48,
    left: 8
  },
  mapSettingsButton: {
    bottom: 40,
    left: 8
  }
};

export default STYLES;
