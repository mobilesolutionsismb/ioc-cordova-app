import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardText, FontIcon } from 'material-ui';
import { withDictionary } from 'ioc-localization';

export const STYLES = {
  tab: { width: '100%', height: '100%', overflow: 'hidden' },
  list: {
    width: '100%',
    height: 'calc(100% - 16px)',
    overflowX: 'hidden',
    overflowY: 'auto',
    position: 'relative',
    padding: '8px 0px'
  },
  scrollControl: { position: 'fixed', bottom: 40, right: 8 },
  listFooter: { width: '100%', height: 60 },
  eventsIndicatorOverlay: {
    top: 0,
    left: 0,
    width: '100vw',
    height: 32,
    display: 'flex',
    justifyContent: 'center', //'space-between',
    alignItems: 'center'
  }
};

export const ListHeader = styled.div.attrs(props => ({ className: 'list-header' }))`
  width: 100%;
  height: 48px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  /* position: relative; */
  color: ${props => props.theme.palette.textColor};
  background-color: ${props => props.theme.palette.appBarColor};

  .list-header__item {
    &.text {
      margin-right: auto;
      margin-left: 0px;
    }
  }
`;

export const SCROLL_OPTS = {
  smooth: true,
  containerId: null
};

//TODO MOVE IN A FILE
export const ListFooter = () => <div className="list-footer" style={STYLES.listFooter} />;

export const TabComponent = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

export const NoEntriesComponent = withDictionary(({ title, subtitle, dictionary }) => (
  <Card>
    <CardHeader
      title={title || 'title'}
      subtitle={subtitle || 'subtile'}
      actAsExpander={true}
      showExpandableButton={true}
    />
    <CardText expandable={true}>
      <h4>{dictionary._no_content_1}</h4>
      <ul>
        <li>{dictionary._no_content_2}</li>
        <li>
          <span>{dictionary._no_content_3} </span>
          <Link to="/settings/timewindow">
            <b>{dictionary._no_content_4} </b>
            <FontIcon className="material-icons" style={{ fontSize: 16 }}>
              settings
            </FontIcon>
          </Link>
        </li>
      </ul>
    </CardText>
  </Card>
));
