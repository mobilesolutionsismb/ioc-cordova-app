import React, { Component } from 'react';
import { DropDownMenu, MenuItem } from 'material-ui';
import { logMain, logError } from 'js/utils/log';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withLoader, withMessages } from 'js/modules/ui';
import nop from 'nop';

const enhance = compose(
  withLoader,
  withMessages,
  withDictionary
);

class SorterSelect extends Component {
  static defaultProps = {
    refreshItems: () => new Promise(succ => setTimeout(succ, 1000)),
    availableSorters: [],
    selectedSorter: '',
    setSorter: nop,
    pushError: nop
  };

  _handleChange = (event, index, value) => {
    logMain('SorterSelect', index, value);
    this._setSorter(value);
  };

  _setSorter = async sorterName => {
    try {
      this.props.setSorter(sorterName);
    } catch (e) {
      logError('SorterSelect', e);
      this.props.pushError(e);
    }
  };

  _getMenuItems = () => {
    return this.props.availableSorters.map((fv, i) => (
      <MenuItem
        style={{ minWidth: '40vw', width: '100%' }}
        className="sorter-menu-item"
        key={i}
        value={fv}
        primaryText={this.props.dictionary(fv)}
      />
    ));
  };

  _manualReload = async () => {
    try {
      await this.props.refreshItems(/* this.props.loadingStart, this.props.loadingStop */);
      logMain('SorterSelect: refresh success');
    } catch (e) {
      logError('SorterSelect: refresh error', e);
    }
  };

  render() {
    return (
      <DropDownMenu
        className="list-header__item"
        autoWidth={true}
        anchorOrigin={{
          horizontal: 'left',
          vertical: 'center'
        }}
        targetOrigin={{
          horizontal: 'left',
          vertical: 'center'
        }}
        // style={{ color: 'lime' }}
        // style={{ position: 'absolute', top: -4, right: 0, height: 40}}
        iconStyle={{
          height: 48,
          position: 'relative',
          // top: -52,
          // right: -32,
          top: -12,
          right: 8,
          padding: 0,

          width: 24
        }}
        labelStyle={{
          height: 48,
          lineHeight: '48px',
          paddingLeft: 0,
          paddingRight: 8,
          display: 'inline-block'
        }}
        underlineStyle={{ margin: 'auto', borderTopColor: 'transparent' }}
        value={this.props.selectedSorter}
        onChange={this._handleChange}
      >
        {this._getMenuItems()}
      </DropDownMenu>
    );
  }
}

export default enhance(SorterSelect);
