import React, { Component } from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { Checkbox } from 'material-ui';
import CheckboxChecked from 'material-ui/svg-icons/toggle/check-box';
import { logMain } from 'js/utils/log';
import nop from 'nop';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withTweetsFilters } from 'js/modules/tweetsFilters';

const enhance = compose(
  withDictionary,
  withTweetsFilters
);

const ColoredCheckbox = ({ color }) => <CheckboxChecked color={color} />;
const themed = muiThemeable();

const STYLES = {
  radioButton: {
    width: 'auto',
    maxWidth: 'calc(33% - 4px)',
    padding: '0 2px'
  },
  cbxLabel: {
    float: 'none',
    left: 0,
    fontSize: 13,
    lineHeight: '24px',
    width: '100%'
  }
};

class TweetsListFilters extends Component {
  static defaultProps = {
    tweetsFilters: [],
    addTweetsFilter: nop,
    removeTweetsFilter: nop
  };

  onCheck = (event, isInputChecked) => {
    logMain(isInputChecked ? 'Checked' : 'Unchecked', event.target.value);
    const value = event.target.value.split(':');
    const filterCategory = value[0];
    const filterName = value[1];
    if (isInputChecked) {
      this.props.addTweetsFilter(filterCategory, filterName);
    } else {
      this.props.removeTweetsFilter(filterCategory, filterName);
    }
  };

  _isActiveFilter = (filterCategory, filterName, nextProps = null) => {
    const props = nextProps ? nextProps : this.props;
    return (
      props.tweetsFilters.hasOwnProperty(filterCategory) &&
      Array.isArray(props.tweetsFilters[filterCategory]) &&
      props.tweetsFilters[filterCategory].indexOf(filterName) > -1
    );
  };

  render() {
    const { muiTheme, dictionary } = this.props;
    const palette = muiTheme.palette;
    const showGeolocalized = this._isActiveFilter('hasGeolocation', '_has_geolocation');
    const showValidated = this._isActiveFilter('isValid', '_is_valid');
    // const showRetweets = this._isActiveFilter('isRetweet', '_is_retweet');
    const showWithPicture = this._isActiveFilter('hasPicture', '_has_picture');

    return (
      <div className="tweets-list-filters">
        <Checkbox
          checkedIcon={showGeolocalized ? <ColoredCheckbox color={palette.textColor} /> : undefined}
          className="tweets-filter-cbx"
          value="hasGeolocation:_has_geolocation"
          label={<span className="filter-cbx-label">{dictionary('_filter_tweets_geo')}</span>}
          checked={showGeolocalized}
          labelStyle={STYLES.cbxLabel}
          style={STYLES.radioButton}
          onCheck={this.onCheck}
        />
        <Checkbox
          checkedIcon={
            showValidated ? <ColoredCheckbox color={palette.validatedColor} /> : undefined
          }
          className="tweets-filter-cbx"
          value="isValid:_is_valid"
          label={<span className="filter-cbx-label">{dictionary('_filter_tweets_valid')}</span>}
          checked={showValidated}
          labelStyle={STYLES.cbxLabel}
          style={STYLES.radioButton}
          onCheck={this.onCheck}
        />
        {/* <Checkbox
          checkedIcon={showRetweets ? <ColoredCheckbox color={palette.textColor} /> : undefined}
          className="tweets-filter-cbx"
          value="isRetweet:_is_retweet"
          label={<span className="filter-cbx-label">{dictionary('_filter_tweets_retweet')}</span>}
          checked={showRetweets}
          labelStyle={STYLES.cbxLabel}
          style={STYLES.radioButton}
          onCheck={this.onCheck}
        /> */}
        <Checkbox
          checkedIcon={showWithPicture ? <ColoredCheckbox color={palette.textColor} /> : undefined}
          className="tweets-filter-cbx"
          value="hasPicture:_has_picture"
          label={<span className="filter-cbx-label">{dictionary('_filter_tweets_picture')}</span>}
          checked={showWithPicture}
          labelStyle={STYLES.cbxLabel}
          style={STYLES.radioButton}
          onCheck={this.onCheck}
        />
      </div>
    );
  }
}

export default enhance(themed(TweetsListFilters));
