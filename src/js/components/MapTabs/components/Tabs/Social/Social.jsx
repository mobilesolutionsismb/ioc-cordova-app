import './styles.scss';
import React, { Component } from 'react';
import TweetsListHeader from './TweetsListHeader';
// import TweetsListFilters from './TweetsListFilters';
import EventIndicatorOverlay from '../EventIndicatorOverlay';
import { Page } from 'js/components/app/commons';
import { compose } from 'redux';
import { withAppSocial } from 'ioc-api-interface';
import { withDictionary } from 'ioc-localization';
import { logError } from 'js/utils/log';
import { GeoJSONFeaturesProvider, GeoJSONItemsList } from 'js/components/GeoJSONFeaturesProvider';
import { withTweetsFilters } from 'js/modules/tweetsFilters';
import { SORTERS } from 'js/modules/tweetsFilters';
import { withRouter } from 'react-router';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';
import { TweetCard } from 'js/components/Social';
const ITEM_TYPE = 'tweet';

const enhance = compose(
  withDictionary,
  withAppSocial,
  withTweetsFilters
);

class Social extends Component {
  _updateTweets = async (force = false) => {
    try {
      await this.props.updateTweets(force);
    } catch (err) {
      logError(err);
    }
  };

  render() {
    const {
      dictionary,
      tweetFeaturesURL,
      tweetsSorter,
      tweetsFiltersDefinition,
      tweetFeaturesStats,
      tweetsUpdating,
      history,
      selectedFeature,
      selectFeature,
      deselectFeature
    } = this.props;

    const total = tweetFeaturesStats ? tweetFeaturesStats.totalFeaturesCount : 0;

    return (
      <Page className="social-tab-inner">
        <GeoJSONFeaturesProvider
          className="reports-provider"
          filters={tweetsFiltersDefinition}
          sorter={SORTERS[tweetsSorter]}
          sourceURL={tweetFeaturesURL}
          itemType={ITEM_TYPE}
        >
          {(features, featuresUpdating, featuresUpdateError, filtering, sorting) => [
            <EventIndicatorOverlay key="tweets-overlay" transparent={false} />,
            // <TweetsListFilters key="tweets-filters" />,
            <TweetsListHeader
              key="tweets-header"
              loading={tweetsUpdating}
              refreshItems={this._updateTweets.bind(this, true)}
              // text={dictionary('_emcomms_list_count', features.length, total)}
              text={dictionary('_results_list_total', total)}
            />,
            <GeoJSONItemsList
              headerSize={196} // 244 with filters
              key="tweets-list"
              updateFeaturesFn={this._updateTweets}
              sorting={sorting}
              filtering={filtering}
              features={features}
              featuresUpdating={featuresUpdating}
              featuresUpdateError={featuresUpdateError}
              itemType={ITEM_TYPE}
              compareFn={areFeaturesEqual}
              selectedFeature={selectedFeature}
              select={selectFeature}
              deselect={deselectFeature}
              listElement={TweetCard}
              listeElementHeight={130}
              history={history}
            />
          ]}
        </GeoJSONFeaturesProvider>
      </Page>
    );
  }
}

export default withRouter(enhance(Social));
