import React, { Component } from 'react';
import { compose } from 'redux';
import { Checkbox } from 'material-ui';
import { withReportFilters } from 'js/modules/reportFilters';
import { withDictionary } from 'ioc-localization';
import muiThemeable from 'material-ui/styles/muiThemeable';
import CheckboxChecked from 'material-ui/svg-icons/toggle/check-box';
const themed = muiThemeable();
const ColoredCheckbox = ({ color }) => <CheckboxChecked color={color} />;

const enhance = compose(withDictionary, withReportFilters);

const STYLES = {
  radioButton: {
    width: 'auto',
    //width: 'calc(33% - 4px)',
    maxWidth: 'calc(33% - 4px)',
    padding: '0 2px'
  },
  // cbIcon: {
  //     /*        width: 12,
  //             height: 12,
  //             fontSize: 12,
  //             marginRight: 2*/
  // },
  cbxLabel: {
    float: 'none',
    left: 0,
    fontSize: 11,
    lineHeight: '24px',
    width: '100%'
  }
};

class ReportsFilters extends Component {
  onCheck = (event, isInputChecked) => {
    console.log(isInputChecked ? 'Checked' : 'Unchecked', event.target.value);
    const value = event.target.value.split(':');
    const filterCategory = value[0];
    const filterName = value[1];
    // The "Validated only" filter is equivalent to remove all status filters but _is_status_validated
    if (filterName === '_is_status_validated') {
      if (isInputChecked) {
        this.props.removeReportFilter(filterCategory, '_is_status_inappropriate');
        this.props.removeReportFilter(filterCategory, '_is_status_inaccurate');
        this.props.removeReportFilter(filterCategory, '_is_status_submitted');
      } else {
        this.props.addReportFilter(filterCategory, '_is_status_inappropriate');
        this.props.addReportFilter(filterCategory, '_is_status_inaccurate');
        this.props.addReportFilter(filterCategory, '_is_status_submitted');
      }
    } else {
      if (isInputChecked) {
        this.props.addReportFilter(filterCategory, filterName);
      } else {
        this.props.removeReportFilter(filterCategory, filterName);
      }
    }
  };

  _isActiveFilter = (filterCategory, filterName, nextProps = null) => {
    const props = nextProps ? nextProps : this.props;
    return (
      props.reportFilters.hasOwnProperty(filterCategory) &&
      Array.isArray(props.reportFilters[filterCategory]) &&
      props.reportFilters[filterCategory].indexOf(filterName) > -1
    );
  };

  render() {
    const { muiTheme, dictionary, reportFilters } = this.props;
    const palette = muiTheme.palette;
    const authChecked = this._isActiveFilter('userType', '_is_authority');
    const citizenChecked = this._isActiveFilter('userType', '_is_citizen');
    const validatedChecked =
      reportFilters.status.length === 1 &&
      reportFilters.status.indexOf('_is_status_validated') > -1;

    return (
      <div className="reports-list-filters">
        {/*                <Checkbox
                    value="all"
                    label="All"
                    iconStyle={STYLES.cbIcon}
                    labelStyle={STYLES.cbxLabel}
                    style={STYLES.radioButton}
                />*/}
        <Checkbox
          checkedIcon={authChecked ? <ColoredCheckbox color={palette.authorityColor} /> : undefined}
          className="report-filter-cbx"
          value="userType:_is_authority"
          label={<span className="filter-cbx-label">{dictionary('_filter_auth_label')}</span>}
          checked={authChecked}
          labelStyle={STYLES.cbxLabel}
          style={STYLES.radioButton}
          onCheck={this.onCheck}
        />
        <Checkbox
          checkedIcon={
            citizenChecked ? <ColoredCheckbox color={palette.citizensColor} /> : undefined
          }
          className="report-filter-cbx"
          value="userType:_is_citizen"
          label={<span className="filter-cbx-label">{dictionary('_filter_citizens_label')}</span>}
          checked={citizenChecked}
          labelStyle={STYLES.cbxLabel}
          style={STYLES.radioButton}
          onCheck={this.onCheck}
        />
        <Checkbox
          checkedIcon={
            validatedChecked ? <ColoredCheckbox color={palette.validatedColor} /> : undefined
          }
          className="report-filter-cbx"
          value="status:_is_status_validated"
          label={<span className="filter-cbx-label">{dictionary('_filter_auth_only_label')}</span>}
          checked={validatedChecked}
          labelStyle={STYLES.cbxLabel}
          style={STYLES.radioButton}
          onCheck={this.onCheck}
        />
      </div>
    );
  }
}

export default enhance(themed(ReportsFilters));
