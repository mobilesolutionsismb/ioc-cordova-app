import './styles.scss';
import React, { Component } from 'react';
import { withReports } from 'ioc-api-interface';
import nop from 'nop';
import { GeoJSONFeaturesProvider, GeoJSONItemsList } from 'js/components/GeoJSONFeaturesProvider';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';
import ReportsListHeader from './ReportsListHeader';
import ReportsFilters from './ReportsFilters';
import EventIndicatorOverlay from '../EventIndicatorOverlay';
import { withDictionary } from 'ioc-localization';
import { SmallReportCard } from 'js/components/ReportsCommons';
import { withLogin } from 'ioc-api-interface';
import { withReportFilters } from 'js/modules/reportFilters'; //TODO withReportFiltersDefinitions (defs only)
import { SORTERS } from 'js/modules/reportFilters';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import { logError } from 'js/utils/log';

const enhance = compose(
  withDictionary,
  withReports,
  withReportFilters
);

const ITEM_TYPE = 'report';

const ReportListElement = withDictionary(
  withLogin(({ user, dictionary, locale, feature, isSelected, onMapButtonClick }) => (
    <SmallReportCard
      requesterType={user.userType}
      requesterId={user.id}
      dictionary={dictionary}
      locale={locale}
      reportProperties={feature.properties}
      coordinates={feature.geometry.coordinates}
      showMapIcon={true}
      onMapButtonClick={onMapButtonClick}
    />
  ))
);

class Reports extends Component {
  static defaultProps = {
    reportFeaturesURL: null,
    reportFeaturesStats: {
      totalFeaturesCount: 0
    },
    updateReports: nop,
    selectedFeature: null,
    selectFeature: nop,
    deselectFeature: nop
  };

  _updateReports = async (reload = false) => {
    try {
      await this.props.updateReports(reload);
    } catch (err) {
      logError(err);
    }
  };

  render() {
    const {
      reportFeaturesURL,
      reportsUpdating,
      selectedFeature,
      selectFeature,
      deselectFeature,
      dictionary,
      reportFeaturesStats,
      reportFiltersDefinition,
      reportSorter,
      history
    } = this.props;

    const total = reportFeaturesStats ? reportFeaturesStats.totalFeaturesCount : 0;

    return (
      <GeoJSONFeaturesProvider
        className="reports-provider"
        filters={reportFiltersDefinition}
        sorter={SORTERS[reportSorter]}
        sourceURL={reportFeaturesURL}
        itemType={ITEM_TYPE}
      >
        {(features, featuresUpdating, featuresUpdateError, filtering, sorting) => [
          <EventIndicatorOverlay key="report-overlay" transparent={false} />,
          <ReportsFilters key="report-filters" />,
          <ReportsListHeader
            key="report-header"
            loading={reportsUpdating}
            refreshItems={this._updateReports.bind(this, true)}
            text={dictionary('_reports_list_count', features.length, total)}
          />,
          <GeoJSONItemsList
            key="report-list"
            updateFeaturesFn={this._updateReports}
            sorting={sorting}
            filtering={filtering}
            features={features}
            featuresUpdating={featuresUpdating}
            featuresUpdateError={featuresUpdateError}
            itemType={ITEM_TYPE}
            compareFn={areFeaturesEqual}
            selectedFeature={selectedFeature}
            select={selectFeature}
            deselect={deselectFeature}
            listElement={ReportListElement}
            listeElementHeight={100}
            history={history}
          />
        ]}
      </GeoJSONFeaturesProvider>
    );
  }
}

export default withRouter(enhance(Reports));
