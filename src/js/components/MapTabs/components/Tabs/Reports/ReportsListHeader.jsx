import React, { Component } from 'react';
import { withReportFilters, availableSorters } from 'js/modules/reportFilters';
import { ListHeader } from '../commons';
import SorterSelect from '../SorterSelect';
import RefreshButton from '../RefreshButton';

class ReportsListHeader extends Component {
  render() {
    const { text, refreshItems, reportSorter, setReportSorter, loading } = this.props;

    return (
      <ListHeader className="reports">
        <RefreshButton tooltip={'_refresh'} refreshItems={refreshItems} loading={loading} />
        <div className="reports list-header__item text">{text}</div>
        <SorterSelect
          availableSorters={availableSorters}
          selectedSorter={reportSorter}
          setSorter={setReportSorter}
        />
      </ListHeader>
    );
  }
}

export default withReportFilters(ReportsListHeader);
