import React, { Component } from 'react';
import { compose } from 'redux';
import { STYLES } from './commons';
import { withAPISettings, withEmergencyEvents } from 'ioc-api-interface';
import { fade } from 'material-ui/utils/colorManipulator';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { withDictionary } from 'ioc-localization';
import { localizeDate } from 'js/utils/localizeDate';
import { IconButton, FontIcon } from 'material-ui';
import moment from 'moment';

const enhance = compose(
  withEmergencyEvents,
  withAPISettings,
  withDictionary
);

const TEXT_STYLE = {
  width: 'calc(100% - 48px)',
  maxWidth: 'calc(100% - 48px)',
  whiteSpace: 'nowrap',
  overflowX: 'hidden',
  textOverflow: 'ellipsis',
  textAlign: 'center'
};

const TEXT_ITEM_STYLE = {
  margin: '0 4px'
};

class EventIndicatorOverlay extends Component {
  static defaultProps = {
    transparent: true
  };

  _deactivate = () => {
    try {
      this.props.deactivateEvent();
      this.props.deselectEvent();
    } catch (e) {
      console.error('Error when deactivating event', e);
    }
  };

  render() {
    const palette = this.props.muiTheme.palette;
    const {
      locale,
      dictionary,
      /* activeEventTW, */ liveTW,
      activeEvent,
      activeEventId,
      style,
      transparent
    } = this.props;
    const hasActiveEvent = activeEventId !== -1;
    const dateStart = hasActiveEvent ? activeEvent.properties.start : liveTW.dateStart;
    // const dateEnd = hasActiveEvent ? activeEvent.properties.end : liveTW.dateEnd;
    const ds = localizeDate(dateStart, locale, 'll');
    const now = moment();
    const de = localizeDate(now, locale, 'll');

    const hasOngoingEvent = hasActiveEvent && activeEvent.properties.isOngoing;

    const bgColor =
      transparent === true
        ? hasOngoingEvent
          ? palette.ongoingEventsColor
          : style.backgroundColor
            ? style.backgroundColor
            : palette.primary2Color
        : palette.canvasColor;
    const _style = {
      ...style,
      backgroundColor: bgColor === 'transparent' ? bgColor : fade(bgColor, 0.6),
      ...STYLES.eventsIndicatorOverlay
    };

    let eventLabel;
    if (hasActiveEvent) {
      if (activeEvent.properties.displayName) {
        eventLabel = dictionary('_event_name', '' + activeEvent.properties.displayName);
      } else {
        eventLabel = dictionary('_event_id', '' + activeEvent.properties.id);
      }
    } else {
      eventLabel = 'LIVE'; //dictionary('_realtime_event_mode');
    }

    const color = palette.textColor;

    return (
      <div className="events-indicator-overlay" style={_style}>
        <span className="events-indicator-overlay-text" style={{ ...TEXT_STYLE, color }}>
          <small style={TEXT_ITEM_STYLE}>{eventLabel}</small>
          <small style={TEXT_ITEM_STYLE}>{`${ds} - ${de}`}</small>
        </span>
        {hasActiveEvent && (
          <IconButton className="back-button" onClick={this._deactivate}>
            <FontIcon className="material-icons" color={color}>
              clear
            </FontIcon>
          </IconButton>
        )}
      </div>
    );
  }
}

export default enhance(muiThemeable()(EventIndicatorOverlay));
