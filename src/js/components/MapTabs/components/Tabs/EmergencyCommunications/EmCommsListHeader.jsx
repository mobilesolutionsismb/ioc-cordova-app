import React, { Component } from 'react';
import { withEmergencyCommunicationFilters, availableSorters } from 'js/modules/emcommFilters';
import { ListHeader } from '../commons';
import SorterSelect from '../SorterSelect';
import RefreshButton from '../RefreshButton';

class EmCommListHeader extends Component {
  render() {
    const { text, refreshItems, emcommSorter, setEmcommSorter, loading } = this.props;

    return (
      <ListHeader className="emcomms">
        <RefreshButton tooltip={'_refresh'} refreshItems={refreshItems} loading={loading} />
        <div className="emcomms list-header__item text">{text}</div>
        <SorterSelect
          availableSorters={availableSorters}
          selectedSorter={emcommSorter}
          setSorter={setEmcommSorter}
        />
      </ListHeader>
    );
  }
}

export default withEmergencyCommunicationFilters(EmCommListHeader);
