import React, { Component } from 'react';
import { compose } from 'redux';
import { Checkbox } from 'material-ui';
import {
  withEmergencyCommunicationFilters /* , availableFilters */
} from 'js/modules/emcommFilters';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { withDictionary } from 'ioc-localization';
import { logMain } from 'js/utils/log';
// import CheckboxOutline from 'material-ui/svg-icons/toggle/check-box-outline-blank';
import CheckboxChecked from 'material-ui/svg-icons/toggle/check-box';

const ColoredCheckbox = ({ color }) => <CheckboxChecked color={color} />;
const themed = muiThemeable();

const enhance = compose(withDictionary, withEmergencyCommunicationFilters);

const STYLES = {
  radioButton: {
    width: 'auto',
    //width: 'calc(33% - 4px)',
    maxWidth: 'calc(33% - 4px)',
    padding: '0 2px'
  },
  // cbIcon: {
  //     /*        width: 12,
  //             height: 12,
  //             fontSize: 12,
  //             marginRight: 2*/
  // },
  cbxLabel: {
    float: 'none',
    left: 0,
    fontSize: 11,
    lineHeight: '24px',
    width: '100%'
  }
};

class EmCommFilters extends Component {
  onCheck = (event, isInputChecked) => {
    logMain(isInputChecked ? 'Checked' : 'Unchecked', event.target.value);
    const value = event.target.value.split(':');
    const filterCategory = value[0];
    const filterName = value[1];
    if (isInputChecked) {
      this.props.addEmcommFilter(filterCategory, filterName);
    } else {
      this.props.removeEmcommFilter(filterCategory, filterName);
    }
  };

  _isActiveFilter = (filterCategory, filterName, nextProps = null) => {
    const props = nextProps ? nextProps : this.props;
    return (
      props.emcommFilters.hasOwnProperty(filterCategory) &&
      Array.isArray(props.emcommFilters[filterCategory]) &&
      props.emcommFilters[filterCategory].indexOf(filterName) > -1
    );
  };

  render() {
    const { muiTheme, dictionary } = this.props;
    const palette = muiTheme.palette;
    const showAlertsChecked = this._isActiveFilter('emcommType', '_is_alert');
    const showWarningsChecked = this._isActiveFilter('emcommType', '_is_warning');
    const showReportRequestsChecked = this._isActiveFilter('emcommType', '_is_report_request');

    return (
      <div className="emcomm-list-filters">
        <Checkbox
          checkedIcon={
            showAlertsChecked ? <ColoredCheckbox color={palette.textColor} /> : undefined
          }
          className="emcomm-filter-cbx"
          value="emcommType:_is_alert"
          label={<span className="filter-cbx-label">{dictionary('_filter_alerts_label')}</span>}
          checked={showAlertsChecked}
          labelStyle={STYLES.cbxLabel}
          style={STYLES.radioButton}
          onCheck={this.onCheck}
        />
        <Checkbox
          checkedIcon={
            showWarningsChecked ? <ColoredCheckbox color={palette.textColor} /> : undefined
          }
          className="emcomm-filter-cbx"
          value="emcommType:_is_warning"
          label={<span className="filter-cbx-label">{dictionary('_filter_warnings_label')}</span>}
          checked={showWarningsChecked}
          labelStyle={STYLES.cbxLabel}
          style={STYLES.radioButton}
          onCheck={this.onCheck}
        />
        <Checkbox
          checkedIcon={
            showReportRequestsChecked ? <ColoredCheckbox color={palette.textColor} /> : undefined
          }
          className="emcomm-filter-cbx"
          value="emcommType:_is_report_request"
          label={
            <span className="filter-cbx-label">{dictionary('_filter_report_requests_label')}</span>
          }
          checked={showReportRequestsChecked}
          labelStyle={STYLES.cbxLabel}
          style={STYLES.radioButton}
          onCheck={this.onCheck}
        />
      </div>
    );
  }
}

export default enhance(themed(EmCommFilters));
