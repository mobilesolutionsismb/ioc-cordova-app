import './styles.scss';
import React, { Component } from 'react';
import nop from 'nop';
import { withEmergencyCommunicationsApp } from 'ioc-api-interface';
import { withEmergencyCommunicationFilters } from 'js/modules/emcommFilters';
import { withDictionary } from 'ioc-localization';
import { withLogin } from 'ioc-api-interface';
import EmCommsListHeader from './EmCommsListHeader';
import EmCommsListFilters from './EmCommsListFilters';
import { GeoJSONFeaturesProvider, GeoJSONItemsList } from 'js/components/GeoJSONFeaturesProvider';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';
import { SORTERS } from 'js/modules/emcommFilters';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import EventIndicatorOverlay from '../EventIndicatorOverlay';
import {
  EmergencyCommunicationCard,
  ReportRequestCard
} from 'js/components/EmergencyCommunicationsCommons';
import { logError } from 'js/utils/log';

// const ITEM_TYPES = ['emergencyCommunication', 'reportRequest'];
const ITEM_TYPE = 'emergencyCommunication';

const EmCommListElement = withDictionary(
  withLogin(({ dictionary, locale, feature, isSelected, onMapButtonClick }) => {
    let component = null;
    if (feature) {
      switch (feature.properties.itemType) {
        case 'emergencyCommunication':
          component = (
            <EmergencyCommunicationCard
              dictionary={dictionary}
              locale={locale}
              featureProperties={feature.properties}
              showMapIcon={true}
              onMapButtonClick={onMapButtonClick}
            />
          );
          break;
        case 'reportRequest':
          component = (
            <ReportRequestCard
              dictionary={dictionary}
              featureProperties={feature.properties}
              showMapIcon={true}
              onMapButtonClick={onMapButtonClick}
            />
          );
          break;
        default:
          break;
      }
    }
    return component;
  })
);

const enhance = compose(
  withDictionary,
  withEmergencyCommunicationsApp,
  withEmergencyCommunicationFilters
);

class EmergencyCommunications extends Component {
  static defaultProps = {
    emergencyCommunicationFeaturesURL: null,
    emergencyCommunicationFeaturesStats: {
      totalFeaturesCount: 0
    },
    updateEmergencyCommunications: nop,
    selectedFeature: null,
    selectFeature: nop,
    deselectFeature: nop
  };

  _updateEmergencyCommunications = async (reload = false) => {
    try {
      await this.props.updateEmergencyCommunications(reload);
    } catch (err) {
      logError(err);
    }
  };

  render() {
    const {
      emergencyCommunicationFeaturesURL,
      emergencyCommunicationsUpdating,
      selectedFeature,
      selectFeature,
      deselectFeature,
      dictionary,
      emergencyCommunicationFeaturesStats,
      emcommFiltersDefinition,
      emcommSorter,
      history
    } = this.props;

    const total = emergencyCommunicationFeaturesStats
      ? emergencyCommunicationFeaturesStats.totalFeaturesCount
      : 0;

    return (
      <GeoJSONFeaturesProvider
        className="reports-provider"
        filters={emcommFiltersDefinition}
        sorter={SORTERS[emcommSorter]}
        sourceURL={emergencyCommunicationFeaturesURL}
        itemType={ITEM_TYPE}
      >
        {(features, featuresUpdating, featuresUpdateError, filtering, sorting) => [
          <EventIndicatorOverlay key="emcomms-overlay" transparent={false} />,
          <EmCommsListFilters key="emcomms-filters" />,
          <EmCommsListHeader
            key="emcomms-header"
            loading={emergencyCommunicationsUpdating}
            refreshItems={this._updateEmergencyCommunications.bind(this, true)}
            text={dictionary('_emcomms_list_count', features.length, total)}
          />,
          <GeoJSONItemsList
            key="emcomms-list"
            updateFeaturesFn={this._updateEmergencyCommunications}
            sorting={sorting}
            filtering={filtering}
            features={features}
            featuresUpdating={featuresUpdating}
            featuresUpdateError={featuresUpdateError}
            itemType={ITEM_TYPE}
            compareFn={areFeaturesEqual}
            selectedFeature={selectedFeature}
            select={selectFeature}
            deselect={deselectFeature}
            listElement={EmCommListElement}
            listeElementHeight={120}
            history={history}
          />
        ]}
      </GeoJSONFeaturesProvider>
    );
  }
}

export default withRouter(enhance(EmergencyCommunications));
