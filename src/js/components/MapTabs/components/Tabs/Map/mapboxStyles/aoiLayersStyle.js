import {
  //   REPORT_COLOR,
  ALERTS_OR_WARNINGS_CLUSTERED_COLOR,
  REPORT_REQUEST_COLOR,
  MISSION_COLOR,
  //   MISSION_TASK_COLOR,
  //   AGENTLOCATION_COLOR,
  MAPREQUEST_COLOR
} from './featureColors';

import { darken } from 'material-ui/utils/colorManipulator';

const FEATURE_COLORS = {
  emergencyCommunication: ALERTS_OR_WARNINGS_CLUSTERED_COLOR,
  mission: MISSION_COLOR,
  reportRequest: REPORT_REQUEST_COLOR,
  mapRequest: MAPREQUEST_COLOR
};

const FEATURE_BORDER_COLORS = Object.keys(FEATURE_COLORS).reduce((values, colorKey) => {
  values[colorKey] = darken(FEATURE_COLORS[colorKey], 0.15);
  return values;
}, {});

function getByItemType(colorMap) {
  return [
    'match',
    ['get', 'itemType'],
    'reportRequest',
    colorMap.reportRequest,
    'mission',
    colorMap.mission,
    'emergencyCommunication',
    colorMap.emergencyCommunication,
    'mapRequest',
    colorMap.mapRequest,
    '#666'
  ];
}

const featureAOILayerStyle = {
  type: 'fill',
  paint: {
    'fill-color': getByItemType(FEATURE_COLORS),
    'fill-opacity': 0.2,
    'fill-outline-color': getByItemType(FEATURE_COLORS)
  }
};

const featureAOIOutlineLayerStyle = {
  type: 'line',
  layout: {
    'line-cap': 'round',
    'line-join': 'round'
  },
  paint: {
    'line-dasharray': [0.2, 2],
    'line-width': 2,
    'line-color': getByItemType(FEATURE_BORDER_COLORS)
  }
};

export function addAOIFeatureLayers(map, aoiFeaturesSrcName, mapMinZoom, mapMaxZoom) {
  map.addLayer(
    {
      id: 'feature-polygon',
      minzoom: mapMinZoom,
      maxzoom: mapMaxZoom,
      source: aoiFeaturesSrcName,
      ...featureAOILayerStyle
    },
    'clusters'
  );
  map.addLayer(
    {
      id: 'feature-polygon-outline',
      minzoom: mapMinZoom,
      maxzoom: mapMaxZoom,
      source: aoiFeaturesSrcName,
      ...featureAOIOutlineLayerStyle
    },
    'clusters'
  );
  return Promise.resolve();
}
