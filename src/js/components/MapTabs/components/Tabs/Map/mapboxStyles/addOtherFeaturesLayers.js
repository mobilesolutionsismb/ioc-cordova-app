import {
  CLICKED_PINPOINT_COLOR,
  GEOLOCATION_COLOR,
  OLD_GEOLOCATION_COLOR,
  INACCURATE_LOCATION_COLOR
} from './featureColors';
import { green600 } from 'material-ui/styles/colors';
import { EmptyFeatureCollection } from '../mapViewSettings';
import { darken } from 'material-ui/utils/colorManipulator';
import { point, featureCollection } from '@turf/helpers';
import {
  makeDefaultCircle as makeDefaultPositionCircle,
  variableCircleInMeters
} from 'js/modules/map';
import { isOldGeolocation } from 'js/utils/localizeDate';
import bboxPolygon from '@turf/bbox-polygon';

export const CLICKED_POINT_SRC_NAME = 'clicked-point';
export const USER_POSITION_SRC_NAME = 'userposition';
export const DATA_BOUNDS_SRC_NAME = 'databounds';

const MAX_ALLOWED_ACCURACY = 300; //threshold on accuracy (m)
const CLICKED_PINPOINT_BORDER_COLOR = darken(CLICKED_PINPOINT_COLOR, 0.15);

const invalidLocationPaint = { 'circle-radius': 1 };
const invalidLocationStyle = { paint: invalidLocationPaint, visibility: 'none' };

function getGeolocationStyle(latitude, longitude, accuracyInMeters, markAsOld = false) {
  let userposition = invalidLocationStyle;
  let accuracy = invalidLocationStyle;
  if (typeof latitude === 'number' && typeof longitude === 'number') {
    const circleColor = markAsOld ? OLD_GEOLOCATION_COLOR : GEOLOCATION_COLOR;
    userposition = {
      paint: makeDefaultPositionCircle(circleColor, darken(circleColor, 0.3)),
      visibility: 'visible'
    };
  }
  if (typeof accuracyInMeters === 'number' && typeof latitude === 'number') {
    const accuracyColor = markAsOld
      ? OLD_GEOLOCATION_COLOR
      : accuracyInMeters <= MAX_ALLOWED_ACCURACY
      ? GEOLOCATION_COLOR
      : INACCURATE_LOCATION_COLOR;
    accuracy = {
      paint: variableCircleInMeters(
        accuracyInMeters,
        latitude,
        accuracyColor,
        darken(accuracyColor, 0.3)
      ),
      visibility: 'visible'
    };
  }

  return {
    userposition,
    accuracy
  };
}

function _updatePositionLayerProperties(map, layerName, properties) {
  const currentLayerVisibility = map.getLayoutProperty(layerName, 'visibility');
  if (properties.visibility === 'none') {
    if (currentLayerVisibility !== 'none') {
      map.setLayoutProperty(layerName, 'visibility', 'none');
    }
    return;
  } else {
    const paintProperties = Object.entries(properties.paint);
    for (const pp of paintProperties) {
      map.setPaintProperty(layerName, pp[0], pp[1]);
    }
    if (currentLayerVisibility === 'none') {
      map.setLayoutProperty(layerName, 'visibility', 'visible');
    }
  }
}

export function updateUserPositionDataOnMap(map, prevGeolocation, currentGeolocation) {
  if (!map) {
    return;
  }
  const repaintLayersEnabled =
    typeof map.getLayer('position-marker') !== 'undefined' &&
    typeof map.getLayer('position-marker-accuracy') !== 'undefined';
  if (repaintLayersEnabled) {
    const isOld = isOldGeolocation(prevGeolocation, currentGeolocation);
    const currentCoords = currentGeolocation.coords;
    const { longitude, latitude, accuracy } = currentCoords;
    const styleProperties = getGeolocationStyle(latitude, longitude, accuracy, isOld);
    _updatePositionLayerProperties(map, 'position-marker', styleProperties.userposition);
    _updatePositionLayerProperties(map, 'position-marker-accuracy', styleProperties.accuracy);
    const positionSrc = map.getSource(USER_POSITION_SRC_NAME);
    if (positionSrc) {
      positionSrc.setData(point([longitude, latitude], { description: 'GPS Location' }));
    }
  }
}

export async function addGeolocationPositionFeaturesLayers(
  map,
  // mapMinZoom,
  // mapMaxZoom,
  geolocationPosition,
  mapDataBounds
) {
  const isValidGeolocation =
    geolocationPosition && geolocationPosition.coords
      ? typeof geolocationPosition.coords.latitude === 'number' &&
        typeof geolocationPosition.coords.longitude === 'number'
      : false;
  const latitude = isValidGeolocation ? geolocationPosition.coords.latitude : null;
  const longitude = isValidGeolocation ? geolocationPosition.coords.longitude : null;
  const accuracy = isValidGeolocation
    ? typeof geolocationPosition.coords.accuracy === 'number'
      ? geolocationPosition.coords.accuracy
      : 1000
    : null;
  const initialLocation = isValidGeolocation
    ? point([longitude, latitude], { description: 'GPS Location' })
    : EmptyFeatureCollection;

  map.addSource(USER_POSITION_SRC_NAME, {
    type: 'geojson',
    data: initialLocation
  });

  const initialDataBounds = Array.isArray(mapDataBounds)
    ? bboxPolygon(mapDataBounds, { description: 'current data boundaries' })
    : EmptyFeatureCollection;

  map.addSource(DATA_BOUNDS_SRC_NAME, {
    type: 'geojson',
    data: initialDataBounds
  });

  const initialLocationStyles = getGeolocationStyle(latitude, longitude, accuracy, false);

  map.addLayer({
    id: 'position-marker-accuracy',
    type: 'circle',
    minzoom: 10,
    maxzoom: 24,
    source: USER_POSITION_SRC_NAME,
    paint: initialLocationStyles.accuracy.paint,
    layout: {
      visibility: initialLocationStyles.accuracy.visibility
    }
  });

  map.addLayer({
    id: 'position-marker',
    type: 'circle',
    minzoom: 0,
    maxzoom: 24,
    source: USER_POSITION_SRC_NAME,
    paint: initialLocationStyles.userposition.paint,
    layout: {
      visibility: initialLocationStyles.userposition.visibility
    }
  });

  map.addLayer({
    id: DATA_BOUNDS_SRC_NAME,
    source: DATA_BOUNDS_SRC_NAME,
    minzoom: 0,
    maxzoom: 24,
    type: 'line',
    // layout:{

    // },
    paint: {
      'line-color': green600,
      'line-width': 2,
      'line-dasharray': [1, 3]
    }
  });

  return Promise.resolve();
}

// Red feature on user click - update
export function updateClickedPoint(map, clickedPoint) {
  const src = map.getSource(CLICKED_POINT_SRC_NAME);
  if (!src) {
    return;
  }
  const data = Array.isArray(clickedPoint)
    ? featureCollection([point(clickedPoint, {})])
    : EmptyFeatureCollection;
  if (src) {
    src.setData(data);
  }
  return Promise.resolve();
}

export function updateMapDataBounds(map, mapDataBounds) {
  const src = map.getSource(DATA_BOUNDS_SRC_NAME);

  const dataBounds = Array.isArray(mapDataBounds)
    ? bboxPolygon(mapDataBounds, { description: 'current data boundaries' })
    : EmptyFeatureCollection;
  if (src) {
    src.setData(dataBounds);
  }
  return Promise.resolve();
}

// Red feature on user click - init
export async function addClickedPositionFeaturesLayers(map, mapMinZoom, mapMaxZoom) {
  map.addSource(CLICKED_POINT_SRC_NAME, {
    type: 'geojson',
    data: EmptyFeatureCollection
  });

  const clickedPoint = {
    id: 'clicked-point',
    source: CLICKED_POINT_SRC_NAME,
    type: 'symbol',
    minzoom: 0,
    maxzoom: 24,
    layout: {
      'text-font': ['I-REACT_Pinpoints_v4'],
      'text-field': '\ue90a',
      'text-size': ['interpolate', ['linear'], ['zoom'], 0, 2, 4, 7, 7, 12, 24, 32],
      'text-allow-overlap': true
    },
    paint: {
      'text-halo-width': 2,
      'text-color': CLICKED_PINPOINT_COLOR,
      'text-halo-color': CLICKED_PINPOINT_BORDER_COLOR
    }
  };

  map.addLayer(clickedPoint);

  return Promise.resolve();
}
