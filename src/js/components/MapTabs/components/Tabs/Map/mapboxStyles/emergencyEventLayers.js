import { EmptyFeatureCollection } from '../mapViewSettings';
import {
  ONGOING_EVT_COLOR,
  // CLOSED_EVT_COLOR,
  SELECTED_EVENT_LAYER_COLOR,
  ACTIVE_EVENT_LAYER_COLOR
} from './featureColors';
import { darken } from 'material-ui/utils/colorManipulator';

import { DEFAULT_ICON_LAYOUT, hazardExpr } from './singleFeaturesLayers';

const EVT_COLORS = {
  selected: SELECTED_EVENT_LAYER_COLOR,
  active: ACTIVE_EVENT_LAYER_COLOR
};

const FALLBACK_COLOR = '#FFF';

export async function addEmergencyEventLayers(map, featuresSrcName, mapMaxZoom, type) {
  const srcName = `${featuresSrcName}_${type}`;
  const color = EVT_COLORS[type];

  map.addSource(srcName, {
    type: 'geojson',
    data: EmptyFeatureCollection,
    maxzoom: mapMaxZoom,
    tolerance: 0,
    cluster: false
  });

  const singleFeatureStyle = {
    id: `${type}_event`,
    source: srcName,
    type: 'symbol',
    layout: {
      'text-font': ['I-REACT_Pinpoints'],
      'text-field': '\ue91c',
      'text-size': ['interpolate', ['linear'], ['zoom'], 0, 2, 4, 7, 7, 12, 24, 32],
      'text-allow-overlap': true
    },
    paint: {
      'text-halo-width': 2,
      'text-halo-color': [
        'case',
        ['==', true, ['get', 'isOngoing']],
        darken(ONGOING_EVT_COLOR, 0.15),
        ['==', false, ['get', 'isOngoing']],
        ONGOING_EVT_COLOR,
        FALLBACK_COLOR
      ],
      'text-color': [
        'case',
        ['==', true, ['get', 'isOngoing']],
        ONGOING_EVT_COLOR,
        ['==', false, ['get', 'isOngoing']],
        color,
        FALLBACK_COLOR
      ]
    },
    filter: ['==', '$type', 'Point']
  };

  const iconLayer = {
    id: `${type}_event_icon`,
    source: srcName,
    type: 'symbol',
    layout: {
      ...DEFAULT_ICON_LAYOUT,
      'text-field': hazardExpr
    },
    paint: {
      'text-color': [
        'case',
        ['==', true, ['get', 'isOngoing']],
        color,
        ['==', false, ['get', 'isOngoing']],
        ONGOING_EVT_COLOR,
        FALLBACK_COLOR
      ],
      'text-halo-width': 1,
      'text-halo-color': [
        'case',
        ['==', true, ['get', 'isOngoing']],
        ONGOING_EVT_COLOR,
        ['==', false, ['get', 'isOngoing']],
        color,
        FALLBACK_COLOR
      ]
    },
    filter: ['==', '$type', 'Point']
  };

  // Area (Polygon)
  const areaLayer = {
    id: `${type}_event_aoi`,
    type: 'fill',
    source: srcName,
    paint: {
      'fill-color': ONGOING_EVT_COLOR,
      'fill-opacity': 0.2,
      'fill-outline-color': ONGOING_EVT_COLOR
    },
    filter: ['!=', '$type', 'Point']
  };
  // Area Outline (Polygon dashed border)
  const areaOutlineLayer = {
    id: `${type}_event_aoi_outline`,
    type: 'line',
    source: srcName,
    paint: {
      'line-color': ONGOING_EVT_COLOR,
      'line-dasharray': [0.2, 2],
      'line-width': 2
    },
    filter: ['!=', '$type', 'Point']
  };

  map.addLayer(singleFeatureStyle);
  map.addLayer(iconLayer);
  map.addLayer(areaLayer);
  map.addLayer(areaOutlineLayer);
  return Promise.resolve();
}
