import Supercluster from 'supercluster';
import { logClusterWorker as log, logClusterWorkerError as logError } from 'js/utils/log';
import { makeDataURL } from 'js/utils/dataURL'; // using that from api will break the code
import { getFeatureStore } from 'js/utils/ireactFeatureStorage';
import { Spiderifier } from './Spiderifier';

import { MAX_ZOOM, DOTS_PER_BORDER } from '../mapViewSettings';
import { featureFilter } from '@mapbox/mapbox-gl-style-spec';
/* eslint-disable */
const worker = self;
/* eslint-enable */

worker.onerror = e => {
  logError(e);
};

const pointsPerBorder = DOTS_PER_BORDER; // no. of dots for category colors

let ready = false;
let allFeaturesUrl = null; // will change only on features init or update
let clusteredFeaturesUrl = null; // will change on each map move end

const featureStore = getFeatureStore();

/**
 * Get cluster configuration
 * @param {Object} stats
 * @param {Number} nPoints
 */
function getClusterConfiguration(stats, nPoints) {
  const population = stats.point_count;
  const entries = Object.entries(stats.categories);

  const _stops = entries.map(e => Math.round((e[1] * (nPoints - entries.length)) / population));
  const totalStops = _stops.reduce((t, n) => {
    t += n;
    return t;
  }, 0);
  if (totalStops + entries.length !== nPoints) {
    const minIndex = _stops.indexOf(Math.min(..._stops));
    _stops[minIndex] += 1;
  }
  const stops = Array(nPoints).fill('_pause_el');
  let current = 0;
  for (let entry of entries) {
    const index = entries.findIndex(e => e[0] === entry[0]);
    const _start = current;
    const _end = current + _stops[index];
    stops.fill(entry[0], _start, _end);
    current = _end + 1;
  }

  const props = stops
    .map((sv, i) => ({
      [`_border-${i}`]: sv
    }))
    .reduce((o, n) => {
      o = { ...n, ...o };
      return o;
    }, {});
  return { ...stats, ...props };
}
// const ITEM_TYPES = [
//   'report',
//   'reportRequest',
//   'mission',
//   'mapRequest',
//   'emergencyCommunication',
//   'agentLocation'
// ];

const STORE_ITEM_TYPES = [
  'report',
  'reportRequest',
  'mission',
  // 'missionTask',
  'mapRequest',
  'emergencyCommunication',
  'agentLocation',
  'tweet'
];

// Since we added tweets that may have NaN as geolocation
function _validGeometry(feature) {
  return !isNaN(feature.geometry.coordinates[0]) && !isNaN(feature.geometry.coordinates[1]);
}

const spiderifier = new Spiderifier();

function initialCategories() {
  return {
    categories: {}
  };
}

function mapProperties(properties) {
  const itemType = properties.itemType;
  return { categories: itemType ? { [itemType]: 1 } : { uncategorized: 1 } };
  // return { [itemType || 'uncategorized']: 1 };
}

function reduceProperties(accumulated, properties) {
  for (const id in properties.categories) {
    accumulated.categories[id] = (accumulated.categories[id] || 0) + properties.categories[id];
  }
  // const itemTypes = Object.keys(properties);
  // for (const itemType of itemTypes) {
  //   accumulated[itemType] = (accumulated[itemType] || 0) + properties[itemType];
  // }
}

const clusterAlgorithmParams = {
  initial: initialCategories,
  map: mapProperties,
  reduce: reduceProperties,
  maxZoom: MAX_ZOOM, // Max zoom to cluster points on
  radius: 50 // Radius of each cluster when clustering points (defaults to 50),
};

async function getFeaturesFromStorage() {
  // const loadedFeatures = await Promise.all(
  //   STORE_ITEM_TYPES.map(itemType => featureStore.getItem(itemType))
  // );
  let loadedFeatures = [];
  for (let itemType of STORE_ITEM_TYPES) {
    try {
      let storedItems = await featureStore.getItem(itemType);
      if (Array.isArray(storedItems)) {
        loadedFeatures = [...loadedFeatures, ...storedItems];
      } else {
        console.warn(`getFeaturesFromStorage: no ${itemType} features found in DB`);
      }
    } catch (e) {
      console.warn(`getFeaturesFromStorage: cannot read ${itemType} features`, e);
    }
  }
  return loadedFeatures.filter(_validGeometry);

  // return flatten(loadedFeatures.filter(Array.isArray));
}

function send(messageType, payload = {}) {
  worker.postMessage({ messageType, payload });
}

const clusterIndex = new Supercluster(clusterAlgorithmParams);

async function workerLoadFeatures(preFilteringFunction = null) {
  let featuresLoaded = false;
  let count = 0;
  try {
    let features = await getFeaturesFromStorage();
    if (Array.isArray(features) && typeof preFilteringFunction === 'function') {
      features = features.filter(preFilteringFunction);
    }
    count = features.length;

    clusterIndex.load(features);
    featuresLoaded = true;

    // Create an object URL with the whole FeatureCollection - use it for access outside mapbox
    allFeaturesUrl = makeDataURL(allFeaturesUrl, {
      type: 'FeatureCollection',
      features
    });
  } catch (e) {
    console.error('Error loading features', e);
  }
  return { featuresLoaded, count, allFeaturesUrl };
}

async function initOrUpdate(filterDefinitions = null) {
  try {
    if (ready === true) {
      ready = false;
      send('initOrUpdate', { featuresLoaded: [], count: 0, allFeaturesUrl: null, ready });
    }
    const preFilteringFunction = Array.isArray(filterDefinitions)
      ? featureFilter(filterDefinitions).bind(null, null)
      : null;
    const result = await workerLoadFeatures(preFilteringFunction);
    log(`Loaded ${result.count} point features`);
    // log(`Loaded ${result.count} point features`);
    ready = true;
    send('initOrUpdate', { ...result, ready });
  } catch (e) {
    logError('Error loading features', e);
    throw e;
  }
}

function _getSpiderifiedFeatures(bbox, zoom) {
  const allClusters = clusterIndex.getClusters(bbox, zoom - 1);
  const clusters = allClusters.filter(f => f.properties.cluster);
  const nonClusters = allClusters.filter(f => !f.properties.cluster);
  const clusterChildren = clusters.map(cluster =>
    clusterIndex.getLeaves(cluster.properties.cluster_id, Infinity)
  );
  return [...nonClusters, ...spiderifier.getSpiderifiedFeatures(clusters, clusterChildren)];
}

function getClusters(bbox, zoom) {
  if (!ready) {
    console.warn('getClusters: Worker not ready');
    return;
  }
  if (!Array.isArray(bbox) || typeof zoom !== 'number') {
    throw new Error('Invalid arguments for getClusters');
  }
  let features = [];
  const _zoom = Math.round(zoom);
  if (_zoom < clusterIndex.options.maxZoom) {
    features = clusterIndex.getClusters(bbox, _zoom).map(f => {
      if (f.properties.cluster === true) {
        f.properties = getClusterConfiguration(f.properties, pointsPerBorder);
      }
      return f;
    });
  } else {
    features = _getSpiderifiedFeatures(bbox, _zoom);
  }

  const featureCollection = { features, type: 'FeatureCollection' };
  clusteredFeaturesUrl = makeDataURL(clusteredFeaturesUrl, featureCollection);
  log('Cluster computed', clusteredFeaturesUrl, featureCollection);
  send('getClusters', { clusteredFeaturesUrl });
}

function getChildren(clusterId, limit, offset) {
  if (!ready || !clusterId) {
    return;
  }
  const _limit = typeof limit === 'number' ? limit : Infinity;
  const children = clusterIndex.getChildren(clusterId, _limit, offset);
  send('getChildren', { children });
}

function getClusterBy(clusterId) {
  const zoomLevel = (clusterId % 32) - 1;
  let cluster = null;
  if (zoomLevel < clusterIndex.options.maxZoom) {
    const points = clusterIndex.trees[zoomLevel].points;
    if (Array.isArray(points)) {
      let p = points.find(p => p.id === clusterId);
      if (p) {
        cluster =
          clusterIndex.getChildren(p.parentId).find(c => c.properties.cluster_id === clusterId) ||
          null;
      }
    }
  }
  return cluster;
}

function zoomToFeature(featureId, itemType, spiderBodyId = null) {
  if (!ready) {
    return;
  }
  let zoom = null,
    center = null,
    feature = null;
  const maxZoom = clusterIndex.options.maxZoom;
  if (itemType === 'cluster') {
    zoom = clusterIndex.getClusterExpansionZoom(featureId);
    const cluster = getClusterBy(featureId);
    center = cluster.geometry.coordinates;
  } else {
    if (spiderBodyId !== null) {
      //cluster id whose spider this feature belongs to
      const spiderSiblings = clusterIndex.getLeaves(spiderBodyId);
      const spiderBody = getClusterBy(spiderBodyId);
      const spiderFeatures = spiderifier.getSpiderifiedFeatures([spiderBody], [spiderSiblings]);
      feature = spiderFeatures.find(
        f => f.properties.id === featureId && f.properties.itemType === itemType
      );
    } else {
      feature = clusterIndex.points.find(
        f => f.properties.id === featureId && f.properties.itemType === itemType
      );
    }

    if (feature) {
      zoom = maxZoom; //maxZoom - 1;
      center = feature.geometry.coordinates;
    } else {
      feature = null;
    }
  }
  if (typeof zoom === 'number' && Array.isArray(center)) {
    send('zoomToFeature', { zoom, center, feature });
  }
}

// Return feature position or cluster to which it belongs
function findFeature(featureId, itemType, zoom, bbox, interactionType) {
  log('findFeature: finding...', featureId, itemType, zoom, bbox, interactionType);
  if (!ready || itemType === 'cluster' || !bbox) {
    log('findFeature: NOT FOUND stop reason', ready, itemType, bbox);
    return;
  }
  const findPredicate = f => f.properties.id === featureId && f.properties.itemType === itemType;
  const _zoom = Math.round(zoom);
  let found = false;
  if (_zoom < clusterIndex.options.maxZoom) {
    const featuresAtCurrentZoomLevel = clusterIndex.getClusters(bbox, _zoom);
    const data = featuresAtCurrentZoomLevel.reduce(
      (result, f) => {
        if (f.properties.cluster === true) {
          result.clusteredFeatures = [f, ...result.clusteredFeatures];
        } else {
          result.singleFeatures = [f, ...result.singleFeatures];
        }
        return result;
      },
      { singleFeatures: [], clusteredFeatures: [] }
    );
    // Search first in single features
    let singleFeature = data.singleFeatures.find(findPredicate);
    if (typeof singleFeature !== 'undefined') {
      found = true;
      log('findFeature: FOUND ', singleFeature, null);

      send('findFeature', { feature: singleFeature, interactionType, parentFeature: null });
    } else {
      // find in clusters
      for (let clusterFeature of data.clusteredFeatures) {
        let childFeatures = clusterIndex.getLeaves(
          clusterFeature.properties.cluster_id,
          Infinity,
          0
        );
        singleFeature = childFeatures.find(findPredicate);
        if (typeof singleFeature !== 'undefined') {
          found = true;
          log('findFeature: FOUND ', singleFeature, clusterFeature);
          send('findFeature', {
            feature: singleFeature,
            interactionType,
            parentFeature: clusterFeature
          }); // cluster to which belongs
          break;
        }
      }
      if (found === false) {
        // try in points
        const feature = clusterIndex.points.find(findPredicate);
        if (feature) {
          found = true;
          log('findFeature: FOUND ', feature, null);
        } else {
          log('findFeature: NOT FOUND stop reason', ready, itemType, bbox);
        }
        send('findFeature', {
          feature: typeof feature === 'undefined' ? null : feature,
          interactionType,
          parentFeature: null
        });
      }
    }
  } else {
    // Try spider
    const features = _getSpiderifiedFeatures(bbox, _zoom);
    const spiderBody = features.find(f => f.properties.cluster);
    const spiderBodyId = spiderBody ? spiderBody.properties.cluster_id : null;
    let singleFeature = features.find(findPredicate);
    if (typeof singleFeature !== 'undefined') {
      found = true;
      log('findFeature: FOUND ', singleFeature, null, 'SPIDER BODY ID', spiderBodyId);
      singleFeature.properties.spiderBodyId = spiderBodyId;
      send('findFeature', { feature: singleFeature, interactionType, parentFeature: null });
    } else {
      const feature = clusterIndex.points.find(findPredicate);
      if (feature) {
        found = true;
        log('findFeature: FOUND ', feature, null);
      } else {
        log('findFeature: NOT FOUND stop reason', ready, itemType, bbox);
      }
      send('findFeature', {
        feature: typeof feature === 'undefined' ? null : feature,
        interactionType,
        parentFeature: null
      });
    }
  }
}

worker.onmessage = e => {
  const { messageType, payload } = e.data;
  switch (messageType) {
    case 'init':
    case 'update':
      const { filterDefinitions } = payload;
      initOrUpdate(filterDefinitions);
      break;
    case 'getClusters':
      {
        const { bbox, zoom } = payload;
        getClusters(bbox, zoom);
      }
      break;
    case 'getChildren':
      {
        const { clusterId, limit, offset } = payload;
        getChildren(clusterId, limit, offset);
      }
      break;
    case 'zoomToFeature':
      {
        const { featureId, itemType, spiderBodyId } = payload;
        zoomToFeature(featureId, itemType, spiderBodyId);
      }
      break;
    case 'findFeature':
      {
        const { featureId, itemType, zoom, bbox, interactionType } = payload;
        findFeature(featureId, itemType, zoom, bbox, interactionType);
      }
      break;
    default:
      log(messageType ? `Unknown message type "${messageType}"` : `Unknown message "${e.data}"`);
      break;
  }
};

log('Worker Inited');
