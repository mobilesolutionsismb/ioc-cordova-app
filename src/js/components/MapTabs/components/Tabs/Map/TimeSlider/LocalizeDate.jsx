import React from 'react';
import { localizeDate, getLocalDate } from 'js/utils/localizeDate';
import { withDictionary } from 'ioc-localization';
import moment from 'moment';
import 'moment-precise-range-plugin';

function formatLeadTime(leadtime, locale, format = 'L LT', timespan = null) {
  if (!timespan || timespan === 0) {
    return localizeDate(leadtime, locale, format);
  } else {
    const lt = localizeDate(leadtime, locale, format);
    const start = getLocalDate(leadtime, -timespan, 'hours');
    const end = getLocalDate(leadtime);
    const nextDay = end.day() > start.day();
    return `${lt} - ${localizeDate(end, locale, 'LT')}${nextDay ? '*' : ''}`;
  }
}

function formatLeadTimeDiff(leadtime, locale, format = 'L LT', timespan = null) {
  if (!timespan || timespan === 0) {
    return localizeDate(leadtime, locale, format);
  } else {
    const momentStart = getLocalDate(leadtime, -timespan, 'hours');
    const momentEnd = getLocalDate(leadtime);

    const { years, months, days, hours } = moment.preciseDiff(momentStart, momentEnd, true);
    const yearsString = years <= 0 ? '' : `${years}y`;
    const monthsString = months <= 0 ? '' : `${months}mth`;
    const daysString = days <= 0 ? '' : `${days}d`;
    const hoursString = hours <= 0 ? '' : `${hours}h`;
    const dateFormat = 'L HH:mm z';
    const localTime = localizeDate(leadtime, locale, dateFormat);
    const diffString = [yearsString, monthsString, daysString, hoursString].join(' ').trim();
    const dateString = `${localTime}${diffString ? ` + ${diffString}` : ''}`;
    return dateString;
  }
}

export const LocalizedDate = withDictionary(
  ({ locale, style, date, format, timespan, diffFormat = true }) => {
    const formatter = diffFormat ? formatLeadTimeDiff : formatLeadTime;
    return <span style={style}>{formatter(date, locale, format, timespan)}</span>;
  }
);
