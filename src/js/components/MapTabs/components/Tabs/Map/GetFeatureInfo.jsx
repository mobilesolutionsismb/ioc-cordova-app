import React from 'react';
import styled from 'styled-components';
import { guessDate } from 'js/utils/localizeDate';

const GetFeatureInfoTitleInner = styled.div.attrs(props => ({ className: 'getfeatureinfo-title' }))`
  font-size: 14px;
  width: 100%;
  max-width: 100%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  padding: 12px 4px;
  text-align: center;
  position: relative;
`;

const GetFeatureInfoContentInner = styled.div.attrs(props => ({
  className: 'getfeatureinfo-content'
}))`
  width: 100%;
  max-width: 100%;
  height: calc(70vh - 76px);
  overflow: hidden auto;
`;

const FillBox = styled.div.attrs(props => ({ className: 'getfeatureinfo-content' }))`
  width: 16px;
  height: 16px;
  background-color: ${props => props.fill || props.theme.palette.canvasColor};
  display: inline-block;
`;

const METADATA_BLACKLIST = [
  'stroke',
  'stroke_width',
  'stroke_opacity',
  'file_name',
  //   'fill',
  'fill_opacity',
  'shape_rendering',
  'geom'
];

export function GetFeatureInfoTitle({ label, coordinates }) {
  return (
    <GetFeatureInfoTitleInner>{`${label} (${coordinates[1].toFixed(5)}, ${coordinates[0].toFixed(
      5
    )})`}</GetFeatureInfoTitleInner>
  );
}

export function GetFeatureInfoContent({ features, dictionary, locale }) {
  const content = features.features.map((ft, i) => (
    <div key={i}>
      {Object.entries(ft.properties)
        .filter(entry => !METADATA_BLACKLIST.includes(entry[0]))
        .map(entry => (
          <div key={i + '' + entry[0]}>
            {entry[0] === 'fill' && (
              <span>
                <b>{entry[0]}</b>:&nbsp;
                <FillBox fill={entry[1]} />
              </span>
            )}
            {entry[0] !== 'fill' && (
              <span>
                <b>{entry[0]}</b>:&nbsp;<span>{guessDate(entry[1], locale)}</span>
              </span>
            )}
          </div>
        ))}
    </div>
  ));

  return (
    <GetFeatureInfoContentInner>
      {content.length > 0 ? content : dictionary('_no_contents_title')}
    </GetFeatureInfoContentInner>
  );
}
