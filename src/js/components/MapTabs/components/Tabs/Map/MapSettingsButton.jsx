import React, { Component } from 'react';
import { FloatingActionButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';

import { withRouter } from 'react-router';

import STYLES from '../../styles';

class MapSettingsButton extends Component {
  onClick = () => {
    this.props.history.push('/settings/map');
  };

  render() {
    const bottom = STYLES.mapSettingsButton.bottom + this.props.offsetTop;

    return (
      <div
        className="floating-controls"
        style={{ ...STYLES.controls, ...STYLES.mapSettingsButton, ...this.props.style, bottom }}
      >
        <FloatingActionButton
          mini={true}
          iconStyle={{ color: this.props.muiTheme.palette.textColor }}
          onClick={this.onClick}
        >
          <FontIcon className="material-icons">layers</FontIcon>
        </FloatingActionButton>
      </div>
    );
  }
}

export default withRouter(muiThemeable()(MapSettingsButton));
