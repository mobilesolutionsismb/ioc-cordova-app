import React, { Component } from 'react';
import { compose } from 'redux';
import EventIndicatorOverlay from '../EventIndicatorOverlay';
import { logMain } from 'js/utils/log';
import MapInner from './MapInner';
import { withMessages } from 'js/modules/ui';
import { withLayersAndSettings } from 'js/modules/layersAndSettings';
// import Compass from './Compass';
// import PositionButton from './PositionButton';
import MapSettingsButton from './MapSettingsButton';
import { withAllFeatureFilters } from 'js/modules/preferences';
import { withMapLayers } from 'ioc-api-interface';
import { GetFeatureInfoTitle, GetFeatureInfoContent } from './GetFeatureInfo';
import { withDictionary } from 'ioc-localization';

const TIME_SLIDER_HEIGHT = 72;

const enhance = compose(
  withLayersAndSettings,
  withMessages,
  withAllFeatureFilters,
  withMapLayers,
  withDictionary
);

class Map extends Component {
  _onAOILoading = () => {
    this.props.setFeatureDetailsLoading(true);
  };

  _onAOILoaded = () => {
    this.props.setFeatureDetailsLoading(false);
  };

  _onError = err => this.props.pushError(err);

  showFeatureInfo = infoData => {
    logMain('Show Feature info', infoData);
    const title = () => <GetFeatureInfoTitle {...infoData} />;
    const message = (
      <GetFeatureInfoContent
        {...infoData}
        dictionary={this.props.dictionary}
        locale={this.props.locale}
      />
    );
    this.props.pushModalMessage(title, message, {
      _close: null
    });
  };

  render() {
    const {
      // feature selection
      // featureDetailsLoading,
      requestGeolocationUpdate,
      geolocationPosition,
      geolocationUpdating,
      maxDataBounds,
      clickedPoint,
      selectedFeature,
      selectedMissionTask,
      // State updaters
      setClickedPoint,
      selectFeature,
      selectMissionTask,
      deselectFeature,
      deselectMissionTask,
      // setFeatureDetailsLoading
      // FILTERS
      allFeatureFiltersDefinitions,
      // Layers and settings
      activeSettingName,
      activeSetting,
      activeLayersSet,
      removeAllLayers,
      mapLayers
    } = this.props;

    const hasActiveLayers = activeLayersSet.length > 0;
    const mapHeight = hasActiveLayers ? `calc(100% - ${TIME_SLIDER_HEIGHT}px)` : '100%';
    const buttonOffsetTop = hasActiveLayers ? TIME_SLIDER_HEIGHT : 0;
    const defaultGeoServerURL = GEOSERVER_URL ? GEOSERVER_URL : mapLayers.baseUrl;

    const mapProps = {
      requestGeolocationUpdate,
      geolocationPosition,
      geolocationUpdating,
      mapDataBounds: maxDataBounds,
      clickedPoint,
      selectedFeature,
      selectedMissionTask,
      // State updaters
      setClickedPoint,
      selectFeature,
      selectMissionTask,
      deselectFeature,
      deselectMissionTask,
      // FILTERS
      allFeatureFiltersDefinitions,
      // Layers and settings
      activeLayersSet,
      activeSettingName,
      activeSetting,
      removeAllLayers,
      mapHeight,
      defaultGeoServerURL
    };

    return [
      <MapInner
        key="map-inner"
        {...mapProps}
        geolocationUpdating={geolocationUpdating}
        requestGeolocationUpdate={requestGeolocationUpdate}
        buttonOffsetTop={buttonOffsetTop}
        onAOILoaded={this._onAOILoaded}
        onAOILoading={this._onAOILoading}
        onError={this._onError}
        onGetFeatureInfo={this.showFeatureInfo}
      />,
      <EventIndicatorOverlay
        key="evt-indicator"
        style={{
          position: 'absolute',
          display: 'block',
          zIndex: 99
        }}
      />,
      // <Compass key="compass" />,
      // <PositionButton
      //   key="position-btn"
      //   onClick={requestGeolocationUpdate}
      //   geolocationUpdating={geolocationUpdating}
      //   offsetTop={buttonOffsetTop}
      // />,
      <MapSettingsButton key="map-settings-btn" offsetTop={buttonOffsetTop} />
      // <TimeSlider key="ts" />
    ];
  }
}

export default enhance(Map);
