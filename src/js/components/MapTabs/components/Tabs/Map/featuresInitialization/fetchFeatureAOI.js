import { API } from 'ioc-api-interface';
import { logMain } from 'js/utils/log';
import wellknown from 'wellknown';
import { feature } from '@turf/helpers';
import bbox from '@turf/bbox';
import intersect from '@turf/intersect';
import bboxPolygon from '@turf/bbox-polygon';
import { LngLatBounds } from 'mapbox-gl';
import { getFeatureStore } from 'js/utils/ireactFeatureStorage';

const api = API.getInstance();
const featureStore = getFeatureStore();
const ZOOM_PADDING_PX = 24; // 24 on app

const getFeatureKey = (id, itemType) => `${itemType}-aoi-${id}`;

async function searchFeatureInStorage(id, itemType) {
  const feature = await featureStore.getItem(getFeatureKey(id, itemType));
  return feature;
}

async function storeFeatureInStorage(id, itemType, feature) {
  await featureStore.setItem(getFeatureKey(id, itemType), feature);
}

async function fetchFeatureFromServerIfNeeded(id, itemType) {
  let aoi = await searchFeatureInStorage(id, itemType);
  if (!aoi) {
    let response = null;
    const failureMsg = `Error loading details ${itemType}:${id}`;
    switch (itemType) {
      case 'emergencyCommunication':
      case 'reportRequest':
        response = await api.emergencyCommunication.getEmergencyCommunicationById(id, failureMsg);
        if (response.success) {
          const aoiGeometry = wellknown.parse(response.result.areaOfInterest);
          aoi = feature(aoiGeometry, { id, itemType });
          await storeFeatureInStorage(id, itemType, aoi);
        }
        break;
      case 'mission':
        response = await api.mission.get(id, failureMsg);
        if (response.success) {
          const aoiGeometry = wellknown.parse(response.result.areaOfInterest);
          aoi = feature(aoiGeometry, { id, itemType });
          await storeFeatureInStorage(id, itemType, aoi);
        }
        break;
      case 'mapRequest':
        response = await api.mapRequest.get(id, failureMsg);
        if (response.success) {
          const aoiGeometry = wellknown.parse(response.result.areaOfInterest);
          aoi = feature(aoiGeometry, { id, itemType });
          await storeFeatureInStorage(id, itemType, aoi);
        }
        break;
      default:
        break;
    }
    if (!response) {
      throw new Error(`Unhandled feature type ${itemType}`);
    }
    if (!response.success) {
      throw response;
    }
  }
  return aoi;
}

// todo: for itemTypes = emergencyCommunication, mapRequest, mission, reportRequest
export async function fetchFeatureAOI(id, itemType) {
  let aoiCollection = {
    type: 'FeatureCollection',
    features: []
  };

  try {
    const aoi = await fetchFeatureFromServerIfNeeded(id, itemType);
    if (aoi) {
      logMain('AOI', aoi);
      aoiCollection.features.push(aoi);
    }
    return aoiCollection;
  } catch (err) {
    throw err;
  }
}

export function flyToPolygonFeature(map, feature) {
  if (!map) {
    return;
  }
  if (map.isMoving()) {
    map.stop();
  }
  const featureBounds = bbox(feature);
  const maxBounds = map.getMaxBounds();
  const animationOptions = {
    padding: ZOOM_PADDING_PX,
    linear: false,
    maxZoom: map.getMaxZoom() - 1
  };
  if (!maxBounds) {
    map.fitBounds(featureBounds, animationOptions);
  } else {
    const maxBoundPoly = bboxPolygon([
      maxBounds.getWest(),
      maxBounds.getSouth(),
      maxBounds.getEast(),
      maxBounds.getNorth()
    ]);
    const inters = intersect(maxBoundPoly, feature);
    if (inters) {
      const intersBbox = bbox(inters);
      const intersBounds = LngLatBounds.convert([
        [intersBbox[0], intersBbox[1]],
        [intersBbox[2], intersBbox[3]]
      ]);
      map.fitBounds(intersBounds, animationOptions);
    }
  }
}
