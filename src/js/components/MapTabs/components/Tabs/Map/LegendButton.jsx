import React, { Component } from 'react';
import { FloatingActionButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import STYLES from '../../styles';
import { Legend } from 'js/components/LayersLegend';
import styled from 'styled-components';
import { withMessages } from 'js/modules/ui';

const ModalContenWrapper = styled.div`
  width: 100%;
  height: 90vw;
  overflow-x: hidden;
  overflow-y: auto;
`;

class LegendButton extends Component {
  _showLegend = () => {
    this.props.pushModalMessage(
      '_layers_legend',
      <ModalContenWrapper>
        <Legend />
      </ModalContenWrapper>,
      {
        _close: null
      }
    );
  };

  render() {
    const bottom = STYLES.legendButton.bottom + this.props.offsetTop;
    return (
      <div
        className="floating-controls"
        style={{ ...STYLES.controls, ...STYLES.legendButton, bottom }}
      >
        <FloatingActionButton
          mini={true}
          iconStyle={{ color: this.props.muiTheme.palette.textColor }}
          onClick={this._showLegend}
        >
          <FontIcon className="material-icons">list</FontIcon>
        </FloatingActionButton>
      </div>
    );
  }
}

export default withMessages(muiThemeable()(LegendButton));
