import React, { Component } from 'react';
import { FloatingActionButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import STYLES from '../../styles';

class PositionButton extends Component {
  _onClick = () => {
    this.props.onClick(true);
  };

  render() {
    const bottom = STYLES.positionButton.bottom + this.props.offsetTop;
    return (
      <div
        className="floating-controls"
        style={{ ...STYLES.controls, ...STYLES.positionButton, bottom }}
      >
        <FloatingActionButton
          disabled={this.props.geolocationUpdating}
          mini={true}
          iconStyle={{ color: this.props.muiTheme.palette.textColor }}
          onClick={this._onClick}
        >
          <FontIcon className="material-icons">my_location</FontIcon>
        </FloatingActionButton>
      </div>
    );
  }
}

export default muiThemeable()(PositionButton);
