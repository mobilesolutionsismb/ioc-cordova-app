// import './passwordForgot.scss';
import React, { Component } from 'react';
import { withMessages } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import PasswordForgotForm from './PasswordForgotForm';
import { Header } from 'js/components/app';
import { API } from 'ioc-api-interface';
import { CloseButton } from 'js/components/Registration';
import { logMain } from 'js/utils/log';

const api = API.getInstance();

class PasswordForgot extends Component {
  static defaultProps = {
    passwordForgot: () => Promise.resolve()
  };

  onSubmit = async formData => {
    let response = await api.account.passwordForgot(formData.emailAddress);
    logMain('PasswordForgot success', response);
    this.props.pushMessage('_password_update_link_sent', 'default', null, formData.emailAddress);
  };

  render() {
    return (
      <div className="password-forgot page">
        <Header title={'_password_forgot'} leftButtonType="clear" leftButton={<CloseButton />} />
        <PasswordForgotForm
          onSubmit={this.onSubmit}
          dictionary={this.props.dictionary}
          locale={this.props.locale}
        />
      </div>
    );
  }
}

export default withMessages(withDictionary(PasswordForgot));
