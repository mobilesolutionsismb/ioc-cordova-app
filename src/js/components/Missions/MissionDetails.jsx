import React, { Component } from 'react';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { Swipeable } from 'react-swipeable';
import { GeoJSONItemsList } from 'js/components/GeoJSONFeaturesProvider';
import { logMain } from 'js/utils/log';
import { Header, Content } from 'js/components/app';
import {
  HEADER_COLOR,
  DetailsIcon,
  getTookOverTasksFilterFn,
  getCompletedTaskIdsFilterFn,
  MissionColorDot,
  MDMissionTitle,
  MDTitleAddressLine,
  MDDescriptionContainer,
  MDStatusContainer,
  MDStatusLine,
  MDProgressLine,
  MDProgressContainer,
  MissionTaskCard
} from 'js/components/MissionCard';
import { /* ShareIcon, */ GoToMapIcon } from 'js/components/ReportsCommons';
import { localizeDate } from 'js/utils/localizeDate';
import { LinearProgress, DropDownMenu, MenuItem } from 'material-ui';
import { withLogin } from 'ioc-api-interface';
import nop from 'nop';
import { Redirect } from 'react-router-dom';

const SorterMenu = ({ dictionary, taskSorter, taskSorterChange, availableSorters }) => (
  <DropDownMenu
    className="md-sorter-menu"
    anchorOrigin={{
      horizontal: 'left',
      vertical: 'center'
    }}
    targetOrigin={{
      horizontal: 'left',
      vertical: 'center'
    }}
    iconStyle={{ top: -8, right: 8, height: 16, lineHeight: '16px', padding: 2 }}
    labelStyle={{
      width: '100%',
      height: 16,
      lineHeight: '16px',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      fontSize: 13
    }}
    menuStyle={{ maxWidth: '100%', overflow: 'hidden' }}
    style={{ width: '50%', height: 16, lineHeight: '16px', marginLeft: 'auto', fontSize: 13 }}
    autoWidth={false}
    onChange={taskSorterChange}
    value={taskSorter}
    menuItemStyle={{ padding: 4, maxWidth: 'calc(100% - 8px)', border: 0 }}
  >
    {availableSorters.map((taskSorter, i) => (
      <MenuItem key={i} value={taskSorter} primaryText={dictionary(`_${taskSorter}`)} />
    ))}
  </DropDownMenu>
);

const themed = muiThemeable();

const TITLE_ICON_STYLE = { fontSize: 24 };

const SWIPE_EFFECT_TIMEOUT = 300; //ms

class MissionDetails extends Component {
  static defaultProps = {
    selectedFeature: null,
    missionTasks: [],
    match: { params: { id: -1 } }
  };

  timeout = null;

  state = {
    // feature mgmt
    nextFeatureIndex: -1,
    currentFeatureIndex: -1,
    prevFeatureIndex: -1,
    sweepingButCantGoLeft: false,
    sweepingButCantGoRight: false
  };

  // See https://stackoverflow.com/questions/34544314/setstate-can-only-update-a-mounted-or-mounting-component-this-usually-mea/40969739#40969739
  _autoRef = null;

  _setAutoRef = e => {
    this._autoRef = e;
  };

  _findSelectedFeatureIndex(props) {
    const { match, features } = props;
    const matchId =
      typeof match.params.id === 'number' ? match.params.id : parseInt(match.params.id, 10);
    const featureIndex = features.findIndex(f => f.properties.id === matchId);
    return {
      nextFeatureIndex:
        featureIndex >= 0 && featureIndex < features.length - 1 ? featureIndex + 1 : -1,
      currentFeatureIndex: featureIndex,
      prevFeatureIndex: featureIndex > 0 && featureIndex < features.length ? featureIndex - 1 : -1
    };
  }

  _computeNextState(props) {
    const { nextFeatureIndex, currentFeatureIndex, prevFeatureIndex } = this.state;
    const nextState = this._findSelectedFeatureIndex(props);

    if (
      nextFeatureIndex !== nextState.nextFeatureIndex ||
      currentFeatureIndex !== nextState.currentFeatureIndex ||
      prevFeatureIndex !== nextState.prevFeatureIndex
    ) {
      this.setState(nextState);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      !areFeaturesEqual(prevProps.selectedFeature, this.props.selectedFeature) ||
      prevProps.match.params.id !== this.props.match.params.id ||
      (prevProps.featuresUpdating === true && this.props.featuresUpdating === false)
    ) {
      this._computeNextState(this.props);
    }
    if (
      this.state.nextFeatureIndex !== prevState.nextFeatureIndex ||
      this.state.currentFeatureIndex !== prevState.currentFeatureIndex ||
      this.state.prevFeatureIndex !== prevState.prevFeatureIndex
    ) {
      const feature =
        this.state.currentFeatureIndex > -1
          ? this.props.features[this.state.currentFeatureIndex]
          : null;
      if (feature) {
        this.props.selectFeature(feature);
      }
    }
  }

  componentDidMount() {
    this._computeNextState(this.props);
  }

  _onSwipingLeft = () => {
    logMain('MissionDetails::Swiping LEFT');
    if (this.state.nextFeatureIndex === -1 && !this.state.sweepingButCantGoLeft) {
      this.setState({ sweepingButCantGoLeft: true });
    }
  };

  _onSwipingRight = () => {
    logMain('MissionDetails::Swiping RIGHT');
    if (this.state.prevFeatureIndex === -1 && !this.state.sweepingButCantGoRight) {
      this.setState({ sweepingButCantGoRight: true });
    }
  };

  _onSwipedLeft = () => {
    logMain('MissionDetails::Swiped LEFT');
    if (this.state.sweepingButCantGoLeft === true) {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        if (this._autoRef) {
          this.setState({ sweepingButCantGoLeft: false });
        }
      }, SWIPE_EFFECT_TIMEOUT);
    }
    if (this.state.nextFeatureIndex > -1) {
      const nextId = this.props.features[this.state.nextFeatureIndex].properties.id;
      this.props.history.replace(`/missiondetails/${nextId}`);
    }
  };

  _onSwipedRight = () => {
    logMain('MissionDetails::Swiped RIGHT');
    if (this.state.sweepingButCantGoRight === true) {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        if (this._autoRef) {
          this.setState({ sweepingButCantGoRight: false });
        }
      }, SWIPE_EFFECT_TIMEOUT);
    }
    if (this.state.prevFeatureIndex > -1) {
      const prevId = this.props.features[this.state.prevFeatureIndex].properties.id;
      this.props.history.replace(`/missiondetails/${prevId}`);
    }
  };

  _showOnMap = () => {
    this.props.history.replace('/tabs/map');
  };

  render() {
    const {
      user,
      loggedIn,
      location,
      history,
      dictionary,
      locale,
      muiTheme,
      features,
      missionTasks,
      missionTasksUpdating,
      missionTasksUpdateError,
      missionTasksFiltering,
      missionTaskSorting,
      selectMissionTask,
      deselectMissionTask,
      selectedMissionTask,
      taskSorterChange,
      taskSorter,
      availableSorters
    } = this.props; // use featuresUpdating for loader?
    if (!loggedIn) {
      return <Redirect to="/" />;
    }
    const { currentFeatureIndex } = this.state;
    const selectedFeature = currentFeatureIndex > -1 ? features[currentFeatureIndex] : null;
    const palette = muiTheme.palette;
    const featureId = selectedFeature ? selectedFeature.properties.id : -1;
    const featureProperties = selectedFeature ? selectedFeature.properties : {};
    const {
      address,
      title,
      start,
      end,
      taskIds,
      temporalStatus,
      missionAssigneeLabel
    } = featureProperties;
    const _taskIds = Array.isArray(taskIds) ? taskIds : [];

    const taskSorterProps = {
      dictionary,
      taskSorterChange,
      taskSorter,
      availableSorters
    };

    // const thisMissionTasks = missionTasks.filter(filterByMission(_taskIds)).sort(currentSorter);
    const completedTasks = missionTasks.filter(getCompletedTaskIdsFilterFn(_taskIds));
    const tookOverTasks = missionTasks.filter(getTookOverTasksFilterFn(_taskIds));
    const numOfCompletedTasks = completedTasks.length;
    const numOfTookOverTasks = tookOverTasks.length;
    const totalMissionTasks = _taskIds.length;
    const percentCompleted = (numOfCompletedTasks / totalMissionTasks) * 100;
    const toBeProcessed = totalMissionTasks - numOfCompletedTasks - numOfTookOverTasks;
    const dateStartedString = start ? localizeDate(start, locale, 'L LT') : '';
    const dateFinishedString = end ? localizeDate(end, locale, 'L LT') : '';
    const missionStartDateString = dictionary('_event_date_started', dateStartedString);
    const missionEndDateString = end ? dictionary('_event_date_ended', dateFinishedString) : '';

    const pageStyle = {
      color: palette.textColor,
      backgroundColor: palette.backgroundColor
    };
    const effectStyle = { backgroundColor: palette.accent1Color };
    const headerStyle = {
      zIndex: 99,
      backgroundColor: HEADER_COLOR
      // backgroundColor: fade(palette.backgroundColor, 0.8)
    };
    return (
      <Swipeable
        ref={this._setAutoRef}
        className="mission-details page"
        style={pageStyle}
        onSwipingLeft={this._onSwipingLeft}
        onSwipedLeft={this._onSwipedLeft}
        onSwipingRight={this._onSwipingRight}
        onSwipedRight={this._onSwipedRight}
      >
        {featureId > -1 && [
          <div
            key="left-effect"
            className={`left-effect${this.state.sweepingButCantGoRight ? '' : ' hidden'}`}
            style={effectStyle}
          />,
          <div
            key="right-effect"
            className={`right-effect${this.state.sweepingButCantGoLeft ? '' : ' hidden'}`}
            style={effectStyle}
          />,
          <Header
            key="missions-details-header"
            style={headerStyle}
            title={
              <MDMissionTitle className="header-title">
                <div>{dictionary._mission_details}</div>
                <MDTitleAddressLine>{address}</MDTitleAddressLine>
              </MDMissionTitle>
            }
            leftButtonType="back"
            rightButton={
              <div className="report-details-actions">
                <GoToMapIcon iconStyle={TITLE_ICON_STYLE} onClick={this._showOnMap} />
                {/* <ShareIcon iconStyle={TITLE_ICON_STYLE} /> */}
              </div>
            }
          />,
          <Content key="missions-details-content" className="missions-details-content">
            <MDDescriptionContainer>{title}</MDDescriptionContainer>
            <MDStatusContainer>
              <MDStatusLine>
                <DetailsIcon iconText="event" />
                <span>{missionStartDateString}</span>
                <MissionColorDot temporalStatus={temporalStatus} />
              </MDStatusLine>
              {end && (
                <MDStatusLine>
                  <DetailsIcon iconText="event" />
                  <span>{missionEndDateString}</span>
                </MDStatusLine>
              )}
              <MDStatusLine>
                <DetailsIcon iconText="people" />
                <span>{missionAssigneeLabel}</span>
              </MDStatusLine>
            </MDStatusContainer>
            <MDProgressContainer>
              <MDProgressLine>
                <span>{`${taskIds.length} TASKS`}</span>
                <span style={{ fontSize: 11 }}>
                  {dictionary('_mission_details_tasks_list_items', toBeProcessed)}
                </span>
              </MDProgressLine>
              <LinearProgress
                className="progress-bar"
                color={palette.missionProgressBarColor}
                style={{ margin: 0, width: '90%' }}
                mode="determinate"
                value={percentCompleted}
              />
              <SorterMenu {...taskSorterProps} />
            </MDProgressContainer>
            <GeoJSONItemsList
              headerSize={280}
              updateFeaturesFn={nop}
              features={missionTasks}
              featuresUpdating={missionTasksUpdating}
              featuresUpdateError={missionTasksUpdateError}
              filtering={missionTasksFiltering}
              sorting={missionTaskSorting}
              select={selectMissionTask}
              deselect={deselectMissionTask}
              selectedFeature={selectedMissionTask}
              compareFn={areFeaturesEqual}
              listeElementHeight={100}
              listElement={MissionTaskCard}
              itemType={'missionTask'}
              otherListItemProps={{ currentUserId: user.id }}
              history={history}
              location={location}
            />
          </Content>
        ]}
      </Swipeable>
    );
  }
}
export default withLogin(themed(MissionDetails));
