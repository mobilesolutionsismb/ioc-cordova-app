import React /* , { Component } */ from 'react';
import styled from 'styled-components';
import { MissionCard } from 'js/components/MissionCard';
// import { Link } from 'react-router-dom';

const ListItemContainer = styled.div`
  width: 100%;
  height: 100px;
  display: block;
  margin-bottom: 5px;
`;

const MissionListItem = ({ mission, onMapButtonClick, selectedMissionId, toggleSelection }) => (
  <ListItemContainer
    className="missions-list-item"
    onClick={e => {
      e.preventDefault();
      e.stopPropagation();
      toggleSelection(mission.properties.id);
    }}
  >
    <MissionCard
      missionProperties={mission.properties}
      showMapIcon={true}
      onMapButtonClick={onMapButtonClick.bind(null, mission.properties.id)}
      isSelected={selectedMissionId === mission.properties.id}
    />
  </ListItemContainer>
);

const ListViewContainer = styled.div`
  width: 100%;
  height: calc(100% - 48px);
  overflow-y: auto;
  background-color: ${props => props.theme.palette.appBarColor};
`;

export const MissionsList = ({ missions, selectMission, selectedMissionId, toggleSelection }) => (
  <ListViewContainer className="missions-list">
    {missions.map((m, i) => (
      <MissionListItem
        key={i}
        mission={m}
        onMapButtonClick={selectMission}
        selectedMissionId={selectedMissionId}
        toggleSelection={toggleSelection}
      />
    ))}
  </ListViewContainer>
);
