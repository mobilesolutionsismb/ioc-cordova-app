import React from 'react';
import styled from 'styled-components';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { fade } from 'material-ui/utils/colorManipulator';
import { FontIcon } from 'material-ui';

import { getAsset } from 'js/utils/getAssets';

const themeable = muiThemeable();

const footerHeight = 64;
const infoItemHeight = 96;
const fieldItemHeight = 48;
const contentItemHeight = 3 * fieldItemHeight;
const horizontalPadding = 16;
const verticalPadding = 8;

const borderColor = props => fade(props.theme.palette.textColor, 0.4);

export const TDField = styled.div`
  box-sizing: border-box;
  padding: ${verticalPadding}px ${horizontalPadding}px;
  width: 100%;
`;

export const TDLine = styled(TDField)`
  width: 100%;
  padding: 0;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const contendIconWidth = 64;

export const TDIconContainer = styled.div`
  width: ${contendIconWidth}px;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
`;

export const TDContentDescription = styled.div`
  width: calc(100% - ${contendIconWidth}px);
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid ${props => (props.bottomBorder === false ? 'transparent' : borderColor)};
  box-sizing: border-box;
  padding-right: 16px;
`;

const TDContentLineInner = styled(TDLine)`
  height: 33.3%;
`;

export const TDContentLine = ({ iconText, label, valueString, bottomBorder }) => {
  return (
    <TDContentLineInner>
      <TDIconContainer>
        <FontIcon className="material-icons">{iconText}</FontIcon>
      </TDIconContainer>
      <TDContentDescription bottomBorder={bottomBorder}>
        <span>{label}</span>
        <span>{valueString}</span>
      </TDContentDescription>
    </TDContentLineInner>
  );
};

// Date, etc
export const TDInfo = styled(TDField)`
  height: ${infoItemHeight}px;
  display: flex;
  flex-direction: column;
  border-bottom: 1px solid ${borderColor};
  justify-content: space-evenly;
`;

// Text
export const TDDescription = styled(TDField)`
  height: calc(100% - ${infoItemHeight + contentItemHeight + fieldItemHeight + footerHeight}px);
  overflow-x: hidden;
  overflow-y: auto;
  border-bottom: 1px solid ${borderColor};
`;

export const TDContents = styled(TDField)`
  height: ${contentItemHeight}px;
  border-bottom: 1px solid ${borderColor};
  padding: 0;
`;

export const TDOrganizationInner = styled(TDField)`
  height: ${fieldItemHeight}px;
  display: flex;
  flex-direction: row;
  align-items: center;
  border-bottom: 1px solid ${borderColor};
  padding: 0;
`;

export const TDOrganization = themeable(({ muiTheme, avatarImageURL, organizationName }) => {
  const { palette } = muiTheme;
  const avatarStyle = avatarImageURL
    ? { color: palette.authorityColor, backgroundImage: `url(${avatarImageURL})` }
    : { color: palette.authorityColor, backgroundImage: getAsset('logo', true) };
  return (
    <TDOrganizationInner>
      <TDIconContainer>
        <div className="ireact-org-avatar" style={avatarStyle} />
      </TDIconContainer>
      <span
        className="role-color-dot role-authority"
        style={{ backgroundColor: palette.authorityColor, marginRight: '0.5em' }}
      />
      <span>{organizationName}</span>
    </TDOrganizationInner>
  );
});

export const TDFooter = styled(TDField)`
  height: ${footerHeight}px;
  position: fixed;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  bottom: 0;
`;
