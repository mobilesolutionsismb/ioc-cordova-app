import React, { Component } from 'react';
import { withLogin } from 'ioc-api-interface';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { logMain } from 'js/utils/log';
import { Swipeable } from 'react-swipeable';
import { ColumnPage } from 'js/components/app/commons';
import { CircularProgress } from 'material-ui';
import { Header, Content /* , HeaderButton */ } from 'js/components/app';
import { /* ShareIcon, */ GoToMapIcon } from 'js/components/ReportsCommons';
import {
  HEADER_COLOR,
  MDMissionTitle,
  MDTitleAddressLine,
  DetailsIcon,
  MissionColorDot,
  ChipTookOverBy,
  ChipNoneTookOver
} from 'js/components/MissionCard';
import { localizeDate } from 'js/utils/localizeDate';
import {
  TDInfo,
  TDDescription,
  TDContents,
  TDOrganization,
  TDLine,
  TDFooter,
  TDContentLine
} from './taskDetailsComponents';
import { FlatButton } from 'material-ui';
import { withMessages } from 'js/modules/ui';
import moment from 'moment';
import { openMapAppLink } from 'js/utils/getAssets';

const POS_PRECISION = 5;
const themed = muiThemeable();

const TITLE_ICON_STYLE = { fontSize: 24 };

const SWIPE_EFFECT_TIMEOUT = 300; //ms

const assigneeBadgeStyle = { margin: 0, height: 18, lineHeight: '16px' };

function isMine(taskProperties, userId) {
  const { taskUsers } = taskProperties;
  return taskUsers.findIndex(tu => tu.user.id === userId) > -1;
}

const MissionTaskActions = withMessages(
  ({
    userId,
    isOfCompetence,
    taskProperties,
    history,
    dictionary,
    accomplishTask,
    takeChargeOfTask,
    releaseTaskOfMission,
    pushMessage,
    pushError
  }) => {
    const { isCompleted, isTookOver, temporalStatus, id } = taskProperties;
    if (isCompleted || temporalStatus === 'completed') {
      // Return green bar with checkmark
      return [
        <div key={0} style={{ textTransform: 'uppercase' }}>
          {dictionary._missiontask_accomplished}
        </div>,
        <DetailsIcon key={1} iconText="check_circle" style={{ fontSize: 32 }} />
      ];
    } else if (isOfCompetence) {
      // Define handlers and buttons
      const downButtonLabel = isTookOver ? '_missiontask_dismiss' : '_missiontask_decline';
      const downButtonHandler = isTookOver
        ? releaseTaskOfMission.bind(this, id)
        : () => history.goBack();
      const upButtonLabel = isTookOver ? '_missiontask_accomplish' : '_missiontask_accept';
      const upButtonHandler = async () => {
        try {
          const handler = isTookOver ? accomplishTask : takeChargeOfTask;

          const message = isTookOver ? 'Task Accomplished!' : 'Task taken in charge';
          const status = isTookOver ? 'success' : 'default';

          await handler(id);
          pushMessage(message, status);
        } catch (err) {
          console.error('Error', err);
          if (
            err.details &&
            err.details.status === 400 &&
            err.details.serverDetails &&
            err.details.serverDetails.error
          ) {
            const ssError = err.details.serverDetails.error;
            pushError(ssError.details);
          } else {
            pushError(dictionary(err.message));
          }
        }
      };
      if (isTookOver) {
        // COMPLETE OR RELEASE
        return [
          <FlatButton
            key={0}
            disabled={!isMine(taskProperties, userId)}
            label={dictionary[downButtonLabel]}
            onClick={downButtonHandler}
          />,
          <FlatButton
            key={1}
            disabled={!isMine(taskProperties, userId)}
            label={dictionary[upButtonLabel]}
            onClick={upButtonHandler}
          />
        ];
      } else {
        // TAKE IN CHARGE
        return [
          <div key={0} />,
          // <FlatButton key={0}
          //     disabled={isTookOver && !isMine(taskProperties, userId)}
          //     label={dictionary[downButtonLabel]}
          //     onClick={downButtonHandler}
          // />,
          <FlatButton
            key={1}
            // disabled={isTookOver && !isMine(taskProperties, userId)}
            label={dictionary[upButtonLabel]}
            onClick={upButtonHandler}
          />
        ];
      }
    } else {
      return null;
    }
  }
);

class MissionTaskDetails extends Component {
  static defaultProps = {
    selectedMissionTask: null,
    selectedFeature: null,
    missionTasks: [],
    features: [],
    match: { params: { id: -1, taskId: -1 } }
  };

  timeout = null;

  state = {
    loading: true,
    currentMissionIndex: -1,
    // feature mgmt
    nextMissionTaskIndex: -1,
    currentMissionTaskIndex: -1,
    prevMissionTaskIndex: -1,
    sweepingButCantGoLeft: false,
    sweepingButCantGoRight: false
  };

  // See https://stackoverflow.com/questions/34544314/setstate-can-only-update-a-mounted-or-mounting-component-this-usually-mea/40969739#40969739
  _autoRef = null;

  _setAutoRef = e => {
    this._autoRef = e;
  };

  _findSelectedMissionTaskIndex(props) {
    const { match, features, missionTasks } = props;
    const missionId =
      typeof match.params.id === 'number' ? match.params.id : parseInt(match.params.id, 10);
    const taskId =
      typeof match.params.taskId === 'number' ? match.params.id : parseInt(match.params.taskId, 10);

    const currentMissionIndex = features.findIndex(f => f.properties.id === missionId);

    const missionTaskIndex = missionTasks.findIndex(
      f => f.properties.id === taskId && f.properties.missionId === missionId
    );
    return {
      loading: false,
      currentMissionIndex,
      nextMissionTaskIndex:
        missionTaskIndex >= 0 && missionTaskIndex < missionTasks.length - 1
          ? missionTaskIndex + 1
          : -1,
      currentMissionTaskIndex: missionTaskIndex,
      prevMissionTaskIndex:
        missionTaskIndex > 0 && missionTaskIndex < missionTasks.length ? missionTaskIndex - 1 : -1
    };
  }

  _computeNextState(props) {
    this.setState({ loading: true }, () => {
      const {
        currentMissionIndex,
        nextMissionTaskIndex,
        currentMissionTaskIndex,
        prevMissionTaskIndex
      } = this.state;
      const nextState = this._findSelectedMissionTaskIndex(props);

      if (
        currentMissionIndex !== nextState.currentMissionIndex ||
        nextMissionTaskIndex !== nextState.nextMissionTaskIndex ||
        currentMissionTaskIndex !== nextState.currentMissionTaskIndex ||
        prevMissionTaskIndex !== nextState.prevMissionTaskIndex
      ) {
        this.setState(nextState);
      } else {
        this.setState({ loading: false });
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      !areFeaturesEqual(prevProps.selectedFeature, this.props.selectedFeature) ||
      !areFeaturesEqual(prevProps.selectedMissionTask, this.props.selectedMissionTask) ||
      prevProps.match.params.id !== this.props.match.params.id ||
      prevProps.match.params.taskId !== this.props.match.params.taskId ||
      (prevProps.featuresUpdating === true && this.props.featuresUpdating === false) ||
      (prevProps.missionTasksUpdating === true && this.props.missionTasksUpdating === false)
    ) {
      this._computeNextState(this.props);
    }
    if (
      (this.currentMissionIndex !== prevState.currentMissionIndex,
      this.state.nextMissionTaskIndex !== prevState.nextMissionTaskIndex ||
        this.state.currentMissionTaskIndex !== prevState.currentMissionTaskIndex ||
        this.state.prevMissionTaskIndex !== prevState.prevMissionTaskIndex)
    ) {
      const feature =
        this.state.currentMissionIndex > -1
          ? this.props.features[this.state.currentMissionIndex]
          : null;
      const task =
        this.state.currentMissionTaskIndex > -1
          ? this.props.missionTasks[this.state.currentMissionTaskIndex]
          : null;
      if (feature) {
        this.props.selectFeature(feature);
      }
      if (task) {
        this.props.selectMissionTask(task);
      }
    }
  }

  componentDidMount() {
    this._computeNextState(this.props);
  }

  _onSwipingLeft = () => {
    logMain('MissionTASKDetails::Swiping LEFT');
    if (this.state.nextMissionTaskIndex === -1 && !this.state.sweepingButCantGoLeft) {
      this.setState({ sweepingButCantGoLeft: true });
    }
  };

  _onSwipingRight = () => {
    logMain('MissionTASKDetails::Swiping RIGHT');
    if (this.state.prevMissionTaskIndex === -1 && !this.state.sweepingButCantGoRight) {
      this.setState({ sweepingButCantGoRight: true });
    }
  };

  _onSwipedLeft = () => {
    logMain('MissionTASKDetails::Swiped LEFT');
    if (this.state.sweepingButCantGoLeft === true) {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        if (this._autoRef) {
          this.setState({ sweepingButCantGoLeft: false });
        }
      }, SWIPE_EFFECT_TIMEOUT);
    }
    if (this.state.nextMissionTaskIndex > -1) {
      const missionId = parseInt(this.props.match.params.id, 10);
      const nextId = this.props.missionTasks[this.state.nextMissionTaskIndex].properties.id;
      this.props.history.replace(`/missiondetails/${missionId}/${nextId}`);
    }
  };

  _onSwipedRight = () => {
    logMain('MissionTASKDetails::Swiped RIGHT');
    // const expanded = this.props.location.search.indexOf('expanded') > -1;
    if (this.state.sweepingButCantGoRight === true) {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        if (this._autoRef) {
          this.setState({ sweepingButCantGoRight: false });
        }
      }, SWIPE_EFFECT_TIMEOUT);
    }
    if (this.state.prevMissionTaskIndex > -1) {
      const missionId = parseInt(this.props.match.params.id, 10);
      const prevId = this.props.missionTasks[this.state.prevMissionTaskIndex].properties.id;
      this.props.history.replace(`/missiondetails/${missionId}/${prevId}`);
    }
  };

  _showOnMap = () => {
    this.props.history.replace('/tabs/map');
  };

  render() {
    const {
      user,
      history,
      locale,
      dictionary,
      // locale,
      muiTheme,
      features,
      featuresUpdating,
      missionTasks,
      missionTasksUpdating,
      // missionTasksUpdateError,
      // missionTasksFiltering,
      // missionTaskSorting,
      // selectMissionTask,
      // deselectMissionTask,
      // selectedMissionTask
      accomplishTask,
      takeChargeOfTask,
      releaseTaskOfMission
    } = this.props;
    const palette = muiTheme.palette;
    const { currentMissionIndex, currentMissionTaskIndex, loading } = this.state;
    const headerStyle = {
      zIndex: 99,
      backgroundColor: HEADER_COLOR
      // backgroundColor: fade(palette.backgroundColor, 0.8)
    };
    const updating = loading || featuresUpdating || missionTasksUpdating;
    if (updating) {
      return [
        <Header
          key="missiontaskdetails-header"
          style={headerStyle}
          title={
            <MDMissionTitle className="header-title">
              <div>{dictionary._missiontask_details}</div>
            </MDMissionTitle>
          }
          leftButtonType="back"
          rightButton={<div className="report-details-actions" />}
        />,
        <Content key="missiontaskdetails-content">
          <ColumnPage>
            <CircularProgress size={80} thickness={5} />
          </ColumnPage>
        </Content>
      ];
    } else {
      const selectedTask =
        currentMissionTaskIndex > -1 ? missionTasks[currentMissionTaskIndex] : null;

      const taskId = selectedTask ? selectedTask.properties.id : -1;

      const pageStyle = {
        color: palette.textColor,
        backgroundColor: palette.backgroundColor
      };
      const effectStyle = { backgroundColor: palette.accent1Color };
      const coordinates = selectedTask ? selectedTask.geometry.coordinates : null;
      const missionTaskProperties = selectedTask ? selectedTask.properties : {};
      const {
        address,
        description,
        start,
        end,
        temporalStatus,
        isCompleted,
        isTookOver,
        taskUsers,
        numberOfNecessaryPeople,
        tools,
        taskTools,
        organization,
        organizationLogo
      } = missionTaskProperties;

      const dateStartedString = start ? localizeDate(start, locale, 'L LT') : '';
      const dateFinishedString = end ? localizeDate(end, locale, 'L LT') : '';
      const taskStartDateString = `${dictionary('_event_date_started', dateStartedString)}`;
      const taskEndDateString = `${
        end ? '' + dictionary('_event_date_ended', dateFinishedString) : ''
      }`;
      // const missionDateString = `${dictionary('_event_date_started', dateStartedString)}${end ? ' - ' + dictionary('_event_date_ended', dateFinishedString) : ''}`;
      const toolsString = taskTools
        ? taskTools
        : Array.isArray(tools)
        ? tools.length > 0
          ? tools.map(t => t.name).join(', ')
          : '-'
        : '-';
      const assignee =
        isTookOver || isCompleted
          ? taskUsers
              .map(tu => (tu.user.id === user.id ? dictionary._me : tu.user.userName))
              .join(', ')
          : null;

      const taskMgmtProps = {
        accomplishTask,
        takeChargeOfTask,
        releaseTaskOfMission
      };

      const currentMission =
        features.length && currentMissionIndex > -1 ? features[currentMissionIndex] : null;
      const currentMissionTeamId = currentMission ? currentMission.properties.teamId : null;
      const firstResponderId = currentMission ? currentMission.properties.firstResponderId : null;
      const currentUserTeams = user.teams;
      const isThisUserTeamTask =
        typeof currentMissionTeamId === 'number' &&
        currentUserTeams.some(team => team.id === currentMissionTeamId);
      const isThisFirstResponderTask =
        typeof firstResponderId === 'number' && user.id === firstResponderId;

      const organizationName = organization
        ? organization
        : user && Array.isArray(user.organizationUnits) && user.organizationUnits[0]
        ? user.organizationUnits[0].displayName
        : 'Unknow Organization';

      return (
        <Swipeable
          ref={this._setAutoRef}
          className="report-details page"
          style={pageStyle}
          onSwipingLeft={this._onSwipingLeft}
          onSwipedLeft={this._onSwipedLeft}
          onSwipingRight={this._onSwipingRight}
          onSwipedRight={this._onSwipedRight}
        >
          {taskId > -1 && [
            <div
              key="left-effect"
              className={`left-effect${this.state.sweepingButCantGoRight ? '' : ' hidden'}`}
              style={effectStyle}
            />,
            <div
              key="right-effect"
              className={`right-effect${this.state.sweepingButCantGoLeft ? '' : ' hidden'}`}
              style={effectStyle}
            />,
            <Header
              key="missiontaskdetails-header"
              style={headerStyle}
              title={
                <MDMissionTitle className="header-title">
                  <div className="taskdetails-title">
                    <span>{dictionary._missiontask_details}</span>
                    <span>
                      {Array.isArray(coordinates) && (
                        <span onClick={openMapAppLink(coordinates)}>
                          {coordinates[1].toFixed(POS_PRECISION)},{' '}
                          {coordinates[0].toFixed(POS_PRECISION)}
                        </span>
                      )}
                    </span>
                  </div>
                  <MDTitleAddressLine>{address}</MDTitleAddressLine>
                </MDMissionTitle>
              }
              leftButtonType="back"
              rightButton={
                <div className="report-details-actions">
                  <GoToMapIcon iconStyle={TITLE_ICON_STYLE} onClick={this._showOnMap} />
                  {/* <ShareIcon iconStyle={TITLE_ICON_STYLE} /> */}
                </div>
              }
            />,
            <Content key="missiontaskdetails-content">
              <TDInfo className="md-info">
                <TDLine>
                  <DetailsIcon iconText="event" />
                  <span>{taskStartDateString}</span>
                  <MissionColorDot temporalStatus={temporalStatus} />
                </TDLine>
                {end && (
                  <TDLine>
                    <DetailsIcon iconText="event" />
                    <span>{taskEndDateString}</span>
                  </TDLine>
                )}
                <TDLine>
                  <DetailsIcon iconText="label_outline" />
                  <span>{`${dictionary._missiontask_acceptedBy}:`}&nbsp;</span>
                  {assignee ? (
                    <ChipTookOverBy style={assigneeBadgeStyle}>{assignee}</ChipTookOverBy>
                  ) : (
                    <ChipNoneTookOver style={assigneeBadgeStyle}>
                      {dictionary._nobody}
                    </ChipNoneTookOver>
                  )}
                </TDLine>
              </TDInfo>
              <TDDescription className="md-description">{description}</TDDescription>
              <TDContents className="md-contents">
                <TDContentLine
                  iconText="access_time"
                  label={dictionary._missiontask_remaining_time}
                  valueString={moment(end).fromNow()}
                />
                <TDContentLine
                  iconText="person"
                  label={dictionary._missiontask_agents_needed}
                  valueString={numberOfNecessaryPeople}
                />
                <TDContentLine
                  iconText="build"
                  label={dictionary._missiontask_tools}
                  valueString={toolsString}
                  bottomBorder={false}
                />
              </TDContents>
              <TDOrganization
                className="md-organization"
                organizationName={organizationName}
                avatarImageURL={organizationLogo}
              />
              <TDFooter
                className="md-footer"
                style={
                  isCompleted
                    ? {
                        backgroundColor: muiTheme.palette.validatedColor,
                        justifyContent: 'flex-end'
                      }
                    : {}
                }
              >
                <MissionTaskActions
                  userId={user.id}
                  isOfCompetence={isThisUserTeamTask || isThisFirstResponderTask}
                  history={history}
                  dictionary={dictionary}
                  taskProperties={selectedTask.properties}
                  {...taskMgmtProps}
                />
              </TDFooter>{' '}
            </Content>
          ]}
          {taskId === -1 && [
            <Header
              key="missiontaskdetails-header"
              style={headerStyle}
              title={
                <MDMissionTitle className="header-title">
                  <div>{dictionary._missiontask_details}</div>
                </MDMissionTitle>
              }
              leftButtonType="back"
              rightButton={<div className="report-details-actions" />}
            />,
            <Content key="missiontaskdetails-content">
              No selected mission - go back and select one
            </Content>
          ]}
        </Swipeable>
      );
    }
  }
}
export default themed(withLogin(MissionTaskDetails));
