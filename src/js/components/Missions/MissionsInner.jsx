import React, { Component } from 'react';
import { withDictionary } from 'ioc-localization';
import { GeoJSONItemsList } from 'js/components/GeoJSONFeaturesProvider';
import { Header, Content } from 'js/components/app';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';
import { MissionCard, HEADER_COLOR } from 'js/components/MissionCard';
import MissionsFilters from './MissionsFilters';
import EventIndicatorOverlay from '../MapTabs/components/Tabs/EventIndicatorOverlay';
import MissionDetails from './MissionDetails';
import MissionTaskDetails from './MissionTaskDetails';
import { SORTERS } from 'js/modules/missionsFilters';
import qs from 'qs';

function filterByMission(taskIds) {
  return t => (Array.isArray(taskIds) && t ? taskIds.indexOf(t.properties.id) > -1 : false);
}

const { start_asc, start_desc, end_asc, end_desc } = SORTERS;

// Tasks don't have last modified
const TASK_SORTERS = {
  start_asc,
  start_desc,
  end_asc,
  end_desc
};

const availableSorters = Object.keys(TASK_SORTERS);

const DEFAULT_TASK_SORTER = availableSorters[0];

class MissionsInner extends Component {
  constructor(props) {
    super(props);
    const search = this.props.location.search.replace(/^\?/, '');
    const searchParams = qs.parse(search);
    const taskSorter = searchParams.taskSorter || DEFAULT_TASK_SORTER;
    this.state = {
      taskSorter
    };
  }

  _taskSorterChange = (event, index, taskSorter) =>
    this.setState({ taskSorter }, () => {
      this.props.history.replace(`${this.props.location.pathname}?taskSorter=${taskSorter}`);
    });

  render() {
    const {
      missions,
      missionFeaturesStats,
      missionsUpdating,
      missionsUpdateError,
      missionsFiltering,
      missionsSorting,
      missionTasks,
      missionTasksUpdating,
      missionTasksUpdateError,
      missionTasksFiltering,
      missionTaskSorting,
      selectedFeature,
      selectFeature,
      deselectFeature,
      updateMissions,
      accomplishTask,
      takeChargeOfTask,
      releaseTaskOfMission,
      selectMissionTask,
      deselectMissionTask,
      selectedMissionTask,
      dictionary,
      locale,
      history,
      match,
      location
    } = this.props;

    const mission = selectedFeature
      ? missions.find(f => f.properties.id === selectedFeature.properties.id)
      : null;
    const taskIds = mission ? mission.properties.taskIds : null;

    const currentSorter = selectedFeature ? TASK_SORTERS[this.state.taskSorter] : null;

    const _missionTasks = selectedFeature
      ? missionTasks.filter(filterByMission(taskIds)).sort(currentSorter)
      : missionTasks;

    const detailsProps = {
      match,
      history,
      location,
      dictionary,
      locale,
      features: missions,
      featuresUpdating: missionsUpdating,
      selectedFeature,
      selectFeature,
      deselectFeature,
      missionTasks: _missionTasks,
      missionTasksUpdating,
      missionTasksUpdateError,
      missionTasksFiltering,
      missionTaskSorting,
      selectMissionTask,
      deselectMissionTask,
      selectedMissionTask
    };

    const taskMgmtProps = {
      accomplishTask,
      takeChargeOfTask,
      releaseTaskOfMission
    };

    if (match.path === '/missions') {
      let thisUserTeamMissionsCount = 0;
      let totalFeaturesCount = 0;
      if (missionFeaturesStats) {
        thisUserTeamMissionsCount = missionFeaturesStats.thisUserTeamMissionsCount;
        totalFeaturesCount = missionFeaturesStats.totalFeaturesCount;
      }

      return [
        <Header
          key="missions-header"
          style={{ backgroundColor: HEADER_COLOR }}
          title={dictionary('_missions_title', totalFeaturesCount, thisUserTeamMissionsCount)}
          leftButtonType="back"
        />,
        <Content key="missions-content" className="missions-content">
          <EventIndicatorOverlay style={{ backgroundColor: 'transparent', textAlign: 'center' }} />
          <MissionsFilters onRefreshButtonClick={() => updateMissions(true)} />
          <GeoJSONItemsList
            headerSize={144}
            updateFeaturesFn={updateMissions}
            sorting={missionsSorting}
            filtering={missionsFiltering}
            features={missions}
            featuresUpdating={missionsUpdating}
            featuresUpdateError={missionsUpdateError}
            compareFn={areFeaturesEqual}
            selectedFeature={selectedFeature}
            select={selectFeature}
            deselect={deselectFeature}
            listeElementHeight={100}
            listElement={MissionCard}
            otherListItemProps={{ missionTasks }}
            itemType={'mission'}
            history={history}
            location={location}
          />
        </Content>
      ];
    }
    if (match.path === '/missiondetails/:id') {
      return (
        <MissionDetails
          {...detailsProps}
          taskSorterChange={this._taskSorterChange}
          taskSorter={this.state.taskSorter}
          availableSorters={availableSorters}
        />
      );
    } else if (match.path === '/missiondetails/:id/:taskId') {
      return <MissionTaskDetails {...detailsProps} {...taskMgmtProps} />;
    } else {
      return null;
    }
  }
}

export default withDictionary(MissionsInner);
