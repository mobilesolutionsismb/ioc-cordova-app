import React from 'react';
import { Header } from 'js/components/app';
import { appInfo } from 'js/utils/getAppInfo';
import { API } from 'ioc-api-interface';
import { withDictionary } from 'ioc-localization';
import { localizeDate } from 'js/utils/localizeDate';
import styled from 'styled-components';
import { TopCenteredPage } from 'js/components/app';
import { List, ListItem, Subheader, Divider } from 'material-ui';
import { getAsset, openExternalLink } from 'js/utils/getAssets';
import { credits } from './quizAndTipsURLs';
const api = API.getInstance();
const LOGO = getAsset('logo', false);

const BUILD_INFO_KEY_MAP = {
  title: '',
  packageName: '_app_info_pkg_name', //'Package Name',
  description: '_app_info_description', //'Description',
  version: '_app_info_version', //'Version',
  buildDate: '_app_info_build_date', //'Build Date',
  environment: '_app_info_version', //'Bundle Environment',
  bundleIdentifier: IS_CORDOVA ? '_app_info_bundle_id' : 'Build Type', //'Bundle ID' : 'Build Type',
  backendEnvironment: '_app_info_environment_be', //'Backend Environment',
  geoServerURL: 'Geoserver URL',
  rasterLayersMode: 'Raster Layers Mode'
};

const Text = styled.div`
  text-transform: capitalize;
`;

const LinkedSpan = styled.span`
  text-decoration: underline;
  color: ${props => props.theme.palette.primary1Color};
`;

const innerDivStyle = {
  padding: 8
};

const BUILD_INFO_VALUES_MAP = {
  title: v => <Subheader>{v}</Subheader>,
  packageName: v => (
    <span>
      &nbsp;
      {v}
    </span>
  ),
  description: v => (
    <Text>
      &nbsp;
      {v}
    </Text>
  ),
  version: v => (
    <Text>
      &nbsp;
      {v}
    </Text>
  ),
  buildDate: (v, locale) => (
    <Text>
      &nbsp;
      {localizeDate(v, locale, 'LLLL Z')}
    </Text>
  ),
  environment: v => (
    <Text>
      &nbsp;
      {v}
    </Text>
  ),
  bundleIdentifier: v => (
    <Text style={{ textTransform: 'none' }}>
      &nbsp;
      {v === 'test' && IS_CORDOVA && IS_PRODUCTION ? 'Production' : v}
    </Text>
  ),
  backendEnvironment: v => (
    <Text>
      &nbsp;
      {v === 'test' && IS_CORDOVA && IS_PRODUCTION ? 'Production' : v}
    </Text>
  ),
  geoServerURL: v => (
    <span>
      &nbsp;
      {v}
    </span>
  ),
  rasterLayersMode: v => (
    <Text>
      &nbsp;
      {v}
    </Text>
  )
};

const getEntryKey = (key, dictionary) =>
  dictionary(BUILD_INFO_KEY_MAP.hasOwnProperty(key) ? BUILD_INFO_KEY_MAP[key] : key);

const getEntryValue = (key, value, locale) =>
  BUILD_INFO_VALUES_MAP.hasOwnProperty(key) ? BUILD_INFO_VALUES_MAP[key](value, locale) : value;

const Content = styled(List)`
  overflow-y: auto;
  height: calc(100% - 164px);
  width: 100%;
  overflow-x: hidden;
`;

// Prevent annoying react warning
const ApiDivider = () => <Divider />;
const ApiSub = () => <Subheader>API Module version</Subheader>;

// Keys we are not interested into
const KEYS_BLACKLIST = ['packageName'];

const About = ({ dictionary, locale }) => {
  console.log(
    '%c    About this app     ',
    'color: black; background: linear-gradient(to right, orange , yellow, green, cyan, blue, violet); font-weight: bold',
    appInfo
  );

  const appDetailsItems = Object.entries(appInfo)
    .filter(entry => !KEYS_BLACKLIST.includes(entry[0]))
    .map((entry, i) => (
      <ListItem
        key={i}
        innerDivStyle={innerDivStyle}
        primaryText={getEntryKey(entry[0], dictionary)}
        secondaryText={getEntryValue(entry[0], entry[1], locale)}
      />
    ));

  return (
    <TopCenteredPage className="about page">
      <Header showLogo={true} leftButtonType="back" />
      <img src={LOGO} alt="IOC Logo" height={100} />
      <Content>
        <Divider />
        <Subheader>About I-REACT (original project)</Subheader>
        <ListItem
          primaryText={
            <LinkedSpan onClick={() => openExternalLink('http://www.i-react.eu/')}>
              Overview
            </LinkedSpan>
          }
        />
        <ListItem
          primaryText={
            <LinkedSpan onClick={() => openExternalLink('http://www.project.i-react.eu/')}>
              The Project
            </LinkedSpan>
          }
        />
        <Divider />
        <Subheader>Privacy & Terms</Subheader>
        <ListItem
          primaryText={
            <LinkedSpan onClick={() => openExternalLink(PRIVACY_URL)}>Privacy Policy</LinkedSpan>
          }
        />
        <ListItem
          primaryText={
            <LinkedSpan onClick={() => openExternalLink(TOS_URL)}>Terms of Service</LinkedSpan>
          }
        />
        <Divider />
        {/* <Subheader>Build Info</Subheader> */}
        <ListItem
          primaryText="I-REACT App Build Info"
          initiallyOpen={false}
          primaryTogglesNestedList={true}
          nestedItems={[
            ...appDetailsItems,
            <ApiDivider key="d1" />,
            <ApiSub key="s1" />,
            <ListItem
              key="l1"
              innerDivStyle={innerDivStyle}
              primaryText={getEntryKey('packageName', dictionary)}
              secondaryText={getEntryValue('packageName', api.moduleVersion.pkgName, locale)}
            />,
            <ListItem
              key="l2"
              innerDivStyle={innerDivStyle}
              primaryText={getEntryKey('version', dictionary)}
              secondaryText={getEntryValue('version', api.moduleVersion.version, locale)}
            />,
            <ListItem
              key="l3"
              innerDivStyle={innerDivStyle}
              primaryText={getEntryKey('buildDate', dictionary)}
              secondaryText={getEntryValue('buildDate', api.moduleVersion.buildDate, locale)}
            />,
            <ListItem
              key="l4"
              innerDivStyle={innerDivStyle}
              primaryText={getEntryKey('environment', dictionary)}
              secondaryText={getEntryValue('environment', api.moduleVersion.environment, locale)}
            />
          ]}
        />
        <Divider />
        <Subheader>Credits</Subheader>
        <ListItem
          primaryText="Credits for Tips and Qizzes"
          initiallyOpen={false}
          primaryTogglesNestedList={true}
          nestedItems={credits.map((cItem, index) => (
            <ListItem
              key={index}
              primaryText={
                <LinkedSpan onClick={() => openExternalLink(cItem.url)}>{cItem.name}</LinkedSpan>
              }
            />
          ))}
        />
        <ListItem
          primaryText="3rd party libraries (Special Thanks ❤️)"
          initiallyOpen={false}
          primaryTogglesNestedList={true}
          nestedItems={[
            <ListItem
              key="1"
              primaryText={
                <LinkedSpan onClick={() => openExternalLink('https://v0.material-ui.com/#/')}>
                  Material UI
                </LinkedSpan>
              }
            />,
            <ListItem
              key="2"
              primaryText={
                <LinkedSpan onClick={() => openExternalLink('https://materialdesignicons.com/')}>
                  Material Design Icons
                </LinkedSpan>
              }
            />,
            <ListItem
              key="3"
              primaryText={
                <LinkedSpan
                  onClick={() => openExternalLink('https://www.mapbox.com/mapbox-gl-js/api/')}
                >
                  Mapbox GL-js
                </LinkedSpan>
              }
            />,
            <ListItem
              key="4"
              primaryText={
                <LinkedSpan onClick={() => openExternalLink('https://cordova.apache.org/')}>
                  Apache Cordova
                </LinkedSpan>
              }
            />,
            <ListItem
              key="5"
              primaryText={
                <LinkedSpan onClick={() => openExternalLink('https://reactjs.org/')}>
                  React JS
                </LinkedSpan>
              }
            />,
            <ListItem
              key="6"
              primaryText={
                <LinkedSpan onClick={() => openExternalLink('https://redux.js.org/')}>
                  Redux
                </LinkedSpan>
              }
            />,
            <ListItem key="7" primaryText="...and many others more" />
          ]}
        />
      </Content>
    </TopCenteredPage>
  );
};

export default withDictionary(About);
