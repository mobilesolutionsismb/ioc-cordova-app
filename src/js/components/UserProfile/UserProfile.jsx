import React, { Component } from 'react';
import { compose } from 'redux';
import { withMessages } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import { withLogin, withGamification } from 'ioc-api-interface';
import { Route } from 'react-router-dom';
import { TopCenteredPage } from 'js/components/app/Pages';
import { Header, Content } from 'js/components/app';
import { lighten } from 'material-ui/utils/colorManipulator';
import { getAsset } from 'js/utils/getAssets';

import styled from 'styled-components';

const AvatarPreview = styled.div.attrs(props => ({ className: 'avatar-preview' }))`
  width: 100%;
  height: 30%;
  background-color: ${props => props.theme.palette.primary1Color};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const UserAvatar = styled.div.attrs(props => ({ className: 'user-avatar' }))`
  width: ${props => props.size || 100}px;
  height: ${props => props.size || 100}px;
  min-width: ${props => props.size || 100}px;
  min-height: ${props => props.size || 100}px;
  background-image: ${props => `url(${props.src})`};
  background-position: center top;
  background-size: 85%;
  background-repeat: no-repeat;
  background-color: rgba(0, 0, 0, 0.25);
  border: 4px solid ${props => (props.selected ? props.theme.palette.accent1Color : 'transparent')};
  box-sizing: border-box;
  border-radius: 100%;
`;

const AvatarChoice = styled.div.attrs(props => ({ className: 'avatar-choice' }))`
  width: 100%;
  height: 70%;
  overflow-x: hidden;
  overflow-y: scroll;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;

  .avatar-list-item {
    width: ${((window.innerWidth - 6) / 3) * 0.85}px;
    height: ${((window.innerWidth - 6) / 3) * 0.85}px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    border: 1px solid transparent;
    box-sizing: border-box;
    border-radius: 100%;
    padding: 0;
    font-size: 16px;
    text-transform: uppercase;
    cursor: pointer;
    color: ${props => props.theme.palette.textColor};
    background-color: ${props => props.theme.palette.canvasColor};
    outline: none;
    -webkit-appearance: none;
    margin: 8px 0;

    &.selected.button {
      position: relative;
      &::before {
        position: absolute;
        content: 'checkmark';
        font-family: Material Icons;
        top: 0;
        left: 25%;
        color: ${props => props.theme.palette.accent1Color};
        line-height: ${((window.innerWidth - 6) / 3) * 0.85}px;
        font-size: ${((window.innerWidth - 6) / 3) * 0.85 * 0.5}px;
        font-weight: normal;
        font-style: normal;
        text-transform: none;
        -webkit-font-smoothing: antialiased;
        text-rendering: optimizeLegibility;
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-feature-settings: 'liga';
        font-feature-settings: 'liga';
        text-align: center;
        font-weight: bold;
        z-index: 1;
      }

      &::after {
        position: absolute;
        content: '';
        top: 0;
        left: 0;
        color: ${props => props.theme.palette.accent1Color};
        background-color: rgba(255, 255, 255, 0.3);
        border-radius: 100%;
        width: ${((window.innerWidth - 6) / 3) * 0.85}px;
        height: ${((window.innerWidth - 6) / 3) * 0.85}px;
        box-sizing: border-box;
        border: 4px solid;
      }
    }

    &.ripple {
      background-position: center;
      transition: background 0.8s;
    }
    &.ripple:hover {
      background: ${props => {
        const color = lighten(props.theme.palette.canvasColor, 0.2);
        return `${color} radial-gradient(circle, transparent 1%, ${color} 1%) center/15000%`;
      }};
    }
    &.ripple:active {
      background-color: ${props => lighten(props.theme.palette.canvasColor, 0.35)};
      background-size: 100%;
      transition: background 0s;
    }
  }
`;

const enhance = compose(
  withLogin,
  withGamification,
  withDictionary,
  withMessages
);

class UserProfileComponent extends Component {
  render() {
    const { dictionary, user, updateUserProfilePicture } = this.props;
    if (!user) {
      return <div />;
    }

    const { userPicture } = user;
    const avatar = getAsset(AVATAR_FILE_NAMES[userPicture]);

    return (
      <TopCenteredPage>
        <Header title="Avatar" leftButtonType="back" primary={true} />
        <Content>
          <AvatarPreview>
            <UserAvatar src={avatar} size={window.innerHeight * 0.2} selected={true} />
            <div>{dictionary('_prof_avatar_select_message')}</div>
          </AvatarPreview>
          <AvatarChoice>
            {AVATAR_FILE_NAMES.map((availableAvatar, i) => (
              <button
                onClick={() => updateUserProfilePicture(i)}
                key={i}
                className={`avatar-list-item ripple${userPicture === i ? ' selected button' : ''}`}
              >
                <UserAvatar
                  src={getAsset(availableAvatar)}
                  size={((window.innerWidth - 6) / 3) * 0.85}
                  // size={window.innerWidth * 0.32 * 0.8}
                  selected={userPicture === i}
                />
              </button>
            ))}
          </AvatarChoice>
        </Content>
      </TopCenteredPage>
    );
  }
}

const UserProfile = enhance(UserProfileComponent);

const UserProfileRoute = () => (
  <Route
    render={() => {
      return <UserProfile />;
    }}
  />
);
export default UserProfileRoute;
