import React from 'react';
import { NavLink } from 'react-router-dom';

export const TabLink = ({ to, label }) =>
    <NavLink
        replace={true}
        activeClassName="active"
        className="tab-link"
        to={to}>
        {label}
    </NavLink>;

export default TabLink;