import React, { Component } from 'react';
import { getAsset } from 'js/utils/getAssets';
import { withLogin, withGamification } from 'ioc-api-interface';
import { withDictionary } from 'ioc-localization';
import styled from 'styled-components';
import { UserAvatar } from './personal/Medals';
import { MedalIcon, AwardBadge } from 'js/components/Achievements/personal/Medals';
import {
  getItemAttributeTranslationString,
  getMedalImageAsset
} from 'js/components/Achievements/personal/assetNamesMaps';
import { compose } from 'redux';
import { logMain } from 'js/utils/log';
const enhance = compose(
  withGamification,
  withLogin,
  withDictionary
);
const RIBBON_PATH = '/assets/misc/ribbon.svg';

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const CongratsElements = styled.div.attrs(props => ({ className: 'congratulations' }))`
  width: 100%;
  height: 100%;
  /* min-height: 480px; */
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  .ribbon {
    width: 100%;
    height: 22%;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    background-image: url(${getAsset(RIBBON_PATH)});
    position: absolute;
    z-index: 10;
    bottom: 8%;

    /* filter: sepia(100%) saturate(4) hue-rotate(${getRandomInt(0, 359)}deg); */
  }

  .user-avatar {
    margin-top: 0;
    margin-bottom: auto;
  }

  .main-medal-container {
    position: absolute;
    z-index: 10;
    bottom: 0%;
  }
  .level-or-label,
  .score {
    height: 40px;
    line-height: 40px;
    font-size: 32px;
  }

  .level-or-label {
    bottom: -20px;
    font-weight: bold;
    text-transform: capitalize;
  }

  .score{
    font-size: 28px;
    color: ${props => props.theme.palette.textColor};
    &.delta{
      font-size: 32px;
    }
  }

  .username,
  .description {
    /* min-height: 40px; */
    line-height: 40px;
    margin: 8px 0 0 0;
  }

  .description{
    top: 36px;
    font-style: italic;
    padding: 0px 32px;
    font-size: 18px;
    box-sizing: border-box;
    line-height: 20px;
    z-index:99;
  }
  .username{
    top: 0;
    color: ${props => props.theme.palette.textColor};
    font-size: 30px;
    text-transform: capitalize;
  }
`;

// es ?newContentType=medal&newContentName=read_tip&metal=bronze&scoreDifference=300

class Congratulations extends Component {
  static defaultProps = {
    scoreDifference: 0, //if positive, will be highlighted
    newContentName: null, // es top_reporter
    newContentType: null, // es medal, badge, award
    metal: null, // medal only: bronze, gold...
    hazard: null, // badge only: fire, flood...
    isLearner: false,
    baseWidth: window.innerWidth,
    scaleFactor: 0.95
  };

  _updateScores = async () => {
    // TODO check is pro and avoid if pro
    try {
      const result = await this.props.updateScores();
      console.log('Gamification profile updated', result);
    } catch (err) {
      //TODO a decent print error for network errors
      console.error('Cannot update gamification scores: ', err);
    }
  };

  _areBadgesAvailable = (hazard, learner, reporter, reviewer) => {
    return (
      hazard &&
      Array.isArray(learner.badges) &&
      Array.isArray(reporter.badges) &&
      Array.isArray(reviewer.badges)
    );
  };

  _areMedalsAvailable = (metal, learner, reporter, reviewer) => {
    return (
      metal &&
      Array.isArray(learner.medals) &&
      Array.isArray(reporter.medals) &&
      Array.isArray(reviewer.medals)
    );
  };

  _getConfig = (
    newContentType,
    newContentName,
    hazard,
    metal,
    direction = 'up',
    competence = ''
  ) => {
    let labelKey = null,
      descriptionKey = null,
      threshold = null,
      assetURL = null;
    const { achievements } = this.props.gamificationSkillsAndAchievements;
    const { learner, reporter, reviewer } = achievements;
    const name = typeof newContentName === 'string' ? newContentName.toLowerCase() : '';
    const type = typeof newContentType === 'string' ? newContentType.toLowerCase() : '';
    if (newContentType && (newContentType === 'level' || (learner && reporter && reviewer))) {
      const validBadge =
        newContentType !== 'badge' || this._areBadgesAvailable(hazard, learner, reporter, reviewer);
      const validMedal =
        newContentType !== 'medal' || this._areMedalsAvailable(metal, learner, reporter, reviewer);
      const validContent = validMedal && validBadge;
      if (validContent) {
        const _hazard = typeof hazard === 'string' ? hazard.toLowerCase() : hazard;
        const _metal = typeof metal === 'string' ? metal.toLowerCase() : metal;
        const item = {
          name,
          award: name,
          hazard: _hazard,
          type: _metal,
          direction
        };
        labelKey = getItemAttributeTranslationString(item, type, 'name');
        descriptionKey = getItemAttributeTranslationString(item, type, 'congratulations');
        assetURL = getMedalImageAsset(item, type);
      }

      const relevantItem =
        achievements[competence] && Array.isArray(achievements[competence][`${type}s`])
          ? achievements[competence][`${type}s`].find(item => item.name === name)
          : undefined;
      threshold = relevantItem ? relevantItem.threshold || null : null;
    }
    return { labelKey, descriptionKey, assetURL, threshold };
  };

  componentDidMount() {
    this._updateScores();
  }

  render() {
    const {
      user,
      gamificationProfile,
      scoreDifference,
      newContentType,
      newContentName,
      direction,
      hazard,
      metal,
      competence,
      dictionary,
      style,
      isLearner,
      scaleFactor,
      baseWidth
    } = this.props;
    const { userName, userPicture } = user;
    const { currentLevel /* , score */ } = gamificationProfile;
    const avatar = getAsset(AVATAR_FILE_NAMES[userPicture]);
    const levelImage = getMedalImageAsset({ name: currentLevel.toLowerCase() }, 'level');
    const size = baseWidth * scaleFactor;
    const avatarSize = (isLearner ? 1 : 0.9) * baseWidth * scaleFactor;

    const _scoreDifference = parseInt(scoreDifference, 10);
    const _direction = typeof direction === 'string' ? direction.toLowerCase() : '';
    const _competence = typeof competence === 'string' ? competence.toLowerCase() : '';
    const { labelKey, descriptionKey, assetURL, threshold } = this._getConfig(
      newContentType,
      newContentName,
      hazard,
      metal,
      _direction,
      _competence
    );

    logMain('Congratulations: params', labelKey, descriptionKey, assetURL);
    return (
      <CongratsElements style={style}>
        {isLearner && (
          <div className="username">
            <span>
              {userName}, {currentLevel}
            </span>
          </div>
        )}
        <div className="description">
          {descriptionKey ? dictionary(descriptionKey, threshold) : ''}
        </div>
        <UserAvatar height={size} width={size} avatarSize={avatarSize}>
          <div className="avatar-circle">
            <div className="avatar" style={{ backgroundImage: `url(${avatar})` }} />
          </div>
          {!isLearner && _direction !== 'down' && <div className="ribbon" />}
          {!isLearner && (
            <div className="main-medal-container">
              <MedalIcon
                className="congrats-badge"
                src={assetURL ? assetURL : levelImage}
                fallBackLetter={currentLevel[0]}
                color={'#FFF5'}
                isLocked={false}
                medalSize={`${avatarSize * 0.5}px`}
              />
            </div>
          )}
          {isLearner && (
            <AwardBadge
              style={{
                bottom: 0,
                right: 0
              }}
              size={avatarSize * 0.35}
              className="bottom-right"
              src={levelImage}
              fallbackColor={'transparent'}
            />
          )}
        </UserAvatar>

        {!isLearner && (
          <div className="level-or-label">{labelKey ? dictionary(labelKey) : currentLevel}</div>
        )}
        <div className={`score${_scoreDifference === 0 ? '' : ' delta'}`}>
          {_scoreDifference === 0
            ? '' //`${score} pts`
            : isLearner
            ? dictionary('_learner_session_points', _scoreDifference)
            : `+ ${_scoreDifference} pts`}
        </div>
      </CongratsElements>
    );
  }
}

export default enhance(Congratulations);
