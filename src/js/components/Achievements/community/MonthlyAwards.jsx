import React, { Component } from 'react';
import { List, Subheader } from 'material-ui';
import { MonthlyAward } from '../personal/Medals';
import { withGamification, withLogin } from 'ioc-api-interface';
import { withMessages } from 'js/modules/ui';
import { compose } from 'redux';
import AwardDetails from '../AwardDetails';
import { getItemAttributeTranslationString } from '../personal/assetNamesMaps';
import { Redirect } from 'react-router-dom';

const enhance = compose(
  withGamification,
  withLogin,
  withMessages
);

class MonthlyAwards extends Component {
  componentDidMount() {
    if (this.props.loggedIn) {
      this.props.updateMonthlyAwards();
    }
  }

  onItemClick = (item, itemType) => {
    // console.log('Clicked', item);
    const nameDictKey = getItemAttributeTranslationString(item, itemType, 'name');
    const descriptionKey = getItemAttributeTranslationString(item, itemType, 'description');

    const title = this.props.dictionary(nameDictKey);
    const message = (
      <AwardDetails item={item} itemType={itemType} descriptionKey={descriptionKey} />
    );
    this.props.pushModalMessage(title, message, { _close: null });
  };

  mapAwards = awards =>
    awards.map((award, i) => {
      const theAward = { name: award.award, ...award };
      return (
        <MonthlyAward
          key={i}
          onClick={() => this.onItemClick(theAward, 'award')}
          dictionary={this.props.dictionary}
          thisUser={this.props.user}
          {...theAward}
        />
      );
    });

  render() {
    const { gamificationMonthlyAwards, palette, dictionary, loggedIn } = this.props;
    if (!loggedIn) {
      return <Redirect to="/" />;
    }
    const { reporter, reviewer /* , socializer */ } = gamificationMonthlyAwards;
    // console.log('MonthlyAwards', reporter, reviewer /* , socializer */);

    const style = { color: palette.textColor };

    return (
      <div className="monthlyawards-list" style={style}>
        <List className="monthlyawards-list__listview reporter">
          <Subheader className="monthlyawards-list-subheader">
            {dictionary('_maw_sub_reporter')}
          </Subheader>
          {this.mapAwards(reporter)}
        </List>
        <List className="monthlyawards-list__listview reviewer">
          <Subheader className="monthlyawards-list-subheader">
            {dictionary('_maw_sub_reviewer')}
          </Subheader>
          {this.mapAwards(reviewer)}
        </List>
      </div>
    );
  }
}

export default enhance(MonthlyAwards);
