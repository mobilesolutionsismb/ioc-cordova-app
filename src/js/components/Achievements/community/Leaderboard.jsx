import React, { Component } from 'react';
import { Avatar, List, ListItem, Subheader, RefreshIndicator } from 'material-ui';
import styled from 'styled-components';
import { compose } from 'redux';
import { getAsset } from 'js/utils/getAssets';
import { withGamification, withLogin } from 'ioc-api-interface';
const enhance = compose(
  withGamification,
  withLogin
);
const LEADERBOARD_ITEMS_PER_PAGE = 4;

const STYLES = {
  container: {
    position: 'relative',
    height: '100%',
    width: 0
  },
  refresh: {
    display: 'inline-block',
    position: 'absolute'
  },
  myUser: {
    backgroundColor: 'rgba(128, 222, 234, 0.3)' //cyan300
  }
};

const StyledAvatar = styled(Avatar)`
  border-color: ${props =>
    props['its-me'] === 'true'
      ? props.theme.palette.accent1Color
      : props.theme.palette.citizensColor};
  background-color: ${props => props.theme.palette.primary2Color} !important;
  border-width: 2px;
  border-style: solid;
  box-sizing: border-box;
`;

class LeaderBoard extends Component {
  active = false;

  state = {
    leaderBoardPageSkip: 0,
    moreLoading: false
  };

  componentDidMount() {
    this.props.updateLeaderBoard(
      this.state.leaderBoardPageSkip * LEADERBOARD_ITEMS_PER_PAGE,
      LEADERBOARD_ITEMS_PER_PAGE,
      false
    );
    this.active = true;
  }

  componentWillUnmount() {
    this.active = false;
  }

  _loadingState = moreLoading => (this.active ? this.setState({ moreLoading }) : null);

  loadMoreLoadingStart = () => this._loadingState(true);

  loadMoreLoadingStop = () => this._loadingState(false);

  loadMoreLeaderBoard = async () => {
    const nextValue = this.state.leaderBoardPageSkip + 1;
    this.loadMoreLoadingStart();
    try {
      await this.props.updateLeaderBoard(
        nextValue * LEADERBOARD_ITEMS_PER_PAGE,
        LEADERBOARD_ITEMS_PER_PAGE,
        true
      );
    } catch (e) {
      console.error('Error updating leaderboard', e);
    }
    this.loadMoreLoadingStop();
    this.setState({ leaderBoardPageSkip: nextValue });
  };

  mapContributors = (player, i) => {
    const itsMe = this.props.user.userName === player.userName;
    return (
      <ListItem
        key={i}
        leftAvatar={
          <StyledAvatar its-me={`${itsMe}`} src={getAsset(AVATAR_FILE_NAMES[player.userPicture])} />
        }
        primaryText={
          <span style={{ textTransform: 'capitalize' }}>{`${i + 1}. ${player.userName}`}</span>
        }
        secondaryText={`${player.level} - ${player.score} pts`}
      />
    );
  };

  _getDisplayRanking = (i, player, offset) => {
    const itsMe = this.props.user.userName === player.userName;
    const rank = this.props.gamificationProfile.rank <= 0 ? 1 : this.props.gamificationProfile.rank;
    if (itsMe) {
      return rank;
    } else {
      return i < offset ? rank - i - offset : rank + i;
    }
  };

  mapContributorsAroundMyRank = (offset, player, i) => {
    const itsMe = this.props.user.userName === player.userName;
    return (
      <ListItem
        key={i}
        leftAvatar={
          <StyledAvatar its-me={`${itsMe}`} src={getAsset(AVATAR_FILE_NAMES[player.userPicture])} />
        }
        style={this.props.user.userName === player.userName ? STYLES.myUser : {}}
        primaryText={
          <span style={{ textTransform: 'capitalize' }}>
            {`${this._getDisplayRanking(i, player, offset)}. ${player.userName}`}
          </span>
        }
        secondaryText={`${player.level} - ${player.score} pts`}
      />
    );
  };

  render() {
    const { gamificationLeaderboard, palette, user, dictionary } = this.props;
    const { aroundMyRank, players, topContributors } = gamificationLeaderboard;
    const topContributorsItems =
      topContributors && Array.isArray(topContributors.items) ? topContributors.items : null;
    const aroundMyRankItems =
      aroundMyRank && Array.isArray(aroundMyRank.items) ? aroundMyRank.items : null;
    const myPositionIndex = Array.isArray(aroundMyRank.items)
      ? aroundMyRank.items.findIndex(p => user.userName === p.userName)
      : -1;
    console.log('LeaderBoard', aroundMyRank, aroundMyRankItems, players, topContributors);
    const style = { color: palette.textColor };

    return (
      <div className="leaderboard-list" style={style}>
        <List className="leaderboard-list__listview top-contributors">
          <Subheader className="leaderboard-sub-header">
            <span>{dictionary('_maw_sub_top_contrib')}</span>
            <span>{dictionary('_maw_players_count', players)}</span>
          </Subheader>
          {!topContributorsItems && (
            <div>
              <span role="img" aria-label="cry">
                Ooops! Content not available 😢
              </span>
            </div>
          )}
          {topContributorsItems && topContributorsItems.map(this.mapContributors)}
        </List>
        {topContributorsItems && topContributors.totalCount < players && (
          <div className="load-more" onClick={this.loadMoreLeaderBoard}>
            <div style={STYLES.container}>
              <RefreshIndicator
                size={40}
                left={-40}
                top={0}
                status={this.state.moreLoading ? 'loading' : 'hide'}
                style={STYLES.refresh}
              />
            </div>
            <span>
              {dictionary('_load_more')}
              ...
            </span>
          </div>
        )}

        <List className="leaderboard-list__listview around-me">
          <Subheader className="leaderboard-sub-header">
            <span>{dictionary('_maw_sub_my_ranking')}</span>
            <span>{dictionary('_maw_players_count', players)}</span>
          </Subheader>
          {!aroundMyRankItems && (
            <div>
              <span role="img" aria-label="cry">
                Ooops! Content not available 😢
              </span>
            </div>
          )}
          {aroundMyRankItems &&
            aroundMyRankItems.map(this.mapContributorsAroundMyRank.bind(-1, myPositionIndex))}
        </List>
      </div>
    );
  }
}

export default enhance(LeaderBoard);
