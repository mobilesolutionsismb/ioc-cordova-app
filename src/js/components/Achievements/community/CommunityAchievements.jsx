import '../styles.scss';
import React, { Component } from 'react';
import { Tabs, Tab } from 'material-ui';
import { TabLink } from '../TabLink';
import LeaderBoard from './Leaderboard'; //Badge
import MonthlyAwards from './MonthlyAwards';

class CommunityAchievements extends Component {
  render() {
    const { palette, cardname, dictionary } = this.props;

    return (
      <Tabs
        className="achievements-content"
        contentContainerClassName="achievements-content__container"
        tabTemplateStyle={{ height: '100%' }}
        value={cardname}
      >
        <Tab
          label={
            <TabLink
              to="/achievements/community/leaderboard"
              label={dictionary('_maw_tab_leaderboard')}
            />
          }
          className="achievements-content__tab"
          value="leaderboard"
        >
          <LeaderBoard palette={palette} dictionary={dictionary} />
        </Tab>
        <Tab
          label={
            <TabLink
              to="/achievements/community/monthlyawards"
              label={dictionary('_maw_tab_monthly_aw')}
            />
          }
          className="achievements-content__tab"
          value="monthlyawards"
        >
          <MonthlyAwards palette={palette} dictionary={dictionary} />
        </Tab>
      </Tabs>
    );
  }
}

export default CommunityAchievements;
