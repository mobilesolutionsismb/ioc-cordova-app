import React from 'react';
import { Redirect } from 'react-router-dom';
import { withRouter } from 'react-router';

export const ACHIEVEMENTS_COMPONENTS = {
    log: 0,
    personal: 1,
    community: 2
};

export const ACHIEVEMENTS_PATHS = Object.keys(ACHIEVEMENTS_COMPONENTS);

const VALID_PATHS_RX = new RegExp(`/achievements/${ACHIEVEMENTS_PATHS.join('|')}`);

/**
 * Wraps the specified component into a connected Lock component that renders the original
 * component only if the `user` property is truthy, redirecting the user to the `lockScreenPath`
 * otherwise.
 * 
 * @param {any} Component the component to lock
 * @param {any} unauthenticatedLandingPage the path to the unauthenticated landing page
 * @param {any} authenticatedLandingPage the path to the authenticated landing page
 * @returns 
 */
const AchievementsRedirectLock = (Component) => {

    const Lock = ({ match, location, history, ...rest }) => {
        //const selectedPath = location.pathname.replace('/achievements/', '');
        const isValidPath = VALID_PATHS_RX.test(location.pathname); //ACHIEVEMENTS_PATHS.indexOf(selectedPath) > -1;
        // console.log('AchievementsRedirectLock::::isValidPath', location.pathname, isValidPath, rest);
        if (isValidPath) {
            return <Component location={location} {...rest} />;
        }
        else {
            return <div><Redirect to='/achievements/personal/skills' /></div>;
        }
    };

    return withRouter(Lock);
};

export default AchievementsRedirectLock;