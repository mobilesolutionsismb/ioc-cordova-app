import './styles.scss';
import React, { Component } from 'react';
import { LinearProgress, IconButton, IconMenu, MenuItem } from 'material-ui';
import { withMessages } from 'js/modules/ui';
import { withLogin, withGamification } from 'ioc-api-interface';
import { withDictionary } from 'ioc-localization';
import { getUserType } from 'js/utils/userPermissions';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { getAsset } from 'js/utils/getAssets';
import { LEVEL_ICONS_MAP } from './personal/assetNamesMaps';
import { compose } from 'redux';
import AwardDetails from './AwardDetails';
import { getItemAttributeTranslationString } from './personal/assetNamesMaps';

const BASE_CLASS_NAME = 'achievements-header';

const enhance = compose(
  withGamification,
  withLogin,
  withMessages,
  withDictionary
);

const AvatarContainer = ({ avatar, rank, score, userType, baseClassName, currentLevel }) => (
  <div className={`${baseClassName}__header-avatar-container`}>
    {userType === 'citizen' && (
      <div className="rank-indicator">
        <span>{rank}</span>
        <small>rank</small>
      </div>
    )}
    <div className="avatar-circle">
      <div className="avatar" style={{ backgroundImage: `url(${avatar})` }} />
      {userType === 'citizen' && (
        <div
          style={{
            width: 32,
            height: 32,
            borderRadius: '100%',
            backgroundRepeat: 'no-repeat',
            backgroundImage: getAsset(LEVEL_ICONS_MAP[currentLevel.toLowerCase()], true),
            backgroundSize: 'contain',
            position: 'absolute',
            right: 0,
            bottom: 0,
            zIndex: 90
          }}
        />
      )}
    </div>
    {userType === 'citizen' && (
      <div className="score-indicator">
        <span>{score}</span>
        <small>pts</small>
      </div>
    )}
  </div>
);

function NameLine({
  baseClassName,
  userName,
  userType,
  name,
  surname,
  teams,
  organizationUnits,
  dictionary
}) {
  const isCitizen = userType === 'citizen';

  const className = `${baseClassName}__header-avatar-name-line ${isCitizen ? 'citizen' : 'pro'}`;
  const style = isCitizen ? {} : { flexDirection: 'column' };
  const nameString = isCitizen ? userName : `${name} ${surname}`;
  const organizationsList = organizationUnits.map(o => o.displayName).join(', ');
  const teamsList = teams.map(t => t.displayName).join(', ');
  return (
    <div className={className} style={style}>
      <div className="name-string">{nameString}</div>
      {!isCitizen && organizationUnits.length > 0 && (
        <div className="orgs-line">
          <span>{organizationsList}</span>
        </div>
      )}
      {!isCitizen && teams.length > 0 && (
        <div className="teams-line">
          <b>{dictionary._team}</b>: <span>{teamsList}</span>
        </div>
      )}
    </div>
  );
}

const ProgressContainer = props => (
  <div className={`${props.baseClassName}__header-progress-container`}>
    <small className="progress-label left">{props.currentLevel}</small>
    <LinearProgress
      className="progress-bar"
      color={props.palette.accent2Color}
      style={{ margin: '0px 0px auto 0px' }}
      mode="determinate"
      value={
        props.score <= 0
          ? 0
          : (props.pointsToNextLevel <= 0
              ? 0.95
              : props.score / (props.score + props.pointsToNextLevel)) * 100
      }
    />
    <small className="progress-label right">{props.nextLevel}</small>
  </div>
);

function getErrorMessage(err) {
  let msg = '';
  if (err) {
    if (err.hasOwnProperty('details')) {
      const errDetails = err.details;
      if (errDetails && errDetails.hasOwnProperty('serverDetails')) {
        const serverDetails = errDetails.serverDetails;
        if (serverDetails) {
          if (serverDetails.hasOwnProperty('error')) {
            msg = serverDetails.error.message || '';
          }
          if (serverDetails.hasOwnProperty('message')) {
            msg = serverDetails.message;
          }
        }
      }
    }
  }
  return msg;
}

class AchievementsHeader extends Component {
  static defaultProps = {
    baseClassName: BASE_CLASS_NAME,
    avatarIsLink: false
  };

  componentDidMount() {
    console.log('AchievementsHeader DID MOUNT');
    const userType = getUserType(this.props.user);
    if (userType === 'citizen') {
      this._updateScores();
    }
  }

  _updateScores = async () => {
    // TODO check is pro and avoid if pro
    try {
      const result = await this.props.updateScores();
      console.log('Gamification profile updated', result);
    } catch (err) {
      //TODO a decent print error for network errors
      console.error('Cannot update gamification scores: ', err, getErrorMessage(err));
    }
  };

  componentDidUpdate(prevProps) {
    if (
      getUserType(this.props.user) === 'citizen' &&
      this.props.open === true &&
      prevProps.open === false
    ) {
      this._updateScores();
    }
  }

  render() {
    const {
      user,
      gamificationProfile,
      muiTheme,
      baseClassName,
      style,
      closeDrawer,
      history,
      avatarIsLink,
      dictionary,
      pushModalMessage
    } = this.props;
    if (!user) {
      return <div />;
    }
    const { userType, userPicture } = user;
    // RESULTS IN DUPLICATE PATH NAME
    const avatar = getAsset(AVATAR_FILE_NAMES[userPicture]);

    const themeStyle = {
      backgroundColor: muiTheme.palette.primary1Color,
      color: muiTheme.palette.textColor
    };
    return (
      <div
        className={`${baseClassName}__header`}
        style={{ ...themeStyle, ...style }}
        onClick={e => {
          if (typeof closeDrawer === 'function') {
            closeDrawer();
          }
          if (avatarIsLink && userType === 'citizen') {
            history.push('/achievements/personal/skills');
          } else {
            const level = gamificationProfile ? gamificationProfile.currentLevel : null;
            if (level) {
              const levelObj = { name: level.toLowerCase() };
              const nameDictKey = getItemAttributeTranslationString(levelObj, 'level', 'name');
              const descriptionKey = getItemAttributeTranslationString(
                levelObj,
                'level',
                'description'
              );

              const title = () => (
                <h3 style={{ textTransform: 'capitalize' }}>{dictionary(nameDictKey)}</h3>
              );
              const message = (
                <AwardDetails item={levelObj} itemType={'level'} descriptionKey={descriptionKey} />
              );
              pushModalMessage(title, message, { _close: null });
            }
          }
        }}
      >
        <IconMenu
          useLayerForClickAway={true}
          className="edit-icon-menu"
          style={{ position: 'absolute', top: 0, right: 0, zIndex: 99, padding: 8 }}
          iconButtonElement={
            <IconButton className="edit-profile" touch={true} iconClassName="material-icons">
              edit
            </IconButton>
          }
          anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
          targetOrigin={{ horizontal: 'right', vertical: 'top' }}
          onClick={e => {
            e.preventDefault();
            e.stopPropagation();
          }}
        >
          <MenuItem
            primaryText={dictionary('_prof_edit_change_avatar')}
            onClick={e => {
              e.preventDefault();
              e.stopPropagation();
              if (typeof closeDrawer === 'function') {
                closeDrawer();
              }
              if (history) {
                history.push('/editprofile');
              }
            }}
          />

          <MenuItem
            primaryText={dictionary('_prof_edit_change_password')}
            onClick={e => {
              e.preventDefault();
              e.stopPropagation();
              if (typeof closeDrawer === 'function') {
                closeDrawer();
              }
              if (history) {
                history.push('/editpassword');
              }
            }}
          />
        </IconMenu>
        <AvatarContainer
          avatar={avatar}
          {...user}
          {...gamificationProfile}
          userType={userType}
          baseClassName={baseClassName}
          currentLevel={gamificationProfile.currentLevel}
        />
        <NameLine
          {...user}
          baseClassName={baseClassName}
          userType={userType}
          dictionary={dictionary}
        />
        {userType === 'citizen' && (
          <ProgressContainer
            {...user}
            {...gamificationProfile}
            baseClassName={baseClassName}
            palette={muiTheme.palette}
          />
        )}
      </div>
    );
  }
}

export default enhance(muiThemeable()(AchievementsHeader));
