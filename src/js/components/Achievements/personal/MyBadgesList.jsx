import React, { Component } from 'react';
import { List, Subheader, Divider } from 'material-ui';
import { Medal, Badge, UnlockableContent } from './Medals';
import { withMessages } from 'js/modules/ui';
import AwardDetails from '../AwardDetails';
import { getItemAttributeTranslationString } from './assetNamesMaps';
const LIST_STYLE = { padding: '0px 0px 16px' };
const prefix = '_gm_skheader_';

class MyBadgesList extends Component {
  onItemClick = (item, itemType) => {
    console.log('Clicked', item);
    const nameDictKey = getItemAttributeTranslationString(item, itemType, 'name');
    const descriptionKey = getItemAttributeTranslationString(item, itemType, 'description');

    const title = this.props.dictionary(nameDictKey);
    const message = (
      <AwardDetails item={item} itemType={itemType} descriptionKey={descriptionKey} />
    );
    this.props.pushModalMessage(title, message, { _close: null });
  };

  mapMedals = medals => {
    let mappedMedals = medals.map((medal, i) => (
      <Medal key={i} {...medal} onClick={() => this.onItemClick(medal, 'medal')} />
    ));
    const dividersToInsert = mappedMedals.length > 8 ? mappedMedals.length / 4 - 1 : 0;
    for (let j = 0; j < dividersToInsert; j++) {
      const n = (j + 1) * 4 + j;
      mappedMedals.splice(n, 0, <Divider key={'div-' + n} className="badge-category-divider" />);
    }
    return mappedMedals;
  };

  mapBadges = badges =>
    badges.map((badge, i) => (
      <Badge key={i} {...badge} onClick={() => this.onItemClick(badge, 'badge')} />
    ));

  render() {
    const { badges, unlockedContents, palette, dictionary } = this.props;
    const style = { color: palette.textColor };
    const badgeCategories = Object.keys(badges).filter(b => b !== 'socializer');

    return (
      <div className="badges-list" style={style}>
        <List className="badge-category-list" style={LIST_STYLE}>
          <Subheader className="badge-category-subheader">
            {dictionary(`${prefix}cont-unlock`)}
          </Subheader>
          {unlockedContents.map((content, i) => (
            <UnlockableContent
              key={i}
              {...content}
              onClick={() => this.onItemClick(content, 'unlocked-content')}
            />
          ))}
        </List>
        {badgeCategories.map((category, i) => (
          <List key={i} className="badge-category-list" style={LIST_STYLE}>
            <Subheader className="badge-category-subheader">
              {dictionary(`${prefix}${category}`)}
            </Subheader>
            {this.mapMedals(badges[category].medals)}
            <Divider className="badge-category-divider" />
            {this.mapBadges(badges[category].badges)}
          </List>
        ))}
      </div>
    );
  }
}

export default withMessages(MyBadgesList);
