import { getAsset } from 'js/utils/getAssets';

export const MEDALS_BADGES_DICT_NAME_PREFIX = '_mb_';
export const MEDALS_BADGES_DICT_DESC_PREFIX = '_mbd_';
export const MEDALS_BADGES_CONGR_DESC_PREFIX = '_mbc_';
export const CONT_UNL_DICT_NAME_PREFIX = '_cu_';
export const CONT_UNL_DICT_DESC_PREFIX = '_cud_';
export const CONT_UNL_DICT_CONGR_PREFIX = '_cuc_';

export const MONTH_AWR_PREFIX = '_maw_';
export const MONTH_AWR_DESC_PREFIX = '_mawd_';
export const MONTH_AWR_CONGR_PREFIX = '_mawc_';

export const LEVEL_DESC_PREFIX = '_leveld_';
export const LEVEL_CONGR_PREFIX = '_levelc_';

// name pattern = {name}-{hazard}
export const BADGE_NAMES_MAP = {
  // learner
  'read_all_tq-fire': '/assets/badges/read_all_tq-fire.svg',
  'read_all_tq-flood': '/assets/badges/read_all_tq-flood.svg',
  'read_all_tq-extremeWeather': '/assets/badges/read_all_tq-extremeWeather.svg',
  'read_all_tq-landslide': '/assets/badges/read_all_tq-landslide.svg',
  'read_all_tq-earthquake': '/assets/badges/read_all_tq-earthquake.svg',
  // reporter
  'report_on_hazard-fire': '/assets/badges/report_on_hazard-fire.svg',
  'report_on_hazard-flood': '/assets/badges/report_on_hazard-flood.svg',
  'report_on_hazard-extremeWeather': '/assets/badges/report_on_hazard-extremeWeather.svg',
  'report_on_hazard-landslide': '/assets/badges/report_on_hazard-landslide.svg',
  'report_on_hazard-earthquake': '/assets/badges/report_on_hazard-earthquake.svg'
  //reviewer
  // NONE
  // socializer - DEPRECATED
  //   'share_content-fire': '',
  //   'share_content-flood': '',
  //   'share_content-extremeWeather': ''
};

// name pattern = {name}-{type}
export const MEDAL_NAMES_MAP = {
  // learner

  'read_tip-bronze': '/assets/medals/read_tip-bronze.svg',
  'read_tip-silver': '/assets/medals/read_tip-silver.svg',
  'read_tip-gold': '/assets/medals/read_tip-gold.svg',
  'read_tip-platinum': '/assets/medals/read_tip-platinum.svg',
  'do_correct_quiz-bronze': '/assets/medals/do_correct_quiz-bronze.svg',
  'do_correct_quiz-silver': '/assets/medals/do_correct_quiz-silver.svg',
  'do_correct_quiz-gold': '/assets/medals/do_correct_quiz-gold.svg',
  'do_correct_quiz-platinum': '/assets/medals/do_correct_quiz-platinum.svg',
  // reporter
  'spont_reporter-bronze': '/assets/medals/spont_reporter-bronze.svg',
  'spont_reporter-silver': '/assets/medals/spont_reporter-silver.svg',
  'spont_reporter-gold': '/assets/medals/spont_reporter-gold.svg',
  'spont_reporter-platinum': '/assets/medals/spont_reporter-platinum.svg',
  'ondemand_reporter-bronze': '/assets/medals/ondemand_reporter-bronze.svg',
  'ondemand_reporter-silver': '/assets/medals/ondemand_reporter-silver.svg',
  'ondemand_reporter-gold': '/assets/medals/ondemand_reporter-gold.svg',
  'ondemand_reporter-platinum': '/assets/medals/ondemand_reporter-platinum.svg',
  //reviewer
  'vote_given-bronze': '/assets/medals/vote_given-bronze.svg',
  'vote_given-silver': '/assets/medals/vote_given-silver.svg',
  'vote_given-gold': '/assets/medals/vote_given-gold.svg',
  'vote_given-platinum': '/assets/medals/vote_given-platinum.svg',
  'vote_validated-bronze': '/assets/medals/vote_validated-bronze.svg',
  'vote_validated-silver': '/assets/medals/vote_validated-silver.svg',
  'vote_validated-gold': '/assets/medals/vote_validated-gold.svg',
  'vote_validated-platinum': '/assets/medals/vote_validated-platinum.svg'
  // socializer - DEPRECATED
  //   'share_content-bronze': '',
  //   'share_content-silver': '',
  //   'share_content-gold': '',
  //   'share_content-platinum': '',
  //   'invite_friend-bronze': '',
  //   'invite_friend-silver': '',
  //   'invite_friend-gold': '',
  //   'invite_friend-platinum': ''
};

export const AWARD_ICONS_MAP = {
  top_reporter: '/assets/monthly_awards/top_reporter.svg',
  best_report: '/assets/monthly_awards/best_report.svg',
  fire_reporter: '/assets/monthly_awards/fire_reporter.svg',
  flood_reporter: '/assets/monthly_awards/flood_reporter.svg',
  extreme_weather_reporter: '/assets/monthly_awards/extreme_weather_reporter.svg',
  landslide_reporter: '/assets/monthly_awards/landslide_reporter.svg',
  earthquake_reporter: '/assets/monthly_awards/earthquake_reporter.svg',
  top_reviewer: '/assets/monthly_awards/top_reviewer.svg',
  active_reviewer: '/assets/monthly_awards/active_reviewer.svg',
  accurate_reviewer: '/assets/monthly_awards/accurate_reviewer.svg'
};

export const LEVEL_ICONS_MAP = {
  rookie: '/assets/levels/rookie.svg',
  watcher: '/assets/levels/watcher.svg',
  sentinel: '/assets/levels/sentinel.svg',
  explorer: '/assets/levels/explorer.svg',
  master: '/assets/levels/master.svg',
  'i-reactor': '/assets/levels/i-reactor.svg'
};

export const UNLOCKED_CONTENTS_MAP = {
  climate_change: '/assets/unlocked_contents/climate_change.svg',
  copernicus_ems: '/assets/unlocked_contents/copernicus_ems.svg',
  extreme_weather: '/assets/unlocked_contents/extreme_weather.svg',
  fire_risk: '/assets/unlocked_contents/fire_risk.svg',
  flood_risk: '/assets/unlocked_contents/flood_risk.svg',
  heatwaves: '/assets/unlocked_contents/heatwaves.svg',
  hot_spots: '/assets/unlocked_contents/hot_spots.svg',
  super_extreme_weather: '/assets/unlocked_contents/super_extreme_weather.svg',
  weather_nowcast: '/assets/unlocked_contents/weather_nowcast.svg'
};

export function getItemAttributeTranslationString(item, itemType, attribute = 'name') {
  let str = '';
  let prefix = '';
  if (!item.name || !itemType) {
    return '_unknown';
  }
  switch (itemType) {
    case 'medal':
      prefix =
        attribute === 'name'
          ? MEDALS_BADGES_DICT_NAME_PREFIX
          : attribute === 'description'
            ? MEDALS_BADGES_DICT_DESC_PREFIX
            : attribute === 'congratulations'
              ? MEDALS_BADGES_CONGR_DESC_PREFIX
              : '';
      str = `${prefix}${item.name}_${item.type}`;
      break;
    case 'badge':
      prefix =
        attribute === 'name'
          ? MEDALS_BADGES_DICT_NAME_PREFIX
          : attribute === 'description'
            ? MEDALS_BADGES_DICT_DESC_PREFIX
            : attribute === 'congratulations'
              ? MEDALS_BADGES_CONGR_DESC_PREFIX
              : '';
      str = `${prefix}${item.name}_${item.hazard}`;
      break;
    case 'unlocked-content':
    case 'unlockedcontent':
    case 'unlockedContent':
      prefix =
        attribute === 'name'
          ? CONT_UNL_DICT_NAME_PREFIX
          : attribute === 'description'
            ? CONT_UNL_DICT_DESC_PREFIX
            : attribute === 'congratulations'
              ? CONT_UNL_DICT_CONGR_PREFIX
              : '';
      str = `${prefix}${item.name}`;
      break;
    case 'level':
      prefix =
        attribute === 'name'
          ? ''
          : attribute === 'description'
            ? LEVEL_DESC_PREFIX
            : attribute === 'congratulations'
              ? LEVEL_CONGR_PREFIX
              : '';
      const suffix = attribute === 'congratulations' ? `_${item.direction}` : '';
      str = `${prefix}${item.name}${suffix}`;
      break;
    case 'award':
      prefix =
        attribute === 'name'
          ? MONTH_AWR_PREFIX
          : attribute === 'description'
            ? MONTH_AWR_DESC_PREFIX
            : attribute === 'congratulations'
              ? MONTH_AWR_CONGR_PREFIX
              : '';
      str = `${prefix}${item.award}`;
      break;
    default:
      str = '_unknown';
      break;
  }
  return str;
}

export function getMedalImageAsset(item, itemType) {
  let assetName = null;
  switch (itemType) {
    case 'medal':
      assetName = MEDAL_NAMES_MAP[`${item.name}-${item.type}`] || null;
      break;
    case 'badge':
      assetName = BADGE_NAMES_MAP[`${item.name}-${item.hazard}`] || null;
      break;
    case 'unlocked-content':
    case 'unlockedcontent':
    case 'unlockedContent':
      assetName = UNLOCKED_CONTENTS_MAP[item.name] || null;
      break;
    case 'award':
      assetName = AWARD_ICONS_MAP[item.award] || null;
      break;
    case 'level':
      assetName = LEVEL_ICONS_MAP[item.name] || null;
      break;
    default:
      break;
  }

  return assetName ? getAsset(assetName) : null;
}
