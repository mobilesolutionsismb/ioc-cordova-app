import React from 'react';
import styled from 'styled-components';
import {
  blueGrey300,
  yellowA700,
  grey200,
  brown300,
  cyan100,
  blue300,
  deepOrange500,
  blueA700
} from 'material-ui/styles/colors';
import { withDictionary } from 'ioc-localization';
import {
  MEDALS_BADGES_DICT_NAME_PREFIX,
  CONT_UNL_DICT_NAME_PREFIX,
  MONTH_AWR_PREFIX,
  MEDAL_NAMES_MAP,
  BADGE_NAMES_MAP,
  AWARD_ICONS_MAP,
  LEVEL_ICONS_MAP,
  UNLOCKED_CONTENTS_MAP
} from './assetNamesMaps';
import { getAsset } from 'js/utils/getAssets';
import { FontIcon } from 'material-ui';

export function typeToMedalColor(type) {
  let color = '#333333';
  switch (type) {
    case 'platinum':
      color = blueGrey300; //'#5C878B';
      break;

    case 'gold':
      color = yellowA700; //'#FEC71C';
      break;

    case 'silver':
      color = grey200; //'#E0E0E0';
      break;

    case 'bronze':
      color = brown300; //'#878050';
      break;
    default:
      break;
  }
  return color;
}

const LockBadge = () => (
  <div className="lock small-badge bottom-right">
    <FontIcon className="material-icons" style={{ fontSize: 16 }}>
      lock
    </FontIcon>
  </div>
);

const MedalElement = styled.div.attrs(props => ({ className: 'medal-or-badge' }))`
  width: 25%;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  align-items: center;
  justify-content: space-around;
  height: ${props => (props.height ? `${props.height}px` : ' auto')};
  position: relative;

  .badge-category-list-item__label {
    max-width: 90%;
    text-align: center;

    .badge-category-list-item__label-inner {
      line-clamp: 2;
      -webkit-line-clamp: 2;
      line-height: 12px;
      max-height: 24px;
      height: 24px;
    }

    .badge-category-list-item__label-count {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      opacity: 0.7;
    }

    .badge-category-list-item__label-count,
    .badge-category-list-item__label-inner {
      width: 100%;
      font-size: 10px;
      word-break: break-word;
    }
  }

  .small-badge {
    background-color: ${props => props.theme.palette.accent1Color};
    &.lock {
      filter: saturate(50%);
    }
  }
`;

const MedalAvatar = styled.div.attrs(props => ({ className: 'medal-avatar' }))`
  background-image: ${props => (props.src ? `url('${props.src}')` : 'none')};
  background-size: contain;
  background-repeat: no-repeat;
  background-color: ${props => props.color};
  background-position: center;
  color: ${props => props.theme.palette.alternateTextColor};
  height: ${props => props.size};
  width: ${props => props.size};
  border-radius: 100%;
  text-transform: capitalize;
  text-align: center;
  line-height: 40px;
  position: relative;
  margin-top: 8px;

  &.locked {
    filter: brightness(30%) saturate(50%);
    /* opacity: 0.3; */
    /* filter: blur(2px); */
  }
  &.badge {
    box-sizing: border-box;
    border: 2px solid ${props => props.theme.palette.textColor};
  }
`;

export const MedalIcon = ({
  src,
  fallBackLetter,
  className,
  color,
  isLocked,
  medalSize = '20vw'
}) => [
  <MedalAvatar
    key="medal"
    src={src}
    className={isLocked ? `${className} locked` : className}
    color={src ? 'transparent' : color}
    size={medalSize}
  >
    {src ? '' : fallBackLetter}
  </MedalAvatar>,
  isLocked ? <LockBadge key="loc" /> : null
];

export const Medal = withDictionary(
  ({
    type,
    name,
    isLocked,
    numberOfActionsDoneToWinThisAchievement,
    threshold,
    dictionary,
    onClick
  }) => {
    const assetName = MEDAL_NAMES_MAP[`${name}-${type}`];
    const image = assetName ? getAsset(assetName) : null;
    return (
      <MedalElement onClick={onClick}>
        <MedalIcon
          className="badge-category-list-item__avatar"
          src={image}
          fallBackLetter={name[0]}
          color={typeToMedalColor(type)}
          isLocked={isLocked}
        />
        <div className="badge-category-list-item__label">
          <div className="badge-category-list-item__label-inner">
            {/* {name}-{type} */}
            {dictionary(`${MEDALS_BADGES_DICT_NAME_PREFIX}${name}_${type}`)}
          </div>
          <div className="badge-category-list-item__label-count">{`${numberOfActionsDoneToWinThisAchievement}/${threshold}`}</div>
        </div>
      </MedalElement>
    );
  }
);

export const Badge = withDictionary(
  ({
    name,
    hazard,
    isLocked,
    numberOfActionsDoneToWinThisAchievement,
    threshold,
    dictionary,
    onClick
  }) => {
    const assetName = BADGE_NAMES_MAP[`${name}-${hazard}`];
    const image = assetName ? getAsset(assetName) : null;
    return (
      <MedalElement onClick={onClick}>
        <MedalIcon
          className="badge-category-list-item__avatar badge"
          src={image}
          fallBackLetter={name[0]}
          color={cyan100}
          isLocked={isLocked}
        />
        <div className="badge-category-list-item__label">
          <div className="badge-category-list-item__label-inner">
            {/* {name}-{hazard} */}
            {dictionary(`${MEDALS_BADGES_DICT_NAME_PREFIX}${name}_${hazard}`)}
          </div>
          <div className="badge-category-list-item__label-count">{`${numberOfActionsDoneToWinThisAchievement}/${threshold}`}</div>
        </div>
      </MedalElement>
    );
  }
);

export const UnlockableContent = withDictionary(
  ({ dictionary, level, name, isLocked, onClick }) => {
    const assetName = UNLOCKED_CONTENTS_MAP[name];
    const image = assetName ? getAsset(assetName) : null;

    return (
      <MedalElement onClick={onClick}>
        <MedalIcon
          className="badge-category-list-item__avatar content"
          src={image}
          fallBackLetter={name[0]}
          color={cyan100}
          isLocked={isLocked}
        />
        <div className="badge-category-list-item__label" style={{ textTransform: 'capitalize' }}>
          <div className="badge-category-list-item__label-inner">
            {dictionary(`${CONT_UNL_DICT_NAME_PREFIX}${name}`)}
          </div>
          <div className="badge-category-list-item__label-count">{level}</div>
        </div>
      </MedalElement>
    );
  }
);

export const UserAvatar = styled.div.attrs(props => ({ className: 'user-avatar' }))`
  width: ${props => (props.width ? `${props.width}px` : '100%')};
  height: ${props => (props.height ? `${props.height}px` : '100%')};
  min-width: ${props => (props.width ? `${props.width}px` : '100%')};
  min-height: ${props => (props.height ? `${props.height}px` : '100%')};
  position: relative;
  border-radius: 100%;
  background-color: transparent;
  flex-direction: column;
  display: flex;
  justify-content: center;
  align-items: center;

  .avatar-circle {
    position: relative;
    width: ${props => (props.avatarSize ? `${props.avatarSize}px` : '20vw')};
    height: ${props => (props.avatarSize ? `${props.avatarSize}px` : '20vw')};
    border-radius: 100%;
    background-color: transparent;

    .avatar {
      position: absolute;
      background-repeat: no-repeat;
      background-position-x: center;
      background-position-y: 75%;
      background-size: 85%;
      border-radius: 100%;
      background-color: transparent;
      width: ${props => (props.avatarSize ? `${props.avatarSize}px` : '20vw')};
      height: ${props => (props.avatarSize ? `${props.avatarSize}px` : '20vw')};
      z-index: 1;
    }

    &::before {
      content: '';
      position: absolute;
      background-color: ${blue300};
      /* background-color: ${props =>
        props.backgroundColor ? props.backgroundColor : props.theme.palette.homeButtonsColor}; */
      width: 72%;
      height: 72%;
      bottom: 0px;
      left: 13.5%;
      border-radius: 100%;
      z-index: 0;
    }
  }
`;

export const AwardBadge = styled.div.attrs(props => ({ className: 'award-badge' }))`
  position: absolute;
  border-radius: 100%;
  width: ${props => (props.size ? props.size : 24)}px;
  height: ${props => (props.size ? props.size : 24)}px;
  line-height: ${props => (props.size ? props.size : 24)}px;
  font-size: ${props => (props.size ? props.size / 2 : 12)}px;
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  font-weight: 500;
  align-content: center;
  justify-content: center;
  background-repeat: no-repeat;
  background-position-x: center;
  background-position-y: center;
  background-size: contain;
  background-image: ${props => (props.src ? `url('${props.src}')` : 'none')};
  background-color: ${props => (props.fallbackColor ? props.fallbackColor : 'transparent')};
  z-index: 100;
  &.top-left {
    top: ${props => (props.top ? props.top : `${props.size ? props.size / 2 : 12}px`)};
    left: 10%;
  }

  &.top-right {
    top: ${props => (props.top ? props.top : `${props.size ? props.size / 2 : 12}px`)};
    right: 10%;
  }

  &.bottom-left {
    bottom: ${props => (props.bottom ? props.bottom : `${props.size ? props.size / 2 : 12}px`)};
    left: 10%;
  }

  &.bottom-right {
    bottom: ${props => (props.bottom ? props.bottom : `${props.size ? props.size / 2 : 12}px`)};
    right: 10%;
  }
`;

export const MonthlyAward = withDictionary(
  ({
    onClick,
    thisUser,
    dictionary,
    award,
    /* itemCount, */ level,
    points,
    // userId,
    userPicture,
    username
  }) => {
    const avatar = getAsset(AVATAR_FILE_NAMES[userPicture]);
    // const itsMe = thisUser.userId === userId || thisUser.userName === username;
    const awardImage = getAsset(AWARD_ICONS_MAP[award]);
    const levelImage = getAsset(LEVEL_ICONS_MAP[level.toLowerCase()]);
    return (
      <MedalElement height={130} onClick={onClick}>
        <UserAvatar height={80}>
          <div className="avatar-circle">
            <div className="avatar" style={{ backgroundImage: `url(${avatar})` }} />
          </div>
          <AwardBadge
            className="bottom-left"
            bottom={'5%'}
            src={awardImage}
            fallbackColor={blueA700}
          />
          <AwardBadge
            className="bottom-right"
            bottom={'5%'}
            src={levelImage}
            fallbackColor={deepOrange500}
          />
        </UserAvatar>
        <div className="badge-category-list-item__label">
          <div className="badge-category-list-item__label-inner">
            <b>{dictionary(`${MONTH_AWR_PREFIX}${award}`)}</b>
          </div>
          <div
            className="badge-category-list-item__label-count"
            style={{ fontWeight: 100, textTransform: 'capitalize' }}
          >
            {username}
          </div>
          <div className="badge-category-list-item__label-count">
            {points}
            pts
          </div>
        </div>
      </MedalElement>
    );
  }
);
