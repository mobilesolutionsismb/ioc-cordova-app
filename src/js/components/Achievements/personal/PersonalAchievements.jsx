import '../styles.scss';
import React, { Component } from 'react';
import { Tabs, Tab } from 'material-ui';
import { TabLink } from '../TabLink';
import MySkillsList from './MySkillsList';
import MyBadgesList from './MyBadgesList'; //Badges

import { withGamification } from 'ioc-api-interface';
import { logError } from '../../../utils/log';

class PersonalAchievements extends Component {
  _updateSkillsAndAchievements = async () => {
    try {
      await this.props.updateSkillsAndAchievements();
    } catch (err) {
      logError(err);
    }
  };

  componentDidMount() {
    this._updateSkillsAndAchievements();
  }

  render() {
    const { palette, cardname, gamificationSkillsAndAchievements, dictionary } = this.props;
    const { skills, achievements, unlockedContents } = gamificationSkillsAndAchievements;
    //const tabStyle = { color: palette.textColor };

    return (
      <Tabs
        className="achievements-content"
        contentContainerClassName="achievements-content__container"
        tabTemplateStyle={{ height: '100%' }}
        value={cardname}
      >
        <Tab
          //style={tabStyle}
          label={
            <TabLink to="/achievements/personal/skills" label={dictionary('_gm_tab_my_skills')} />
          }
          className="achievements-content__tab"
          value="skills"
        >
          <MySkillsList palette={palette} skills={skills} dictionary={dictionary} />
        </Tab>
        <Tab
          //style={tabStyle}
          label={
            <TabLink to="/achievements/personal/badges" label={dictionary('_gm_tab_my_badges')} />
          }
          className="achievements-content__tab"
          value="badges"
        >
          <MyBadgesList
            dictionary={dictionary}
            palette={palette}
            badges={achievements}
            unlockedContents={unlockedContents}
          />
        </Tab>
      </Tabs>
    );
  }
}

export default withGamification(PersonalAchievements);
