export const EMCOMM_HAZARD_ICONS = {
  none: ' ',
  fire: '\ue905',
  flood: '\ue906',
  extremeWeather: '\ue904',
  landslide: '\ue907',
  avalanches: '\ue902',
  earthquake: '\ue903',
  unknown: '?'
};
