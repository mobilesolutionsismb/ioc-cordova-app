import React /* , { Component } */ from 'react';
import { Chip, Avatar, CircularProgress, FontIcon } from 'material-ui';
import { fade } from 'material-ui/utils/colorManipulator';
import styled from 'styled-components';
import { ColumnPage } from 'js/components/app/commons';
import { CATEGORY_MAP, CONTENT_TYPES_LETTERS } from 'js/components/ReportsCommons';
// import { EMCOMM_HAZARD_ICONS } from './emcommIconsMap';
import changeCase from 'change-case';
import EMCOMM_HAZARD_ICONS from './ireact_pinpoint_emcomm_hazard_map';

const DETAILS_LOADER_OFFSET_TOP = 64;

export function EmCommLanguage({ language, size = 24 }) {
  return (
    <Avatar className="emcomm-language" size={size} style={{ height: size, width: size }}>
      <FontIcon style={{ width: size }} className={`flag-icon flag-icon-${language}`} />
    </Avatar>
  );
}

export function EmCommLanguageChip({ language, palette, dictionary, size = 24 }) {
  return (
    <Chip className="language-chip" style={{ backgroundColor: palette.backgroundColor }}>
      <Avatar className="emcomm-language" size={size} style={{ height: size, width: size }}>
        <FontIcon style={{ width: size }} className={`flag-icon flag-icon-${language}`} />
      </Avatar>
      <div className="language-label">
        <span>
          {dictionary._language}: {language.toUpperCase()}
        </span>
      </div>
    </Chip>
  );
}

export const DetailsLoader = ({ top = DETAILS_LOADER_OFFSET_TOP }) => (
  <ColumnPage
    style={{
      position: 'absolute',
      top: top,
      left: 0,
      zIndex: 100,
      width: '100%',
      height: `calc(100% - ${top}px)`
    }}
  >
    <CircularProgress size={80} thickness={5} />
  </ColumnPage>
);

export const EmCommChip = ({ emComm, palette, dictionary }) => {
  return (
    <Chip className="hazard-chip" style={{ backgroundColor: palette.backgroundColor }}>
      <Avatar
        className="ireact-pinpoint-icons"
        size={24}
        style={{ height: 24, width: 24 }}
        color={palette.canvasColor}
        backgroundColor={palette.textColor}
      >
        {EMCOMM_HAZARD_ICONS[emComm.hazard]}
      </Avatar>
      <div className="hazard-label">
        <span>{dictionary['_ech_' + changeCase.snake(emComm.hazard)]}</span>
      </div>
    </Chip>
  );
};

export const LevelBox = styled.div`
  margin-right: 4px;
  margin-left: 2px;
  width: ${props => props.size || '18px'};
  height: ${props => props.size || '18px'};
  background-color: ${props =>
    props.theme.palette.emcommSeverity[props.severity] ||
    props.theme.palette.emcommSeverity.unknown};
`;

export const Level = ({ level, type, palette, isLarge, dictionary }) => {
  if (type === 'warning') {
    return (
      <div className={`level warning${isLarge ? ' large' : ''}`}>
        <div
          className="box"
          style={{
            borderColor: palette.bePreparedEmCommColor,
            color: isLarge
              ? level === 'bePrepared'
                ? palette.canvasColor
                : palette.textColor
              : palette.bePreparedEmCommColor,
            backgroundColor:
              level === 'bePrepared' ? palette.bePreparedEmCommColor : palette.canvasColor
          }}
        >
          {'' /* isLarge ? dictionary._emcomm_be_prepared : '' */}
        </div>
        <div
          className="box"
          style={{
            borderColor: palette.actionRequiredEmCommColor,
            color: isLarge
              ? level === 'actionRequired'
                ? palette.canvasColor
                : palette.textColor
              : palette.actionRequiredEmCommColor,
            backgroundColor:
              level === 'actionRequired' ? palette.actionRequiredEmCommColor : palette.canvasColor
          }}
        >
          {'' /* isLarge ? dictionary._emcomm_action_required : '' */}
        </div>
        <div
          className="box"
          style={{
            borderColor: palette.dangerToLifeEmCommColor,
            color: isLarge ? palette.textColor : palette.dangerToLifeEmCommColor,
            backgroundColor:
              level === 'dangerToLife' ? palette.dangerToLifeEmCommColor : palette.canvasColor
          }}
        >
          {'' /* isLarge ? dictionary._emcomm_danger_to_life : '' */}
        </div>
      </div>
    );
  } else if (type === 'alert') {
    return (
      <div
        className={`level alert${isLarge ? ' large' : ''}`}
        style={{
          borderColor: palette.dangerToLifeEmCommColor,
          backgroundColor: palette.dangerToLifeEmCommColor,
          color: palette.textColor
        }}
      >
        {'' /* isLarge ? dictionary._emcomm_danger_to_life : '' */}
      </div>
    );
  } else {
    return (
      <div className="level" style={{ background: palette.accent1Color, color: palette.textColor }}>
        {`Unknown Emergency Communication type ${type}`}
      </div>
    );
  }
};

export const CategoryDotIcon = ({ category, palette }) => (
  <Avatar
    className={`category-icon mdi mdi-${CATEGORY_MAP[category]}`}
    // className="category-icon material-icons"
    size={24}
    style={{ height: 24, width: 24 }}
    color={palette.textColor}
    backgroundColor={palette.backgroundColor}
  >
    {/* {CATEGORY_MAP[category]} */}
  </Avatar>
);

export const ContentTypeChip = ({ contentType, palette, highlighted, measureCount }) => {
  const color = highlighted ? palette.textColor : fade(palette.textColor, 0.4);
  return (
    <Chip
      className="content-type-chip"
      style={{ backgroundColor: palette.canvasColor }}
      labelStyle={{ paddingLeft: 2, fontSize: 16 }}
    >
      <Avatar
        className={contentType === 'measure' ? '' : 'ireact-icons'}
        size={24}
        style={{ height: 24, width: 24, fontSize: contentType === 'measure' ? 14 : 16 }}
        color={color}
        backgroundColor={palette.canvasColor}
      >
        {contentType === 'measure' ? measureCount : CONTENT_TYPES_LETTERS[contentType]}
      </Avatar>
      <div className="content-type-chip-label">
        <span style={{ color }}>{contentType.substr(0, 3)}</span>
      </div>
    </Chip>
  );
};

export const RESOURCES_IN_ORDER = ['damage', 'resource', 'people', 'measure'];

/**
 * Sort content type strings in the order
 * Damage, Resource, People, Measures
 *
 * @param {String} contentTypeString
 */
export function getContentTypesInOrder(contentTypeString) {
  return contentTypeString
    .split(', ')
    .sort((a, b) => RESOURCES_IN_ORDER.indexOf(a) - RESOURCES_IN_ORDER.indexOf(b));
}
