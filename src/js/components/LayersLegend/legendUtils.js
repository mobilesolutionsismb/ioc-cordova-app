import { iReactTheme } from 'js/startup/iReactTheme';
import axios from 'axios';
import { rgb2hex } from 'js/utils/rgb2hex';

const legend_options = {
  layout: 'vertical',
  columnwidth: 50,
  columns: 1,
  fontSize: 14,
  fontAntiAliasing: true,
  forceLabels: 'on',
  labelMargin: 0,
  dpi: 120,
  fontColor: rgb2hex(iReactTheme.palette.textColor)
  // transparent: true
  // bgColor: rgb2hex(iReactTheme.palette.canvasColor)
};

function getLegendOptions(cfg) {
  return Object.entries(cfg)
    .map(entry => entry.join(':'))
    .join(';');
}

async function getMeta(url) {
  return new Promise((success, fail) => {
    const img = new Image();
    img.addEventListener('load', function() {
      success({ src: url, img, width: this.naturalWidth, height: this.naturalHeight });
    });
    img.addEventListener('error', fail);
    img.src = url;
  });
}

export async function getLegendGraphic(
  layer,
  url,
  WIDTH = null,
  HEIGHT = null,
  language = 'en',
  format = 'image/png',
  headers = {}
) {
  const params = {
    request: 'GetLegendGraphic',
    format,
    layer,
    exceptions: 'application/json',
    language
  };
  if (WIDTH) {
    params['WIDTH'] = WIDTH;
    legend_options.rowwidth = Math.round(WIDTH * 0.5);
  }

  if (HEIGHT) {
    params['WIDTH'] = HEIGHT;
  }

  params['legend_options'] = getLegendOptions(legend_options);
  params['transparent'] = true;
  try {
    const response = await axios({
      method: 'get',
      headers,
      url: `${url.replace(/\/$/, '')}/wms`,
      params,
      responseType: 'arraybuffer'
    });
    const image = btoa(
      new Uint8Array(response.data).reduce((data, byte) => data + String.fromCharCode(byte), '')
    );
    const b64URL = `data:${response.headers['content-type'].toLowerCase()};base64,${image}`;
    const meta = await getMeta(b64URL);
    return meta;
  } catch (err) {
    throw err;
  }
}
