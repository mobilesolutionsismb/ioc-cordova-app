import React, { Component } from 'react';
import { compose } from 'redux';
import { logMain, logSevereWarning } from 'js/utils/log';
import { withDictionary } from 'ioc-localization';
import { withMapLayers, withLogin } from 'ioc-api-interface';
import { withLayersAndSettings /* , findInSettingsTree */ } from 'js/modules/layersAndSettings';
import { getLegendGraphic } from './legendUtils';
import { Page } from 'js/components/app/commons';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { ErrorContainer, FlexContainer } from 'js/components/app/commons';
import { appInfo } from 'js/utils/getAppInfo';

const enhance = compose(
  withMapLayers,
  withDictionary,
  withLayersAndSettings,
  withLogin
);

class Legend extends Component {
  constructor(props) {
    super(props);
    this._autoRef = React.createRef();
    this.state = {
      legendImages: [],
      legendErr: null,
      geoserverService: this._getGeoserverServiceConfigFromProps(props.user)
    };
  }

  _getGeoserverServiceConfigFromProps(user) {
    return user && Array.isArray(user.services)
      ? user.services.find(s => s.serviceName === 'Geoserver') || null
      : null;
  }

  _retrieveLegend = async (width = null, height = null) => {
    let legendImages = [];
    const { activeLayersSet, locale, mapLayers } = this.props;
    if (activeLayersSet.length > 0) {
      try {
        const layer = activeLayersSet[0];
        const url = GEOSERVER_URL
          ? GEOSERVER_URL
          : appInfo.backendEnvironment === 'dev'
          ? mapLayers.baseUrl
          : layer.url || mapLayers.baseUrl;
        // const apikey = this.state.geoserverService.key;
        legendImages = await Promise.all(
          layer.name
            .split(',')
            .map(name =>
              getLegendGraphic(name, url, width, height, locale, 'image/png' /* { apikey } */)
            )
        );
        logMain('Retrieve Legend', legendImages.length);
      } catch (err) {
        logSevereWarning('Cannot fetch legend', err);
        appInsights.trackException(err, 'Legend::_retrieveLegend', {}, {}, SeverityLevel.Error);
        this.setState({ legendErr: err });
      }
    }
    this.setState({ legendImages });
  };

  _getWidth = () => {
    return this._autoRef && this._autoRef.current
      ? Math.round(this._autoRef.current.clientWidth - 32)
      : 0;
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      (prevProps.user === null && this.props.user !== null) ||
      (prevProps.user !== null &&
        this.props.user !== null &&
        prevProps.user.username !== this.props.user.username)
    ) {
      this.setState({
        geoserverService: this._getGeoserverServiceConfigFromProps(this.props.user)
      });
    } else if (prevProps.user !== null && this.props.user === null) {
      this.setState({
        geoserverService: null
      });
    }
  }

  componentDidMount() {
    if (this._autoRef) {
      const width = Math.round(this._autoRef.current.clientWidth * 0.5);
      const height = null; //this._autoRef.current.clientHeight;
      this._retrieveLegend(width, height);
    }
  }

  render() {
    const { activeSetting, dictionary } = this.props;
    const { legendErr, legendImages } = this.state;
    if (activeSetting) {
      //   const label = activeSetting.name;
      // const maxWidth = this._getWidth();

      return (
        <Page
          ref={this._autoRef}
          className="layers-legend"
          style={{ backgroundColor: 'rgba(255,255,255,0.12)' }}
        >
          {legendErr && (
            <ErrorContainer>
              <h4>{dictionary('_ls_legend_error')}</h4>
            </ErrorContainer>
          )}
          {legendImages.map((meta, index) => (
            <div
              key={index}
              className="legend-image"
              style={{
                backgroundImage: `url(${meta.src})`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'contain',
                backgroundPosition: 'center',
                width: '100%',
                height: '100%'
              }}
            />
          ))}
        </Page>
      );
    } else {
      return (
        <Page className="layers-legend">
          <FlexContainer>
            <h4>{dictionary('_ls_no_legend')}</h4>
          </FlexContainer>
        </Page>
      );
    }
  }
}
export default enhance(Legend);
