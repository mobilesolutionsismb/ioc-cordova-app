import React, { Component } from 'react';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withMapLayers } from 'ioc-api-interface';
import { withLayersAndSettings /* , findInSettingsTree */ } from 'js/modules/layersAndSettings';
import { ColumnPage, Page } from 'js/components/app/commons';
import { API } from 'ioc-api-interface';
import { /* logMain, */ logSevereWarning, logFatal } from 'js/utils/log';
import { CircularProgress } from 'material-ui';
import { guessDate } from 'js/utils/localizeDate';
import styled from 'styled-components';
import { List } from 'material-ui';
import isPojo from 'is-pojo';
import decamelize from 'decamelize';

const enhance = compose(
  withMapLayers,
  withDictionary,
  withLayersAndSettings
);
const api = API.getInstance();

const MetadataListItem = styled.div`
  font-size: 12px;
  width: 100%;
  margin-bottom: 0.5em;
`;

const KeyItem = styled.div`
  text-transform: capitalize;
  font-weight: bold;
  width: 100%;
  color: ${props => props.theme.palette.textColor};
`;

const ValueItem = styled.div`
  width: 100%;
  opacity: 0.85;
  padding-left: 0.5em;
`;

class Metadata extends Component {
  static defaultProps = {
    activeSetting: null,
    metadataId: null
  };

  constructor(props) {
    super(props);
    this._autoRef = React.createRef();
    this.state = {
      loading: true,
      metadata: null,
      metadataError: null
    };
  }

  _loadStart = () => this.setState({ loading: true });

  _loadEnd = ({ metadata = null, metadataError = null }) =>
    this._autoRef ? this.setState({ loading: false, metadata, metadataError }) : null;

  _retrieveLayerMetadata = async () => {
    const metadataId = this.props.metadataId;
    if (metadataId === null) {
      return;
    }
    try {
      this._loadStart();
      const response = await api.maps.getLayerInfo(metadataId);
      const metadata = response.result;
      this._loadEnd({ metadata });
    } catch (err) {
      this._loadEnd({ metadataError: err });
      logSevereWarning(`Error retrieving metadata for metadataId=${metadataId}`, err);
    }
  };

  _flattenMetadata(metadata) {
    const _metadata = Array.isArray(metadata) ? metadata : [metadata];
    return _metadata.map((meta, index) => Object.entries(this._reduceMetadata(meta, index)));
  }

  _renderMetadata = metadata => {
    const items = this._flattenMetadata(metadata);
    return items.map((metadata, i) => (
      <List key={`m-${i}`} style={{ width: '100%' }}>
        {metadata.map(entry => {
          const key = entry[0];
          const value = entry[1];
          return (
            <MetadataListItem key={`m-${i}-${key}`} className="metadata-list-item">
              <KeyItem>{decamelize(key, ' ')}</KeyItem>
              <ValueItem>{value}</ValueItem>
            </MetadataListItem>
          );
        })}
      </List>
    ));
  };

  componentDidCatch(err) {
    logFatal(err);
    this.setState({ metadataError: err, metadata: null, loading: false });
  }

  _getKeyName(key, prefix = null) {
    return typeof prefix === 'string' ? `${prefix}.${key}` : key;
  }

  _reduceMetadata(metadata, key = null) {
    return Object.keys(metadata).reduce((allItems, nextMetdataKey) => {
      const value = metadata[nextMetdataKey];
      const nextKeyName = this._getKeyName(nextMetdataKey, key);
      if (Array.isArray(value)) {
        const flat = value
          .map(v => this._reduceMetadata(v, nextKeyName))
          .reduce((pre, next) => {
            pre = { ...pre, ...next };
            return pre;
          }, {});
        allItems = { ...allItems, ...flat };
      } else if (isPojo(value)) {
        const flat = this._reduceMetadata(value, nextKeyName);
        allItems = { ...allItems, ...flat };
      } else {
        allItems[nextKeyName] = guessDate(value, undefined, 'LLLL Z');
      }
      return allItems;
    }, {});
  }

  componentDidMount() {
    this._retrieveLayerMetadata();
  }

  render() {
    const { activeSetting, metadataId } = this.props;
    if (activeSetting && metadataId) {
      return (
        <Page ref={this._autoRef} className="layers-metadata">
          {this.state.loading && (
            <ColumnPage>
              <CircularProgress size={80} thickness={5} />
            </ColumnPage>
          )}
          {!this.state.loading && this.state.metadata && this._renderMetadata(this.state.metadata)}
          {!this.state.loading && this.state.metadataError && (
            <ColumnPage className="layers-legend">
              Error loading metadata: {this.state.metadataError.message || ''}
            </ColumnPage>
          )}
        </Page>
      );
    } else {
      return <ColumnPage className="layers-legend">No Metadata Available</ColumnPage>;
    }
  }
}

export default enhance(Metadata);
