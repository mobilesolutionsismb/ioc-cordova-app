import React from 'react';
import { Header, Content } from 'js/components/app';
import Legend from './Legend';

const LayersLegend = () => (
  <div className="layers-legend page">
    <Header title="_layers_legend" leftButtonType="close" />
    <Content>
      <Legend />
    </Content>
  </div>
);

export default LayersLegend;
