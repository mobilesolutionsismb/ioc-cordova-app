import React from 'react';
import { List } from 'react-virtualized'; //TODO use react-window
import { lighten } from 'material-ui/utils/colorManipulator';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';
import { logError } from 'js/utils/log';
import { ColumnPage } from 'js/components/app/commons';
import { CircularProgress } from 'material-ui';
import ScrollToTopButton from './ScrollToTopButton';
import { NoEntriesComponent } from 'js/components/MapTabs';
import { withDictionary } from 'ioc-localization';

const themed = muiThemeable();

const ITEMS_SPACING = 8;

const LIST_ITEMS_HEIGHTS = {
  report: 100,
  reportRequest: 120,
  emergencyCommunication: 120,
  mission: 120,
  missionTask: 110,
  agentLocation: 100
  // mapRequest: 158
};

const ITEMS_INNER_STYLE = {
  width: '100%',
  height: `calc(100% - ${ITEMS_SPACING}px)`,
  background: 'rgba(255, 255, 255, 0.3)'
};

function getRowRenderer(
  ListElement,
  otherListItemProps,
  goToMap,
  goToFeatureDetails,
  features,
  select,
  deselect,
  selectedFeature,
  compareFn,
  muiTheme,
  dictionary,
  locale
) {
  function rowRenderer({
    key, // Unique key within array of rows
    index, // Index of row within collection
    isScrolling, // The List is currently being scrolled
    isVisible, // This row is visible within the List (eg it is not an overscanned row)
    style // Style object to be applied to row (to position it)
  }) {
    const feature = Array.isArray(features) ? features[index] : null;
    const isSelected = compareFn(feature, selectedFeature);

    const selectedStyle = {
      background: lighten(muiTheme.palette.canvasColor, 0.3)
    };

    const innerStyle = isSelected ? { ...ITEMS_INNER_STYLE, ...selectedStyle } : ITEMS_INNER_STYLE;

    return (
      <div
        key={key}
        className={isSelected ? 'selected' : ''}
        style={style}
        onClick={goToFeatureDetails.bind(null, feature, isSelected)}
      >
        {/* Render card depending on itemType - height and width should be 100% */}
        <div style={innerStyle}>
          <ListElement
            feature={feature}
            {...otherListItemProps}
            isSelected={isSelected}
            onMapButtonClick={goToMap.bind(null, feature, isSelected)}
          />
        </div>
      </div>
    );
  }
  return rowRenderer;
}

const FIXED_HEADER_SIZE = 240; // all headers sum (48 + 48 + 32 + 48 + 64)
const OFFSET_TOP = 48 + 48 + 32;

const TYPE_LABELS_MAP = {
  report: '_drawer_label_reports',
  emergencyCommunication: '_drawer_label_notifications',
  mission: '_drawer_label_mission',
  missionTask: 'Task'
};

class GeoJSONItemsList extends React.Component {
  static defaultProps = {
    updateFeaturesFn: null,
    headerSize: FIXED_HEADER_SIZE,
    otherListItemProps: {}
  };

  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      height: Math.max(window.innerHeight - this.props.headerSize, 100) // we don't want negative numbers
    };
    this.listRef = React.createRef();
  }

  _onScroll = ({ clientHeight, scrollHeight, scrollTop }) => {
    // logMain('clientHeight, scrollHeight, scrollTop', clientHeight, scrollHeight, scrollTop);
    this.setState({ offset: scrollTop });
  };

  _onScrollToTopButtonClick = () => {
    if (this.listRef && this.listRef.current) {
      this.listRef.current.scrollToRow(0);
    }
  };

  updateHeight = () => {
    this.setState({ height: Math.max(window.innerHeight - this.props.headerSize, 100) });
  };

  componentDidMount = () => {
    window.addEventListener('resize', this.updateHeight);
    if (typeof this.props.updateFeaturesFn === 'function') {
      this.props.updateFeaturesFn();
    }
  };

  componentDidUpdate(prevProps) {
    if (this.props.headerSize !== prevProps.headerSize) {
      this.updateHeight();
    }
  }

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateHeight);
  };

  _goToFeatureDetails = (feature, isSelected, evt) => {
    evt.stopPropagation();
    evt.preventDefault();

    try {
      if (!isSelected && this.props.history) {
        const search = this.props.location ? this.props.location.search : '';
        const { itemType, id, missionId } = feature.properties;
        this.props.select(feature);
        switch (itemType) {
          case 'report':
            this.props.history.push(`/reportdetails/${id}`);
            // this.props.history.push(`/reportdetails/${id}?expanded`);
            break;
          case 'emergencyCommunication':
            this.props.history.push(`/emcommdetails/${id}?objectType=${feature.properties.type}`);
            break;
          case 'reportRequest':
            this.props.history.push(`/emcommdetails/${id}?objectType=report_request`);
            break;
          case 'mission':
            this.props.history.push(
              `/missiondetails/${id}${search.length > 0 ? search + '&' : '?'}objectType=mission`
            );
            break;
          case 'missionTask':
            this.props.history.push(
              `/missiondetails/${missionId}/${id}${
                search.length > 0 ? search + '&' : '?'
              }objectType=mission_task`
            );
            break;
          case 'tweet':
            this.props.history.push(`/social/${id}?objectType=tweet`);
            // this.props.history.push(`/social/${id}?expanded`);
            break;

          default:
            break;
        }
      } else {
        this.props.deselect();
      }
    } catch (err) {
      logError(err);
    }
  };

  _goToMap = (feature, isSelected, evt) => {
    evt.stopPropagation();
    evt.preventDefault();

    if (this.props.history) {
      try {
        if (!isSelected) {
          this.props.select(feature);
        }
        this.props.history.push('/tabs/map');
      } catch (err) {
        logError(err);
      }
    }
  };

  render() {
    // TODO use updating and error to display loader and error message
    const {
      features,
      featuresUpdating,
      /* updating, error, */ sorting,
      select,
      deselect,
      selectedFeature,
      compareFn,
      itemType,
      muiTheme,
      dictionary,
      locale,
      style,
      listElement,
      listeElementHeight,
      otherListItemProps
    } = this.props;
    const cardHeight = listeElementHeight
      ? listeElementHeight
      : itemType
      ? LIST_ITEMS_HEIGHTS[itemType] || 20
      : 20;
    const scrollToIndex = selectedFeature
      ? features.findIndex(f => areFeaturesEqual(f, selectedFeature))
      : undefined;

    const drawerHeight = this.state.height;

    if (featuresUpdating === false && features.length === 0) {
      const translatedItemName = dictionary(TYPE_LABELS_MAP[itemType] || itemType);
      const noFeaturesTitle = dictionary('_no_content_title', translatedItemName);
      const noFeaturesSubTitle = dictionary('_no_content_subtitle', translatedItemName);

      return <NoEntriesComponent title={noFeaturesTitle} subtitle={noFeaturesSubTitle} />;
    } else {
      return [
        featuresUpdating === true ? (
          <ColumnPage
            key="items-list-loader"
            style={{
              position: 'absolute',
              top: OFFSET_TOP,
              left: 0,
              width: '100%',
              height: `calc(100% - ${OFFSET_TOP}px)`
            }}
          >
            <CircularProgress size={80} thickness={5} />
          </ColumnPage>
        ) : (
          <div key="items-list-loader" style={{ display: 'none' }} />
        ),
        <List
          ref={this.listRef}
          key="items-list"
          onScroll={this._onScroll}
          style={{ background: muiTheme.palette.backgroundColor, ...style }}
          sorting={sorting}
          width={window.innerWidth}
          height={drawerHeight}
          rowCount={features.length}
          rowHeight={cardHeight + ITEMS_SPACING}
          scrollToIndex={scrollToIndex}
          scrollToAlignment="auto"
          rowRenderer={getRowRenderer(
            listElement,
            otherListItemProps,
            this._goToMap,
            this._goToFeatureDetails,
            features,
            select,
            deselect,
            selectedFeature,
            compareFn,
            muiTheme,
            dictionary,
            locale
          )}
        />,
        <ScrollToTopButton
          key="scroll-btn"
          offset={this.state.offset}
          onClick={this._onScrollToTopButtonClick}
        />
      ];
    }
  }
}

export default themed(withDictionary(GeoJSONItemsList));
