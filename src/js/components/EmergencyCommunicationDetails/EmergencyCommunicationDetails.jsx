import './styles.scss';
import React, { Component } from 'react';
import { withEmergencyCommunicationsApp } from 'ioc-api-interface';
import { withEmergencyCommunicationFilters } from 'js/modules/emcommFilters';
import { GeoJSONFeaturesProvider } from 'js/components/GeoJSONFeaturesProvider';
import { FeatureSelectionContext } from 'js/components/app/FeatureSelectionContext';
import { SORTERS } from 'js/modules/emcommFilters';
import qs from 'qs';

import { withRouter } from 'react-router';

import EmergencyCommunicationDetailsInner from './EmergencyCommunicationDetailsInner';

class EmergencyCommunicationDetails extends Component {
  render() {
    const {
      emergencyCommunicationFeaturesURL,
      emcommFiltersDefinition,
      emergencyCommunicationsUpdating,
      emcommSorter,
      match,
      history,
      location
    } = this.props;

    const routerProps = { match, history, location };
    const searchParams = qs.parse(location.search.replace(/^\?/, ''));
    const { objectType } = searchParams; // comes from push notification
    return (
      <GeoJSONFeaturesProvider
        sourceURL={emergencyCommunicationFeaturesURL}
        filters={emcommFiltersDefinition}
        sorter={SORTERS[emcommSorter]}
        itemType={'emergencyCommunication'}
      >
        {(features, featuresUpdating, featuresUpdateError, filtering, sorting) => (
          <FeatureSelectionContext.Consumer>
            {FeatureSelectionState => (
              <EmergencyCommunicationDetailsInner
                {...FeatureSelectionState}
                features={features}
                featuresUpdating={featuresUpdating || emergencyCommunicationsUpdating}
                {...routerProps}
                objectType={objectType}
              />
            )}
          </FeatureSelectionContext.Consumer>
        )}
      </GeoJSONFeaturesProvider>
    );
  }
}

export default withRouter(
  withEmergencyCommunicationsApp(withEmergencyCommunicationFilters(EmergencyCommunicationDetails))
);
