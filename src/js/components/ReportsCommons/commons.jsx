import React from 'react';
import { IconButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { white /* , red900 */ } from 'material-ui/styles/colors';
import { withDictionary } from 'ioc-localization';

const themeable = muiThemeable();

export const DateIcon = ({ style }) => (
  <FontIcon style={style} className="material-icons">
    access_time
  </FontIcon>
);
export const PlaceIcon = ({ style }) => (
  <FontIcon style={style} className="material-icons">
    place
  </FontIcon>
);
export const InfoIcon = ({ style }) => (
  <FontIcon style={style} className="material-icons">
    info
  </FontIcon>
);

export const DeleteReportButton = themeable(({ muiTheme, onClick, tooltip }) => (
  <IconButton
    className="menu-button"
    onClick={onClick}
    tooltip={tooltip}
    style={{ backgroundColor: muiTheme.palette.accent1Color, borderRadius: '100%', marginRight: 8 }}
  >
    <FontIcon className="material-icons" color={white}>
      clear
    </FontIcon>
  </IconButton>
));

// Blacklist hazard values which should not be selectable or displayed by the app
export const HAZARD_VALUES_BLACKLIST = ['all', 'other', 'unknown', 'none'];

export const CONTENT_TYPES_LETTERS = {
  measure: '\ue91a',
  damage: '\ue904', //!WRONG
  resource: '\ue929',
  people: '\ue91f'
};

//Export look up tables damage name -> iconName
export const DAMAGE_ICONS = {
  roads: '\ue909',
  bridges: '\ue900',
  underpass: '\ue90b',
  parks: '\ue906',
  other: '\ue905',
  school: '\ue90a',
  hospital: '\ue902',
  shopping_mall: '\ue903',
  private_house: '\ue908',
  farm: '\ue901',
  powerNetwork: '\ue907',
  power_network: '\ue907',
  waterSupply: '\ue90c',
  water_supply: '\ue90c'
};

//Export look up tables resource name -> iconName
export const RESOURCE_ICONS = {
  safe_place: '\ue926',
  'evacuation_area area': '\ue923',
  evacuation_area: '\ue923',
  drinkable_water: '\ue921',
  power_network: '\ue922',
  food: '\ue924',
  sleep: '\ue927',
  toilet: '\ue928',
  internet: '\ue925'
};

//Export look up tables people category -> iconName
export const PEOPLE_ICONS = {
  _injured: '\ue91d', // ic_sentiment_very_dissatisfied
  _to_be_rescued: '\ue920',
  _casualties: '\ue91b',
  _disabled: '\ue91c'
};

export const ContentTypeIcon = ({ iconSize, contentType, color }) => (
  <FontIcon
    style={{ lineHeight: iconSize * 0.6 + 'px', fontSize: iconSize * 0.6 }}
    className="ireact-icons"
    // className="material-icons"
    color={color}
  >
    {CONTENT_TYPES_LETTERS[contentType]}
  </FontIcon>
);

export const iconStyle = {
  fontSize: 18,
  lineHeight: '24px',
  fontWeight: 'bold'
};

export const iconRoundWrapperStyle = {
  width: 24,
  height: 24,
  lineHeight: '24px',
  textAlign: 'center',
  borderRadius: '100%'
};
export const CheckMarkIcon = themeable(({ muiTheme, style, ...rest }) => (
  <div
    className="checkmark-wrapper"
    style={{ ...iconRoundWrapperStyle, backgroundColor: muiTheme.palette.primary1Color, ...style }}
  >
    <FontIcon
      className="material-icons"
      style={{ ...iconStyle, ...rest.iconStyle }}
      color={muiTheme.palette.textColor}
    >
      done
    </FontIcon>
  </div>
));
export const PersonIcon = () => (
  <FontIcon className="material-icons" style={iconStyle}>
    person
  </FontIcon>
);

export const ValidatedIcon = themeable(({ muiTheme, tooltip, style, iconStyle }) => (
  <IconButton
    className="menu-button"
    style={{
      backgroundColor: muiTheme.palette.validatedColor,
      overflow: 'hidden',
      borderRadius: '100%',
      fontSize: '14px',
      width: 24,
      height: 24,
      margin: 0,
      padding: 0,
      lineHeight: '28px',
      ...style
    }}
    iconStyle={{ fontSize: '18px', ...iconStyle }}
    tooltip={tooltip}
    touch={true}
    tooltipStyles={{ top: 0 }}
    tooltipPosition="top-left"
  >
    <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
      done
    </FontIcon>
  </IconButton>
));

export const GoToMapIcon = themeable(
  ({ muiTheme, tooltip, onClick, style, iconStyle, tooltipPosition }) => (
    <IconButton
      className="menu-button"
      style={{
        width: 24,
        height: 24,
        borderRadius: '100%',
        fontSize: '14px',
        padding: 0,
        ...style
      }}
      iconStyle={{ fontSize: '18px', ...iconStyle }}
      //   tooltipPosition={tooltipPosition || 'top-center'}
      //   tooltip={tooltip}
      //   touch={true}
      //   tooltipStyles={{ whiteSpace: 'normal', padding: 2 }}
      onClick={onClick}
    >
      <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
        place
      </FontIcon>
    </IconButton>
  )
);

export const ShareIcon = themeable(({ muiTheme, tooltip, onClick, style, iconStyle }) => (
  <IconButton
    className="menu-button"
    style={{ width: 24, height: 24, borderRadius: '100%', fontSize: '14px', padding: 0, ...style }}
    iconStyle={{ fontSize: '18px', ...iconStyle }}
    tooltip={tooltip}
    touch={true}
    tooltipPosition="top-left"
    tooltipStyles={{ top: 0 }}
    onClick={onClick}
  >
    <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
      share
    </FontIcon>
  </IconButton>
));

// TODO align depending on user role
export const RoleColorDot = themeable(({ userRole, muiTheme }) => (
  <span
    className={`role-color-dot role-${userRole}`}
    style={{
      background:
        userRole === 'authority' ? muiTheme.palette.authorityColor : muiTheme.palette.citizensColor
    }}
  />
));

/**
 * Extract unique category names from a set of measures
 * @param {Array} measures
 */
export function getUniqueCategories(measures) {
  if (Array.isArray(measures)) {
    return Array.from(new Set(measures.map(m => m.category)));
  } else {
    return [];
  }
}

export const ReportContentStats = withDictionary(
  ({ reportProperties, dictionary, hideMeasureIcons }) => {
    const { measures, damages, resources, people } = reportProperties;
    let count = 0;
    let type = reportProperties.type || 'unknown'; // override the server's type, for now
    let categories = [];
    if (measures.length > 0) {
      type = 'measures';
      count = measures.length;
      categories = getUniqueCategories(measures);
    } else if (damages.length > 0) {
      type = 'damages';
      count = damages.length;
    } else if (resources.length > 0) {
      type = 'resources';
      count = resources.length;
    } else if (people.length > 0) {
      type = 'people';
      count = people.reduce((count, next) => {
        count += next.quantity;
        return count;
      }, 0);
    }

    return (
      <div className="report-stats">
        <span>
          {count === 1
            ? dictionary(`_r_stat_single_${type}`, count)
            : dictionary(`_r_stat_multi_${type}`, count)}
        </span>
        {type === 'measures' &&
          hideMeasureIcons !== true && (
            <span className="category-icons">
              {categories.map((c, i) => (
                <MeasureCategoryIcon key={i} style={{ fontSize: 18 }} category={c} />
              ))}
            </span>
          )}
      </div>
    );
  }
);

export const HazardBanner = withDictionary(
  themeable(
    ({ hazard, dictionary, muiTheme, style }) =>
      hazard && HAZARD_VALUES_BLACKLIST.indexOf(hazard) === -1 ? (
        <div
          className="hazard-banner"
          style={{
            color: muiTheme.palette.textColor,
            backgroundColor: muiTheme.palette.hazardBannerColor,
            ...style
          }}
        >
          {dictionary(`_${hazard}`)}
        </div>
      ) : null
  )
);

export const CATEGORY_MAP = {
  unknown: 'block-helper',
  fire: 'fire',
  water: 'water',
  landslide: 'terrain',
  weather: 'cloud'
};

export const MEASURE_UNITS = {
  degree: '°C',
  m: 'm',
  mm: 'mm',
  cm: 'cm',
  m3: 'm³'
};

// export const MeasureCategoryIcon = ({ color, category, style }) => (
//   <FontIcon color={color} style={style} className="material-icons">
//     {CATEGORY_MAP[category]}
//   </FontIcon>
// );

export const MeasureCategoryIcon = ({ color, category, style }) => (
  <FontIcon color={color} style={style} className={`mdi mdi-${CATEGORY_MAP[category]}`} />
);
