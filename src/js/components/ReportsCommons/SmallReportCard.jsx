import './styles.scss';
import React, { Component } from 'react';
import { LikesCounter } from 'js/components/LikeButtons';
import {
  CheckMarkIcon,
  // ShareIcon,
  RoleColorDot,
  ReportContentStats,
  HazardBanner,
  GoToMapIcon
} from './commons';
import { localizeDate } from 'js/utils/localizeDate';
import TextTruncate from 'react-text-truncate';

function is(x) {
  return !!x;
}
const POS_PRECISION = 5;

class SmallReportCard extends Component {
  static defaultProps = {
    showMapIcon: false,
    reportProperties: null
  };

  render() {
    const {
      locale,
      reportProperties,
      coordinates,
      showMapIcon,
      // onShareButtonClick,
      onMapButtonClick,
      dictionary,
      requesterId, // id of the app user
      requesterType, // type of the app user
      style
    } = this.props;

    if (reportProperties) {
      const reporter = reportProperties.user;
      const proReporter = reportProperties.organization !== null;
      // Show employeeId only if requesterType is authorty
      const reporterName =
        requesterType === 'citizen' ? null : proReporter ? reporter.employeeId : null; //reporter.nickname;
      const displayedUserName =
        requesterId === reportProperties.creatorUserId ? dictionary._me : reporterName;

      const text = proReporter
        ? [reportProperties.organization, displayedUserName].filter(is).join(', ') //`${reportProperties.organization}, ${displayedUserName}`
        : [displayedUserName, `${reporter.level || 'Novice'}`].filter(is).join(', ');

      return (
        <div className="small-report-card" style={style}>
          <div
            className="report-popup-thumbnail"
            style={{ backgroundImage: `url(${reportProperties.thumbnail})` }}
          >
            <HazardBanner hazard={reportProperties.hazard} />
          </div>
          <div className="report-popup-info">
            <div className="content-info">
              <div className="contents">
                <ReportContentStats reportProperties={reportProperties} />
              </div>
              <div className="author">
                <RoleColorDot userRole={proReporter ? 'authority' : 'citizen'} />
                <TextTruncate containerClassName="author-name" line={2} text={text} />
              </div>
            </div>
            <div className="date-votes-validation">
              <div className="date">
                {localizeDate(reportProperties.userCreationTime, locale, 'L LT')}
              </div>
              {requesterType !== 'citizen' && !showMapIcon && (
                <div className="location">
                  {coordinates[1].toFixed(POS_PRECISION)}, {coordinates[0].toFixed(POS_PRECISION)}
                </div>
              )}
              <div className="votes">
                {reportProperties.status === 'submitted' && (
                  <LikesCounter
                    size="xs"
                    style={{ width: 'calc(100% - 8px)' }}
                    upvotes={reportProperties.upvoteCount}
                    downvotes={reportProperties.downvoteCount}
                    alreadyVoted={reportProperties.alreadyVoted}
                  />
                )}
                {reportProperties.status === 'validated' && <CheckMarkIcon />}
              </div>
            </div>
            <div className="actions">
              {/* <ShareIcon tooltip="Share" onClick={onShareButtonClick} /> */}
              {showMapIcon && <GoToMapIcon tooltip="Show On Map" onClick={onMapButtonClick} />}
            </div>
          </div>
        </div>
      );
    } else {
      return <div className="small-report-card" />;
    }
  }
}

export default SmallReportCard;
