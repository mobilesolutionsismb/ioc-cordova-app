import { SOCIAL_HAZARD_VALUES, SOCIAL_INFORMATION_TYPE_VALUES } from './commons';

/**
 * Separates hazards and infoTypes from the 'classes' array of the original tweet
 * @param {Array} classes
 * @returns {Object} with hazards and infoTypes array of values
 */
export function separateHazardAndInfoTypes(classes) {
  const result = {
    hazards: classes.filter(c => SOCIAL_HAZARD_VALUES.indexOf(c) > -1),
    infoTypes: classes.filter(c => SOCIAL_INFORMATION_TYPE_VALUES.indexOf(c) > -1)
  };

  return result;
}

const removeURLRX = /^http(s)?:\/\/(\d|\w|\.)*\//;
// use geonameID as string, or extract it from IRI
const mapEntities = (ent, index) => ({
  id: ent.id ? `${ent.id}` : ent.iri.replace(removeURLRX, ''), //string
  iri: ent.iri,
  label: ent.name,
  _count: index // remove duplicated
});

const mapOutputEntities = ({ id, iri, label }) => ({ id, iri, label });

/**
 * Prepare feedback for the BE api
 * @param {Object} tweetProperties
 * @param {Object} editedProperties
 * @param {string|Number} userId
 * @returns {Object} feedback for the be api
 */
export function prepareTweetFeedback(tweetProperties, editedProperties, userId) {
  // The tweet Id
  const tweetId = tweetProperties.id;

  // The classnames as edited by the user
  const editedClassNames = [...editedProperties.hazards, ...editedProperties.infoTypes];

  // Classnames added by the user wrt original
  const newClassNames = editedClassNames.filter(cName => !tweetProperties.classes.includes(cName));

  // Classnames removed by the user wrt original
  const removedClassNames = tweetProperties.classes.filter(
    cName => !editedClassNames.includes(cName)
  );

  // Original entities
  const tweetEntities = tweetProperties.entities.map(mapEntities);
  // User-entered entities
  const editedEntities = editedProperties.entities.map(mapEntities);

  // Entities added by the user wrt original
  const newEntities = editedEntities
    .filter(
      eEnt => !tweetEntities.some(twEnt => twEnt.id === eEnt.id && twEnt._count === eEnt._count)
    )
    .map(mapOutputEntities);
  // Entities removed by the user wrt original
  const removedEntities = tweetEntities
    .filter(
      twEnt => !editedEntities.some(eEnt => twEnt.id === eEnt.id && twEnt._count === eEnt._count)
    )
    .map(mapOutputEntities);

  // Original informativeness
  const tweetInformative = tweetProperties.informative;

  // Original sentiment
  const tweetSentiment = tweetProperties.sentiment;

  // Values edited
  const tagsObject = {
    panic: editedProperties.tags.includes('panic'),
    informative: editedProperties.tags.includes('informative')
  };
  const newInformative = tagsObject.informative;
  const newSentiment = tagsObject.panic ? 'panic' : 'none';

  let updatedFeedbackProperties = {};

  let feedback = {
    userId: `${userId}`,
    tweetId,
    properties: tweetProperties
  };
  if (newClassNames.length > 0) {
    // feedback['AddedClasses'] = newClassNames;
    updatedFeedbackProperties['AddedClasses'] = newClassNames;
  }
  if (removedClassNames.length > 0) {
    // feedback['RemovedClasses'] = removedClassNames;
    updatedFeedbackProperties['RemovedClasses'] = removedClassNames;
  }
  if (newEntities.length > 0) {
    // feedback['AddedEntities'] = newEntities;
    updatedFeedbackProperties['AddedEntities'] = newEntities;
  }
  if (removedEntities.length > 0) {
    // feedback['RemovedEntities'] = removedEntities;
    updatedFeedbackProperties['RemovedEntities'] = removedEntities;
  }
  if (tweetSentiment !== newSentiment) {
    // feedback['SentimentNewValue'] = newSentiment;
    updatedFeedbackProperties['SentimentNewValue'] = newSentiment;
  }
  if (tweetInformative !== newInformative) {
    // feedback['InformativeNewValue'] = newInformative;
    updatedFeedbackProperties['InformativeNewValue'] = newInformative;
  }
  if (Object.keys(updatedFeedbackProperties).length > 0) {
    feedback = { ...feedback, ...updatedFeedbackProperties };
  } else {
    // _SHOULD_BE_ null
    updatedFeedbackProperties = {
      // AddedClasses: [],
      // RemovedClasses: [],
      // AddedEntities: [],
      // RemovedEntities: [],
      // SentimentNewValue: newSentiment,
      InformativeNewValue: newInformative
    };
  }

  return { feedback, updatedFeedbackProperties };
}
