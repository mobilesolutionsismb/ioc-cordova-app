import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withAppSocial } from 'ioc-api-interface';
import { withTweetsFilters } from 'js/modules/tweetsFilters';
import { SORTERS } from 'js/modules/tweetsFilters';
import { GeoJSONFeaturesProvider } from 'js/components/GeoJSONFeaturesProvider';
import { FeatureSelectionContext } from 'js/components/app/FeatureSelectionContext';
import { compose } from 'redux';
import SocialInner from './SocialInner';

const enhance = compose(
  withAppSocial,
  withTweetsFilters
);

class Social extends Component {
  render() {
    const {
      tweetFeaturesURL,
      tweetsFiltersDefinition,
      tweetsSorter,
      match,
      history,
      location,
      sendTweetFeedback
    } = this.props;

    const routerProps = { match, history, location };

    return (
      <GeoJSONFeaturesProvider
        sourceURL={tweetFeaturesURL}
        filters={tweetsFiltersDefinition}
        sorter={SORTERS[tweetsSorter]}
        itemType={'report'}
      >
        {(features, featuresUpdating, featuresUpdateError, filtering, sorting) => (
          <FeatureSelectionContext.Consumer>
            {FeatureSelectionState => (
              <SocialInner
                {...FeatureSelectionState}
                features={features}
                featuresUpdating={featuresUpdating}
                {...routerProps}
                sendTweetFeedback={sendTweetFeedback}
              />
            )}
          </FeatureSelectionContext.Consumer>
        )}
      </GeoJSONFeaturesProvider>
    );
  }
}

export default withRouter(enhance(Social));
