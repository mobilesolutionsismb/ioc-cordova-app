import React, { Component } from 'react';
import { FontIcon } from 'material-ui';
import { Header, Content } from 'js/components/app';
import { TWITTER_BLUE as HEADER_COLOR } from 'js/startup/iReactTheme';
import { logMain } from 'js/utils/log';
import { TopCenteredPage } from 'js/components/app/Pages';
import {
  linkifyTweet,
  SocialDetailsCard,
  SocialDetailsCardContent,
  SocialDetailsCardText,
  SocialDetailsBottom,
  TweetCardInfo,
  TweetCardProfile
} from './commons';
import { LocalizedDate } from 'js/components/MapTabs';
import { /* ShareIcon, */ GoToMapIcon } from 'js/components/ReportsCommons';
import SocialVotingControls from './SocialVotingControls';
import { ZoomableImage } from 'js/components/ReportDetails/ZoomableImage';
import { withDictionary } from 'ioc-localization';
import { openExternalLink } from 'js/utils/getAssets';

const headerStyle = {
  zIndex: 99,
  backgroundColor: HEADER_COLOR
};

const TITLE_ICON_STYLE = { fontSize: 24 };

const CONTENTS_STYLE = {
  display: 'flex',
  flexDirection: 'column'
};

const ZOOMABLE_IMAGE_STYLE = {
  top: 0,
  position: 'absolute',
  width: '100%',
  height: 'calc(100% - 32px)',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'column'
};

class SocialDetails extends Component {
  _openLink = e => {
    const el = e.target;
    if (el.className === 'tagged') {
      e.preventDefault();
      e.stopPropagation();
      const url = el.dataset['url'];
      if (url) {
        openExternalLink(url);
      }
    }
  };

  render() {
    logMain('SOCIAL DETAILS', this.props);
    const {
      updating,
      featureProperties,
      showOnMap,
      detailsExpanded,
      dictionary,
      sendTweetFeedback
    } = this.props;
    const {
      id,
      author,
      pictureURL,
      entities,
      // authorLocation, //NO! prendi entities.join(', ')
      content,
      positionSource,
      originalTweet,
      timestamp
      // tweetOrigin
    } = featureProperties;
    const twitterUser = originalTweet && originalTweet.user ? originalTweet.user : null;
    const profileImageUrl = twitterUser ? twitterUser.originalProfileImageURLHttps : null;
    const displayName = twitterUser ? twitterUser.screenName : '';
    const locationLabel = Array.isArray(entities) ? entities.map(e => e.name).join(', ') : '';
    // const isValid = tweetOrigin === 'professional';
    // const isTagged = tweetOrigin === 'user';

    return (
      <TopCenteredPage className="social-details page">
        <Header
          title={
            <LocalizedDate
              date={timestamp}
              style={{ textTransform: 'capitalize', fontWeight: 100, marginRight: 'auto' }}
            />
          }
          leftButtonType="back"
          rightButton={
            <span
              style={{ padding: 12, display: 'flex', justifyContent: 'space-between', width: 64 }}
            >
              {positionSource !== 'none' && (
                <GoToMapIcon iconStyle={TITLE_ICON_STYLE} onClick={showOnMap} />
              )}
              {displayName && (
                <span
                  onClick={() =>
                    openExternalLink(`https://twitter.com/${displayName}/status/${id}`)
                  }
                >
                  <FontIcon className="materialdesignicons mdi mdi-twitter" />
                </span>
              )}
            </span>
          }
          style={headerStyle}
        />
        {!updating &&
          content && (
            <Content style={CONTENTS_STYLE}>
              <SocialDetailsCard>
                <TweetCardProfile size={48} margin="8px 0 0 0" src={profileImageUrl} />
                <SocialDetailsCardContent>
                  <TweetCardInfo large={true}>
                    <span>{`\u00A0${author} \u00A0`}</span>
                    <span onClick={() => openExternalLink(`http://twitter.com/${displayName}`)}>
                      @{displayName}
                    </span>
                  </TweetCardInfo>
                  <SocialDetailsCardText
                    dangerouslySetInnerHTML={linkifyTweet(content)}
                    onClick={this._openLink}
                  />
                  <TweetCardInfo large={true}>
                    {positionSource !== 'none' && <GoToMapIcon />}
                    {`\u00A0${locationLabel ? locationLabel : ''}`}
                  </TweetCardInfo>
                </SocialDetailsCardContent>
              </SocialDetailsCard>
              <SocialDetailsBottom>
                <ZoomableImage
                  style={ZOOMABLE_IMAGE_STYLE}
                  loading={updating}
                  pictureURL={pictureURL}
                  loadingText={dictionary('_report_image_loading')}
                />
              </SocialDetailsBottom>
              <SocialVotingControls
                updating={updating}
                featureProperties={featureProperties}
                expanded={detailsExpanded}
                dictionary={dictionary}
                sendTweetFeedback={sendTweetFeedback}
              />
            </Content>
          )}
      </TopCenteredPage>
    );
  }
}

export default withDictionary(SocialDetails);
