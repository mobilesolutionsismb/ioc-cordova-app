import React /* , { Component } */ from 'react';
import {
  linkifyTweet,
  TweetCardContainer,
  TweetCardContent,
  TweetCardActions,
  TweetCardText,
  TweetCardInfo,
  TweetCardProfile
} from './commons';
import { CheckMarkIcon, GoToMapIcon } from 'js/components/ReportsCommons';
import { LocalizedDate } from 'js/components/MapTabs';
import { FontIcon } from 'material-ui';
import { openExternalLink } from 'js/utils/getAssets';

export const TagIcon = ({ style }) => (
  <FontIcon style={style} className="materialdesignicons mdi mdi-tag-multiple" />
);

const openLink = e => {
  const el = e.target;
  if (el.className === 'tagged') {
    const url = el.dataset['url'];
    if (url) {
      openExternalLink(url);
    }
  }
};

export const TweetCard = ({ feature, showMapIcon = true, onMapButtonClick }) => {
  const { properties } = feature;
  const {
    author,
    entities,
    content,
    originalTweet,
    timestamp,
    tweetOrigin,
    positionSource
  } = properties;
  const twitterUser = originalTweet && originalTweet.user ? originalTweet.user : null;
  const profileImageUrl = twitterUser ? twitterUser.miniProfileImageURLHttps : null;
  const isValid = tweetOrigin === 'professional';
  const isTagged = tweetOrigin === 'user';
  const locationLabel = Array.isArray(entities) ? entities.map(e => e.name).join(', ') : '';

  // const hasGeolocation = !isNaN(geometry.coordinates[0]) && !isNaN(geometry.coordinates[1]);

  return (
    <TweetCardContainer>
      <TweetCardContent>
        <TweetCardInfo>
          <TweetCardProfile src={profileImageUrl} />
          {`\u00A0${author} -\u00A0`}
          <LocalizedDate
            date={timestamp}
            style={{ textTransform: 'capitalize', fontWeight: 100 }}
          />
        </TweetCardInfo>
        <TweetCardText dangerouslySetInnerHTML={linkifyTweet(content)} onClick={openLink} />
        <TweetCardInfo>{locationLabel ? locationLabel : ''}</TweetCardInfo>
      </TweetCardContent>
      <TweetCardActions>
        {showMapIcon &&
          positionSource !== 'none' && (
            <GoToMapIcon tooltip="Show On Map" onClick={onMapButtonClick} />
          )}
        {isValid && <CheckMarkIcon />}
        {isTagged && <TagIcon />}
      </TweetCardActions>
    </TweetCardContainer>
  );
};
