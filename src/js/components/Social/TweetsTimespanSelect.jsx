import React, { Component } from 'react';
import { withAppSocial } from 'ioc-api-interface';
import { DropDownMenu, MenuItem } from 'material-ui';

const tweetFetchingTimespanValues = [72, 48, 36, 24, 12];

class TweetsTimespanSelect extends Component {
  _getMenuItems = () => {
    return tweetFetchingTimespanValues.map(v => (
      <MenuItem key={v} value={v} primaryText={`${v} h`} />
    ));
  };

  _renderSelection = v => `${v} h`;

  _onChange = (e, i, v) => this.props.setTweetFetchingTimespan(v);

  render() {
    return (
      <DropDownMenu
        className="tweets list-header__item"
        autoWidth={true}
        anchorOrigin={{
          horizontal: 'left',
          vertical: 'center'
        }}
        targetOrigin={{
          horizontal: 'left',
          vertical: 'center'
        }}
        // style={{ position: 'absolute', top: -4, right: 125, height: 40 }}
        iconStyle={{
          height: 48,
          position: 'relative',
          top: -12,
          right: 8,
          padding: 0,
          width: 24
        }}
        labelStyle={{
          height: 48,
          lineHeight: '48px',
          paddingLeft: 0,
          paddingRight: 8,
          display: 'inline-block'
        }}
        underlineStyle={{ margin: 'auto', borderTopColor: 'transparent' }}
        onChange={this._onChange}
        value={this.props.tweetsFetchingTimespan}
        selectionRenderer={this._renderSelection}
      >
        {this._getMenuItems()}
      </DropDownMenu>
    );
  }
}

export default withAppSocial(TweetsTimespanSelect);
