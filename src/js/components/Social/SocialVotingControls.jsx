import React, { Component } from 'react';
import { compose } from 'redux';
import { CheckMarkIcon } from 'js/components/ReportsCommons';
import { Link } from 'react-router-dom';
import { FlatButton, FontIcon } from 'material-ui';
import { withLogin } from 'ioc-api-interface';
import { withMessages, withLoader } from 'js/modules/ui';
import {
  SocialVotingControlsPanel,
  TweetValidationControls,
  TweetCategories,
  TweetInfoLine,
  TweetInfoLineLabel,
  TweetInfoValues,
  OPPOSITE_TAGS
} from './commons';
import { logMain, logError } from 'js/utils/log';
import {
  AttributeChip,
  EditEntities,
  EditTags,
  EditHazards,
  EditInfoTypes
} from './contentEditing';
import { prepareTweetFeedback, separateHazardAndInfoTypes } from './tweetUtils';
import { TagIcon } from './TweetCard';
// import { userIsValidator } from 'js/utils/userPermissions';

const enhance = compose(
  withLogin,
  withMessages,
  withLoader
);

const CollapsedIndicator = () => (
  <FontIcon className="material-icons collapse-icon">keyboard_arrow_up</FontIcon>
);

const ExpandedIndicator = () => (
  <FontIcon className="material-icons collapse-icon">keyboard_arrow_down</FontIcon>
);

class SocialVotingControls extends Component {
  constructor(props) {
    super(props);
    this.state = {
      entities: [],
      tags: [],
      hazards: [],
      infoTypes: [],
      unsavedChanges: false,
      containerHeight: null
    };
  }

  _initState = props => {
    const { entities, classes, sentiment, informative } = props.featureProperties;
    const { hazards, infoTypes } = separateHazardAndInfoTypes(classes);
    const tags = [
      sentiment === 'panic' ? 'panic' : 'no_panic',
      informative === true ? 'informative' : 'uninformative'
    ];
    const containerHeight =
      this._element && this._element.offsetHeight !== this._innerElement.offsetHeight
        ? this._innerElement.offsetHeight
        : this.state.containerHeight;
    this.setState(
      {
        entities,
        tags,
        hazards,
        infoTypes,
        unsavedChanges: false,
        containerHeight
      },
      this._checkHeight
    );
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      (!prevProps.featureProperties && this.props.featureProperties) ||
      prevProps.featureProperties.id !== this.props.featureProperties.id
    ) {
      this._initState(this.props.featureProperties);
    } else if (prevProps.featureProperties && !this.props.featureProperties) {
      this._initState({
        entities: [],
        tags: [],
        hazards: [],
        infoTypes: [],
        unsavedChanges: false
      });
    }
    if (prevProps.updating !== this.props.updating) {
      if (this.props.updating === true) {
        this.props.loadingStart();
      } else {
        this.props.loadingStop();
      }
    }
    if (prevProps.expanded !== this.props.expanded) {
      this._checkHeight();
    }
  }

  _checkHeight = () => {
    if (this._element && this._element.offsetHeight !== this._innerElement.offsetHeight) {
      this.setState({ containerHeight: this._innerElement.offsetHeight });
    }
  };

  componentDidMount() {
    this._initState(this.props);
  }

  _addEntity = entity => {
    const entityIndex = this.state.entities.findIndex(e => e.iri === entity.iri);
    if (entityIndex === -1) {
      this.setState(
        { entities: [...this.state.entities, entity], unsavedChanges: true },
        this._checkHeight
      );
    }
  };

  _deleteEntity = entity => {
    this.setState(prevState => {
      let entities = [...prevState.entities];
      let unsavedChanges = prevState.unsavedChanges;
      const entityIndex = entities.findIndex(e => e.iri === entity.iri);
      if (entityIndex > -1) {
        const e = entities.splice(entityIndex, 1);
        if (e && unsavedChanges === false) {
          unsavedChanges = true;
        }
      }
      return { entities, unsavedChanges };
    }, this._checkHeight);
  };

  _changeHazards = hazards => {
    this.setState(prevState => {
      let unsavedChanges = prevState.unsavedChanges;
      if (prevState.unsavedChanges === false) {
        //check difference
        unsavedChanges =
          hazards.length === prevState.hazards.length
            ? !hazards.every(e => prevState.hazards.includes(e))
            : true;
      }
      return { hazards, unsavedChanges };
    }, this._checkHeight);
  };

  _changeInfoTypes = infoTypes => {
    this.setState(prevState => {
      let unsavedChanges = prevState.unsavedChanges;
      if (prevState.unsavedChanges === false) {
        //check difference
        unsavedChanges =
          infoTypes.length === prevState.infoTypes.length
            ? !infoTypes.every(e => prevState.infoTypes.includes(e))
            : true;
      }
      return { infoTypes, unsavedChanges };
    }, this._checkHeight);
  };

  _deleteHazard = hazard => {
    this.setState(prevState => {
      let hazards = [...prevState.hazards];
      let unsavedChanges = prevState.unsavedChanges;
      const hazardIndex = hazards.findIndex(h => h === hazard);
      if (hazardIndex > -1) {
        const e = hazards.splice(hazardIndex, 1);
        if (e && unsavedChanges === false) {
          unsavedChanges = true;
        }
      }
      return { hazards, unsavedChanges };
    }, this._checkHeight);
  };

  _onTagsChange = tagsObject => {
    const tags = Object.entries(tagsObject).map(entry =>
      entry[0] === 'panic'
        ? entry[1]
          ? 'panic'
          : 'no_panic'
        : entry[1]
        ? 'informative'
        : 'uninformative'
    );
    this.setState(prevState => {
      let unsavedChanges = prevState.unsavedChanges;
      if (prevState.unsavedChanges === false) {
        //check difference
        unsavedChanges =
          tags.length === prevState.tags.length
            ? !tags.every(e => prevState.tags.includes(e))
            : true;
      }
      return { tags, unsavedChanges };
    });
  };

  _deleteTag = tag => {
    const tags = [...this.state.tags];
    const index = tags.indexOf(tag);
    if (index > -1) {
      const opposite = OPPOSITE_TAGS[tag];
      tags.splice(index, 1, opposite);
      this.setState({ tags, unsavedChanges: true });
    }
  };

  _deleteInfoType = infoType => {
    this.setState(prevState => {
      let infoTypes = [...prevState.infoTypes];
      let unsavedChanges = prevState.unsavedChanges;
      const infoTypeIndex = infoTypes.findIndex(h => h === infoType);
      if (infoTypeIndex > -1) {
        const e = infoTypes.splice(infoTypeIndex, 1);
        if (e && unsavedChanges === false) {
          unsavedChanges = true;
        }
      }
      return { infoTypes, unsavedChanges };
    }, this._checkHeight);
  };

  _saveEditing = async () => {
    this.props.loadingStart();
    const tweetProperties = this.props.featureProperties;
    const userId = this.props.user.id;
    const userType = this.props.user.userType;
    const isValidator = userType === 'authority'; //userIsValidator(this.props.user);

    const { entities, tags, hazards, infoTypes } = this.state;
    const editState = { entities, hazards, infoTypes, tags };
    const { feedback, updatedFeedbackProperties } = prepareTweetFeedback(
      tweetProperties,
      editState,
      userId
    );
    logMain('Save editing', feedback, updatedFeedbackProperties);
    try {
      const result = await this.props.sendTweetFeedback(tweetProperties, updatedFeedbackProperties);
      const { points } = result;
      const feedbackMessage = !isValidator
        ? points > 0
          ? '_soc_feedb_succ_ctz_pts'
          : '_soc_feedb_succ_ctz'
        : '_soc_feedb_succ_pro';
      this.props.pushMessage(feedbackMessage, 'success', null, points > 0 ? points : undefined);
    } catch (err) {
      logError(err);
      const errorMessage = this.props.dictionary('_soc_feedb_error_message');
      this.props.pushError(errorMessage);
    }
    this.props.loadingStop();
  };

  _element = null;
  _setElRef = e => {
    this._element = e;
  };

  _innerElement = null;

  _setInnerElRef = e => {
    this._innerElement = e;
  };

  render() {
    const { expanded, featureProperties, dictionary, user /* , loading, history */ } = this.props;
    // const appUserType = user.userType;

    const collapseLink = `/social/${featureProperties.id}${expanded ? '' : '?expanded'}`;
    const { entities, classes, sentiment, informative, tweetOrigin } = featureProperties;
    const { hazards, infoTypes } = separateHazardAndInfoTypes(classes);
    const tags = [
      sentiment === 'panic' ? 'panic' : 'no_panic',
      informative === true ? 'informative' : 'uninformative'
    ];

    const tagsObject = {
      panic: this.state.tags.includes('panic'),
      informative: this.state.tags.includes('informative')
    };

    // const canEdit = tweetOrigin !== 'professional';
    const isValid = tweetOrigin === 'professional';
    const isTagged = tweetOrigin === 'user';
    const canEdit = !(isValid || isTagged);

    return (
      <SocialVotingControlsPanel
        ref={this._setElRef}
        expanded={expanded}
        canEdit={canEdit}
        className={expanded ? 'expanded' : ''}
        style={this.state.containerHeight && expanded ? { height: this.state.containerHeight } : {}}
      >
        <div className="controls-inner" ref={this._setInnerElRef}>
          <Link className="details-inner-heading" to={collapseLink} replace={true}>
            {expanded ? <ExpandedIndicator /> : <CollapsedIndicator />}
            <span>{expanded ? dictionary('_soc_lbl_labels') : dictionary('_expand')}</span>
            {isValid && (
              <CheckMarkIcon
                style={{
                  width: 18,
                  height: 18,
                  marginRight: 16,
                  marginLeft: 'auto'
                }}
                iconStyle={{
                  fontSize: '14px',
                  lineHeight: '18px'
                }}
              />
            )}
            {isTagged && (
              <TagIcon
                style={{
                  width: 18,
                  height: 18,
                  marginRight: 16,
                  marginLeft: 'auto'
                }}
                iconStyle={{
                  fontSize: '14px',
                  lineHeight: '18px'
                }}
              />
            )}
          </Link>
          {expanded && (
            <TweetCategories canEdit={canEdit}>
              <TweetInfoLine>
                <TweetInfoLineLabel>{dictionary('_soc_lbl_named_loc')}</TweetInfoLineLabel>
                <TweetInfoValues>
                  {!canEdit &&
                    Array.isArray(entities) &&
                    entities.map((e, i) => <AttributeChip key={i} name={e.name || 'Unknown'} />)}
                  {canEdit && (
                    <EditEntities
                      entities={this.state.entities}
                      addEntity={this._addEntity}
                      deleteEntity={this._deleteEntity}
                    />
                  )}
                </TweetInfoValues>
              </TweetInfoLine>

              <TweetInfoLine>
                <TweetInfoLineLabel>{dictionary('_soc_lbl_hazards')}</TweetInfoLineLabel>
                <TweetInfoValues>
                  {!canEdit &&
                    hazards.map((h, i) => (
                      <AttributeChip dictionary={dictionary} key={i} name={h} type={'hazard'} />
                    ))}
                  {canEdit && (
                    <EditHazards
                      hazards={this.state.hazards}
                      dictionary={dictionary}
                      onChange={this._changeHazards}
                      deleteHazard={this._deleteHazard}
                    />
                  )}
                </TweetInfoValues>
              </TweetInfoLine>

              <TweetInfoLine>
                <TweetInfoLineLabel>{dictionary('_soc_lbl_tags')}</TweetInfoLineLabel>
                <TweetInfoValues>
                  {!canEdit &&
                    tags.map((t, i) => (
                      <AttributeChip dictionary={dictionary} key={i} name={t} type={'tag'} />
                    ))}
                  {canEdit && (
                    <EditTags
                      dictionary={dictionary}
                      tags={this.state.tags}
                      tagsObject={tagsObject}
                      onTagsChange={this._onTagsChange}
                      deleteTag={this._deleteTag}
                    />
                  )}
                </TweetInfoValues>
              </TweetInfoLine>

              <TweetInfoLine>
                <TweetInfoLineLabel>{dictionary('_soc_lbl_info_type')}</TweetInfoLineLabel>
                <TweetInfoValues>
                  {!canEdit &&
                    infoTypes.map((h, i) => (
                      <AttributeChip dictionary={dictionary} key={i} name={h} type={'info'} />
                    ))}
                  {canEdit && (
                    <EditInfoTypes
                      infoTypes={this.state.infoTypes}
                      dictionary={dictionary}
                      onChange={this._changeInfoTypes}
                      deleteInfoType={this._deleteInfoType}
                    />
                  )}
                </TweetInfoValues>
              </TweetInfoLine>
            </TweetCategories>
          )}
          {expanded && canEdit && (
            <TweetValidationControls>
              {
                <FlatButton
                  label={dictionary(
                    user.userType !== 'authority'
                      ? this.state.unsavedChanges === false
                        ? '_soc_lbl_confirm_information'
                        : '_soc_lbl_save'
                      : '_soc_lbl_validate'
                  )}
                  onClick={this._saveEditing}
                />
              }
            </TweetValidationControls>
          )}
        </div>
      </SocialVotingControlsPanel>
    );
  }
}

export default enhance(SocialVotingControls);
