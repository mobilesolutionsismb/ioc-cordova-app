import React, { Component } from 'react';
import { compose } from 'redux';
import Geonames from 'geonames.js';
import { withDictionary } from 'ioc-localization';
import {
  AutoComplete,
  FontIcon,
  IconButton,
  Dialog,
  FlatButton,
  CircularProgress
} from 'material-ui';
import { logError, logMain } from 'js/utils/log';
// import { withMessages } from 'js/modules/ui';
import nop from 'nop';

const enhance = compose(
  // withMessages,
  withDictionary
);

const dataSourceConfig = {
  text: 'fullName',
  value: 'geonameId'
};

class GeocodingBox extends Component {
  static defaultProps = {
    addLocation: nop
  };

  state = {
    names: [],
    searchText: '',
    newLocation: {},
    updating: false,
    openDialog: false
  };

  _autoComplete = null;

  _handleOpen = () => {
    this.setState({ openDialog: true }, () => {
      if (this._autoComplete) {
        this._autoComplete.focus();
      }
    });
  };

  _handleClose = () => {
    this.setState({ openDialog: false });
  };

  _selectLocation = newLocation => {
    logMain('SelectLocation', newLocation);
    if (newLocation) {
      // const newLocation = this.state.names[index === -1 ? 0 : index];
      this.setState(
        {
          newLocation,
          searchText: '',
          openDialog: false
        },
        () => {
          this.props.addLocation(newLocation);
        }
      );
    }
  };

  _setAutocompleteRef = c => (this._autoComplete = c);

  _geonames = null;

  _place2Name = item => ({
    name: item.name,
    type: item.fcl,
    longitude: item.lng,
    latitude: item.lat,
    country: item.countryCode,
    population: item.population,
    iri: `http://sws.geonames.org/${item.geonameId}`,
    id: item.geonameId,
    countryName: item.countryName,
    adminName1: item.adminName1,
    fullName: `${item.name} (${item.adminName1}, ${item.countryName})`
  });

  _geocode = async () => {
    try {
      const searchText = this.state.searchText;
      const places = await this._geonames.search({
        q: searchText,
        isNameRequired: true,
        maxRows: 3
      });
      const names = places.geonames.map(this._place2Name);
      this.setState({ names, updating: false });
    } catch (err) {
      this.setState({ updating: false });
      logError('Geocode Error', err);
    }
  };

  _updateInput = value => {
    // if (!this.state.updating) {
    this.setState({ updating: true, searchText: value }, this._geocode);
    // }
  };

  componentDidMount() {
    this._geonames = new Geonames({
      username: GEONAMES_API_CREDENTIALS.username,
      lan: this.props.locale,
      encoding: 'JSON'
    });
  }

  componentWillUnmount() {
    this._geonames = null;
  }

  render() {
    const { dictionary } = this.props;
    const actions = [
      <FlatButton label={dictionary('_cancel')} primary={false} onClick={this._handleClose} />
    ];

    const title = dictionary('_soc_ent_loc_search');

    return (
      <div style={{ margin: 'auto 4px' }}>
        <IconButton
          key="btnadd"
          style={{ padding: 0, width: 32, height: 32 }}
          onClick={this._handleOpen}
        >
          <FontIcon key="add_location_btn" className="material-icons">
            add_circle_outline
          </FontIcon>
        </IconButton>
        <Dialog
          title={title}
          actions={actions}
          modal={false}
          open={this.state.openDialog}
          onRequestClose={this._handleClose}
        >
          <AutoComplete
            key="autocomplete"
            fullWidth={true}
            ref={this._setAutocompleteRef}
            hintText={dictionary('_soc_ent_hint_text')}
            searchText={this.state.searchText}
            dataSource={this.state.names}
            openOnFocus={false}
            onUpdateInput={this._updateInput}
            dataSourceConfig={dataSourceConfig}
            filter={AutoComplete.noFilter}
            onNewRequest={this._selectLocation}
            popoverProps={{
              style: { width: 'calc(100vw - 16px)', left: '8px' },
              className: 'soc-geo-autocomplete-popover',
              canAutoPosition: false
            }}
          />
          {this.state.updating && (
            <CircularProgress
              size={32}
              thickness={7}
              style={{ position: 'absolute', left: 'calc(50% - 16px)' }}
            />
          )}
        </Dialog>
      </div>
    );
  }
}

export default enhance(GeocodingBox);
