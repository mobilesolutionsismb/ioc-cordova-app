import React from 'react';
import { white } from 'material-ui/styles/colors';
import GeocodingBox from './GeocodingBox';
import ClosedContentBox from './ClosedContentBox';
import TagEditBox from './TagEditBox';
import { FontIcon, Chip, Avatar } from 'material-ui';
import {
  HAZARD_SOCIAL_ICONS,
  INFO_TYPE_ICONS,
  TAGS_COLOR_MAP,
  SOCIAL_HAZARD_VALUES,
  SOCIAL_INFORMATION_TYPE_VALUES
} from './commons';
const CHIP_STYLE = { margin: '2px' };
const CHIP_LABEL_STYLE = {
  maxWidth: 80,
  fontSize: '12px',
  fontWeight: '300',
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap',
  overflow: 'hidden'
};
const CHIP_ICON_STYLE = {}; //{ lineHeight: '22px', fontSize: '20px' };
const AVATAR_STYLE = {}; // { height: '22px', width: '25px' };

const ATTRIBUTE_ICONS_CFG = {
  namedLocation: () => ({
    text: 'location_city',
    className: 'material-icons',
    isEmoji: false,
    translPrefix: ''
  }),
  hazard: name => ({
    text: HAZARD_SOCIAL_ICONS[name],
    className: 'ireact-icons',
    isEmoji: true,
    style: {
      width: 'auto',
      height: 'auto',
      margin: 'auto'
    },
    translPrefix: '_soc_cls_'
  }),
  tag: name => ({
    text: 'fiber_manual_record',
    className: 'material-icons',
    isEmoji: false,
    color: TAGS_COLOR_MAP[name],
    translPrefix: '_soc_tag_'
  }),
  info: name => ({
    text: '',
    className: `mdi mdi-${INFO_TYPE_ICONS[name]}`,
    isEmoji: false,
    translPrefix: '_soc_cls_'
  })
};

export const AttributeChip = ({
  name,
  type = 'namedLocation',
  onRequestDelete = undefined,
  dictionary = undefined
}) => {
  const iconCfg = ATTRIBUTE_ICONS_CFG[type](name);
  const iconAttrs = iconCfg.isEmoji ? { role: 'img', 'aria-label': type } : {};
  const color = iconCfg.color || white;
  return (
    <Chip
      className="attribute-chip"
      style={CHIP_STYLE}
      labelStyle={CHIP_LABEL_STYLE}
      onRequestDelete={onRequestDelete ? () => onRequestDelete(name) : undefined}
    >
      <Avatar
        className="chip-avatar"
        size={24}
        style={AVATAR_STYLE}
        color={color}
        icon={
          <FontIcon
            className={iconCfg.className}
            style={{ ...CHIP_ICON_STYLE, ...iconCfg.style }}
            {...iconAttrs}
          >
            {iconCfg.text}
          </FontIcon>
        }
      />
      {dictionary ? dictionary(`${iconCfg.translPrefix}${name}`) : name}
    </Chip>
  );
};

function renderEntities({ onRequestDelete, entities }) {
  return entities.map((entity, i) => (
    <AttributeChip
      key={i}
      name={entity.name || 'Unknown'}
      onRequestDelete={() => onRequestDelete(entity)}
    />
  ));
}

export const EditEntities = ({ entities, addEntity, deleteEntity }) => [
  ...renderEntities({ entities, onRequestDelete: deleteEntity }),
  <GeocodingBox key="geobox" addLocation={addEntity} />
];

function renderTags({ onRequestDelete, tags, dictionary }) {
  return tags.map((tag, i) => (
    <AttributeChip
      key={i}
      name={tag || 'Unknown'}
      dictionary={dictionary}
      type={'tag'}
      onRequestDelete={onRequestDelete ? () => onRequestDelete(tag) : undefined}
    />
  ));
}

export const EditTags = ({ tags, tagsObject = {}, onTagsChange, deleteTag, dictionary }) => [
  ...renderTags({ tags, dictionary /* onRequestDelete:   deleteTag */ }), // by leaving deleteTag the following is useless
  <TagEditBox
    key={'edit-tags'}
    dictionary={dictionary}
    onConfirm={onTagsChange}
    panic={tagsObject.panic}
    informative={tagsObject.informative}
    title="_soc_lbl_tags"
  />
];

function renderHazards({ onRequestDelete, hazards, dictionary }) {
  return hazards.map((hazard, i) => (
    <AttributeChip
      key={i}
      dictionary={dictionary}
      name={hazard || 'Unknown'}
      type={'hazard'}
      onRequestDelete={() => onRequestDelete(hazard)}
    />
  ));
}

export const EditHazards = ({ hazards, onChange, deleteHazard, dictionary }) => [
  ...renderHazards({ onRequestDelete: deleteHazard, hazards, dictionary }),
  <ClosedContentBox
    key="add-values"
    dictionary={dictionary}
    onChange={onChange}
    title="Add Hazard Name"
    hint="Type an hazard name"
    closedSetOfNames={SOCIAL_HAZARD_VALUES}
    iconsMap={HAZARD_SOCIAL_ICONS}
    iconsClassName="ireact-icons"
    values={hazards}
    name="hazards"
  />
];

function renderInfoType({ onRequestDelete, infoTypes, dictionary }) {
  return infoTypes.map((infoType, i) => (
    <AttributeChip
      key={i}
      dictionary={dictionary}
      type={'info'}
      name={infoType || 'Unknown'}
      onRequestDelete={() => onRequestDelete(infoType)}
    />
  ));
}

export const EditInfoTypes = ({ infoTypes, onChange, deleteInfoType, dictionary }) => [
  ...renderInfoType({ infoTypes, onRequestDelete: deleteInfoType, dictionary }),
  <ClosedContentBox
    key="add-values"
    dictionary={dictionary}
    onChange={onChange}
    title="Add Information Type"
    hint="Type an information value"
    closedSetOfNames={SOCIAL_INFORMATION_TYPE_VALUES}
    iconsMap={INFO_TYPE_ICONS}
    iconsClassName="mdi"
    values={infoTypes}
    name="infoTypes"
  />
];
