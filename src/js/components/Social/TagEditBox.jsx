import React, { Component } from 'react';
import { Toggle, Dialog, FlatButton, FontIcon, IconButton } from 'material-ui';
import { TWEET_POSITIVE_TAG_COLOR, TWEET_NEGATIVE_TAG_COLOR } from 'js/startup/iReactTheme';
import nop from 'nop';

const STYLES = {
  negative: {
    backgroundColor: TWEET_NEGATIVE_TAG_COLOR
  },
  positive: {
    backgroundColor: TWEET_POSITIVE_TAG_COLOR
  }
};

class TagEditBox extends Component {
  static defaultProps = {
    title: 'Edit tags',
    panic: false,
    informative: false,
    onConfirm: nop,
    dictionary: s => s
  };

  constructor(props) {
    super(props);
    this.state = {
      openDialog: false,
      panic: props.panic,
      informative: props.informative
    };
  }

  _handleOpen = () => {
    this.setState({ openDialog: true });
  };

  _handleClose = () => {
    this.setState({ openDialog: false });
  };

  _onConfirm = () => {
    this.setState({ openDialog: false }, () => {
      this.props.onConfirm({
        panic: this.state.panic,
        informative: this.state.informative
      });
    });
  };

  _onToggle = (evt, checked) => {
    const name = evt.target.name;
    if (name === 'panic' || name === 'informative') {
      this.setState({
        [name]: checked
      });
    }
  };

  _updateState = props => {
    if (props.panic !== this.state.panic || props.informative !== this.state.informative) {
      this.setState({
        panic: props.panic,
        informative: props.informative
      });
    }
  };

  componentDidMount() {
    this._updateState(this.props);
  }

  componentDidUpdate(prevProps) {
    if (this.props.panic !== prevProps.panic || this.props.informative !== prevProps.informative) {
      this._updateState(this.props);
    }
  }

  render() {
    const { dictionary } = this.props;

    const actions = [
      <FlatButton label={dictionary('_cancel')} primary={false} onClick={this._handleClose} />,
      <FlatButton label={dictionary('_confirm')} primary={true} onClick={this._onConfirm} />
    ];

    return (
      <div>
        <IconButton
          key="btnadd"
          style={{ padding: 0, width: 32, height: 32 }}
          onClick={this._handleOpen}
          iconStyle={{
            border: '2px solid',
            borderRadius: '100%',
            fontSize: '14px',
            padding: '2px'
          }}
        >
          <FontIcon key="add_location_btn" className="material-icons">
            edit
          </FontIcon>
        </IconButton>
        <Dialog
          title={dictionary(this.props.title)}
          actions={actions}
          modal={false}
          open={this.state.openDialog}
          onRequestClose={this._handleClose}
        >
          <div>
            <Toggle
              label={dictionary(this.state.panic ? '_soc_tag_panic' : '_soc_tag_no_panic')}
              name="panic"
              onToggle={this._onToggle}
              toggled={this.state.panic}
              thumbSwitchedStyle={STYLES.negative}
              thumbStyle={STYLES.positive}
            />
          </div>
          <div>
            <Toggle
              label={dictionary(
                this.state.informative ? '_soc_tag_informative' : '_soc_tag_uninformative'
              )}
              name="informative"
              onToggle={this._onToggle}
              toggled={this.state.informative}
              thumbSwitchedStyle={STYLES.positive}
              thumbStyle={STYLES.negative}
            />
          </div>
        </Dialog>
      </div>
    );
  }
}

export default TagEditBox;
