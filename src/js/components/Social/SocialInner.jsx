import React, { Component } from 'react';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';
import { logMain } from 'js/utils/log';
import { Swipeable } from 'react-swipeable';
import SocialDetails from './SocialDetails';
import { Effect } from './commons';
import { DetailsLoader } from 'js/components/EmergencyCommunicationsCommons';

const SWIPE_EFFECT_TIMEOUT = 300; //ms

class SocialInner extends Component {
  static defaultProps = {
    selectedFeature: null,
    match: { params: { id: -1 } }
  };

  timeout = null;

  state = {
    loading: true,
    nextFeatureIndex: -1,
    currentFeatureIndex: -1,
    prevFeatureIndex: -1,
    sweepingButCantGoLeft: false,
    sweepingButCantGoRight: false
  };

  _autoRef = null;

  _setAutoRef = e => {
    this._autoRef = e;
  };

  _findSelectedFeatureIndex(props) {
    const { match, features } = props;
    const matchId = match.params.id;
    const featureIndex = features.findIndex(f => f.properties.id === matchId);
    return {
      loading: false,
      nextFeatureIndex:
        featureIndex >= 0 && featureIndex < features.length - 1 ? featureIndex + 1 : -1,
      currentFeatureIndex: featureIndex,
      prevFeatureIndex: featureIndex > 0 && featureIndex < features.length ? featureIndex - 1 : -1
    };
  }

  _computeNextState(props) {
    this.setState({ loading: true }, () => {
      const { nextFeatureIndex, currentFeatureIndex, prevFeatureIndex } = this.state;
      const nextState = this._findSelectedFeatureIndex(props);

      if (
        nextFeatureIndex !== nextState.nextFeatureIndex ||
        currentFeatureIndex !== nextState.currentFeatureIndex ||
        prevFeatureIndex !== nextState.prevFeatureIndex
      ) {
        this.setState(nextState);
      } else {
        this.setState({ loading: false });
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      !areFeaturesEqual(prevProps.selectedFeature, this.props.selectedFeature) ||
      prevProps.match.params.id !== this.props.match.params.id ||
      (prevProps.featuresUpdating === true && this.props.featuresUpdating === false)
    ) {
      this._computeNextState(this.props);
    }
    if (
      this.state.nextFeatureIndex !== prevState.nextFeatureIndex ||
      this.state.currentFeatureIndex !== prevState.currentFeatureIndex ||
      this.state.prevFeatureIndex !== prevState.prevFeatureIndex
    ) {
      const feature =
        this.state.currentFeatureIndex > -1
          ? this.props.features[this.state.currentFeatureIndex]
          : null;
      if (feature) {
        this.props.selectFeature(feature);
      } else {
        this.props.deselectFeature();
        this.props.history.replace('/tabs/social');
      }
    }
  }

  _onSwipingLeft = () => {
    logMain('SocialInner::Swiping LEFT');
    if (this.state.nextFeatureIndex === -1 && !this.state.sweepingButCantGoLeft) {
      this.setState({ sweepingButCantGoLeft: true });
    }
  };

  _onSwipingRight = () => {
    logMain('SocialInner::Swiping RIGHT');
    if (this.state.prevFeatureIndex === -1 && !this.state.sweepingButCantGoRight) {
      this.setState({ sweepingButCantGoRight: true });
    }
  };

  _onSwipedLeft = () => {
    logMain('SocialInner::Swiped LEFT');
    const expanded = this.props.location.search.indexOf('expanded') > -1;
    if (this.state.sweepingButCantGoLeft === true) {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        if (this._autoRef) {
          this.setState({ sweepingButCantGoLeft: false });
        }
      }, SWIPE_EFFECT_TIMEOUT);
    }
    if (this.state.nextFeatureIndex > -1) {
      const nextId = this.props.features[this.state.nextFeatureIndex].properties.id;
      this.props.history.replace(`/social/${nextId}${expanded ? '?expanded' : ''}`);
    }
  };

  _onSwipedRight = () => {
    logMain('SocialInner::Swiped RIGHT');
    const expanded = this.props.location.search.indexOf('expanded') > -1;
    if (this.state.sweepingButCantGoRight === true) {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        if (this._autoRef) {
          this.setState({ sweepingButCantGoRight: false });
        }
      }, SWIPE_EFFECT_TIMEOUT);
    }
    if (this.state.prevFeatureIndex > -1) {
      const prevId = this.props.features[this.state.prevFeatureIndex].properties.id;
      this.props.history.replace(`/social/${prevId}${expanded ? '?expanded' : ''}`);
    }
  };

  _showOnMap = () => {
    this.props.history.replace('/tabs/map');
  };

  render() {
    const { features, match, location, featuresUpdating, sendTweetFeedback } = this.props; // use featuresUpdating for loader?
    const { currentFeatureIndex, loading } = this.state;
    const updating = loading || featuresUpdating;

    const selectedFeature = currentFeatureIndex > -1 ? features[currentFeatureIndex] : null;
    // const palette = muiTheme.palette;
    const featureId = selectedFeature ? selectedFeature.properties.id : -1;
    const featureProperties = selectedFeature ? selectedFeature.properties : {};
    const itemType = featureProperties.itemType ? featureProperties.itemType : null;

    const validFeature = updating === false && featureId > -1;
    const detailsExpanded = location.search.indexOf('expanded') > -1;

    return (
      <Swipeable
        ref={this._setAutoRef}
        className="social-details page"
        onSwipingLeft={this._onSwipingLeft}
        onSwipedLeft={this._onSwipedLeft}
        onSwipingRight={this._onSwipingRight}
        onSwipedRight={this._onSwipedRight}
      >
        {updating === true && <DetailsLoader />}
        {validFeature && (
          <Effect
            key="left-effect"
            className={`left-effect${this.state.sweepingButCantGoRight ? '' : ' hidden'}`}
          />
        )}
        {validFeature && (
          <Effect
            key="right-effect"
            className={`right-effect${this.state.sweepingButCantGoLeft ? '' : ' hidden'}`}
          />
        )}
        <SocialDetails
          key="social-content"
          updating={updating}
          itemType={itemType}
          featureProperties={featureProperties}
          showOnMap={validFeature ? this._showOnMap : null}
          matchId={match.params.id}
          detailsExpanded={detailsExpanded}
          sendTweetFeedback={sendTweetFeedback}
        />
      </Swipeable>
    );
  }
}

export default SocialInner;
