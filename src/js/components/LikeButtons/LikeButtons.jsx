import React, { Component } from 'react';
import { IconButton, FontIcon } from 'material-ui';
// import { lightGreen900, red900, white } from 'material-ui/styles/colors';
import { withReports } from 'ioc-api-interface';
//import { withReportFilters } from 'js/modules/reportFilters';
import { withMessages } from 'js/modules/ui';
import muiThemeable from 'material-ui/styles/muiThemeable';

const STYLES = {
  buttonsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignContents: 'center',
    alignItems: 'center',
    height: 48
  },
  likesCounter: {
    width: '100%',
    maxWidth: 120,
    height: 'auto',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  likeIcon: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  smallIcon: {
    fontSize: '14px'
  }
};

const IconSizes = {
  xs: {
    padding: 2,
    fontSize: '14px'
  },
  s: {
    padding: 3,
    fontSize: '16px'
  },
  m: {
    padding: 4,
    fontSize: '18px'
  },
  l: {
    padding: 6,
    fontSize: '24px'
  },
  xl: {
    padding: 8,
    fontSize: '32px'
  }
};

export const LikeIcon = ({ buttonType, color, backgroundColor, size }) => (
  <FontIcon
    className="material-icons"
    color={color}
    style={{ ...IconSizes[size || 'm'], backgroundColor, borderRadius: '100%' }}
  >
    {buttonType === 'up' ? 'thumb_up' : 'thumb_down'}
  </FontIcon>
);

export const LikeIconWithCount = ({ buttonType, count, color, backgroundColor, size = 'm' }) => (
  <div style={STYLES.likeIcon}>
    <LikeIcon size={size} buttonType={buttonType} color={color} backgroundColor={backgroundColor} />
    <span className="like-icon-counter" style={IconSizes[size || 'm']}>
      {typeof count === 'number' ? count : 0}
    </span>
  </div>
);

export const LikesCounter = muiThemeable()(
  ({ upvotes, downvotes, muiTheme, size, style, alreadyVoted, onClick }) => (
    <div className="likes-counter" style={{ ...STYLES.likesCounter, ...style }} onClick={onClick}>
      <LikeIconWithCount
        buttonType="up"
        size={size || 'm'}
        count={upvotes}
        color={alreadyVoted === 'up' ? muiTheme.palette.canvasColor : muiTheme.palette.textColor}
        backgroundColor={
          alreadyVoted === 'up' ? muiTheme.palette.textColor : muiTheme.palette.canvasColor
        }
      />
      <LikeIconWithCount
        size={size || 'm'}
        buttonType="down"
        count={downvotes}
        color={alreadyVoted === 'down' ? muiTheme.palette.canvasColor : muiTheme.palette.textColor}
        backgroundColor={
          alreadyVoted === 'down' ? muiTheme.palette.textColor : muiTheme.palette.canvasColor
        }
      />
    </div>
  )
);

export const LikeButton = muiThemeable()(({ buttonType, count, onClick, disabled, muiTheme }) => (
  <span>
    <IconButton disabled={typeof disabled === 'boolean' ? disabled : false} onClick={onClick}>
      <LikeIcon
        buttonType={buttonType}
        color={
          buttonType === 'up'
            ? muiTheme.palette.primary1Color
            : muiTheme.palette.validatedColor.accent1Color
        }
        // color={(buttonType === 'up' ? muiTheme.palette.validatedColor : muiTheme.palette.validatedColor.rejectedColor)}
      />
    </IconButton>
    <span>{count}</span>
  </span>
));

class LikeButtons extends Component {
  state = {
    voteDisabled: false
  };

  enableVote = () => {
    this.setState({ voteDisabled: false });
  };

  disableVote = () => {
    this.setState({ voteDisabled: true });
  };

  async onVoteButtonClick(reportId, voteType, evt) {
    evt.stopPropagation();
    evt.nativeEvent.stopImmediatePropagation();
    try {
      this.disableVote();
      if (voteType === 'up') {
        await this.props.upVoteReport(reportId);
      } else {
        await this.props.downVoteReport(reportId);
      }
      console.log('Voted', reportId, voteType);
      this.enableVote();
    } catch (err) {
      this.props.pushError(err);
      this.enableVote();
    }
  }

  render() {
    const { report, containerStyle } = this.props;
    if (report.voteEnabled) {
      return (
        <div className="controls" style={{ ...STYLES.buttonsContainer, ...containerStyle }}>
          <LikeButton
            buttonType="up"
            disabled={this.state.voteDisabled}
            count={report.upvoteCount}
            onClick={this.onVoteButtonClick.bind(this, report.id, 'up')}
          />
          <LikeButton
            buttonType="down"
            disabled={this.state.voteDisabled}
            count={report.downvoteCount}
            onClick={this.onVoteButtonClick.bind(this, report.id, 'down')}
          />
        </div>
      );
    } else {
      return <div className="controls" style={{ ...STYLES.buttonsContainer, ...containerStyle }} />;
    }
  }
}

export default withMessages(withReports(LikeButtons));
