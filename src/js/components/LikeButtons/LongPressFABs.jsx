import React, { Component } from 'react';
import { FontIcon, FloatingActionButton } from 'material-ui';
import { /* lightGreen900, red900, */ white } from 'material-ui/styles/colors';
import nop from 'nop';

const STYLES = {
  voteButtonsContainer: {
    // position: 'fixed',
    // width: '100%',
    // bottom: '25%'
    //top: 'calc(50% - 56px)'
  },
  voteButtons: {
    // position: 'relative',
    // width: '100%',
    // height: 56,
    // display: 'flex',
    // alignItems: 'center',
    // justifyContent: 'space-around'
  },
  floatingHint: {
    // position: 'relative',
    // width: 'auto',
    // margin: 'auto',
    // maxWidth: 200,
    // padding: '20px 16px',
    // borderRadius: 20,
    // top: -56,
    // backgroundColor: 'rgba(0,0,0,0.5)',
    // color: 'white',
    // height: 20,
    // display: 'flex',
    // alignItems: 'center',
    // justifyContent: 'center',
    // boxShadow: '0px 0px 1px 1px white'
  }
};

const LONG_PRESS_TIMEOUT = 1000;
const SUSTAIN_TIMEOUT = 1000;

function getButtonTypeIcon(buttonType) {
  // buttonType === 'up' ? 'thumb_up' : 'thumb_down'
  let icon = '';
  switch (buttonType) {
    case 'up':
      icon = 'thumb_up';
      break;
    case 'down':
      icon = 'thumb_down';
      break;
    case 'validate':
      icon = 'check';
      break;
    case 'reject':
      icon = 'close';
      break;
    default:
      break;
  }
  return icon;
}

export const VoteFAB = ({
  mini,
  onTouchStartCapture,
  onTouchEnd,
  onMouseDown,
  onMouseUp,
  onClick,
  onContextMenu,
  disabled,
  buttonType,
  className,
  style
}) => (
  <FloatingActionButton
    mini={mini}
    style={style}
    className={className ? className : 'long-press-fab'}
    onContextMenu={onContextMenu}
    onTouchStartCapture={onTouchStartCapture}
    onTouchEnd={onTouchEnd}
    onMouseDown={onMouseDown}
    onMouseUp={onMouseUp}
    onClick={onClick}
    disabled={disabled}
    secondary={buttonType === 'down' || buttonType === 'reject' ? true : undefined}
    iconStyle={{ color: white }}
  >
    <FontIcon className="material-icons">{getButtonTypeIcon(buttonType)}</FontIcon>
  </FloatingActionButton>
);

export class LongPressFAB extends Component {
  pressTimeout = null;
  state = {
    done: false,
    pressTimeoutExpredCallback: nop
  };

  _setDone = () => {
    this.props.pressTimeoutExpredCallback();
    this.setState({ done: true });
  };

  _reset = () => {
    if (this.pressTimeout !== null) {
      clearTimeout(this.pressTimeout);
      this.pressTimeout = null;
    }
  };

  _onTouchStart = e => {
    // e.preventDefault();
    console.log('TStart');
    this.props.onLongPressStart();
    this.pressTimeout = setTimeout(this._setDone, LONG_PRESS_TIMEOUT);
  };

  _onTouchEnd = e => {
    // e.preventDefault();
    console.log('TEND');
    if (this.state.done) {
      this.props.onLongPressEnd(true);
      this.setState({ done: false });
    } else {
      this.props.onLongPressEnd(false);
    }
    this._reset();
  };

  componentWillUnmount() {
    this._reset();
  }

  render() {
    const { buttonType, disabled, mini } = this.props;
    const isTouchDevice = typeof document.body.ontouchstart !== 'undefined'; // Poor man's solution

    return (
      <VoteFAB
        mini={mini}
        onContextMenu={e => {
          e.preventDefault();
          return false;
        }}
        onTouchStartCapture={isTouchDevice ? this._onTouchStart : undefined}
        onTouchEnd={isTouchDevice ? this._onTouchEnd : undefined}
        onMouseDown={isTouchDevice ? undefined : this._onTouchStart}
        onMouseUp={isTouchDevice ? undefined : this._onTouchEnd}
        onClick={e => e.stopPropagation()}
        disabled={disabled}
        buttonType={buttonType}
      />
    );
  }
}

export class LongPressFABs extends Component {
  lefTimeout = null;
  rightTimeout = null;

  static defaultProps = {
    disabled: false,
    type: 'likes',
    onLeftButtonPressSuccess: nop,
    onRightButtonPressSuccess: nop,
    onPressingMessage: 'Keep Pressed to vote!',
    onPressingDoneMessage: 'You can release the button now!'
  };

  state = {
    showLongPressedHint: false,
    pressTimeoutExpired: false
  };

  _getLeftButtonType = () => {
    let type;
    switch (this.props.type) {
      case 'validation':
        type = 'reject';
        break;
      case 'votes':
      case 'likes':
      default:
        type = 'down';
        break;
    }
    return type;
  };

  _getRightButtonType = () => {
    let type;
    switch (this.props.type) {
      case 'validation':
        type = 'validate';
        break;
      case 'votes':
      case 'likes':
      default:
        type = 'up';
        break;
    }
    return type;
  };

  _onPressTimeoutExpired = () => {
    this.setState({ pressTimeoutExpired: true });
  };

  _resetHint = () => {
    this.setState({ showLongPressedHint: false, pressTimeoutExpired: false });
    this._clearTimeouts();
  };

  _onLeftFABPressEnd = done => {
    this.lefTimeout = setTimeout(() => {
      this._resetHint();
      this.props.onLeftButtonPressSuccess(done);
    }, SUSTAIN_TIMEOUT);
  };

  _onRightFABPressEnd = done => {
    this.rightTimeout = setTimeout(() => {
      this._resetHint();
      if (done) {
        this.props.onRightButtonPressSuccess(done);
      }
    }, SUSTAIN_TIMEOUT);
  };

  _pressing = () => {
    this._clearTimeouts();
    this.setState({ showLongPressedHint: true });
    console.log('pressing');
  };

  _clearTimeouts = () => {
    clearTimeout(this.lefTimeout);
    clearTimeout(this.rightTimeout);
    this.lefTimeout = null;
    this.rightTimeout = null;
  };

  componentWillUnmount() {
    this._clearTimeouts();
  }

  render() {
    return (
      <div
        className="vote-buttons-container"
        style={STYLES.voteButtonsContainer}
        onClick={e => e.stopPropagation()}
      >
        <LongPressFAB
          mini={this.props.mini}
          disabled={this.props.disabled}
          buttonType={this._getLeftButtonType()}
          pressTimeoutExpredCallback={this._onPressTimeoutExpired}
          onLongPressStart={this._pressing}
          onLongPressEnd={this._onLeftFABPressEnd}
        />
        <div className="press-to-vote-hint">
          <span>
            {this.state.showLongPressedHint
              ? this.state.pressTimeoutExpired
                ? this.props.onPressingDoneMessage
                : this.props.onPressingMessage
              : this.props.idleMessage}
          </span>
        </div>
        <LongPressFAB
          mini={this.props.mini}
          disabled={this.props.disabled}
          buttonType={this._getRightButtonType()}
          pressTimeoutExpredCallback={this._onPressTimeoutExpired}
          onLongPressStart={this._pressing}
          onLongPressEnd={this._onRightFABPressEnd}
        />
      </div>
    );
  }
}

export default LongPressFABs;
