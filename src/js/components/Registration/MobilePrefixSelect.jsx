import React, { Component } from 'react';
import SelectField from './SSField';
import { getCountries, CountryItem, Flag, supportedLanguages2countryCodeMap } from './commons';
import { withDictionary } from 'ioc-localization';
import nop from 'nop';
import Geonames from 'geonames.js';
import { logSevereWarning, logWarning } from 'js/utils/log';

class MobilePrefixSelectComponent extends Component {
  static defaultProps = {
    name: 'prefix',
    onChange: nop,
    onFocus: nop,
    dictionary: n => n
  };

  state = {
    startedEditing: false,
    dataLoadError: null,
    loadingData: true,
    callingCodes: [],
    selectedCallingCode: null // country code object
  };

  _geonames = null;

  componentWillUnmount() {
    this._geonames = null;
  }

  componentDidMount() {
    this._geonames = new Geonames({
      username: GEONAMES_API_CREDENTIALS.username,
      lan: this.props.locale,
      encoding: 'JSON'
    });
    this._loadCountries();
  }

  // trigger countries loading
  _loadCountries = () => {
    this.setState({ loadingData: true }, this._loadCountriesFn);
  };

  // Loads countries and update state
  _loadCountriesFn = async () => {
    let dataLoadError = null;
    let callingCodes = this.state.callingCodes;

    try {
      const countriesResponse = await getCountries();
      let countries = countriesResponse.data;
      callingCodes = this._getAllCallingCodes(countries);
    } catch (err) {
      dataLoadError = err;
      logSevereWarning(err);
    }
    this.setState({ dataLoadError, loadingData: false, callingCodes }, this._guessSelected);
  };

  _guessSelected = () => {
    let selectedCallingCode = this.state.selectedCallingCode;
    let initialCallingCode = this.props.value;
    try {
      if (initialCallingCode) {
        selectedCallingCode =
          this.state.callingCodes.find(c => c.callingCode === initialCallingCode) || null;
      } else if (selectedCallingCode === null) {
        const code = supportedLanguages2countryCodeMap[this.props.locale] || null;
        if (code) {
          // try guessing
          const langRX = new RegExp(code, 'gi');
          const thisCountry = this.state.callingCodes.find(c => langRX.test(c.alpha2Code)) || null;
          selectedCallingCode = thisCountry
            ? {
                callingCode: thisCountry.callingCode,
                flag: thisCountry.flag,
                alpha2Code: thisCountry.alpha2Code,
                countryName: thisCountry.name,
                countryNativeName: thisCountry.nativeName
              }
            : null;
        }
      }
      this.setState({ selectedCallingCode }, this._onValueChange);
    } catch (err) {
      logWarning('Cannot guess location from device language', err);
    }
  };

  _onValueChange = () => {
    this.props.onChange({
      [this.props.name]: this.state.selectedCallingCode
        ? this.state.selectedCallingCode.callingCode
        : '',
      fullPrefix: this.state.selectedCallingCode
    });
  };

  _onChange = data => {
    this.setState(prevState => {
      const callingCode = data
        ? prevState.callingCodes.find(c => c.callingCode === data.value) || null
        : null;
      return { selectedCallingCode: callingCode };
    }, this._onValueChange);
  };

  // Flatten calling codes
  _getAllCallingCodes = countries =>
    countries.reduce((result, c) => {
      const codes = c.callingCodes.map(cc => ({
        callingCode: cc,
        flag: c.flag,
        alpha2Code: c.alpha2Code,
        countryName: c.name,
        countryNativeName: c.nativeName
      }));
      result = [...result, ...codes];
      return result;
    }, []);

  _renderCodesList = country => {
    const { callingCode, flag, countryName } = country;
    return (
      <CountryItem className="list" key={callingCode} value={callingCode} label={countryName}>
        <Flag flag={flag} />
        <span>
          + {callingCode} ({countryName})
        </span>
      </CountryItem>
    );
  };

  _renderSelection = selection =>
    selection && selection.value ? (
      <CountryItem className="selection">
        {/* <Flag flag={selection.value.flag} mini={true} /> */}
        <span style={{ margin: 'auto 0px 0px 0px' }}>+ {selection.value.callingCode}</span>
      </CountryItem>
    ) : null;

  _selectRef = null;

  _setSelectRef = elem => (this._selectRef = elem ? elem.root : elem);

  render() {
    const { name, /*value, onChange,  */ /* countries, */ dictionary } = this.props;
    const { callingCodes, selectedCallingCode } = this.state;

    return (
      <SelectField
        ref={this._setSelectRef}
        name={name}
        elementHeight={72}
        className="prefix-select"
        multiple={false}
        floatingLabelStyle={{ lineHeight: '0px' }}
        floatingLabel={
          selectedCallingCode ? (
            dictionary('_reg_form_mobile_pref')
          ) : (
            <CountryItem>{dictionary('_reg_form_mobile_pref')}</CountryItem>
          )
        }
        value={
          selectedCallingCode
            ? { value: selectedCallingCode, label: selectedCallingCode.name }
            : null
        }
        onChange={this._onChange}
        selectionsRenderer={this._renderSelection}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        canAutoPosition={false}
        useLayerForClickAway={false}
        menuStyle={{ width: '100%' }}
        selectedMenuItemStyle={{ width: '100%' }}
        style={{ width: 80 /* , marginBottom: 8 */ }}
        innerDivStyle={{
          width: '100%',
          height: 42,
          lineHeight: '42px'
        }}
        popoverClassName="country-superselect-popover"
        popoverWidth={300}
      >
        {callingCodes.map(this._renderCodesList)}
      </SelectField>
    );
  }
}

export const MobilePrefixSelect = withDictionary(MobilePrefixSelectComponent);
