import React, { Component } from 'react';
import SelectField from './SSField';
// import SelectField from 'material-ui-superselectfield/es';
import { CountryItem } from './commons';
import nop from 'nop';
import { iReactTheme } from 'js/startup/iReactTheme';
import { lighten } from 'material-ui/utils/colorManipulator';

export class GeonameSelect extends Component {
  static defaultProps = {
    name: 'geoname',
    hintText: 'Select a place',
    onChange: nop,
    onFocus: nop,
    value: null,
    geonames: [],
    disabled: false,
    dictionary: n => n
  };

  _renderGeonames = geoname => {
    const { geonameId, name } = geoname;
    return (
      <CountryItem key={geonameId} value={geonameId} label={name} className="menu">
        <span>{name}</span>
      </CountryItem>
    );
  };

  _renderSelection = (selection, hintText) => (
    <CountryItem>
      <span>{selection && selection.value ? selection.value.name : hintText}</span>
    </CountryItem>
  );

  _onMenuOpen = () => {
    if (this._selectRef) {
      this.props.onFocus({ target: this._selectRef });
    }
  };

  _selectRef = null;

  _setSelectRef = elem => (this._selectRef = elem ? elem.root : elem);

  render() {
    const { name, value, onChange, geonames, hintText, disabled, dictionary } = this.props;

    return (
      <SelectField
        ref={this._setSelectRef}
        disabled={disabled}
        name={name}
        elementHeight={72}
        className="geonames-select"
        multiple={false}
        floatingLabelStyle={{ lineHeight: '0px' }}
        floatingLabel={
          value ? dictionary(hintText) : <CountryItem>{dictionary(hintText)}</CountryItem>
        }
        value={value ? { value: value, label: value.name } : null}
        onChange={onChange}
        selectionsRenderer={this._renderSelection}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        canAutoPosition={false}
        useLayerForClickAway={false}
        menuStyle={{ width: '100%' }}
        selectedMenuItemStyle={{ width: '100%' }}
        style={{ width: '100%', height: 72 }}
        innerDivStyle={{
          width: 'calc(100% - 72px)',
          height: 42,
          lineHeight: '42px'
        }}
        // onMenuOpen={this._onMenuOpen}
        // onFocus={this._onMenuOpen}
        popoverStyle={{
          position: 'fixed',
          boxSizing: 'border-box',
          border: `1px solid ${iReactTheme.palette.primary1Color}`,
          backgroundColor: lighten(iReactTheme.palette.canvasColor, 0.1)
        }}
        popoverClassName="geonames-superselect-popover"
        popoverWidth={window.innerWidth}
      >
        {geonames.map(this._renderGeonames)}
      </SelectField>
    );
  }
}
