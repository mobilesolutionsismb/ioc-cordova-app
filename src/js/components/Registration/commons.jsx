import React from 'react';
import styled from 'styled-components';
import axios from 'axios';
import { Content } from 'js/components/app/header2';
import { CircularProgress } from 'material-ui';
import { ErrorContainer } from 'js/components/app/commons';
import { LinkedButton } from 'js/components/app/LinkedButton';
import { IconButton, FontIcon } from 'material-ui';
import { lighten } from 'material-ui/utils/colorManipulator';
import { withDictionary } from 'ioc-localization';

const COUNTRY_SERVICE = 'https://restcountries.eu/rest/v2/all';

export const supportedLanguages2countryCodeMap = {
  da: 'dk',
  de: 'de',
  en: 'gb',
  es: 'es',
  fi: 'fi',
  fr: 'fr',
  it: 'it'
};

export function getCountries() {
  return axios.get(COUNTRY_SERVICE, {
    params: {
      fields: ['name', 'nativeName', 'flag', 'alpha2Code', 'callingCodes'].join(';')
    }
  });
}

// Codes http://www.geonames.org/export/codes.html
const REGION_ADM_CODE = 'ADM1';

function sortGeonamesAZ(g1, g2) {
  if (g1.name < g2.name) return -1;
  if (g1.name > g2.name) return 1;
  return 0;
}

/**
 *
 * @param {Geonames} geonames
 * @param {string} countryCode 2 letter country ISO code
 */
export async function getRegionsForCountryCode(geonames, countryCode) {
  const result = await geonames.search({
    country: countryCode,
    featureCode: REGION_ADM_CODE
  });
  return result.geonames.sort(sortGeonamesAZ);
}

/**
 *
 * @param {Geonames} geonames
 * @param {Number} regionGeonameId - geonameId of the region
 */
export async function getProvincesForRegion(geonames, regionGeonameId) {
  const result = await geonames.children({ geonameId: regionGeonameId });
  return result.geonames.sort(sortGeonamesAZ);
}

/**
 *
 * @param {Geonames} geonames
 * @param {Number} provinceGeonameId - geonameId of the province
 */
export async function getCitiesForProvince(geonames, provinceGeonameId) {
  const result = await geonames.children({ geonameId: provinceGeonameId });
  return result.geonames.sort(sortGeonamesAZ);
}

export const CountryItem = styled.span.attrs(props => ({ className: 'country-menu-item' }))`
  color: inherit;
  background-color: inherit;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  height: 36px;
  max-width: ${256 + 48 - 24}px;

  &.menu {
    line-height: 36px;
    font-size: 28px;
    font-weight: 400;
  }

  &.list {
    font-size: 0.75em;

    span:nth-child(2) {
      max-width: calc(100% - 32px);
      line-height: 1em;
    }
  }

  &.selection span:nth-child(2) {
    white-space: nowrap;
    max-width: calc(100% - 32px);
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

export const Flag = styled.span`
  display: inline-block;
  width: ${props => (props.mini ? 24 : 36)}px;
  min-width: ${props => (props.mini ? 24 : 36)}px;
  min-height: ${props => (props.mini ? 16 : 24)}px;
  height: ${props => (props.mini ? 16 : 24)}px;
  margin-right: 8px;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-image: url(${props => props.flag});
`;

export const GeographySelectsWrapper = styled.div`
  padding: 0;
  width: ${256 + 48}px;
  /* height: 400px; */
  display: flex;
  flex-direction: column;
  position: absolute;
  top: 0px;
  /* margin-top: 32px; */
`;

export const CircularLoaderWrapper = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 1000;
  background-color: transparent;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  pointer-events: none;
`;

export const CircularLoader = () => (
  <CircularLoaderWrapper>
    <CircularProgress size={64} thickness={4} />
  </CircularLoaderWrapper>
);

export const ShowError = ({ error }) =>
  error ? (
    <ErrorContainer style={{ position: 'absolute', zIndex: 99 }} justifyContent="flex-start">
      {error.message ? error.message : 'An Error occurred'}
    </ErrorContainer>
  ) : null;

export const CloseButton = withDictionary(({ dictionary, disabled, ...rest }) => (
  <IconButton
    disabled={disabled}
    className="menu-button"
    tooltip={dictionary._close}
    containerElement={<LinkedButton disabled={disabled} to="/" replace={true} />}
  >
    <FontIcon className="material-icons">close</FontIcon>
  </IconButton>
));

export const PinDigitsActions = styled.div.attrs(props => ({ className: 'pin-digits-actions' }))`
  width: calc(100% - 32px);
  padding: 16px;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  &.top {
    flex-grow: 2;
    background-color: ${props => lighten(props.theme.palette.canvasColor, 0.1)};
    margin-top: 0px;
    margin-bottom: auto;
  }
`;

export const PinDigitsContainer = styled.div.attrs(props => ({
  className: 'pin-digits-container'
}))`
  width: calc(100% - 32px);
  padding: 0 16px;
  margin: 0;
  height: 72px;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  flex-grow: 2;
  background-color: ${props => lighten(props.theme.palette.canvasColor, 0.1)};
`;

export const ValidationPageContent = styled(Content)`
  overflow-y: auto;
  overflow-x: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  position: relative;

  div.validation-content-element {
    box-sizing: border-box;
    padding: 16px;
    width: 100%;

    &.sms-resend {
      margin-bottom: 64px;
      flex-grow: 1;
      display: flex;
      flex-direction: column;
      align-items: center;
    }
  }
  div.validation-progress-element {
    box-sizing: border-box;
    padding: 16px;
    width: 100%;
    height: 38px;
    text-align: center;
    vertical-align: baseline;

    span.text {
      font-size: 14px;
      width: 100%;
      line-height: 16px;
      height: 32px;
      overflow: hidden;
      line-clamp: 2;
    }
  }
`;
