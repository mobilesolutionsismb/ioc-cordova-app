import React, { Component } from 'react';
import nop from 'nop';
import { logWarning, logSevereWarning } from 'js/utils/log';
import Geonames from 'geonames.js';
import {
  getCountries,
  getRegionsForCountryCode,
  getProvincesForRegion,
  getCitiesForProvince,
  GeographySelectsWrapper,
  CircularLoader,
  ShowError
} from './commons';
import { withDictionary } from 'ioc-localization';
import { CountrySelectSimple } from './CountrySelectSimple';
import { GeonameSelect } from './GeonameSelect';
import { PhoneSelect } from './PhoneSelect';
import isEqual from 'react-fast-compare';

const supportedLanguages2countryCodeMap = {
  da: 'dk',
  de: 'de',
  en: 'gb',
  es: 'es',
  fi: 'fi',
  fr: 'fr',
  it: 'it'
};

/*
Interfaces

interface ICountry = {
  name: string
  flag: url,
  alpha2Code: char[2]
}

interface ICallingCode = {
  countryName: string,
  flag: url,
  callingCode: string,
}

interface IPhoneNumber: {
  prefix: ICallingCode,
  phoneNumber: string
}

*/

class CountryRegionCitySelectionComponent extends Component {
  static defaultProps = {
    name: 'geography',
    onChange: nop,
    value: null,
    locale: navigator.language.slice(0, 2),
    dictionary: n => n
  };

  _geonames = null;

  state = {
    countries: [], // ICountry[]
    callingCodes: [], // ICallingCode[]
    regions: [],
    provinces: [],
    cities: [],
    loadingData: false, // loading
    dataLoadError: null,
    selectedCountry: null,
    selectedRegion: null,
    selectedProvince: null,
    selectedCity: null,
    // selectedCallingCode: null,
    selectedPhoneNumber: null // IPhoneNumber
  };

  _setStateFromValue = value => {
    const { phone, country, region, province, city } = value;
    this.setState({
      selectedPhoneNumber: phone || null,
      selectedCountry: country || null,
      selectedRegion: region || null,
      selectedProvince: province || null,
      selectedCity: city || null
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (!isEqual(prevProps.value, this.props.value)) {
      this._setStateFromValue(this.props.value);
    }

    if (!isEqual(prevState.selectedCountry, this.state.selectedCountry)) {
      if (this.state.selectedCountry !== null) {
        //Load regions for country
        this._loadRegions(this.state.selectedCountry.alpha2Code);
      } else {
        //clear regions
        this._clearRegions();
      }
    }

    if (!isEqual(prevState.selectedRegion, this.state.selectedRegion)) {
      if (this.state.selectedRegion !== null) {
        //Load regions for country
        this._loadProvinces(this.state.selectedRegion.geonameId);
      } else {
        //clear provinces
        this._clearProvinces();
      }
    }

    if (!isEqual(prevState.selectedProvince, this.state.selectedProvince)) {
      if (this.state.selectedProvince !== null) {
        //Load regions for country
        this._loadCities(this.state.selectedProvince.geonameId);
      } else {
        //clear cities
        this._clearCities();
      }
    }
  }

  _clearRegions = () => {
    this.setState(
      {
        regions: [],
        provinces: [],
        cities: [],
        selectedRegion: null,
        selectedProvince: null,
        selectedCity: null
      },
      this._onValueChange
    );
  };

  _clearProvinces = () => {
    this.setState(
      {
        provinces: [],
        cities: [],
        selectedProvince: null,
        selectedCity: null
      },
      this._onValueChange
    );
  };

  _clearCities = () => {
    this.setState(
      {
        cities: [],
        selectedCity: null
      },
      this._onValueChange
    );
  };

  // Report relevant values in the form {country, callingCode, region, province, city}
  _onValueChange = () => {
    const {
      selectedCountry,
      selectedPhoneNumber,
      selectedRegion,
      selectedProvince,
      selectedCity
    } = this.state;
    const values = {
      phone: selectedPhoneNumber,
      country: selectedCountry,
      region: selectedRegion,
      province: selectedProvince,
      city: selectedCity
    };
    this.props.onChange({ [this.props.name]: values });
  };

  // Handle phone selection change
  _onPhoneSelectionChange = data => {
    this.setState(prevState => {
      let selectedPhoneNumber;
      if (!data) {
        selectedPhoneNumber = null;
      }
      const callingCode = data.prefix ? data.prefix.callingCode : null;
      const phoneNumber = data.phoneNumber || '';
      const code = callingCode
        ? prevState.callingCodes.find(c => c.callingCode === callingCode) || null
        : null;
      selectedPhoneNumber = { prefix: code, phoneNumber };
      return { selectedPhoneNumber };
    }, this._onValueChange);
  };

  // Handle country selection change
  _onCountrySelectionChange = data => {
    this.setState(prevState => {
      const country = data
        ? prevState.countries.find(c => c.alpha2Code === data.value) || null
        : null;
      return { selectedCountry: country };
    }, this._onValueChange);
  };

  // Handle region selection change
  _onRegionSelectionChange = data => {
    this.setState(prevState => {
      const selectedRegion = data
        ? prevState.regions.find(r => r.geonameId === data.value) || null
        : null;
      return { selectedRegion };
    }, this._onValueChange);
  };

  // Handle province selection change
  _onProvinceSelectionChange = data => {
    this.setState(prevState => {
      const selectedProvince = data
        ? prevState.provinces.find(p => p.geonameId === data.value) || null
        : null;
      return { selectedProvince };
    }, this._onValueChange);
  };

  // Handle city selection change
  _onCitySelectionChange = data => {
    this.setState(prevState => {
      const selectedCity = data
        ? prevState.cities.find(c => c.geonameId === data.value) || null
        : null;
      return { selectedCity };
    }, this._onValueChange);
  };

  // Guess selected country based on
  _guessSelected = data => {
    let selectedCountry = this.state.selectedCountry;
    if (selectedCountry === null) {
      try {
        const code = supportedLanguages2countryCodeMap[this.props.locale] || null;
        if (code) {
          // try guessing
          const langRX = new RegExp(code, 'gi');
          selectedCountry = this.state.countries.find(c => langRX.test(c.alpha2Code)) || null;
        }
        const prefix = selectedCountry
          ? {
              callingCode: selectedCountry.callingCodes[0],
              flag: selectedCountry.flag,
              countryName: selectedCountry.name,
              countryNativeName: selectedCountry.nativeName
            }
          : null;
        const selectedPhoneNumber = {
          prefix,
          phoneNumber: ''
        };
        this.setState({ selectedCountry, selectedPhoneNumber }, this._onValueChange);
      } catch (err) {
        logWarning('Cannot guess location from device language', err);
      }
    }
  };

  // Flatten calling codes
  _getAllCallingCodes = countries =>
    countries.reduce((result, c) => {
      const codes = c.callingCodes.map(cc => ({
        callingCode: cc,
        flag: c.flag,
        countryName: c.name
      }));
      result = [...result, ...codes];
      return result;
    }, []);

  // Loads countries and update state
  _loadCountriesFn = async () => {
    let dataLoadError = null;
    let countries = this.state.countries;
    let callingCodes = this.state.callingCodes;
    try {
      const countriesResponse = await getCountries();
      countries = countriesResponse.data;
      callingCodes = this._getAllCallingCodes(countries);
    } catch (err) {
      dataLoadError = err;
      logSevereWarning(err);
    }
    this.setState(
      { dataLoadError, loadingData: false, countries, callingCodes },
      this._guessSelected
    );
  };

  // trigger countries loading
  _loadCountries = () => {
    this.setState({ loadingData: true }, this._loadCountriesFn);
  };

  _getRegionsForCountryCode = async countryCode => {
    let regions = [];
    let dataLoadError = null;
    if (this._geonames) {
      try {
        regions = await getRegionsForCountryCode(this._geonames, countryCode);
      } catch (err) {
        dataLoadError = err;
        logSevereWarning(err);
      }
    }
    this.setState(
      {
        dataLoadError,
        loadingData: false,
        regions,
        provinces: [],
        cities: [],
        selectedRegion: null,
        selectedProvince: null,
        selectedCity: null
      },
      this._onValueChange
    );
  };

  _loadRegions = countryCode => {
    this.setState(
      {
        loadingData: true
      },
      () => {
        this._getRegionsForCountryCode(countryCode);
      }
    );
  };

  _getProvincesForRegion = async regionGeonameId => {
    let provinces = [];
    let dataLoadError = null;
    if (this._geonames && typeof regionGeonameId === 'number') {
      try {
        provinces = await getProvincesForRegion(this._geonames, regionGeonameId);
      } catch (err) {
        dataLoadError = err;
        logSevereWarning(err);
      }
    }
    this.setState(
      {
        dataLoadError,
        loadingData: false,
        provinces,
        cities: [],
        selectedProvince: null,
        selectedCity: null
      },
      this._onValueChange
    );
  };

  _loadProvinces = regionGeonameId => {
    this.setState(
      {
        loadingData: true
      },
      () => {
        this._getProvincesForRegion(regionGeonameId);
      }
    );
  };

  _getCitiesForProvince = async provinceGeonameId => {
    let cities = [];
    let dataLoadError = null;
    let selectedCity = null;
    if (this._geonames && typeof provinceGeonameId === 'number') {
      try {
        cities = await getCitiesForProvince(this._geonames, provinceGeonameId);
        selectedCity = cities.length === 0 ? this.state.selectedProvince : null;
      } catch (err) {
        logSevereWarning(err);
        dataLoadError = err;
      }
    }
    this.setState({ dataLoadError, loadingData: false, cities, selectedCity }, this._onValueChange);
  };

  _loadCities = provinceGeonameId => {
    this.setState(
      {
        loadingData: true
      },
      () => {
        this._getCitiesForProvince(provinceGeonameId);
      }
    );
  };

  componentDidMount() {
    this._geonames = new Geonames({
      username: GEONAMES_API_CREDENTIALS.username,
      lan: this.props.locale,
      encoding: 'JSON'
    });
    this._loadCountries();
  }

  componentWillUnmount() {
    this._geonames = null;
  }

  render() {
    const {
      loadingData,
      dataLoadError,
      countries,
      regions,
      provinces,
      cities,
      callingCodes,
      selectedCountry,
      selectedRegion,
      selectedProvince,
      selectedCity,
      selectedPhoneNumber
    } = this.state;

    const { onFocus, dictionary } = this.props;

    return (
      <GeographySelectsWrapper>
        {loadingData && <CircularLoader />}
        {dataLoadError && <ShowError error={dataLoadError} />}
        <PhoneSelect
          name="phone"
          onChange={this._onPhoneSelectionChange}
          value={selectedPhoneNumber}
          codes={callingCodes}
          onFocus={onFocus}
          dictionary={dictionary}
        />
        <CountrySelectSimple
          name="country"
          onChange={this._onCountrySelectionChange}
          value={selectedCountry}
          countries={countries}
          onFocus={onFocus}
          dictionary={dictionary}
        />
        <GeonameSelect
          disabled={selectedCountry === null}
          name="region"
          geonames={regions}
          value={selectedRegion}
          onChange={this._onRegionSelectionChange}
          hintText="_reg_form_region"
          onFocus={onFocus}
          dictionary={dictionary}
        />
        <GeonameSelect
          disabled={selectedRegion === null}
          name="province"
          geonames={provinces}
          value={selectedProvince}
          onChange={this._onProvinceSelectionChange}
          hintText="_reg_form_province"
          onFocus={onFocus}
          dictionary={dictionary}
        />
        <GeonameSelect
          disabled={selectedProvince === null || cities.length === 0}
          name="city"
          geonames={cities}
          value={selectedCity}
          onChange={this._onCitySelectionChange}
          hintText="_reg_form_city"
          onFocus={onFocus}
          dictionary={dictionary}
        />
      </GeographySelectsWrapper>
    );
  }
}

export const CountryRegionCitySelection = withDictionary(CountryRegionCitySelectionComponent);

export default CountryRegionCitySelection;
