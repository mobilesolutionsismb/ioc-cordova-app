import React, { Component } from 'react';
// import SelectField from 'material-ui-superselectfield/es';
import SelectField from './SSField';
import { CountryItem, Flag } from './commons';
import nop from 'nop';

export class CountrySelectSimple extends Component {
  static defaultProps = {
    name: 'country',
    onChange: nop,
    onFocus: nop,
    value: null,
    countries: [],
    dictionary: n => n
  };

  _renderCountriesList = country => {
    const { alpha2Code, name, flag, nativeName } = country;
    return (
      <CountryItem
        className="list"
        key={alpha2Code}
        value={alpha2Code}
        label={[nativeName, name].join(' ')}
      >
        <Flag flag={flag} />
        <span>{name === nativeName ? nativeName : `${nativeName} (${name})`}</span>
      </CountryItem>
    );
  };

  _renderSelection = selection =>
    selection && selection.value ? (
      <CountryItem className="selection">
        <Flag flag={selection.value.flag} mini={true} />
        <span>
          {selection.value.name === selection.value.nativeName
            ? selection.value.nativeName
            : `${selection.value.nativeName} (${selection.value.name})`}
        </span>
      </CountryItem>
    ) : null;

  _onMenuOpen = () => {
    if (this._selectRef) {
      this.props.onFocus({ target: this._selectRef });
    }
  };

  _selectRef = null;

  _setSelectRef = elem => (this._selectRef = elem ? elem.root : elem);

  render() {
    const { name, value, onChange, countries, dictionary } = this.props;

    return (
      <SelectField
        ref={this._setSelectRef}
        name={name}
        elementHeight={72}
        className="country-select"
        multiple={false}
        floatingLabelStyle={{ lineHeight: '0px' }}
        floatingLabel={
          value ? (
            dictionary('_reg_form_country')
          ) : (
            <CountryItem>{dictionary('_reg_form_country')}</CountryItem>
          )
        }
        value={value ? { value: value, label: value.name } : null}
        onChange={onChange}
        selectionsRenderer={this._renderSelection}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        canAutoPosition={false}
        useLayerForClickAway={false}
        menuStyle={{ width: '100%' }}
        selectedMenuItemStyle={{ width: '100%' }}
        style={{ width: '100%', height: 72 }}
        innerDivStyle={{
          width: 'calc(100% - 72px)',
          height: 42,
          lineHeight: '42px'
        }}
        // onMenuOpen={this._onMenuOpen}
        // popoverStyle={{ position: 'fixed', top: 0, left: 0 }}
        popoverClassName="country-superselect-popover"
        popoverWidth={window.innerWidth}
      >
        {countries.map(this._renderCountriesList)}
      </SelectField>
    );
  }
}
