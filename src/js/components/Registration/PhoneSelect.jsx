import React, { Component } from 'react';
// import SelectField from 'material-ui-superselectfield/es';
import SelectField from './SSField';
import { CountryItem, Flag } from './commons';
import nop from 'nop';
import { InputField } from 'js/components/app/FormComponent';
import styled from 'styled-components';
import { mobilePhoneRule } from 'js/utils/fieldValidation';
import { trim as trimBoth } from 'validator';

const PhoneSelectWrapper = styled.div.attrs(props => ({ className: 'phone-select' }))`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: space-between;
  margin-bottom: 16px;
`;

export class PhoneSelect extends Component {
  static defaultProps = {
    name: 'phone',
    onChange: nop,
    value: null,
    codes: [],
    dictionary: n => n
  };

  state = {
    startedEditing: false
  };

  _renderCodesList = country => {
    const { callingCode, flag, countryName } = country;
    return (
      <CountryItem className="list" key={callingCode} value={callingCode} label={countryName}>
        <Flag flag={flag} />
        <span>
          + {callingCode} ({countryName})
        </span>
      </CountryItem>
    );
  };

  _renderSelection = selection =>
    selection && selection.value ? (
      <CountryItem className="selection">
        {/* <Flag flag={selection.value.flag} mini={true} /> */}
        <span>+ {selection.value.callingCode}</span>
      </CountryItem>
    ) : null;

  _onCallingCodeChange = data => {
    this.props.onChange({
      prefix: { callingCode: data ? data.value : null },
      phoneNumber: this.props.value ? this.props.value.phoneNumber : ''
    });
  };

  _onPhoneNumberChange = (event, phoneNumber) => {
    if (!this.state.startedEditing) {
      this.setState({ startedEditing: true });
    }
    this.props.onChange({
      prefix: this.props.value ? this.props.value.prefix : null,
      phoneNumber
    });
  };

  _clearNumber = () => {
    this.setState({ startedEditing: false }, () => {
      this.props.onChange({
        prefix: this.props.value ? this.props.value.prefix : null,
        phoneNumber: ''
      });
    });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.value !== null && this.props.value === null) {
      this._clearNumber();
    }
  }

  render() {
    const { name, value /* , onChange */, codes, onFocus, dictionary } = this.props;
    const callingCodeValue = value ? value.prefix : null;
    const phoneNumberValue = value ? value.phoneNumber : '';
    const phoneErrorValidation = this.state.startedEditing ? mobilePhoneRule(phoneNumberValue) : '';
    const label = dictionary('_reg_form_mobile');
    const prefixLabel = dictionary('_reg_form_mobile_pref');
    const phoneErrorText = phoneErrorValidation ? dictionary(phoneErrorValidation.text, label) : '';

    return (
      <PhoneSelectWrapper onFocus={onFocus}>
        <SelectField
          name={`${name}-prefix`}
          elementHeight={72}
          className="calling-code-select"
          multiple={false}
          floatingLabelStyle={{ lineHeight: '0px' }}
          floatingLabel={callingCodeValue ? prefixLabel : <CountryItem>+ ??</CountryItem>}
          value={
            callingCodeValue
              ? { value: callingCodeValue, label: callingCodeValue.callingCode }
              : null
          }
          onChange={this._onCallingCodeChange}
          selectionsRenderer={this._renderSelection}
          anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
          canAutoPosition={true}
          useLayerForClickAway={false}
          menuStyle={{ width: '100%' }}
          selectedMenuItemStyle={{ width: '100%' }}
          style={{ width: 80, marginBottom: 8 }}
          innerDivStyle={{
            width: '100%',
            height: 42,
            lineHeight: '42px'
          }}
          errorText={
            this.state.startedEditing && !callingCodeValue
              ? dictionary('_form_field_not_empty', prefixLabel)
              : ''
          }
          popoverClassName="country-superselect-popover"
          popoverWidth={300}
        >
          {codes.map(this._renderCodesList)}
        </SelectField>
        <InputField
          style={{ width: 'calc(100% - 88px)' }}
          name={`${name}-phone`}
          type="tel"
          autoComplete="puff"
          floatingLabelText={label}
          value={trimBoth(phoneNumberValue)}
          errorText={phoneErrorText}
          onChange={this._onPhoneNumberChange}
          clearAction={this._clearNumber}
        />
      </PhoneSelectWrapper>
    );
  }
}
