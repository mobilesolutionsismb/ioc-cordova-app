import React, { Component } from 'react';
import { withDictionary } from 'ioc-localization';
import SelectField from './SSField';
import { getCountries, CountryItem, Flag, supportedLanguages2countryCodeMap } from './commons';
import nop from 'nop';
import Geonames from 'geonames.js';
import { logSevereWarning, logWarning } from 'js/utils/log';
// import isEqual from 'react-fast-compare';

class CountrySelectComponent extends Component {
  static defaultProps = {
    name: 'country',
    onChange: nop,
    onFocus: nop,
    dictionary: n => n
  };

  state = {
    dataLoadError: null,
    loadingData: true,
    countries: [],
    selectedCountry: null // country object
  };

  _geonames = null;

  componentWillUnmount() {
    this._geonames = null;
  }

  componentDidMount() {
    this._geonames = new Geonames({
      username: GEONAMES_API_CREDENTIALS.username,
      lan: this.props.locale,
      encoding: 'JSON'
    });
    this._loadCountries();
  }

  // trigger countries loading
  _loadCountries = () => {
    this.setState({ loadingData: true }, this._loadCountriesFn);
  };

  // Loads countries and update state
  _loadCountriesFn = async () => {
    let dataLoadError = null;
    let countries = this.state.countries;
    try {
      const countriesResponse = await getCountries();
      countries = countriesResponse.data;
    } catch (err) {
      dataLoadError = err;
      logSevereWarning(err);
    }
    this.setState({ dataLoadError, loadingData: false, countries }, this._guessSelected);
  };

  _guessSelected = () => {
    let selectedCountry = this.state.selectedCountry;
    let initialCountryName = this.props.value;
    // let initialFullCountry = this.props.initialInputValue.fullCountry || null;
    try {
      if (initialCountryName) {
        selectedCountry = this.state.countries.find(c => c.name === initialCountryName) || null;
      } else if (selectedCountry === null) {
        const code = supportedLanguages2countryCodeMap[this.props.locale] || null;
        if (code) {
          // try guessing
          const langRX = new RegExp(code, 'gi');

          selectedCountry = this.state.countries.find(c => langRX.test(c.alpha2Code)) || null;
        }
      }
      this.setState({ selectedCountry }, this._onValueChange);
    } catch (err) {
      logWarning('Cannot guess location from device language', err);
    }
  };

  _renderCountriesList = country => {
    const { alpha2Code, name, flag, nativeName } = country;
    return (
      <CountryItem
        className="list"
        key={alpha2Code}
        value={alpha2Code}
        label={[nativeName, name].join(' ')}
      >
        <Flag flag={flag} />
        <span>{name === nativeName ? nativeName : `${nativeName} (${name})`}</span>
      </CountryItem>
    );
  };

  _onValueChange = () => {
    this.props.onChange({
      [this.props.name]: this.state.selectedCountry ? this.state.selectedCountry.name : '',
      fullCountry: this.state.selectedCountry
    });
  };

  _onChange = data => {
    this.setState(prevState => {
      const country = data
        ? prevState.countries.find(c => c.alpha2Code === data.value) || null
        : null;
      return { selectedCountry: country };
    }, this._onValueChange);
  };

  _renderSelection = selection =>
    selection && selection.value ? (
      <CountryItem className="selection">
        <Flag flag={selection.value.flag} mini={true} />
        <span>
          {selection.value.name === selection.value.nativeName
            ? selection.value.nativeName
            : `${selection.value.nativeName} (${selection.value.name})`}
        </span>
      </CountryItem>
    ) : null;

  _onMenuOpen = () => {
    if (this._selectRef) {
      this.props.onFocus({ target: this._selectRef });
    }
  };

  _selectRef = null;

  _setSelectRef = elem => (this._selectRef = elem ? elem.root : elem);

  render() {
    const { name, /*value, onChange,  */ /* countries, */ dictionary } = this.props;
    const { countries, selectedCountry } = this.state;
    return (
      <SelectField
        ref={this._setSelectRef}
        name={name}
        elementHeight={72}
        className="country-select"
        multiple={false}
        floatingLabelStyle={{ lineHeight: '0px' }}
        floatingLabel={
          selectedCountry ? (
            dictionary('_reg_form_country')
          ) : (
            <CountryItem>{dictionary('_reg_form_country')}</CountryItem>
          )
        }
        value={selectedCountry ? { value: selectedCountry, label: selectedCountry.name } : null}
        onChange={this._onChange}
        selectionsRenderer={this._renderSelection}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        canAutoPosition={false}
        useLayerForClickAway={false}
        menuStyle={{ width: '100%' }}
        selectedMenuItemStyle={{ width: '100%' }}
        style={{ width: 'calc(100% - 48px)' }}
        innerDivStyle={{
          width: 'calc(100% - 72px)',
          height: 42,
          lineHeight: '42px'
        }}
        // onMenuOpen={this._onMenuOpen}
        // popoverStyle={{ position: 'fixed', top: 0, left: 0 }}
        popoverClassName="country-superselect-popover"
        popoverWidth={window.innerWidth}
      >
        {countries.map(this._renderCountriesList)}
      </SelectField>
    );
  }
}

export const CountrySelect = withDictionary(CountrySelectComponent);
