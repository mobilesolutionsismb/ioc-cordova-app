import React, { Component } from 'react';
import { Header } from 'js/components/app/header2';
import { RaisedButton, LinearProgress, FontIcon } from 'material-ui';
import { withMessages, withLoader } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
// import { Attire } from 'react-attire';
import moment from 'moment';
import { PageContainer } from 'js/components/app/commons';
import { compose } from 'redux';
import nop from 'nop';
import { Route, Redirect } from 'react-router';
import { ValidationPageContent } from './commons';
// import styled from 'styled-components';
import { logError, logMain } from 'js/utils/log';
import { withPartialRegistration } from 'js/modules/partialRegistration';

const enhance = compose(
  withDictionary,
  withMessages,
  withLoader,
  withPartialRegistration
);

// const StyledFormAction = styled.div`
//   bottom: 0;
//   left: 0;
//   position: absolute;
//   width: 100%;
//   height: 64px;
//   display: flex;
//   flex-direction: row;
//   align-items: center;
//   justify-content: space-around;
//   background-color: ${props => props.theme.palette.backgroundColor};
// `;

const buttonIconStyle = { width: 24 };

const MAX_WAIT_TIME = IS_PRODUCTION ? 60 : 30; // seconds

export class EmailValidationComponent extends Component {
  state = {
    timeElapsed: 0,
    sending: false,
    checking: false,
    lastTimeChecked: null,
    emailResent: false // becomes true if the user pressed resent
  };

  _incrementTime = () => {
    if (this._componentRef) {
      this.setState(prevState => {
        return { timeElapsed: prevState.timeElapsed + 1 };
      }, this._checkIsFullyRegistered);
    } else {
      this._clearInterval();
    }
  };

  _componentRef = null;

  _setComponentRef = e => (this._componentRef = e);

  _interval = null;

  _clearInterval = () => {
    if (this._interval !== null) {
      clearInterval(this._interval);
      this._interval = null;
    }
  };

  _startInterval = () => {
    if (this._componentRef) {
      this._clearInterval();
      this._interval = setInterval(this._incrementTime, 1000);
    }
  };

  _checkIsFullyRegistered = (force = false) => {
    if (
      this.state.checking === false &&
      (force === true || this.state.timeElapsed > MAX_WAIT_TIME / 3)
    ) {
      const now = moment();
      const canCheck =
        Math.floor(moment.duration(now.diff(this.state.lastTimeChecked)).asSeconds()) >
        MAX_WAIT_TIME / 3;

      if (canCheck && this._componentRef) {
        logMain('Reg checking...');
        this.setState({ checking: true }, async () => {
          try {
            const complete = await this.props.checkRegistrationIsComplete();
            logMain('Reg checking... complete?', complete);
            if (this._componentRef) {
              const lastTimeChecked = moment();
              setTimeout(() => {
                if (this._componentRef) {
                  this.setState({ checking: false, lastTimeChecked });
                }
              }, 5000);
            }
            if (complete === true) {
              this.props.pushMessage('_register_success', 'success');
              this.props.clearUser();
            }
          } catch (err) {
            logError(err);
            if (this._componentRef) {
              this.setState({ checking: false, lastTimeChecked: moment() });
            }
          }
        });
      }
    }
  };

  _startCounting = partialUserEmailAddressLastUpdate => {
    if (partialUserEmailAddressLastUpdate !== null && this._componentRef) {
      const now = moment();
      const ts = moment(partialUserEmailAddressLastUpdate);
      const timeElapsed = Math.floor(moment.duration(now.diff(ts)).asSeconds());
      this.setState({ timeElapsed, lastTimeChecked: ts }, this._startInterval);
    } else {
      this._startInterval();
    }
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.partialUserEmailAddressLastUpdate !== null &&
      this.props.partialUserEmailAddressLastUpdate !== prevProps.partialUserEmailAddressLastUpdate
    ) {
      // user pushed resend restart timer
      this._startCounting(this.props.partialUserEmailAddressLastUpdate);
    }
  }

  componentDidMount() {
    this._checkIsFullyRegistered(true);
    const partialUserEmailAddressLastUpdate = this.props.partialUserEmailAddressLastUpdate;
    this._startCounting(partialUserEmailAddressLastUpdate);
  }

  componentWillUnmount() {
    this._clearInterval();
  }

  _resendEmail = () => {
    if (this.props.hasPendingRegistration && this._componentRef) {
      this.setState({ sending: true }, async () => {
        this.props.loadingStart();
        try {
          const result = this.props.resendEmailActivationLink();
          logMain('click resend Email', result);
          if (this._componentRef) {
            this.setState({ sending: false, emailResent: true });
          }
        } catch (err) {
          logError(err);
          err.message = '_resend_email_failure';
          this.props.pushError(err);
          if (this._componentRef) {
            this.setState({ sending: false });
          }
        } finally {
          this.props.loadingStop();
        }
      });
    }
  };

  _currentDuration() {
    const duration = moment.duration(this.state.timeElapsed, 'seconds');
    const mins = duration.minutes() + '';
    const secs = duration.seconds() + '';
    return `${mins.padStart(2, 0)}:${secs.padStart(2, 0)}`;
  }

  _abortRegistration = async () => {
    if (this.props.hasPendingRegistration) {
      this.props.loadingStart();
      try {
        await this.props.abortRegistration();
        console.log('Registration aborted successfully');
      } catch (err) {
        logError('Error aborting registration', err);
      } finally {
        this.props.loadingStop();
      }
    }
  };

  onAbortButtonClick = e => {
    e.preventDefault();
    const { dictionary } = this.props;
    const title = dictionary('_reg_abort_modal_title');
    const body = dictionary('_reg_abort_modal_body');
    this.props.pushModalMessage(title, body, {
      _cancel: nop,
      _yes: this._abortRegistration
    });
  };

  onBackButtonClick = e => {
    e.preventDefault();
    this.props.history.replace('/registration');
  };

  render() {
    const { sending, timeElapsed, checking, emailResent } = this.state;
    const mayBeExpired = timeElapsed > MAX_WAIT_TIME;
    const duration = this._currentDuration();
    const { dictionary, partialUser, hasPendingRegistration } = this.props;
    if (!hasPendingRegistration) {
      return <Redirect to="/" push={false} />;
    } else {
      return (
        <PageContainer ref={this._setComponentRef} className="email validation page">
          <Header
            title={'_email_validation'}
            leftButtonType="back"
            onLeftHeaderButtonClick={this.onBackButtonClick}
            rightButtonType="close"
            onRightHeaderButtonClick={this.onAbortButtonClick}
          />
          <ValidationPageContent>
            <div className="validation-content-element">
              {partialUser && [
                <p key="pm">
                  {dictionary(
                    '_email_validation_sent_message',
                    partialUser.name,
                    partialUser.emailAddress
                  )}
                </p>,
                <p key="pm-spam">
                  <small>{dictionary('_email_validation_sent_message_check_spam')}</small>
                </p>,
                <small key="ps">
                  <i>{sending ? '' : dictionary('_email_valid_time_elapsed', duration)}</i>
                </small>
              ]}
            </div>
            <div className="validation-progress-element" style={{ textAlign: 'left' }}>
              <span className="text">{dictionary._email_valid_instructions}</span>
            </div>
            <div className="validation-progress-element">
              {checking && <span className="text">{dictionary._email_valid_checking}</span>}
              {checking && <LinearProgress mode="indeterminate" />}
            </div>
            <div className="validation-content-element email-resend" style={{ marginBottom: 64 }}>
              <p>
                {mayBeExpired
                  ? dictionary('_email_valid_ask')
                  : sending
                  ? ' '
                  : emailResent
                  ? dictionary('_email_valid_wait', partialUser.emailAddress)
                  : ' '}
              </p>
              <div style={{ width: '100%', textAlign: 'center' }}>
                <RaisedButton
                  primary={true}
                  disabled={sending === true || !mayBeExpired}
                  labelPosition="before"
                  label={dictionary('_email_valid_btn_resend')}
                  fullWidth={false}
                  onClick={this._resendEmail}
                  icon={
                    <FontIcon style={buttonIconStyle} className="material-icons">
                      email
                    </FontIcon>
                  }
                />
              </div>
            </div>
          </ValidationPageContent>
        </PageContainer>
      );
    }
  }
}

export const EmailValidation = enhance(props => (
  <Route
    render={({ location, history }) => (
      <EmailValidationComponent location={location} history={history} {...props} />
    )}
  />
));

export default EmailValidation;
