import React, { Component } from 'react';
import nop from 'nop';
import { logWarning, logSevereWarning } from 'js/utils/log';
import Geonames from 'geonames.js';
import { getCountries, GeographySelectsWrapper, CircularLoader, ShowError } from './commons';
import { withDictionary } from 'ioc-localization';
import { CountrySelectSimple } from './CountrySelectSimple';
import { PhoneSelect } from './PhoneSelect';
import isEqual from 'react-fast-compare';

const supportedLanguages2countryCodeMap = {
  da: 'da',
  de: 'de',
  en: 'gb',
  es: 'es',
  fi: 'fi',
  fr: 'fr',
  it: 'it'
};

/*
Interfaces

interface ICountry = {
  name: string
  flag: url,
  alpha2Code: char[2]
}

interface ICallingCode = {
  countryName: string,
  flag: url,
  callingCode: string,
}

interface IPhoneNumber: {
  prefix: ICallingCode,
  phoneNumber: string
}

*/

class CountryPhoneSelectionComponent extends Component {
  static defaultProps = {
    name: 'geography',
    onChange: nop,
    value: null,
    locale: navigator.language.slice(0, 2),
    dictionary: n => n
  };

  _geonames = null;

  state = {
    countries: [], // ICountry[]
    callingCodes: [], // ICallingCode[]
    regions: [],
    provinces: [],
    cities: [],
    loadingData: false, // loading
    dataLoadError: null,
    selectedCountry: null,
    selectedPhoneNumber: null // IPhoneNumber
  };

  _setStateFromValue = value => {
    const { phone, country } = value;
    this.setState({
      selectedPhoneNumber: phone || null,
      selectedCountry: country || null
    });
  };

  componentDidUpdate(prevProps) {
    if (!isEqual(prevProps.value, this.props.value)) {
      this._setStateFromValue(this.props.value);
    }
  }

  // Report relevant values in the form {country, callingCode}
  _onValueChange = () => {
    const { selectedCountry, selectedPhoneNumber } = this.state;
    const values = {
      phone: selectedPhoneNumber,
      country: selectedCountry
    };
    this.props.onChange({
      [this.props.name]: values
      // data: { name: this.props.name, value: values }
    });
  };

  // Handle phone selection change
  _onPhoneSelectionChange = data => {
    this.setState(prevState => {
      let selectedPhoneNumber;
      if (!data) {
        selectedPhoneNumber = null;
      }
      const callingCode = data.prefix ? data.prefix.callingCode : null;
      const phoneNumber = data.phoneNumber || '';
      const code = callingCode
        ? prevState.callingCodes.find(c => c.callingCode === callingCode) || null
        : null;
      selectedPhoneNumber = { prefix: code, phoneNumber };
      return { selectedPhoneNumber };
    }, this._onValueChange);
  };

  // Handle country selection change
  _onCountrySelectionChange = data => {
    this.setState(prevState => {
      const country = data
        ? prevState.countries.find(c => c.alpha2Code === data.value) || null
        : null;
      return { selectedCountry: country };
    }, this._onValueChange);
  };

  // Guess selected country based on
  _guessSelected = data => {
    let selectedCountry = this.state.selectedCountry;
    if (selectedCountry === null) {
      try {
        const code = supportedLanguages2countryCodeMap[this.props.locale] || null;
        if (code) {
          // try guessing
          const langRX = new RegExp(code, 'gi');
          selectedCountry = this.state.countries.find(c => langRX.test(c.alpha2Code)) || null;
        }
        const prefix = selectedCountry
          ? {
              callingCode: selectedCountry.callingCodes[0],
              flag: selectedCountry.flag,
              countryName: selectedCountry.name,
              countryNativeName: selectedCountry.nativeName
            }
          : null;
        const selectedPhoneNumber = {
          prefix,
          phoneNumber: ''
        };
        this.setState({ selectedCountry, selectedPhoneNumber }, this._onValueChange);
      } catch (err) {
        logWarning('Cannot guess location from device language', err);
      }
    }
  };

  // Flatten calling codes
  _getAllCallingCodes = countries =>
    countries.reduce((result, c) => {
      const codes = c.callingCodes.map(cc => ({
        callingCode: cc,
        flag: c.flag,
        countryName: c.name
      }));
      result = [...result, ...codes];
      return result;
    }, []);

  // Loads countries and update state
  _loadCountriesFn = async () => {
    let dataLoadError = null;
    let countries = this.state.countries;
    let callingCodes = this.state.callingCodes;
    try {
      const countriesResponse = await getCountries();
      countries = countriesResponse.data;
      callingCodes = this._getAllCallingCodes(countries);
    } catch (err) {
      dataLoadError = err;
      logSevereWarning(err);
    }
    this.setState(
      { dataLoadError, loadingData: false, countries, callingCodes },
      this._guessSelected
    );
  };

  // trigger countries loading
  _loadCountries = () => {
    this.setState({ loadingData: true }, this._loadCountriesFn);
  };

  componentDidMount() {
    this._geonames = new Geonames({
      username: GEONAMES_API_CREDENTIALS.username,
      lan: this.props.locale,
      encoding: 'JSON'
    });
    this._loadCountries();
  }

  componentWillUnmount() {
    this._geonames = null;
  }

  render() {
    const {
      loadingData,
      dataLoadError,
      countries,
      callingCodes,
      selectedCountry,
      selectedPhoneNumber
    } = this.state;

    const { onFocus, dictionary } = this.props;

    return (
      <GeographySelectsWrapper>
        {loadingData && <CircularLoader />}
        {dataLoadError && <ShowError error={dataLoadError} />}
        <PhoneSelect
          name="phone"
          onChange={this._onPhoneSelectionChange}
          value={selectedPhoneNumber}
          codes={callingCodes}
          onFocus={onFocus}
          dictionary={dictionary}
        />
        <CountrySelectSimple
          name="country"
          onChange={this._onCountrySelectionChange}
          value={selectedCountry}
          countries={countries}
          onFocus={onFocus}
          dictionary={dictionary}
        />
      </GeographySelectsWrapper>
    );
  }
}

export const CountryPhoneSelection = withDictionary(CountryPhoneSelectionComponent);

export default CountryPhoneSelection;
