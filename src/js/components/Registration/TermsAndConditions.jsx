import React, { Component } from 'react';
import { Header, Content } from 'js/components/app/header2';
import { getMarkdownTextAsComponent } from 'js/utils/getAssets';
import { withDictionary } from 'ioc-localization';
import { logError } from 'js/utils/log';
import styled from 'styled-components';
import { RaisedButton, FlatButton } from 'material-ui';
import { LinkedButton } from 'js/components/app/LinkedButton';
import { PageContainer } from 'js/components/app/commons';
import { CloseButton } from './commons';
import { openExternalLink } from 'js/utils/getAssets';

const TERMS_FN = 'terms_and_conditions';

const TeCContent = styled(Content)`
  height: calc(100% - 128px);
  overflow-y: auto;
  padding: 16px;
  box-sizing: border-box;

  a,
  a:visited {
    color: ${props => props.theme.palette.primary1Color};
  }
`;

const Footer = styled(Content)`
  height: 64px;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  padding: 8px 16px;
  box-sizing: border-box;
  background-color: ${props => props.theme.palette.backgroundColor};
`;

class TermsAndConditionsComponent extends Component {
  state = {
    TermsComponent: ''
  };

  _teCCRef = null;

  _setTeCCRef = el => {
    this._teCCRef = el;
  };

  _loadTermsComponent = async locale => {
    try {
      const TermsComponent = await getMarkdownTextAsComponent(TERMS_FN, locale);
      this.setState({ TermsComponent });
    } catch (err) {
      logError(err);
    }
  };

  componentDidMount() {
    this._loadTermsComponent(this.props.locale);
  }

  _openLink = e => {
    const el = e.target;
    if (el.tagName.toLowerCase() === 'a') {
      e.preventDefault();
      e.stopPropagation();
      const url = el.href;
      if (url) {
        openExternalLink(url);
      }
    }
  };

  render() {
    const { TermsComponent } = this.state;
    const { dictionary } = this.props;
    return (
      <PageContainer className="terms-and-conditions page">
        <Header title={'_terms'} leftButtonType="clear" leftButton={<CloseButton />} />
        <TeCContent ref={this._setTeCCRef} onClick={this._openLink}>
          {TermsComponent}
        </TeCContent>
        <Footer>
          <FlatButton
            key="login"
            className="ui-button"
            disableTouchRipple={true}
            containerElement={<LinkedButton to="/" />}
            label={dictionary._terms_disagree}
          />
          <RaisedButton
            key="register"
            // type="submit"
            className="ui-button"
            primary={true}
            containerElement={<LinkedButton to="/registration" replace={false} />}
            label={dictionary._terms_agree}
          />
        </Footer>
      </PageContainer>
    );
  }
}

export const TermsAndConditions = withDictionary(TermsAndConditionsComponent);
export default TermsAndConditions;
