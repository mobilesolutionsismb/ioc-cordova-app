import React, { Component } from 'react';
import nop from 'nop';
import { LinearProgress } from 'material-ui';
import { localizeDate } from 'js/utils/localizeDate';
import { compose } from 'redux';
// import { withMissions } from 'ioc-api-interface';
import { withLoader, withMessages } from 'js/modules/ui';
// import { withDictionary } from 'ioc-localization';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { GoToMapIcon } from 'js/components/ReportsCommons';
import { CloseButton } from 'js/components/app/HeaderButton';

import {
  MissionColorDot,
  getCompletedTaskIdsFilterFn,
  CardContainer,
  InfoContainer,
  ActionsContainer,
  DetailsContainer,
  BarContainer,
  DescriptionContainer,
  DateStringContainer
} from './commons';

const enhance = compose(
  /* withDictionary, withMissions,  */ withLoader,
  withMessages
);

const ProgressContainer = props => (
  <div
    className="mission-progress"
    style={{
      width: '70%',
      margin: 10
    }}
  >
    <LinearProgress
      className="progress-bar"
      color={props.palette.missionProgressBarColor}
      style={{ margin: '0px 0px auto 0px' }}
      mode="determinate"
      value={props.indicatorValue}
    />
  </div>
);

class MissionCard extends Component {
  static defaultProps = {
    showMapIcon: true,
    feature: null,
    onMapButtonClick: nop,
    isSelected: false,
    onCloseButtonClick: null,
    missionTasks: []
  };

  render() {
    const {
      feature,
      /* dictionary, */ onMapButtonClick,
      onCloseButtonClick,
      showMapIcon,
      locale,
      style,
      missionTasks,
      isSelected,
      muiTheme
    } = this.props;
    const missionProperties = feature ? feature.properties : null;
    const { palette } = muiTheme;

    if (missionProperties) {
      const { temporalStatus } = missionProperties;
      const completedTasks = missionTasks.filter(
        getCompletedTaskIdsFilterFn(missionProperties.taskIds)
      );
      const numOfCompletedTasks = completedTasks.length;
      const percentCompleted = (numOfCompletedTasks / missionProperties.taskIds.length) * 100;

      const dateStartedString = missionProperties.start
        ? localizeDate(missionProperties.start, locale, 'L LT')
        : '';
      const dateFinishedString = missionProperties.end
        ? localizeDate(missionProperties.end, locale, 'L LT')
        : '';
      const missionDateString = `${dateStartedString}${
        missionProperties.end ? ' - ' + dateFinishedString : ''
      }`;

      return (
        <CardContainer
          className="mission-card"
          palette={palette}
          style={style}
          isSelected={isSelected}
        >
          <InfoContainer className="mission-info">
            <BarContainer className="mission-bar">
              <span>{`${missionProperties.taskIds.length} TASKS`}</span>
              <ProgressContainer palette={palette} indicatorValue={percentCompleted} />
            </BarContainer>
            <DescriptionContainer className="mission-description">
              <div className="mission-description-title">{missionProperties.title}</div>
            </DescriptionContainer>
            <DetailsContainer className="mission-details">
              <div>{missionProperties.address}</div>
              <DateStringContainer>
                <div>{missionDateString}</div>
                <MissionColorDot temporalStatus={temporalStatus} />
              </DateStringContainer>
            </DetailsContainer>
          </InfoContainer>
          <ActionsContainer className="actions" centered={showMapIcon}>
            {showMapIcon && (
              <GoToMapIcon
                tooltip="Show On Map"
                onClick={onMapButtonClick}
                // iconStyle={{ fontSize: 24 }}
              />
            )}
            {!showMapIcon && typeof onCloseButtonClick === 'function' && (
              <CloseButton
                onClick={onCloseButtonClick}
                style={{ padding: 0, width: 24, height: 24 }}
              />
            )}
          </ActionsContainer>
        </CardContainer>
      );
    } else {
      return <CardContainer className="mission-card" palette={palette} style={style} />;
    }
  }
}

export default muiThemeable()(enhance(MissionCard));
