import React from 'react';
import styled from 'styled-components';
import { MISSION_COLORS } from 'js/startup/iReactTheme';
import { featureFilter } from '@mapbox/mapbox-gl-style-spec';
import { FontIcon } from 'material-ui';
import { darken, fade } from 'material-ui/utils/colorManipulator';
import { MISSION_TASK_COLOR } from 'js/startup/iReactTheme';

export const HEADER_COLOR = darken(MISSION_TASK_COLOR, 0.7);

export function getMissionStatusColor(ts) {
  return ts && MISSION_COLORS.temporalStatus[ts] ? MISSION_COLORS.temporalStatus[ts] : 'black';
}

export function getCompletedTaskIdsFilterFn(taskIds) {
  return featureFilter([
    'all',
    ['==', 'temporalStatus', 'completed'],
    ['in', 'id', ...taskIds]
  ]).bind(null, null);
}

export function getTookOverTasksFilterFn(taskIds) {
  return featureFilter(['all', ['==', 'isTookOver', true], ['in', 'id', ...taskIds]]).bind(
    null,
    null
  );
}

const iconStyleDefault = { fontSize: 15, margin: '0 8px' };
export const DetailsIcon = ({ style, iconText, ...props }) => (
  <FontIcon className="material-icons" style={{ ...iconStyleDefault, ...style }} {...props}>
    {iconText}
  </FontIcon>
);

export const CardContainer = styled.div`
  /* padding: 10px 20px;
  width: calc(100% - 40px);
  height: calc(100% - 20px); */
  padding: 8px;
  width: calc(100% - 16px);
  height: calc(100% - 16px);
  display: flex;
  font-size: 12px;
  background-color: ${props =>
    props.isSelected ? fade(props.palette.accent1Color, 0.24) : props.palette.tabsColor};
`;

const TookOverByChip = styled.span`
  margin-left: auto;
  margin-right: 8px;
  text-transform: uppercase;
  font-weight: 300;
  padding: 2px 4px;
  height: 16px;
  line-height: 16px;
  font-size: 14px;
  border-radius: 3px;
`;

export const ChipTookOverBy = styled(TookOverByChip)`
  background-color: ${props => props.theme.palette.textColor};
  color: ${props => props.theme.palette.backgroundColor};
`;

export const ChipNoneTookOver = styled(TookOverByChip)`
  background-color: ${props => props.theme.palette.backgroundColor};
  color: ${props => props.theme.palette.textColor};
  border: 1px dashed;
  box-sizing: border-box;
`;

const actionsWidth = 24;

export const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: calc(100% - ${actionsWidth}px);
  height: 100%;
  justify-content: space-between;
`;

export const ActionsContainer = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  align-items: center;
  justify-content: ${props => (props.centered ? 'center' : 'space-between')};
  width: ${actionsWidth}px;
`;

export const BarContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  height: 24px;
  width: 100%;
`;

export const DescriptionContainer = styled.div`
  font-weight: bold;
  font-size: 14px;
  height: 16px;
  line-height: 16px;
  width: 100%;

  .mission-description-title {
    text-transform: uppercase;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow-x: hidden;
    width: 100%;
  }
`;

export const DetailsContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  width: 100%;
  height: calc(100% - 24px - 16px);
`;

export const DateStringContainer = styled.div`
  width: 100%;
  height: 50%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

// MISSION DETAILS STUFF

export const MDMissionTitle = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: calc(100% - ${2 * 48}px);
  justify-content: space-evenly;

  &.header-title {
    .taskdetails-title {
      display: flex;
      align-items: center;
      justify-content: space-between;
    }
  }
`;

export const MDTitleAddressLine = styled.div`
  font-size: 12px;
  white-space: nowrap;
  width: 100%;
  height: 14px;
  line-height: 14px;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const md_descrHeight = 72;
const md_statusHeight = 72;
const md_progressHeight = 72;
const md_horizontalPadding = 16;

export const MDContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  padding: 0px ${md_horizontalPadding}px;
  width: 100%;
  box-sizing: border-box;
`;

export const MDDescriptionContainer = styled(MDContainer).attrs(props => ({
  className: props => `md-description${props.className ? ' ' + props.className : ''}`
}))`
  height: ${md_descrHeight}px;
  background-color: ${props => props.theme.palette.backgroundColor};
  border-bottom: 1px solid ${props => fade(props.theme.palette.textColor, 0.4)};
  text-transform: uppercase;
`;

export const MDStatusContainer = styled(MDContainer).attrs(props => ({
  className: props => `md-status-container${props.className ? ' ' + props.className : ''}`
}))`
  height: ${md_statusHeight}px;
  font-size: 14px;
  background-color: ${props => props.theme.palette.backgroundColor};
  border-bottom: 1px solid ${props => fade(props.theme.palette.textColor, 0.4)};
`;

export const MDProgressContainer = styled(MDContainer).attrs(props => ({
  className: props => `md-progress-container${props.className ? ' ' + props.className : ''}`
}))`
  height: ${md_progressHeight}px;
  background-color: ${props => props.theme.palette.backgroundColor};
  border-bottom: 1px solid ${props => fade(props.theme.palette.textColor, 0.4)};
`;

export const MDListContainer = styled.div.attrs(props => ({
  className: props => `md-list-view-container${props.className ? ' ' + props.className : ''}`
}))`
  width: 100%;
  overflow-y: auto;
  background-color: ${props => props.theme.palette.backgroundColor};
  padding: 0px;
  height: calc(100% - ${md_descrHeight + md_statusHeight + md_progressHeight}px);
`;

export const MDStatusLine = styled.div.attrs(props => ({
  className: props => `md-status-line${props.className ? ' ' + props.className : ''}`
}))`
  height: 50%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

export const MDProgressLine = styled(MDStatusLine)`
  justify-content: space-between;
  width: 90%;
  height: auto;
`;

export const MDListItemContainer = styled.div`
  width: 100%;
  height: 100px;
  margin-bottom: 2px;
`;

export const MissionColorDot = styled.div.attrs(props => ({
  className: props => `status-mission ${props.temporalStatus}`
}))`
  width: 6px;
  height: 6px;
  border-radius: 50%;
  margin: 0 8px;
  background-color: ${props => getMissionStatusColor(props.temporalStatus)};
`;
