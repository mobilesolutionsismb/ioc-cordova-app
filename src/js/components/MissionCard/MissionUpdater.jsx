import React, { Component } from 'react';
import { withMissions } from 'ioc-api-interface';

const MISSIONS_UPDATE_INTERVAL = ((IS_PRODUCTION && IS_CORDOVA) ? 5 : 1) * 60 * 1000;

class MissionUpdater extends Component {
    _missionUpdateInterval = null;

    _updateMissions = async () => {
        try {
            const now = new Date();
            if (now.getTime() - this.props.missionsLastUpdated >= MISSIONS_UPDATE_INTERVAL) {
                await this.props.updateMissions();
                console.log('%c Missions updated', 'color: lime', now);
            }
            else {
                console.log('%c Missions update skipped', 'color: goldenrod', now);
            }
        }
        catch (e) {
            console.error('Cannot update missions and tasks', e);
        }
    }

    _startMissionsPollingInterval = () => {
        if (this._missionUpdateInterval === null) {
            this._updateMissions();
            this._missionUpdateInterval = setInterval(this._updateMissions, MISSIONS_UPDATE_INTERVAL);
        }
    }

    _stopMissionsPollingInterval = () => {
        if (this._missionUpdateInterval !== null) {
            clearInterval(this._missionUpdateInterval);
            this._missionUpdateInterval = null;
        }
    }

    componentDidMount() {
        this._startMissionsPollingInterval();
    }

    componentWillUnmount() {
        this._stopMissionsPollingInterval();
    }

    render() {
        return <div className="mission-updater" />;
    }
}

export default withMissions(MissionUpdater);

