import React, { Component } from 'react';
import nop from 'nop';
import { localizeDate } from 'js/utils/localizeDate';
import { compose } from 'redux';
import { withMissions } from 'ioc-api-interface';
import { withLoader, withMessages } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { GoToMapIcon } from 'js/components/ReportsCommons';
import { CloseButton } from 'js/components/app/HeaderButton';

import {
  MissionColorDot,
  // getCompletedTaskIdsFilterFn,
  CardContainer,
  InfoContainer,
  ActionsContainer,
  DetailsContainer,
  ChipTookOverBy,
  // BarContainer,
  DescriptionContainer,
  DateStringContainer
} from './commons';
import { CheckMarkIcon, PersonIcon } from 'js/components/ReportsCommons/commons';

const enhance = compose(
  withDictionary,
  withMissions,
  withLoader,
  withMessages
);

const themeable = muiThemeable();

class MissionTaskCard extends Component {
  static defaultProps = {
    showMapIcon: true,
    missionProperties: null,
    onMapButtonClick: nop,
    isSelected: false,
    onCloseButtonClick: null
  };

  render() {
    const {
      feature,
      dictionary,
      onMapButtonClick,
      onCloseButtonClick,
      showMapIcon,
      locale,
      style,
      isSelected,
      muiTheme,
      currentUserId
    } = this.props;
    const { palette } = muiTheme;

    if (feature) {
      // TODO userTasks: wait for next renaming of this props
      const {
        temporalStatus,
        isTookOver,
        isCompleted,
        description,
        address,
        /* id,  */ start,
        end,
        taskUsers
      } = feature.properties;
      const isMine = isTookOver && taskUsers.findIndex(u => u.user.id === currentUserId) > -1;
      const dateStartedString = start ? localizeDate(start, locale, 'L LT') : '';
      const dateFinishedString = end ? localizeDate(end, locale, 'L LT') : '';
      const taskDateString = `${dateStartedString}${end ? ' - ' + dateFinishedString : ''}`;

      return (
        <CardContainer
          className="missiontask-card"
          palette={palette}
          style={style}
          isSelected={isSelected}
        >
          <InfoContainer className="missiontask-info">
            <DescriptionContainer className="mission-description">
              <span>{description}</span>
            </DescriptionContainer>
            <DetailsContainer className="missiontask-details">
              <div>{address}</div>
              <DateStringContainer>
                <div>{taskDateString}</div>
                <MissionColorDot temporalStatus={temporalStatus} />
                {isTookOver && (
                  <ChipTookOverBy>{isMine ? dictionary._me : dictionary._colleague}</ChipTookOverBy>
                )}
              </DateStringContainer>
            </DetailsContainer>
          </InfoContainer>
          <ActionsContainer className="actions">
            {showMapIcon && (
              <GoToMapIcon
                tooltip="Show On Map"
                onClick={onMapButtonClick}
                // iconStyle={{ fontSize: 24 }}
              />
            )}
            {!showMapIcon && typeof onCloseButtonClick === 'function' && (
              <CloseButton
                onClick={onCloseButtonClick}
                style={{ padding: 0, width: 24, height: 24 }}
              />
            )}
            {isCompleted ? <CheckMarkIcon /> : <PersonIcon />}
          </ActionsContainer>
        </CardContainer>
      );
    } else {
      return <CardContainer className="mission-card" palette={palette} style={style} />;
    }
  }
}

export default themeable(enhance(MissionTaskCard));
