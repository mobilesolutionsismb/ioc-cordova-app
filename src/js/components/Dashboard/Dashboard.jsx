import React from 'react';
import { Header, Content } from 'js/components/app';
// import { ComingSoon } from 'js/components/app/ComingSoon';
import { useDashboardAtGlance } from './dashboard.hooks';
import { Summary } from './Summary';
import { Page, ColumnPage } from 'js/components/app/commons';
import { DatePlace } from './DatePlace';
import { useApiSettings } from 'js/hooks/use-apisettings.hook';
import { logWarning } from 'js/utils/log';

function Dashboard() {
  const [{ liveAOI }] = useApiSettings();
  logWarning('AOI', liveAOI);
  const {
    locale,
    geolocationPosition,
    pointDataUpdating,
    geocodeUpdating,
    // placeName,
    place,
    // getFeatureInfoParams,
    setGetFeatureInfoParams,
    pointFeatures,
    error
  } = useDashboardAtGlance(liveAOI ? liveAOI.wktString : null);
  logWarning(
    'DASHBOARD DATA',
    locale,
    geolocationPosition,
    pointDataUpdating,
    geocodeUpdating,
    // placeName,
    place,
    // getFeatureInfoParams,
    // setGetFeatureInfoParams,
    pointFeatures,
    error
  );
  console.log('Error', error, 'geocodeUpdating', geocodeUpdating);
  return (
    <Page className="dashboard page">
      <Header title="Dashboard" leftButtonType="back" />
      <Content>
        <ColumnPage>
          <DatePlace locale={locale} place={place} />
          {error === null && (
            <Summary
              pointDataUpdating={pointDataUpdating}
              geolocationPosition={geolocationPosition}
              dataBounds={liveAOI}
              setGetFeatureInfoParams={setGetFeatureInfoParams}
              pointFeatures={pointFeatures}
            />
          )}
          {error !== null && <div>{error.message}</div>}
          {geocodeUpdating && <div>Loading, please wait...</div>}
          <div
            style={{ display: 'flex', flexGrow: 1, alignItems: 'center', alignContent: 'center' }}
          >
            <p>Coming soon</p>
          </div>
        </ColumnPage>
      </Content>
    </Page>
  );
}
export default Dashboard;
