import { getLocalDate } from 'js/utils/localizeDate';
import moment from 'moment';

export const IREACT_TEMP_TASKID = 4121;
export const IREACT_WIND_TASKID = 4122;
export const IREACT_RAIN24_TASKID = 4124;

export function sortByLeadTimeAscending(l1, l2) {
  return new Date(l1.leadTime) < new Date(l2.leadTime);
}

function _findNearestLeadTime(d) {
  return (l, index, layers) => {
    const leadTime = getLocalDate(l.leadTime, l.timespan, 'hours').toDate();
    const nextLayer = layers[index + 1];
    if (nextLayer) {
      const nextLeadTime = getLocalDate(nextLayer.leadTime, 0, 'hours').toDate();
      // const nextLeadTime = getLocalDate(nextLayer.leadTime, nextLayer.timespan, 'hours').toDate();
      return leadTime < d && d < nextLeadTime;
    } else {
      return d < leadTime;
    }
  };
}

function _selectAndSort(layers, taskId) {
  return layers.filter(l => l.taskId === taskId).sort(sortByLeadTimeAscending); //sorting shouldn't be neede but better safe than sorry
}

/**
 * Find Temperature layer in layers with values closes to date
 * @param {Array<LayerObject>} layers array of layers
 * @param {Date} date reference date
 * @return {LayerObject|undefined} LayerObject with values closes to reference date
 */
export function findTemperatureLayer(layers, date = new Date()) {
  return _selectAndSort(layers, IREACT_TEMP_TASKID).find(_findNearestLeadTime(date));
}

/**
 * Find Wind layer in layers with values closes to date
 * @param {Array<LayerObject>} layers array of layers
 * @param {Date} date reference date
 * @return {LayerObject|undefined} LayerObject with values closes to reference date
 */
export function findWindLayer(layers, date = new Date()) {
  const sorted = _selectAndSort(layers, IREACT_WIND_TASKID).filter(
    l => getLocalDate(l.leadTime, 0, 'hours').toDate() >= date
  );
  const md = getLocalDate(date).startOf('day'); // set to 12:00 am today
  const dateDiffs = sorted.map(l => moment(getLocalDate(l.leadTime, 0, 'hours').diff(md)));
  const minDiff = moment.min(dateDiffs);
  const index = dateDiffs.findIndex(dd => dd.isSame(minDiff));
  return index > -1 ? sorted[index] : undefined;
}

/**
 * Find Rain 24h accumulated layer in layers with values closes to date
 * @param {Array<LayerObject>} layers array of layers
 * @param {Date} date reference date
 * @return {LayerObject|undefined} LayerObject with values closes to reference date
 */
export function findRain24Layer(layers, date = new Date()) {
  const md = getLocalDate(date).endOf('day'); // set to 12:00 am today
  const sorted = _selectAndSort(layers, IREACT_RAIN24_TASKID).filter(l =>
    getLocalDate(l.leadTime, 0, 'hours').isAfter(md)
  );

  const dateDiffs = sorted.map(l => moment(getLocalDate(l.leadTime, 0, 'hours').diff(md)));
  const minDiff = moment.min(dateDiffs);
  const index = dateDiffs.findIndex(dd => dd.isSame(minDiff));
  return index > -1 ? sorted[index] : undefined;
}

/**
 * Look for feature with corresponding metadataFileId
 * @param {Array<GeoJSON.Feature>} features array of features from GetFeatureInfo
 * @param {Number} metadataFileId - metadata file id to look for
 */
export function findFeatureByMetadataFileId(features, metadataFileId) {
  return features.map(f => f.properties).find(f => f.metadatafile_id === metadataFileId);
}
