import React from 'react';
import { withDictionary } from 'ioc-localization';
import { localizeDate } from 'js/utils/localizeDate';
import { CircularProgress } from 'material-ui';

/**
 * Return beaufort scale classification
 * @see https://en.wikipedia.org/wiki/Beaufort_scale
 * @param {Number} windValue - wind value in m/s
 * @return {Number} beaufort scale number
 */
function windBeaufortScale(windValue) {
  if (isNaN(windValue) || typeof windValue !== 'number') {
    return NaN; //not valid
  } else {
    let beaufortNumber = 0;
    if (windValue > 0.5 && windValue <= 1.5) {
      beaufortNumber = 1;
    } else if (windValue > 1.5 && windValue <= 3.3) {
      beaufortNumber = 2;
    } else if (windValue > 3.3 && windValue <= 5.5) {
      beaufortNumber = 3;
    } else if (windValue > 5.5 && windValue <= 7.9) {
      beaufortNumber = 4;
    } else if (windValue > 7.9 && windValue <= 10.7) {
      beaufortNumber = 5;
    } else if (windValue > 10.7 && windValue <= 13.8) {
      beaufortNumber = 6;
    } else if (windValue > 13.8 && windValue <= 17.1) {
      beaufortNumber = 7;
    } else if (windValue > 17.1 && windValue <= 20.7) {
      beaufortNumber = 8;
    } else if (windValue > 20.7 && windValue <= 24.4) {
      beaufortNumber = 9;
    } else if (windValue > 24.4 && windValue <= 28.4) {
      beaufortNumber = 10;
    } else if (windValue > 28.4 && windValue <= 32.6) {
      beaufortNumber = 11;
    } else if (windValue > 32.6) {
      beaufortNumber = 12;
    }

    return beaufortNumber;
  }
}

/**
 * Return beaufort scale-like classification for rain
 * categories:
 * 0mm dry (0)
 * 0-2.5mm/h light (1)
 * 2.5-10mm/h moderate (2)
 * 10-50mm/h heavy (3)
 * >50mm/h violent (4)
 * @param {Number} rainValue - rain value in mm/day
 * @return {Number} category scale number
 */
function rainQualitativeScale(rainValue) {
  if (isNaN(rainValue) || typeof rainValue !== 'number') {
    return 0; // it's NaN also when it's dry with WMS GetFeatureInfo
  } else {
    let category = 0;
    let _rv = rainValue / 24;
    if (_rv > 0 && _rv <= 2.5) {
      category = 1;
    } else if (_rv > 2.5 && _rv <= 10) {
      category = 2;
    } else if (_rv > 10 && _rv <= 50) {
      category = 3;
    } else if (_rv > 50) {
      category = 4;
    }
    return category;
  }
}

function getValue(categoryData) {
  if (!categoryData) {
    return NaN;
  }
  const { hilimit, lolimit } = categoryData;
  const value = (parseFloat(hilimit, 10) + parseFloat(lolimit, 10)) / 2;
  return value;
}

function MiniForecastFn({
  rain,
  temperature,
  wind,
  forecastDate,
  dictionary,
  locale,
  pointDataUpdating
}) {
  const rainV = getValue(rain) * 24;
  const rainQ = rainQualitativeScale(rainV);
  const temperatureV = getValue(temperature);
  const windV = getValue(wind);
  const windQ = windBeaufortScale(windV);
  const dateString = localizeDate(forecastDate, locale, 'HH:mm');
  return (
    <div className="forecast-container">
      <div className="forecast-date">
        {dictionary._db_forecast_time}: {dateString === 'Invalid Date' ? '-' : dateString}
      </div>
      <div className="temperature">
        <div className="value">
          {pointDataUpdating ? '-' : isNaN(temperatureV) ? 'N/A' : temperatureV} °C
        </div>
        <div className="label">{dictionary._temperature}</div>
      </div>
      <div className="other-forecasts">
        <div className="wind block">
          {/* <div>{dictionary._wind_speed}</div> */}
          <div>
            {pointDataUpdating || isNaN(windQ) ? '-' : dictionary(`_db_beauf_wind_${windQ}`)}
          </div>
          <div>{pointDataUpdating ? '-' : isNaN(windV) ? 'N/A' : windV} m/s</div>
        </div>
        <div className="rain block">
          {/* <div>{dictionary._rain}</div> */}
          <div>{pointDataUpdating ? '-' : dictionary(`_db_qual_rain_${rainQ}`)}</div>
          <div>{pointDataUpdating ? '-' : isNaN(rainV) ? 0 : rainV} mm/24h</div>
        </div>
      </div>
      {pointDataUpdating && (
        <div className="loader">
          <CircularProgress size={80} thickness={5} />
        </div>
      )}
    </div>
  );
}

export const MiniForecast = withDictionary(MiniForecastFn);
export default MiniForecast;
