import React from 'react';
import styled from 'styled-components';
import { MiniMap } from './MiniMap';
import { MiniForecast } from './MiniForecast';
const SummaryContainer = styled.div`
  width: 100%;
  height: 148px;
  line-height: 32px;
  display: flex;

  .map-container {
    width: 40%;
    height: 100%;
    background-color: palegoldenrod;
  }

  .forecast-container {
    position: relative;
    width: 60%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    .loader {
      position: absolute;
      width: 100%;
      height: 100%;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      background-color: rgba(black, 0.3);
    }

    .forecast-date,
    .temperature,
    .other-forecasts {
      width: 100%;
    }

    .forecast-date,
    .other-forecasts {
      line-height: 18px;
      font-size: 14px;
      font-weight: lighter;
    }

    .forecast-date {
      margin-top: 8px;
      height: 18px;
      padding-left: 16px;
      box-sizing: border-box;
    }

    .temperature {
      height: calc(100% - 18px - 56px);
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;

      .label {
        font-size: 14px;
        height: 18px;
        line-height: 18px;
      }
      .value {
        font-size: 36px;
        line-height: 36px;
      }
    }

    .other-forecasts {
      height: 56px;
      display: flex;
      flex-direction: row;
      .block {
        width: 50%;
        height: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
      }
    }
  }
`;

export function Summary({
  geolocationPosition,
  dataBounds,
  setGetFeatureInfoParams,
  pointFeatures,
  pointDataUpdating
}) {
  return (
    <SummaryContainer>
      <MiniForecast {...pointFeatures} pointDataUpdating={pointDataUpdating} />
      <MiniMap
        geolocationPosition={geolocationPosition}
        maxBounds={dataBounds}
        setGetFeatureInfoParams={setGetFeatureInfoParams}
      />
    </SummaryContainer>
  );
}

export default Summary;
