import React from 'react';
import styled from 'styled-components';
import { darken } from 'material-ui/utils/colorManipulator';
import { localizeDate } from 'js/utils/localizeDate';
import moment from 'moment';
const DATE_FORMAT = 'LL';

const DatePlaceLine = styled.div`
  width: 100%;
  height: 32px;
  box-sizing: border-box;
  line-height: 32px;
  padding-left: 16px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  background-color: ${props => darken(props.theme.palette.canvasColor, 0.25)};
`;

export function DatePlace({ locale, place }) {
  const date = localizeDate(moment(), locale, DATE_FORMAT);
  return (
    <DatePlaceLine>
      {date}, {place}
    </DatePlaceLine>
  );
}

export default DatePlace;
