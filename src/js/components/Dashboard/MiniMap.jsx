import React, { PureComponent } from 'react';
import { MapView } from 'js/components/MapView';
import { withMapPreferences } from 'js/modules/preferences';
import { areGeolocationsEqual } from 'js/utils/updateRemotePosition';
import { addGeolocationPositionFeaturesLayers } from 'js/components/MapTabs/components/Tabs/Map/mapboxStyles/addFeatureLayers';
class MinMapComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.mapView = React.createRef();
  }

  _getMap = () => {
    return this.mapView && this.mapView.current ? this.mapView.current.getMap() : null;
  };

  _readGetFeatureInfoParams = (map, geolocationPosition) => {
    const center = [geolocationPosition.coords.longitude, geolocationPosition.coords.latitude];
    const canvas = map.getCanvas();
    const bounds = map.getBounds();
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const pixelPoint = map.project(center);
    return {
      center,
      bounds,
      width,
      height,
      pixelPoint,
      lastUpdated: new Date()
    };
  };

  _onMapStyleLoad = async map => {
    if (!map) {
      return;
    }
    const { geolocationPosition, maxBounds } = this.props;
    await addGeolocationPositionFeaturesLayers(map, geolocationPosition, maxBounds.bounds);
    return this._readGetFeatureInfoParams(map, geolocationPosition);
  };

  _onMapLoadEnd = async map => {
    const { center, ...params } = await this._onMapStyleLoad(map);
    this.props.setGetFeatureInfoParams(params);
    map.setCenter(center);
  };

  componentDidUpdate(prevProps) {
    if (!areGeolocationsEqual(prevProps.geolocationPosition, this.props.geolocationPosition)) {
      const map = this._getMap();
      if (map) {
        const { center, ...params } = this._readGetFeatureInfoParams(
          map,
          this.props.geolocationPosition
        );
        this.props.setGetFeatureInfoParams(params);
        map.setCenter(center);
      }
    }
  }

  render() {
    const { geolocationPosition, preferences, maxBounds } = this.props;
    const { latitude, longitude } = geolocationPosition.coords;
    const mapSettings = {
      style: preferences.mapStyle,
      center: [longitude, latitude],
      maxBounds: maxBounds.bounds,
      bounds: maxBounds.bounds,
      interactive: false
    };

    return (
      <div className="map-container">
        <MapView ref={this.mapView} mapSettings={mapSettings} onMapLoadEnd={this._onMapLoadEnd} />
      </div>
    );
  }
}

export const MiniMap = withMapPreferences(MinMapComponent);

export default MiniMap;
