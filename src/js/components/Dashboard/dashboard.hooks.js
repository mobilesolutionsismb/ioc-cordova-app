import { useState, useRef, useEffect } from 'react';
import nop from 'nop';
import {
  findTemperatureLayer,
  findRain24Layer,
  findWindLayer,
  findFeatureByMetadataFileId
} from './dashboardLayerUtils';
import { getFeatureInfoUrl } from 'js/utils/layerUtils';
import { getLocalDate } from 'js/utils/localizeDate';
import { logError, logWarning } from 'js/utils/log';
import Geonames from 'geonames.js';
import { useLocale } from 'js/hooks/use-locale.hook';
import { useGeolocation } from 'js/hooks/use-geolocation.hook';
import { useMapLayers } from 'js/hooks/use-maplayers.hook';
// HOOK PORTING of https://bitbucket.org/mobilesolutionsismb/ireact-cordova-app/src/5a01c9e4ccfcf289e33ff7d3fe0c690c9cad5e78/src/js/components/Dashboard/Dashboard.jsx?at=dashboard

const _pointFeaturesInitial = { temperature: null, wind: null, rain: null, forecastDate: null };

async function geocode(
  { lat, lng, lang },
  geonames,
  onError = nop,
  onData = nop,
  setUpdating = nop
) {
  setUpdating(true);
  try {
    const result = await geonames.findNearbyPlaceName({ lat, lng, lang });
    const data = result.geonames.length > 0 ? { placeName: result.geonames[0].toponymName } : null;
    onData(data ? data.placeName || '' : '');
  } catch (err) {
    logError('geocode', err);
    onError(err);
  } finally {
    setUpdating(false);
  }
}

async function getFeatureInfo(
  { bounds, width, height, pixelPoint, mapLayers },
  onError = nop,
  onData = nop,
  setUpdating = nop
) {
  setUpdating(true);
  try {
    const version = '1.1.0';
    const geoServerURL = mapLayers.baseUrl;
    const now = new Date();
    const tempLayer = findTemperatureLayer(mapLayers.layers, now);
    const windLayer = findWindLayer(mapLayers.layers, now);
    const rain24Layer = findRain24Layer(mapLayers.layers, now);

    const layerName = [tempLayer, windLayer, rain24Layer]
      .map(layer => (layer ? layer.name || null : null))
      .filter(layerName => layerName !== null);
    logWarning('Now', now, layerName.length, '--> Layers', tempLayer, windLayer, rain24Layer);
    let features = [];
    if (layerName.length > 0) {
      const result = await getFeatureInfoUrl(
        pixelPoint,
        width,
        height,
        bounds,
        geoServerURL,
        layerName,
        version
      );

      features = result.data.features;
      logWarning('Info', features);
    }
    const temperatureData = tempLayer
      ? findFeatureByMetadataFileId(features, tempLayer.metadataFileId)
      : null;
    const windData = windLayer
      ? findFeatureByMetadataFileId(features, windLayer.metadataFileId)
      : null;
    const rain24Data = rain24Layer
      ? findFeatureByMetadataFileId(features, rain24Layer.metadataFileId)
      : null;
    const data = {
      temperature: temperatureData || null,
      wind: windData || null,
      rain: rain24Data || null,
      forecastDate: getLocalDate(
        tempLayer ? tempLayer.leadTime : new Date(),
        /* tempLayer.timespan */ 0,
        'hours'
      )
    };
    onData(data);
  } catch (err) {
    logError('getFeatureInfo', err);
    onError(err);
  } finally {
    setUpdating(false);
  }
}

export function useDashboardAtGlance(liveAOIString = null) {
  const [{ geolocationPosition }] = useGeolocation();
  const coords = geolocationPosition.coords;
  const { latitude, longitude } = coords;
  const [{ mapLayers /* , mapLayersUpdating */ }] = useMapLayers();
  const [{ locale }] = useLocale();

  const geonames = useRef(null);
  const getFeatureInfoParamsLastUpdated = useRef(0);

  const [geocodeUpdating, setGeocodeUpdating] = useState(false);
  const [pointDataUpdating, setPointDataUpdating] = useState(false);
  const [placeName, setPlaceName] = useState('');
  const [getFeatureInfoParams, setGetFeatureInfoParams] = useState(null);
  const [pointFeatures, setPointFeatures] = useState(_pointFeaturesInitial);
  const [error, setError] = useState(null);
  useEffect(() => {
    geonames.current = new Geonames({
      username: GEONAMES_API_CREDENTIALS.username,
      lan: locale,
      encoding: 'JSON'
    });
    logWarning('Dashboard glance hook did mount');
    return () => {
      geonames.current = null;
      logWarning('Dashboard glance hook will unmount');
    };
  }, []);

  useEffect(() => {
    if (geonames.current !== null && !geocodeUpdating) {
      geocode(
        { lat: latitude, lng: longitude, lang: locale },
        geonames.current,
        setError,
        setPlaceName,
        setGeocodeUpdating
      );
    }
  }, [latitude, longitude, geonames.current, locale]);

  useEffect(() => {
    if (getFeatureInfoParams != null) {
      if (getFeatureInfoParams.lastUpdated > getFeatureInfoParamsLastUpdated.current) {
        getFeatureInfoParamsLastUpdated.current = getFeatureInfoParams.lastUpdated;
        getFeatureInfo(
          { ...getFeatureInfoParams, mapLayers },
          setError,
          setPointFeatures,
          setPointDataUpdating
        );
      }
    }
  }, [getFeatureInfoParams]);

  const place = placeName
    ? placeName
    : geolocationPosition
    ? `${latitude.toFixed(4)}, ${longitude.toFixed(4)}`
    : '';

  return {
    locale,
    geolocationPosition,
    pointDataUpdating,
    geocodeUpdating,
    placeName,
    place,
    getFeatureInfoParams,
    setGetFeatureInfoParams,
    pointFeatures,
    error
  };
}
