import './styles.scss';
import React, { Component } from 'react';
import { withRouter } from 'react-router';

import { Header } from 'js/components/app';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { List, ListItem, FlatButton, FontIcon } from 'material-ui';
import { compose } from 'redux';
import { withDictionary } from 'ioc-localization';
import { withEmergencyEvents } from 'ioc-api-interface';
import { localizeDate } from 'js/utils/localizeDate';

import { getAddress, LocateButton, EventChip, getDisplayName } from 'js/components/EventCommons';

const enhance = compose(
  withDictionary,
  withEmergencyEvents
);

const INLINE_ICON_STYLE = { fontSize: 16, lineHeight: '28px', marginRight: '4px' };
const INNER_EVT_DAMAGES_DIV_STYLE = { padding: '16px 16px 16px 72px' };

function getExtensionData(event) {
  let extensionData = {};
  if (typeof event.extensionData === 'string') {
    try {
      extensionData = JSON.parse(event.extensionData);
    } catch (err) {
      console.warn(`Cannot parse extensionData for event ${event.id}`, err);
    }
  } else {
    extensionData = event.extensionData || extensionData;
  }
  return extensionData;
}

const ExtDataListItem = ({ quantity, palette, dictionary, iconText, label, unit }) => (
  <ListItem
    containerElement="div"
    disabled={true}
    innerDivStyle={INNER_EVT_DAMAGES_DIV_STYLE}
    leftIcon={
      <FontIcon className="material-icons" color={palette.textColor}>
        {iconText}
      </FontIcon>
    }
    className="extdata-list-item"
    primaryText={
      <div className="extdata-label">
        <span>{dictionary(label)}</span>
        <span>{`${quantity} ${unit}`}</span>
      </div>
    }
  />
);

class EventDetails extends Component {
  _activateEvent = async () => {
    const eventId = parseInt(this.props.match.params.id, 10);
    try {
      this.props.deselectEvent();
      this.props.activateEvent(eventId);
    } catch (e) {
      console.error('Error when activating event', eventId, e);
    }
  };

  _deactivateEvent = async () => {
    try {
      this.props.deactivateEvent();
    } catch (e) {
      console.error('Error when deactivating event', e);
    }
  };

  _checkErrorCondition(props) {
    const { match, selectedEventId, activeEventId } = props;

    const eventId = parseInt(match.params.id, 10);
    const isSelectedEvent = selectedEventId === eventId;
    const isActiveEvent = activeEventId === eventId;
    const errorCondition = !isActiveEvent && !isSelectedEvent;
    if (errorCondition) {
      props.history.replace('/tabs/events');
    }
  }

  componentDidMount() {
    this._checkErrorCondition(this.props);
  }

  componentDidUpdate() {
    this._checkErrorCondition(this.props);
  }

  _goToMap = () => {
    this.props.history.replace('/tabs/map');
  };

  render() {
    const {
      match,
      dictionary,
      locale,
      selectedEventId,
      activeEventId,
      selectedEvent,
      activeEvent,
      muiTheme
    } = this.props;
    const palette = muiTheme.palette;
    const eventId = parseInt(match.params.id, 10);
    const isSelectedEvent = selectedEventId === eventId;
    const isActiveEvent = activeEventId === eventId;
    const errorCondition = !isActiveEvent && !isSelectedEvent;
    if (errorCondition) {
      return (
        <div className="event-details-page page">
          <Header showLogo={false} leftButtonType="back" />
          <h2>{dictionary('_no_event_found_with_id', eventId)}</h2>
        </div>
      );
    } else {
      // const isOpen = isActiveEvent ? typeof activeEvent.end === 'undefined' : typeof selectedEvent.end === 'undefined';
      const event = isActiveEvent ? activeEvent.properties : selectedEvent.properties;
      const displayName = getDisplayName(event);
      const address = getAddress(event);
      const dateStartedString = event.start ? localizeDate(event.start, locale, 'L LT') : '';
      const dateFinishedString = event.end ? localizeDate(event.end, locale, 'L LT') : '';
      const eventDatesString = `${dictionary('_event_date_started', dateStartedString)}${
        event.end ? ' - ' + dictionary('_event_date_ended', dateFinishedString) : ''
      }`;
      const extensionData = getExtensionData(event);
      return (
        <div className="event-details-page page">
          <Header
            showLogo={false}
            leftButtonType="back"
            title={
              <div className="event-details-title">
                <EventChip dictionary={dictionary} palette={palette} event={event} />
              </div>
            }
            rightButton={
              <div className="report-details-actions">
                <LocateButton color={palette.textColor} onClick={this._goToMap} />
              </div>
            }
          />
          <div className="event-details-body">
            <div className="detail-field event__title">
              {event.code ? event.code + ': ' : ''}
              <span>{displayName}</span>
            </div>
            <div className="detail-field event__address">
              <FontIcon className="material-icons" style={INLINE_ICON_STYLE}>
                place
              </FontIcon>
              <span>{address}</span>
            </div>
            <div className="detail-field event__dates">
              <FontIcon className="material-icons" style={INLINE_ICON_STYLE}>
                event
              </FontIcon>
              <span>{eventDatesString}</span>
            </div>
            <div className="detail-field event__comment">
              {event.comment || dictionary._no_event_description}
            </div>
            <div className="detail-field event__extensiondata">
              <List className="extdata-list" style={{ padding: 0 }}>
                <ExtDataListItem
                  palette={palette}
                  dictionary={dictionary}
                  iconText="people"
                  label="_people_affected"
                  unit={extensionData.people_affected ? dictionary('_people') : ''}
                  quantity={extensionData.people_affected || dictionary._not_available}
                />
                <ExtDataListItem
                  palette={palette}
                  dictionary={dictionary}
                  iconText="people"
                  label="_casualties"
                  unit={extensionData.people_killed ? dictionary('_people') : ''}
                  quantity={extensionData.people_killed || dictionary._not_available}
                />
                <ExtDataListItem
                  palette={palette}
                  dictionary={dictionary}
                  label="_estimated_damage"
                  iconText="euro_symbol"
                  unit={extensionData.estimated_damage ? '€' : ''}
                  quantity={extensionData.estimated_damage || dictionary._not_available}
                />
              </List>
            </div>
            <div className="detail-field event__organization">
              <span
                className="event__organization-avatar"
                style={{ color: palette.authorityColor }}
              />
              <span
                className="role-color-dot role-authority"
                style={{ backgroundColor: palette.authorityColor }}
              />
              <span>{event.organization || 'EMDAT'}</span>
            </div>
          </div>
          <div
            className="event-details-footer"
            style={{ backgroundColor: palette.backgroundColor }}
          >
            <FlatButton
              style={{ height: 64 }}
              labelStyle={{ fontSize: 20, fontWeight: 200 }}
              backgroundColor={palette.backgroundColor}
              primary={isActiveEvent}
              onClick={isActiveEvent ? this._deactivateEvent : this._activateEvent}
              label={isActiveEvent ? dictionary._unset_event_active : dictionary._set_event_active}
              fullWidth={true}
            />
          </div>
        </div>
      );
    }
  }
}

export default withRouter(muiThemeable()(enhance(EventDetails)));
