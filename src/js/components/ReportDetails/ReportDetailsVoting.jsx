import React, { Component } from 'react';
import { nop } from 'nop';
import { Link } from 'react-router-dom';
import { LikesCounter } from 'js/components/LikeButtons';
import {
  ValidatedIcon,
  ReportContentStats,
  MEASURE_UNITS,
  MeasureCategoryIcon,
  RESOURCE_ICONS,
  DAMAGE_ICONS,
  PEOPLE_ICONS
} from 'js/components/ReportsCommons';
import { List, ListItem, /* Divider, /* Avatar, */ FontIcon } from 'material-ui';
import { fade } from 'material-ui/utils/colorManipulator';
import Reporter from './Reporter';
import ReportActions from './ReportActions';
// import { getUserType } from 'js/utils/userPermissions';
import { withLogin } from 'ioc-api-interface';

// import { alternateWithNullItems } from 'js/utils/arrayUtils';

const CollapsedIndicator = () => (
  <FontIcon className="material-icons collapse-icon">keyboard_arrow_up</FontIcon>
);

const ExpandedIndicator = () => (
  <FontIcon className="material-icons collapse-icon">keyboard_arrow_down</FontIcon>
);

const innerMeasureDivStyle = { padding: '16px 16px 16px 72px' };

const MeasureListItem = ({ measure, dictionary, color }) => {
  const isQuantitative = measure.hasOwnProperty('measureUnit');
  const { title, category } = measure;
  let content = '';

  if (isQuantitative) {
    const { measureUnit, value } = measure;
    content = `${value} ${MEASURE_UNITS[measureUnit]}`;
  } else {
    content = dictionary(measure.valueLabel);
  }
  return (
    <ListItem
      containerElement="div"
      disabled={true}
      innerDivStyle={innerMeasureDivStyle}
      leftIcon={<MeasureCategoryIcon style={{ top: 0 }} color={color} category={category} />}
      className="contents-list-item"
      primaryText={
        <div className="measure-label">
          <span>{dictionary('_' + title)}</span>
          <span>{content}</span>
        </div>
      }
    />
  );
};

const DamageListItem = ({ damage, dictionary, color }) => {
  const { label, degree } = damage;
  return (
    <ListItem
      containerElement="div"
      disabled={true}
      innerDivStyle={innerMeasureDivStyle}
      leftIcon={
        <FontIcon className="ireact-icons" color={color}>
          {DAMAGE_ICONS[label.replace('_', '')]}
        </FontIcon>
      }
      className="contents-list-item"
      primaryText={
        <div className="damage-label">
          <span>{dictionary(label)}</span>
          <span>{dictionary('_' + degree)}</span>
        </div>
      }
    />
  );
};

const ResourceListItem = ({ resource, dictionary, color }) => {
  const { label, value } = resource;
  return (
    <ListItem
      containerElement="div"
      disabled={true}
      innerDivStyle={innerMeasureDivStyle}
      leftIcon={
        <FontIcon className="ireact-icons" color={color}>
          {RESOURCE_ICONS[value]}
        </FontIcon>
      }
      className="contents-list-item"
      primaryText={
        <div className="resource-label">
          <span>{dictionary(label)}</span>
        </div>
      }
    />
  );
};

const PeopleListItem = ({ item, dictionary, color }) => {
  const { label, quantity } = item;
  return (
    <ListItem
      containerElement="div"
      disabled={true}
      innerDivStyle={innerMeasureDivStyle}
      leftIcon={
        <FontIcon className="ireact-icons" color={color}>
          {PEOPLE_ICONS[label]}
        </FontIcon>
      }
      className="contents-list-item"
      primaryText={
        <div className="people-label">
          <span>{dictionary(label)}</span>
          <span>{quantity}</span>
        </div>
      }
    />
  );
};

const ReportContentsList = ({ reportProperties, dictionary, palette }) => {
  const { measures, damages, resources, people } = reportProperties;
  let type = 'unknown'; // override the server's type, for now
  if (measures.length > 0) {
    type = 'measures';
  } else if (damages.length > 0) {
    type = 'damages';
  } else if (resources.length > 0) {
    type = 'resources';
  } else if (people.length > 0) {
    type = 'people';
  }
  // console.log(alternateWithNullItems(reportProperties.measures))
  return (
    <List className="contents-list" style={{ padding: 0 }}>
      {type === 'measures' &&
        reportProperties.measures.map((m, i) => (
          <MeasureListItem key={i} measure={m} dictionary={dictionary} color={palette.textColor} />
        ))}
      {type === 'damages' &&
        reportProperties.damages.map((d, i) => (
          <DamageListItem key={i} damage={d} dictionary={dictionary} color={palette.textColor} />
        ))}
      {type === 'resources' &&
        reportProperties.resources.map((r, i) => (
          <ResourceListItem
            key={i}
            resource={r}
            dictionary={dictionary}
            color={palette.textColor}
          />
        ))}
      {type === 'people' &&
        reportProperties.people.map((item, i) => (
          <PeopleListItem key={i} item={item} dictionary={dictionary} color={palette.textColor} />
        ))}
    </List>
  );
};

class ReportDetailsVoting extends Component {
  static defautProps = {
    expanded: false,
    history: null,
    onLikesCounterClick: nop
  };

  _getReportStatus(reportProperties) {
    if (reportProperties.status === 'submitted') {
      return (
        <LikesCounter
          onClick={reportProperties.voteEnabled ? this.props.onLikesCounterClick : undefined}
          size="xs"
          style={{ width: 64 }}
          upvotes={reportProperties.upvoteCount}
          downvotes={reportProperties.downvoteCount}
          alreadyVoted={reportProperties.alreadyVoted}
        />
      );
    } else if (reportProperties.status === 'validated') {
      return (
        <ValidatedIcon tooltip="Validated" style={{ width: 20, height: 20, fontSize: '10px' }} />
      );
    }
  }

  render() {
    const { expanded, reportProperties, palette, dictionary, user, loading, history } = this.props;
    const appUser = user;

    const style = { color: palette.textColor, backgroundColor: fade(palette.canvasColor, 0.9) };
    const reportInfo = (
      <div className="report-info">
        <ReportContentStats reportProperties={reportProperties} hideMeasureIcons={true} />
        {this._getReportStatus(reportProperties)}
      </div>
    );

    const collapseLink = `/reportdetails/${reportProperties.id}${expanded ? '' : '?expanded'}`;

    return (
      <div className={`report-details-voting${expanded ? ' expanded' : ''}`} style={style}>
        <div className="report-details-voting-inner">
          <Link className="details-inner-heading" to={collapseLink} replace={true}>
            {expanded ? <ExpandedIndicator /> : <CollapsedIndicator />}
            {expanded ? '' : `(${dictionary._expand})`}
            {reportInfo}
          </Link>
          {expanded && (
            <ReportContentsList
              dictionary={dictionary}
              palette={palette}
              reportProperties={reportProperties}
            />
          )}
          {expanded && (
            <Reporter
              dictionary={dictionary}
              reporterId={reportProperties.creatorUserId}
              requesterId={user.id}
              requesterType={appUser.userType}
              reporter={reportProperties.user}
              organization={reportProperties.organization}
              palette={palette}
            />
          )}
          {expanded && (
            <ReportActions
              disabled={loading}
              appUser={appUser}
              reportProperties={reportProperties}
              dictionary={dictionary}
              history={history}
              // palette={palette}
            />
          )}
        </div>
      </div>
    );
  }
}

export default withLogin(ReportDetailsVoting);
