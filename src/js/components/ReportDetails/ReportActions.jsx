import React, { Component } from 'react';
import { withReports } from 'ioc-api-interface';
import { withMessages, withLoader } from 'js/modules/ui';
import { LongPressFABs, VoteFAB } from 'js/components/LikeButtons';
import { Dialog, RadioButtonGroup, RadioButton, FlatButton } from 'material-ui';
import { compose } from 'redux';
import nop from 'nop';
import styled from 'styled-components';
import { userIsValidator } from 'js/utils/userPermissions';

const VoteDialogWrapper = styled.div`
  /* width: 80vw; */
  height: 25vh;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  & div.content {
    text-align: center;
    font-weight: 400;
    font-size: 1.3em;
    margin-top: 0;
    padding: 0 1em;
  }
`;

const VoteDialogActions = styled.div`
  width: 100%;
  height: 128px;
  /* padding: 8px; */
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;

const ReportActionsWrapper = styled.div.attrs(props => ({ className: 'report-actions' }))`
  /* background-color: ${props => props.theme.palette.backgroundColor}; */
  height: ${props => (props.voteEnabled ? 'auto' : '0px')};
  min-height: ${props => (props.voteEnabled ? 'auto' : '0px')};
`;

const enhance = compose(
  withMessages,
  withLoader,
  withReports
);

class RejectionDialog extends Component {
  state = {
    rejectionReason: null
  };

  close = () => {
    const { closeValidationDialog } = this.props;
    closeValidationDialog(this.state.rejectionReason);
    this.setState({ rejectionReason: null });
  };

  render() {
    const { dictionary, dialogIsOpen } = this.props;

    return (
      <Dialog
        className="reject-reason-dialog"
        open={dialogIsOpen}
        onRequestClose={this.close}
        title={<h3>{dictionary('_rejection_reason')}</h3>}
        actions={[
          <FlatButton label={dictionary('_cancel')} primary={true} onClick={this.close} />,
          <FlatButton
            disabled={this.state.rejectionReason === null}
            label={dictionary('_ok')}
            primary={false}
            onClick={this.close}
          />
        ]}
      >
        <RadioButtonGroup
          name="reject-reason"
          className="reject-reason"
          onChange={(evt, value) => this.setState({ rejectionReason: value })}
        >
          <RadioButton
            checked={this.state.rejectionReason === 'inappropriate'}
            className="reject-reason-option inappropriate"
            value="inappropriate"
            label={dictionary('_inappropriate')}
          />
          <RadioButton
            checked={this.state.rejectionReason === 'inaccurate'}
            className="reject-reason-option inaccurate"
            value="inaccurate"
            label={dictionary('_inaccurate')}
          />
        </RadioButtonGroup>
      </Dialog>
    );
  }
}

export const ReportActionsComponentTypes = {
  bottomBar: 0,
  dialog: 1
};

class ReportActions extends Component {
  static defaultProps = {
    disabled: false,
    componentType: ReportActionsComponentTypes.bottomBar,
    closeDialog: nop
  };

  state = {
    dialogIsOpen: false
  };

  _formatVotingResponse = (voteResult, userType) => {
    if (!voteResult) {
      return 'Invalid result';
    }
    let formattedResponse = '';
    if (userType !== 'citizen') {
      formattedResponse = this.props.dictionary('_report_voted_success');
    } else if (voteResult.achievement && voteResult.achievement.name) {
      // show achievements
      formattedResponse = this.props.dictionary(
        '_report_voted_success_achievement',
        voteResult.achievement.name
      );
    } else if (voteResult.level !== '') {
      // show new level
      formattedResponse = this.props.dictionary('_report_voted_success_level', voteResult.level);
    } else if (voteResult.points !== 0) {
      // show points earned
      formattedResponse = this.props.dictionary(
        '_report_voted_success_points',
        '' + voteResult.points
      );
    } else {
      formattedResponse = this.props.dictionary('_report_voted_success');
    }
    return formattedResponse;
  };

  _validate = async () => {
    const reportId = this.props.reportProperties.id;
    try {
      await this.props.validateReport(reportId, this.props.loadingStart, this.props.loadingStop);
      this.props.pushMessage('_report_validated_success', 'success');
    } catch (err) {
      this.props.pushError(err);
    }
    return Promise.resolve();
  };

  _rejectReport = async rejectReason => {
    const reportId = this.props.reportProperties.id;
    try {
      if (rejectReason === 'inappropriate') {
        await this.props.setInappropriateReport(
          reportId,
          this.props.loadingStart,
          this.props.loadingStop
        );
        this.props.pushMessage('_report_reject_updated', 'success');
      } else if (rejectReason === 'inaccurate') {
        await this.props.setInaccurateReport(
          reportId,
          this.props.loadingStart,
          this.props.loadingStop
        );
        this.props.pushMessage('_report_reject_updated', 'success');
      }
    } catch (err) {
      this.props.pushError(err);
    }
    return Promise.resolve();
  };

  _FAKE_vote = () => {
    return new Promise(success => {
      this.props.loadingStart();
      setTimeout(() => {
        success(null);
        this.props.loadingStop();
      }, 1000);
    });
  };

  _vote = async (voteType, userType = 'citizen') => {
    if (!voteType) {
      return;
    }

    const reportId = this.props.reportProperties.id;
    let voteResult = null;
    try {
      if (voteType === 'up') {
        voteResult = await this.props.upVoteReport(
          reportId,
          this.props.loadingStart,
          this.props.loadingStop
        );
      } else {
        voteResult = await this.props.downVoteReport(
          reportId,
          this.props.loadingStart,
          this.props.loadingStop
        );
      }
      console.log('%cVOTE RESULT', 'background: darkcyan; color: gold', voteResult);
      const formattedResult = this._formatVotingResponse(voteResult, userType);
      console.log('%cVOTE RESULT', 'background: darkcyan; color: gold', formattedResult);
      this.props.pushMessage(formattedResult, 'success');
    } catch (err) {
      this.props.pushError(err);
    }
    return voteResult;
  };

  _openValidationDialog = () => {
    this.setState({ dialogIsOpen: true });
  };

  _closeValidationDialog = (reason = null) => {
    this.setState({ dialogIsOpen: false });
    console.log('Dialog closed', reason);
    if (reason !== null) {
      this._rejectReport(reason);
    }
  };

  voteUp = async done => {
    if (done) {
      const { appUser } = this.props;
      const isUserAValidator = userIsValidator(appUser);

      console.log('UP', isUserAValidator ? 'validation' : 'vote');
      if (!isUserAValidator) {
        await this._vote('up', appUser.userType);
      } else {
        await this._validate();
      }
    }
  };

  voteDown = async done => {
    if (done) {
      const { appUser } = this.props;
      const isUserAValidator = userIsValidator(appUser);

      console.log('DOWN', isUserAValidator ? 'validation' : 'vote');
      if (!isUserAValidator) {
        await this._vote('down', appUser.userType);
      } else {
        this._openValidationDialog();
      }
    }
  };

  render() {
    const {
      style,
      dictionary,
      reportProperties,
      appUser,
      disabled,
      componentType,
      closeDialog,
      history
    } = this.props;
    const isUserAValidator = userIsValidator(appUser);

    const longPressFabsType = isUserAValidator ? 'validation' : 'likes';
    const { voteEnabled } = reportProperties;
    const idleMessage = dictionary(
      isUserAValidator ? '_vote_report_question_is_valid' : '_vote_report_question_is_correct'
    );
    const keepPressingMessage = dictionary('_vote_report_keep_pressing');
    const pressingDoneMessage = dictionary('_vote_report_release_the_button');

    if (componentType !== ReportActionsComponentTypes.dialog) {
      // default
      return (
        <ReportActionsWrapper style={style} voteEnabled={voteEnabled}>
          {/* Add this report can be voted condition */
          voteEnabled && (
            <LongPressFABs
              type={longPressFabsType}
              mini={true}
              disabled={disabled}
              idleMessage={idleMessage}
              onPressingMessage={keepPressingMessage}
              onPressingDoneMessage={pressingDoneMessage}
              onLeftButtonPressSuccess={this.voteDown}
              onRightButtonPressSuccess={this.voteUp}
            />
          )}
          {isUserAValidator && (
            <RejectionDialog
              dictionary={dictionary}
              dialogIsOpen={this.state.dialogIsOpen}
              closeValidationDialog={reason => {
                this._closeValidationDialog(reason);
                if (reason && history) {
                  history.replace('/tabs/reports');
                }
              }}
            />
          )}
        </ReportActionsWrapper>
      );
    } else {
      return [
        <VoteDialogWrapper key="vote-dialog">
          <div className="content">{idleMessage}</div>
          <VoteDialogActions>
            <VoteFAB
              mini={true}
              disabled={disabled}
              buttonType={isUserAValidator ? 'reject' : 'down'}
              onClick={() => {
                closeDialog();
                this.voteDown(true);
              }}
            />
            <VoteFAB
              mini={true}
              disabled={disabled}
              buttonType={isUserAValidator ? 'validate' : 'up'}
              onClick={() => {
                closeDialog();
                this.voteUp(true);
              }}
            />
          </VoteDialogActions>
        </VoteDialogWrapper>,
        <RejectionDialog
          key="rejection-dialog-v"
          dictionary={dictionary}
          dialogIsOpen={this.state.dialogIsOpen}
          closeValidationDialog={reason => {
            this._closeValidationDialog(reason);
            closeDialog();
            if (reason && history) {
              history.replace('/tabs/reports');
            }
          }}
        />
      ];
    }
  }
}

export default enhance(ReportActions);
