import React, { Component } from 'react';
import { compose } from 'redux';
// import styled from 'styled-components';
import { withLogin } from 'ioc-api-interface';
// import { withReportFilters } from 'js/modules/reportFilters';
import { withMessages } from 'js/modules/ui';
import { openMapAppLink } from 'js/utils/getAssets';

import { withDictionary } from 'ioc-localization';
import { fade } from 'material-ui/utils/colorManipulator';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';
import { ZoomableImage } from './ZoomableImage';
import { Swipeable } from 'react-swipeable';
import { localizeDate } from 'js/utils/localizeDate';
import { logMain } from 'js/utils/log';
import { Header, Content } from 'js/components/app/header2';
import { /* ShareIcon, */ GoToMapIcon, HazardBanner } from 'js/components/ReportsCommons';
import ReportDetailsVoting from './ReportDetailsVoting';
import { DetailsLoader } from 'js/components/EmergencyCommunicationsCommons';
import ReportActions, { ReportActionsComponentTypes } from './ReportActions';

const enhance = compose(
  withLogin,
  withDictionary,
  withMessages
);
const themed = muiThemeable();

const datePosStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between'
};

const TITLE_ICON_STYLE = { fontSize: 24 };
const POS_PRECISION = 5;
const SWIPE_EFFECT_TIMEOUT = 300; //ms

class ReportDetailsInner extends Component {
  static defaultProps = {
    selectedFeature: null,
    match: { params: { id: -1 } }
  };

  timeout = null;

  state = {
    loading: true,
    nextFeatureIndex: -1,
    currentFeatureIndex: -1,
    prevFeatureIndex: -1,
    // filteredReports: [],
    sweepingButCantGoLeft: false,
    sweepingButCantGoRight: false
  };

  // See https://stackoverflow.com/questions/34544314/setstate-can-only-update-a-mounted-or-mounting-component-this-usually-mea/40969739#40969739
  _autoRef = null;

  _setAutoRef = e => {
    this._autoRef = e;
  };

  _findSelectedFeatureIndex(props) {
    const { match, features } = props;
    const matchId =
      typeof match.params.id === 'number' ? match.params.id : parseInt(match.params.id, 10);
    const featureIndex = features.findIndex(f => f.properties.id === matchId);
    return {
      loading: false,
      nextFeatureIndex:
        featureIndex >= 0 && featureIndex < features.length - 1 ? featureIndex + 1 : -1,
      currentFeatureIndex: featureIndex,
      prevFeatureIndex: featureIndex > 0 && featureIndex < features.length ? featureIndex - 1 : -1
    };
  }

  _computeNextState(props) {
    this.setState({ loading: true }, () => {
      const { nextFeatureIndex, currentFeatureIndex, prevFeatureIndex } = this.state;
      const nextState = this._findSelectedFeatureIndex(props);

      if (
        nextFeatureIndex !== nextState.nextFeatureIndex ||
        currentFeatureIndex !== nextState.currentFeatureIndex ||
        prevFeatureIndex !== nextState.prevFeatureIndex
      ) {
        this.setState(nextState);
      } else {
        this.setState({ loading: false });
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      !areFeaturesEqual(prevProps.selectedFeature, this.props.selectedFeature) ||
      prevProps.match.params.id !== this.props.match.params.id ||
      (prevProps.featuresUpdating === true && this.props.featuresUpdating === false)
    ) {
      this._computeNextState(this.props);
    }
    if (
      this.state.nextFeatureIndex !== prevState.nextFeatureIndex ||
      this.state.currentFeatureIndex !== prevState.currentFeatureIndex ||
      this.state.prevFeatureIndex !== prevState.prevFeatureIndex
    ) {
      const feature =
        this.state.currentFeatureIndex > -1
          ? this.props.features[this.state.currentFeatureIndex]
          : null;
      if (feature) {
        this.props.selectFeature(feature);
      }
    }
  }

  // componentDidMount() {
  //   this._computeNextState(this.props);
  // }

  _onSwipingLeft = () => {
    logMain('ReportDetailsInner::Swiping LEFT');
    if (this.state.nextFeatureIndex === -1 && !this.state.sweepingButCantGoLeft) {
      this.setState({ sweepingButCantGoLeft: true });
    }
  };

  _onSwipingRight = () => {
    logMain('ReportDetailsInner::Swiping RIGHT');
    if (this.state.prevFeatureIndex === -1 && !this.state.sweepingButCantGoRight) {
      this.setState({ sweepingButCantGoRight: true });
    }
  };

  _onSwipedLeft = () => {
    logMain('ReportDetailsInner::Swiped LEFT');
    const expanded = this.props.location.search.indexOf('expanded') > -1;
    if (this.state.sweepingButCantGoLeft === true) {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        if (this._autoRef) {
          this.setState({ sweepingButCantGoLeft: false });
        }
      }, SWIPE_EFFECT_TIMEOUT);
    }
    if (this.state.nextFeatureIndex > -1) {
      const nextId = this.props.features[this.state.nextFeatureIndex].properties.id;
      this.props.history.replace(`/reportdetails/${nextId}${expanded ? '?expanded' : ''}`);
    }
  };

  _onSwipedRight = () => {
    logMain('ReportDetailsInner::Swiped RIGHT');
    const expanded = this.props.location.search.indexOf('expanded') > -1;
    if (this.state.sweepingButCantGoRight === true) {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        if (this._autoRef) {
          this.setState({ sweepingButCantGoRight: false });
        }
      }, SWIPE_EFFECT_TIMEOUT);
    }
    if (this.state.prevFeatureIndex > -1) {
      const prevId = this.props.features[this.state.prevFeatureIndex].properties.id;
      this.props.history.replace(`/reportdetails/${prevId}${expanded ? '?expanded' : ''}`);
    }
  };

  _showOnMap = () => {
    this.props.history.replace('/tabs/map');
  };

  render() {
    const {
      dictionary,
      locale,
      muiTheme,
      features,
      user,
      location,
      featuresUpdating,
      history,
      pushModalMessage
      // upVoteReport,
      // downVoteReport,
      // validateReport,
      // setInaccurateReport,
      // setInappropriateReport
    } = this.props; // use featuresUpdating for loader?
    const { currentFeatureIndex, loading } = this.state;
    const selectedFeature = currentFeatureIndex > -1 ? features[currentFeatureIndex] : null;
    const palette = muiTheme.palette;
    const featureId = selectedFeature ? selectedFeature.properties.id : -1;
    const featureProperties = selectedFeature ? selectedFeature.properties : {};
    const updating = loading || featuresUpdating;
    const coordinates = selectedFeature ? selectedFeature.geometry.coordinates : null;

    const pageStyle = {
      color: palette.textColor,
      backgroundColor: palette.backgroundColor
    };
    const effectStyle = { backgroundColor: palette.accent1Color };
    const headerStyle = {
      zIndex: 99,
      backgroundColor: fade(palette.backgroundColor, 0.8)
    };
    const validFeature = updating === false && featureId > -1;
    const detailsExpanded = location.search.indexOf('expanded') > -1;

    return (
      <Swipeable
        ref={this._setAutoRef}
        className="report-details page"
        style={pageStyle}
        onSwipingLeft={this._onSwipingLeft}
        onSwipedLeft={this._onSwipedLeft}
        onSwipingRight={this._onSwipingRight}
        onSwipedRight={this._onSwipedRight}
      >
        <Header
          key="header"
          style={headerStyle}
          showLogo={false}
          leftButtonType="back"
          title={
            <div className="report-details-title">
              <div>{featureProperties.address}</div>
              <div style={datePosStyle}>
                {Array.isArray(coordinates) && (
                  <span onClick={openMapAppLink(coordinates)}>
                    {coordinates[1].toFixed(POS_PRECISION)}, {coordinates[0].toFixed(POS_PRECISION)}
                  </span>
                )}
                <span>{localizeDate(featureProperties.userCreationTime, locale, 'L LT')}</span>
              </div>
            </div>
          }
          rightButton={
            <div className="report-details-actions">
              <GoToMapIcon iconStyle={TITLE_ICON_STYLE} onClick={this._showOnMap} />
              {/* <ShareIcon iconStyle={TITLE_ICON_STYLE} /> */}
            </div>
          }
        />
        {updating === true && <DetailsLoader />}
        {validFeature && [
          <div
            key="left-effect"
            className={`left-effect${this.state.sweepingButCantGoRight ? '' : ' hidden'}`}
            style={effectStyle}
          />,
          <div
            key="right-effect"
            className={`right-effect${this.state.sweepingButCantGoLeft ? '' : ' hidden'}`}
            style={effectStyle}
          />,
          <ZoomableImage
            key="picture"
            loading={updating}
            pictureURL={featureProperties.picture}
            loadingText={dictionary('_report_image_loading')}
          />,
          <HazardBanner key="hazard-banner" hazard={featureProperties.hazard} />,
          <ReportDetailsVoting
            key="voting-controls"
            loading={updating}
            user={user}
            reportProperties={featureProperties}
            expanded={detailsExpanded}
            palette={palette}
            dictionary={dictionary}
            history={history}
            onLikesCounterClick={e => {
              e.preventDefault();
              e.stopPropagation();
              pushModalMessage('', closeDialog => (
                <ReportActions
                  disabled={updating}
                  reportProperties={featureProperties}
                  dictionary={dictionary}
                  closeDialog={closeDialog}
                  appUser={user}
                  componentType={ReportActionsComponentTypes.dialog}
                  history={history}
                />
              ));
            }}
          />
        ]}
        {updating === false && featureId === -1 && (
          <Content>
            <h2>{dictionary('_no_report_found_with_id', this.props.match.params.id)}</h2>
          </Content>
        )}
      </Swipeable>
    );
  }
}

export default themed(enhance(ReportDetailsInner));
