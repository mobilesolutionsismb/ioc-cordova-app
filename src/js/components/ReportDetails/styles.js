import { white } from 'material-ui/styles/colors';

export const STYLES = {
    zoomableImage: {
        position: 'absolute',
        width: '100%',
        height: '100%'
    }
};

/*     {detailsTitle: {
        marginLeft: 8,
        height: 64,
        lineHeight: '56px',
        fontSize: 24
    },
    detailsOverlayContainer: {
        position: 'absolute',
        width: '100%',
        height: '40%',
        bottom: 0
    },
    detailsOverlay: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: 'auto',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        backgroundColor: 'rgba(0,0,0,0.75)',
        color: white
    },
    collapsibleContent: {
        width: '100%',
        height: 'auto',
    },
    dateLine: {
        width: 'calc(100% - 64px)',
        height: 32,
        lineHeight: '16px',
        fontSize: '12px',
        textAlign: 'center',
        display: 'flex'
    },
    dateLineItem: {
        padding: '0 8px',
        width: 'calc(100% - 16px)',
        height: 16,
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap'
    },
    line: {
        textAlign: 'left',
        width: '100%',
        height: 36,
        lineHeight: '36px',
        fontSize: 18,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    controlsLine: {
        textAlign: 'left',
        width: '100%',
        height: 48,
        lineHeight: '48px',
        fontSize: 18,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    rIcon: {
        margin: '0 8px',
        fontSize: 28,
        lineHeight: '48px'
    },
    displayStatusContainer: {
        display: 'flex',
        alignItems: 'center',
        height: '100%',
        flexDirection: 'row'
    },
    noReportsFound: {
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        flexDirection: 'row'
    },
    collapsibleIndicator: {
        position: 'absolute',
        left: 8,
        top: 8
    },
    linkToMap: {
        position: 'absolute',
        right: 8,
        top: 8
    }
};
 */