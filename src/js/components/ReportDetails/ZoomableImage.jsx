import React, { Component } from 'react';
import { default as PinchZoomCanvas } from 'pinch-zoom-canvas';
import { RefreshIndicator } from 'material-ui';
import { getAsset } from 'js/utils/getAssets';

const RESIZE_FACTOR = 1;

const STYLES = {
  loader: {
    background: 'white',
    position: 'fixed',
    zIndex: 1000
  },
  loadingText: {
    position: 'fixed',
    zIndex: 1000,
    color: 'white',
    width: '100vw',
    height: 20,
    textAlign: 'center'
  }
};

class ZoomableImage extends Component {
  static defaultProps = {
    pictureURL: null,
    loadingText: 'Image Loading, please wait...'
  };

  canvas = null;
  container = null;
  pinchZoom = null;

  state = {
    loading: true
  };

  _updateSize = () => {
    console.log('%cSize updated', 'color: white; background: purple;');
    this.canvas.width = this.container.clientWidth * RESIZE_FACTOR;
    this.canvas.height = this.container.clientHeight * RESIZE_FACTOR;
    this.pinchZoom.render();
  };

  _initializeCanvas = pictureURL => {
    if (!pictureURL) {
      return;
    }
    // console.log('PinchZoomCanvas', PinchZoomCanvas, this.canvas, this.container, this.container.clientWidth, this.container.clientHeight);
    this.pinchZoom = new PinchZoomCanvas({
      canvas: this.canvas,
      path: pictureURL,
      momentum: true,
      zoomMax: 4,
      doubletap: true,
      onReady: () => {
        //console.log('%c Zoomable Image Ready', 'color: lime; background: pink');
        this.setState({ loading: false });
      },
      // onZoomEnd: function (zoom, zoomed) {
      //     console.log('---> is zoomed: %s', zoomed);
      //     console.log('---> zoom end at %s', zoom);
      // },
      // onZoom: function (zoom) {
      //     console.log('---> zoom is %s', zoom);
      // },
      emptyImage: getAsset('logo', false)
    });

    window.addEventListener('resize', this._updateSize);
  };

  _deinitializeCanvas = () => {
    window.removeEventListener('resize', this._updateSize);
    if (this.pinchZoom) {
      this.pinchZoom.destroy();
    }
    return this;
  };

  componentDidUpdate(prevProps) {
    if (prevProps.pictureURL !== this.props.pictureURL) {
      if (typeof this.props.pictureURL === 'string') {
        this.setState({ loading: true }, () => {
          this._deinitializeCanvas()._initializeCanvas(this.props.pictureURL);
        });
      }
      if (this.props.pictureURL === null) {
        this.setState({ loading: false }, () => {
          this._deinitializeCanvas();
        });
      }
    }
  }

  componentDidMount() {
    this.canvas.width = this.container.clientWidth * RESIZE_FACTOR;
    this.canvas.height = this.container.clientHeight * RESIZE_FACTOR;
    this.setState({ loading: this.props.pictureURL !== null }, () => {
      this._initializeCanvas(this.props.pictureURL);
    });
  }

  componentWillUnmount() {
    this._deinitializeCanvas();
    this.canvas = null;
    this.container = null;
  }

  _setContainerRef = el => {
    this.container = el;
  };

  _setCanvasRef = el => {
    this.canvas = el;
  };

  render() {
    const size = 50;
    const h = this.container ? this.container.clientHeight : window.screen.availHeight;
    const w = this.container ? this.container.clientWidth : window.screen.availWidth;
    const left = (w - size) * 0.5;
    const top = (h - size) * 0.5;
    return (
      <div ref={this._setContainerRef} className="zoomable-image" style={this.props.style}>
        <canvas ref={this._setCanvasRef} style={{ width: '100%', height: '100%' }} />
        {this.state.loading && (
          <RefreshIndicator
            className="zoomable-image-loader"
            size={size}
            status="loading"
            left={left}
            top={top}
            style={STYLES.loader}
          />
        )}
        {this.state.loading && (
          <span style={{ ...STYLES.loadingText, top: top + 70 }}>{this.props.loadingText}</span>
        )}
      </div>
    );
  }
}

export { ZoomableImage };

export default ZoomableImage;
