import React from 'react';
import styled from 'styled-components';
import { VoteFAB } from 'js/components/LikeButtons';
import { userIsValidator } from 'js/utils/userPermissions';

const VoteDialogWrapper = styled.div`
  /* width: 80vw; */
  height: 25vh;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  & div.content {
    text-align: center;
    font-weight: 400;
    font-size: 1.3em;
    margin-top: 0;
    padding: 0 1em;
  }
`;

const VoteDialogActions = styled.div`
  width: 100%;
  height: 128px;
  /* padding: 8px; */
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;

export const VoteDialog = ({
  upVoteReport,
  downVoteReport,
  validateReport,
  setInaccurateReport,
  setInappropriateReport,
  dictionary,
  closeDialog,
  user
}) => {
  // const isUserAuthority = userType === 'authority';
  const isUserAValidator = userIsValidator(user);
  return (
    <VoteDialogWrapper>
      <div className="content">
        {dictionary(
          isUserAValidator ? '_vote_report_question_is_valid' : '_vote_report_question_is_correct'
        )}
      </div>
      <VoteDialogActions>
        <VoteFAB
          mini={true}
          buttonType={isUserAValidator ? 'reject' : 'down'}
          onClick={() => {
            console.log('DOWN');
            closeDialog();
          }}
        />
        <VoteFAB
          mini={true}
          buttonType={isUserAValidator ? 'validate' : 'up'}
          onClick={() => {
            console.log('UP');
            closeDialog();
          }}
        />
      </VoteDialogActions>
    </VoteDialogWrapper>
  );
};
