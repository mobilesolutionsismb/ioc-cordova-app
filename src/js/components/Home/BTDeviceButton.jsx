import React, { Component } from 'react';
import { withBluetooth } from 'js/modules/bluetooth/';
import styled, { css, keyframes } from 'styled-components';
import { Link } from 'react-router-dom';
import { BT_TEXT_COLOR } from 'js/components/Wearable';
import BluetoothConnected from 'material-ui/svg-icons/device/bluetooth-connected';
import BluetoothSearching from 'material-ui/svg-icons/device/bluetooth-searching';
import BluetoothDisabled from 'material-ui/svg-icons/device/bluetooth-disabled';

const blinker = keyframes`
  50% {
    opacity: 0;
  }
`;

const animation = props =>
  css`
    ${blinker} 1s linear infinite;
  `;

const BTLink = ({ to, className, children, ...rest }) => (
  <Link className={className} to={to}>
    {children}
  </Link>
);

const StyledBTLink = styled(BTLink)`
  padding: 4px;
  font-size: 14px;
  text-transform: uppercase;
  color: ${props => (props.isConnected ? BT_TEXT_COLOR : 'inherit')};
  animation: ${props => (props.isConnected && props.isNotifying ? animation : 'none')};
`;

class BTDeviceButton extends Component {
  render() {
    const { isConnected, isConnecting, isBtEnabled, isNotifying } = this.props;
    const icon = isBtEnabled ? (
      isConnected ? (
        <BluetoothConnected color={BT_TEXT_COLOR} />
      ) : isConnecting ? (
        <BluetoothSearching />
      ) : (
        <BluetoothDisabled />
      )
    ) : (
      <BluetoothDisabled />
    );
    return (
      <StyledBTLink
        className="bt-device-button"
        to="/settings/wearable"
        isConnected={isConnected}
        isNotifying={isNotifying}
      >
        {icon}
      </StyledBTLink>
    );
  }
}

export default withBluetooth(BTDeviceButton);
