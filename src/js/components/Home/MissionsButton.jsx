import React from 'react';
import { Link } from 'react-router-dom';
import { Badge } from 'material-ui';
import { withMissions } from 'ioc-api-interface';
import muiThemeable from 'material-ui/styles/muiThemeable';
import styled from 'styled-components';

const themed = muiThemeable();

const StyledMissionsLink = styled(Link)`
  padding: 4px;
  font-size: 14px;
  text-transform: uppercase;
`;

const MissionsButton = ({ label, muiTheme, style, missionFeaturesStats }) => {
  const { palette } = muiTheme;
  // const totalMissions = missionFeaturesStats ? missionFeaturesStats.totalFeaturesCount : 0;
  // const completedMissions = missionFeaturesStats
  //   ? missionFeaturesStats.completedMissionsCount || 0
  //   : 0;

  // const toBeProcessed = totalMissions - completedMissions;
  const thisUserTeamMissionsCount = missionFeaturesStats
    ? missionFeaturesStats.thisUserTeamMissionsCount
    : 0;
  const thisUserTeamCompletedMissionsCount = missionFeaturesStats
    ? missionFeaturesStats.thisUserTeamCompletedMissionsCount
    : 0;
  const toBeProcessed = thisUserTeamMissionsCount - thisUserTeamCompletedMissionsCount;
  const color = palette.textColor;
  const backgroundColor = toBeProcessed > 0 ? palette.primary2Color : palette.backgroundColor;
  const _style = { color, backgroundColor, padding: '4px 8px', borderRadius: 5, ...style };
  return (
    <StyledMissionsLink to="/missions" className="misssions-button">
      {toBeProcessed > 0 && (
        <Badge
          badgeContent={toBeProcessed}
          secondary={true}
          badgeStyle={{ top: 8, right: 8, color: palette.textColor }}
        >
          <span style={_style}>{label}</span>
        </Badge>
      )}
      {toBeProcessed === 0 && <span style={_style}>{label}</span>}
    </StyledMissionsLink>
  );
};

export default withMissions(themed(MissionsButton));
