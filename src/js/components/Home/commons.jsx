import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const HeaderControls = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

export const SquaredLinksContainer = styled.div`
  width: 100%;
  height: calc(100% - 56px);
  overflow: hidden;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  box-sizing: border-box;
`;

export const ActionButtonsContainer = styled.div`
  height: 56px;
  width: 100%;
  overflow: hidden;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  box-sizing: border-box;

  & .action-button {
    width: 50%;
    height: 100%;
    box-sizing: border-box;
    border: 1px solid ${props => props.theme.palette.borderColor};
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const SquaredLink = styled(Link).attrs(props => ({
  className: 'squared-link'
}))`
  width: 50%;
  height: auto;
  flex-grow: 1;
  box-sizing: border-box;
  border: 1px solid ${props => props.theme.palette.borderColor};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  filter: ${props => (props.disabled ? 'saturate(50%) brightness(60%)' : '')};

  &.dashboard_icon
  /* &.emcomms_icon */ {
    /* provisional */
    filter: saturate(50%) brightness(60%);
  }
`;

export const LinkIcon = styled.div.attrs(props => ({
  className: 'squared-link-icon'
}))`
  box-sizing: border-box;
  border: 2px solid ${props => props.theme.palette.accent1Color};
  width: 45%;
  border-radius: 100%;
  background-image: url('${props => props.src}');
  background-repeat: no-repeat;
  background-size: 90%;
  background-position: center;
  position: relative;

  &:after,&.profile:after {
    content: '';
    display: block;
    padding-bottom: 100%;
  }

  &.profile{
    background-position-y: top;
    background-color: ${props => props.theme.palette.homeButtonsColor};
  }

  &.profile:before{
    content: '';
    border: 4px solid ${props => props.theme.palette.canvasColor};
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: 0;
    border-radius: 100%;
    box-sizing: border-box;
  }
`;

export const LinkText = styled.div.attrs(props => ({
  className: 'squared-link-text'
}))`
  text-transform: uppercase;
  text-align: center;
`;
