import React, { Component } from 'react';
import { FontIcon, IconButton, Badge } from 'material-ui';
import { compose } from 'redux';
import styled from 'styled-components';
import nop from 'nop';
import { withReports } from 'ioc-api-interface';
import { withNewReport } from 'js/modules/newReport2';
import { withNetworkStatus } from 'js/modules/networkInformation';
import { logError } from 'js/utils/log';
import axios from 'axios';
// import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';

const enhance = compose(
  withNetworkStatus,
  withNewReport,
  withReports
);

const ReportsQueueIndicatorContainer = styled.div`
  padding: 4px;
  font-size: 14px;
  text-transform: uppercase;
  margin-right: 8px;
  min-width: 55px;
  min-height: 22px;
`;

const SendIcon = ({ style }) => (
  <FontIcon style={style} className="material-icons">
    send
  </FontIcon>
);

const OfflineIcon = ({ style, count }) => (
  <span>
    <FontIcon style={style} className="material-icons">
      ic_signal_cellular_connected_no_internet_4_bar
    </FontIcon>
    {count}
  </span>
);

class ReportsQueueIndicator extends Component {
  static defaultProps = {
    selectedFeature: null,
    selectFeature: nop,
    lastReportId: null,
    reportFeaturesURL: '',
    reportFeaturesStats: {
      newFeaturesCount: 0
    },
    lastSentReportId: null
  };

  // state = {
  //   lastReportId: null
  // };

  _checkReportQueue = props => {
    if (props.isOffline === false && props.outgoingReportsQueueLength > 0) {
      console.log(
        '%c ReportsQueueIndicator will attempt to send reports',
        'color: darkmagenta; font-weight: bold;'
      );
      // const reportId = await
      props.startSendingEnqueuedReports();
      // if (typeof reportId === 'number' && this._autoRef) {
      //   this.setState({ lastReportId: reportId });
      // }
    }
  };

  _findNewReportAndSelect = async matchingId => {
    const { reportFeaturesURL, selectFeature } = this.props;
    try {
      const response = await axios.get(reportFeaturesURL);
      const reports = response.data;
      const report = reports.find(f => f.properties.id === matchingId);
      if (report) {
        selectFeature(report);
        this.props.clearLastSentReportId();
      }
    } catch (err) {
      logError(`Cannot Select Report with id ${matchingId}`, err);
    }
  };

  componentDidUpdate(prevProps /* , prevState */) {
    if (prevProps.isOffline === true && this.props.isOffline === false) {
      this._checkReportQueue(this.props);
    }
    if (this.props.outgoingReportsQueueLength !== prevProps.outgoingReportsQueueLength) {
      if (this.props.outgoingReportsQueueLength > prevProps.outgoingReportsQueueLength) {
        this._checkReportQueue(this.props);
      } else {
        this.props.updateReports();
      }
    }

    if (
      prevProps.reportFeaturesURL !== this.props.reportFeaturesURL &&
      this.props.reportFeaturesURL !== null &&
      this.props.reportFeaturesStats.newFeaturesCount !== 0 &&
      this.props.lastSentReportId !== null
    ) {
      this._findNewReportAndSelect(this.props.lastSentReportId);
    }
    // if (
    //   this.props.selectedFeature !== null &&
    //   this.props.selectedFeature.itemType === 'report' &&
    //   !areFeaturesEqual(prevProps.selectedFeature, this.props.selectedFeature)
    // ) {
    //   if (this.props.selectedFeature.id === this.state.lastReportId && this._autoRef) {
    //     // this.setState({ lastReportId: null });
    //     this.props.clearLastSentReportId();
    //   }
    // }
    // if(prevState.lastReportId !== this.state.lastReportId && this.state.lastReportId !== null){

    // }
  }

  componentDidMount() {
    this._checkReportQueue(this.props);
  }

  _autoRef = null;

  _setAutoRef = e => {
    this._autoRef = e;
  };

  render() {
    const { outgoingReportsQueueLength, isOffline } = this.props;
    const hidden = isOffline === false && outgoingReportsQueueLength <= 0;
    return (
      <ReportsQueueIndicatorContainer
        ref={this._setAutoRef}
        style={hidden ? { visibility: 'hidden' } : {}}
        className="reports-queue-indicator"
      >
        {isOffline === true && 'Offline'}
        {hidden && <span />}
        {isOffline === false &&
          outgoingReportsQueueLength >= 0 && (
            <Badge
              className="reports-queue-indicator"
              style={{ padding: '0 24px' }}
              badgeContent={
                isOffline ? (
                  <OfflineIcon count={outgoingReportsQueueLength} />
                ) : (
                  outgoingReportsQueueLength
                )
              }
              secondary={true}
              badgeStyle={{ top: 0, right: 12 }}
            >
              <IconButton tooltip={`Outgoing reports: ${outgoingReportsQueueLength}`}>
                <SendIcon />
              </IconButton>
            </Badge>
          )}
      </ReportsQueueIndicatorContainer>
    );
  }
}

export default enhance(ReportsQueueIndicator);
