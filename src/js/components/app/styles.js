/**
 * Global app components style
 */

 const APP_BAR_HEIGHT = 64;

export const layoutStyle = {
    width: '100vw',
    height: '100vh',
    position: 'fixed',
    margin: 0,
    top: 0,
    left: 0
};

export const pageStyle = {
    position: 'relative',
    left: 0,
    // top: 0,
    // height: '100%'
    top: APP_BAR_HEIGHT,
    height: `calc(100% - ${APP_BAR_HEIGHT}px)`
};

export const uiLoaderStyle = {
    display: 'inline-block',
    position: 'relative',
};

export const loadingOverlayStyle = {
    width: '100%',
    height: '100%',
    position: 'fixed',
    top: 0,
    left: 0,
    zIndex: 9999,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
};

const STYLE = {
    layoutStyle,
    pageStyle,
    uiLoaderStyle,
    loadingOverlayStyle
};

export default STYLE;