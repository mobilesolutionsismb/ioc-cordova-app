import { ColumnPage } from 'js/components/app/commons';
import styled from 'styled-components';

export const TopCenteredPage = styled(ColumnPage)`
  justify-content: flex-start;
  color: ${props => props.theme.palette.textColor};
  background-color: ${props => props.theme.palette.canvasColor};
`;
