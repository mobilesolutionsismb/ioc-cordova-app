/**
 * Handles sending authority Location updates IN FOREGROUND
 */
import React, { Component } from 'react';
import styled from 'styled-components';
import { withGeolocation } from 'ioc-geolocation';
import { updateRemotePosition } from 'js/utils/updateRemotePosition';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { withBluetooth } from 'js/modules/bluetooth/';
import debounce from 'lodash.debounce';
import { withLogin } from 'ioc-api-interface';
import { logSevereWarning, logMain } from 'js/utils/log';

const REMOTE_POSITION_UPDATE_INTERVAL = 1 * 60 * 1000; // 1 mins

const InnerElement = styled.div`
  display: none;
`;

class AuthorityLocationUpdater extends Component {
  _updateRemotePositionInterval = null;

  _sendPositionUpdate = async () => {
    try {
      const btDeviceData =
        this.props.isDeviceAvailable && this.props.isNotifying ? this.props.lastBleMessage : null;
      let deviceData = {};
      if (this.props.selectedDevice) {
        const { id, name } = this.props.selectedDevice;
        deviceData = {
          deviceId: id,
          deviceName: name
        };
      }
      const wereableData = btDeviceData ? { ...btDeviceData, ...deviceData } : null;
      await updateRemotePosition(this.props.geolocationPosition, wereableData);
      console.log('%c 🛰️ Remote Location updated 📡', 'color: deeppink; background: #583c63;');
    } catch (err) {
      logSevereWarning('Cannot update remote position', err);
      appInsights.trackException(err, 'Home::_updateAreaIfNeeded', {}, {}, SeverityLevel.Error);
    }
  };

  _startUpdatingAgentLocation = debounce(() => {
    if (!this._updateRemotePositionInterval) {
      this._sendPositionUpdate();
      this._updateRemotePositionInterval = setInterval(
        this._sendPositionUpdate,
        REMOTE_POSITION_UPDATE_INTERVAL
      );
    }
  }, 300);

  _stopUpdatingAgentLocation = () => {
    if (this._updateRemotePositionInterval !== null) {
      clearInterval(this._updateRemotePositionInterval);
      this._updateRemotePositionInterval = null;
    }
  };

  componentDidUpdate(prevProps) {
    if (!prevProps.loggedIn && this.props.loggedIn && this.props.user.userType === 'authority') {
      this._startUpdatingAgentLocation();
    }
    if (prevProps.loggedIn && !this.props.loggedIn) {
      logMain('AuthorityLocationUpdater: Stop sending position');
      this._stopUpdatingAgentLocation();
    }
  }

  componentDidMount() {
    if (this.props.loggedIn && this.props.user.userType === 'authority') {
      this._startUpdatingAgentLocation();
    }
  }

  componentWillUnmount() {
    this._stopUpdatingAgentLocation();
  }

  render() {
    return <InnerElement className="hidden-locationUpdater" />;
  }
}

export default withBluetooth(withGeolocation(withLogin(AuthorityLocationUpdater)));
