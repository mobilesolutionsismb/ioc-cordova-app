import React, { Component } from 'react';
import nop from 'nop';
import { withMessages } from 'js/modules/ui';
import { Route } from 'react-router';
import { DEFAULT_PAGE_UNAUTHENTICATED, DEFAULT_PAGE_AUTHENTICATED } from 'js/components/Navigator';
import { IS_ANDROID } from 'js/utils/getAppInfo';

class HardwareBackButtonHandler extends Component {
  closeAppHandle = null;

  componentDidMount() {
    console.warn('HardwareBackButtonHandler mounted');
    document.addEventListener('backbutton', this._onHardwareBackButtonClick);
  }

  componentWillUnmount() {
    document.removeEventListener('backbutton', this._onHardwareBackButtonClick);
  }

  _onExitAppConfirm = () => {
    if (IS_ANDROID) {
      navigator.app.exitApp();
    }
  };

  _onHardwareBackButtonClick = e => {
    e.preventDefault();
    e.stopPropagation();
    if (
      this.props.location.pathname === DEFAULT_PAGE_UNAUTHENTICATED ||
      this.props.location.pathname === DEFAULT_PAGE_AUTHENTICATED
    ) {
      if (!this.props.hasModalsOpen) {
        //No Modals
        const title = '_exit_app';
        const message = '_confirm_exit_app';
        const actions = {
          _cancel: nop,
          _confirm: this._onExitAppConfirm
        };
        this.closeAppHandle = this.props.pushModalMessage(title, message, actions);
      } else {
        //Modal is open
        if (this.props.modal && this.props.modal.id === this.closeAppHandle) {
          //Is this modal
          this.props.popModalMessage();
          this.closeAppHandle = null;
        }
      }
    } else {
      this.props.history.goBack();
    }
  };

  render() {
    return <div className="hardware-backbutton-handler">{this.props.children}</div>;
  }
}
const EnhancedHBBH = withMessages(HardwareBackButtonHandler);

function HardwareBackButtonHandlerRoute() {
  return <Route render={props => <EnhancedHBBH {...props} />} />;
}

export default HardwareBackButtonHandlerRoute;
