import React, { Component } from 'react';
import { compose } from 'redux';
import { withLogin, withIREACTFeatures } from 'ioc-api-interface';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { withGamificationAwards } from 'js/modules/gamificationAwards';
import { withPushNotifications } from 'js/modules/pushNotifications';
import { withRouter } from 'react-router';
import { withMessages, withLoader } from 'js/modules/ui';
import { lighten } from 'material-ui/utils/colorManipulator';
import { black, white } from 'material-ui/styles/colors';
import {
  MISSION_COLOR,
  EMCOMM_LEVEL_REPORT_REQUEST_COLOR,
  EMCOMM_LEVEL_ALERT_COLOR,
  EMCOMM_LEVEL_BE_PREPARED_COLOR,
  EMCOMM_LEVEL_ACTION_REQUIRED_COLOR,
  EMCOMM_LEVEL_DANGER_TO_LIFE_COLOR
} from 'js/startup/iReactTheme';

const enhance = compose(
  withLogin,
  withMessages,
  withLoader,
  withIREACTFeatures,
  withPushNotifications,
  withGamificationAwards
);

class PNHandler extends Component {
  _getNotificationTypeAndId(notificationPayload, pathname) {
    const { customProp } = notificationPayload;
    if (!customProp) {
      return { type: 'unknown' };
    }
    const { Type, Id, ...otherProps } = customProp;
    // const type = notificationPayload.type
    //   ? notificationPayload.type
    //   : notificationPayload.customProp
    //     ? notificationPayload.customProp.Type || notificationPayload.customProp.type
    //     : 'unknown';
    // const id = notificationPayload.customProp
    //   ? notificationPayload.customProp.id || notificationPayload.customProp.Id
    //   : null;

    const id = Id || -1;
    const type = Type ? Type.toLowerCase() : 'unknown';
    const info = {
      id,
      type,
      itemType: Type,
      redirect: null,
      snackBarStyle: {
        backgroundColor: black,
        color: white,
        fontSize: 12
      },
      openSnackbar: true,
      otherProps,
      isGamification: false
    };
    if (typeof type === 'string') {
      switch (type) {
        // MAP ITEMS
        case 'reportrequest':
          info.itemType = 'emergencyCommunication';
          info.snackBarStyle.color = lighten(EMCOMM_LEVEL_REPORT_REQUEST_COLOR, 0.3);
          info.redirect = {
            action: pathname.startsWith('/emcommdetails') ? 'replace' : 'push',
            path: `/emcommdetails/${id}?objectType=report_request`
          };
          break;
        case 'alert':
          info.itemType = 'emergencyCommunication';
          info.snackBarStyle.color = lighten(EMCOMM_LEVEL_ALERT_COLOR, 0.3);
          info.redirect = {
            action: pathname.startsWith('/emcommdetails') ? 'replace' : 'push',
            path: `/emcommdetails/${id}?objectType=${type}`
          };
          break;
        case 'warning':
          info.itemType = 'emergencyCommunication';
          let level = notificationPayload.customProp
            ? notificationPayload.customProp.level
            : undefined;
          if (level === 'bePrepared') {
            info.snackBarStyle.color = EMCOMM_LEVEL_BE_PREPARED_COLOR;
          }
          if (level === 'actionRequired') {
            info.snackBarStyle.color = EMCOMM_LEVEL_ACTION_REQUIRED_COLOR;
          }
          if (level === 'dangerToLife') {
            info.snackBarStyle.color = lighten(EMCOMM_LEVEL_DANGER_TO_LIFE_COLOR, 0.3);
          }
          info.redirect = {
            action: pathname.startsWith('/emcommdetails') ? 'replace' : 'push',
            path: `/emcommdetails/${id}?objectType=${type}`
          };
          break;
        case 'mission':
          info.itemType = 'mission';
          info.snackBarStyle.color = lighten(MISSION_COLOR, 0.3);
          info.redirect = {
            action: pathname.startsWith('/missiondetails') ? 'replace' : 'push',
            path: `/missiondetails/${id}?objectType=${type}`
          };
          break;
        // GAMIFICATION
        case 'award':
          info.isGamification = true;
          info.openSnackbar = false;
          info.type = 'gamification-award';
          info.itemType = '';
          info.redirect = {
            action: 'replace',
            // path: '/achievements/community/monthlyawards'
            path: `/congratulations?newContentType=${type}&newContentName=${
              otherProps.Name
            }&competence=${otherProps.Competence}&scoreDifference=${otherProps.Points}`
          };
          break;
        case 'badge':
          info.isGamification = true;
          info.openSnackbar = false;
          info.type = 'gamification-badge';
          info.itemType = '';
          info.redirect = {
            action: 'replace',
            path: `/congratulations?newContentType=${type}&newContentName=${
              otherProps.Name
            }&hazard=${otherProps.Hazard}&scoreDifference=${otherProps.Points}&competence=${
              otherProps.Competence
            }`
          };
          break;
        case 'medal':
          info.isGamification = true;
          info.openSnackbar = false;
          info.type = 'gamification-medal';
          info.itemType = '';
          info.redirect = {
            action: 'replace',
            path: `/congratulations?newContentType=${type}&newContentName=${
              otherProps.Name
            }&metal=${otherProps.MedalType}&scoreDifference=${otherProps.Points}&competence=${
              otherProps.Competence
            }`
          };
          break;
        case 'level':
          info.isGamification = true;
          info.openSnackbar = false;
          info.type = 'gamification-level';
          info.itemType = '';
          info.redirect = {
            action: 'replace',
            // path: '/achievements/personal/skills'
            path: `/congratulations?newContentType=${type}&newContentName=${
              otherProps.Name
            }&scoreDifference=0&direction=${otherProps.Direction}`
          };
          break;
        case 'unlocked-content':
        case 'unlockedContent':
        case 'unlockedcontent':
          info.isGamification = true;
          info.openSnackbar = false;
          info.type = 'gamification-unlocked-content';
          info.itemType = '';
          info.redirect = {
            action: 'replace',
            path: `/congratulations?newContentType=unlockedContent&newContentName=${
              otherProps.Name
            }`
          };
          break;
        default:
          info.itemType = '';
          break;
      }
    }
    return info;
  }

  _getNotificationRelevantInfo(notification, pathname) {
    const { title, message, additionalData, receivedTimeStamp } = notification;
    let _additionalData = additionalData;
    if (typeof additionalData.customProp === 'string') {
      try {
        _additionalData.customProp = JSON.parse(additionalData.customProp);
      } catch (err) {
        console.warn('Invalid JSON in payload', err);
      }
    }
    const info = this._getNotificationTypeAndId(_additionalData, pathname);
    return {
      title,
      message,
      receivedTimeStamp,
      ...info
    };
  }

  _handleForegroundNotification = async props => {
    const fgNotification = props.lastNotification;
    const addAwardNotification = props.addAwardNotification;
    const gamificationAwards = props.gamificationAwards;
    const info = this._getNotificationRelevantInfo(fgNotification, props.location.pathname);
    if (info.isGamification) {
      addAwardNotification(info);
    }
    try {
      const snackBarMessage =
        info.type === 'unknown' ? info.message : `New ${info.type}: ${info.message}`;
      if (gamificationAwards.length === 0 && info.type.startsWith('gamification-')) {
        props.history[info.redirect.action](info.redirect.path);
      } else {
        await props.updateIREACTFeatures(true);

        const actions = {};
        if (info.redirect && info.openSnackbar) {
          console.log('REDIRECT', info.redirect);
          actions['_expand'] = async () => {
            try {
              switch (info.itemType) {
                case 'mission':
                case 'emergencyCommunication':
                  // ft will be null if deleted
                  // const feat = {
                  //   properties: { ...info },
                  //   geometry: { coordinates: [] }
                  // };
                  // const ft = props.selectFeature(feat);
                  // if (ft) {
                  props.history[info.redirect.action](info.redirect.path);
                  // }
                  break;
                default:
                  break;
              }
            } catch (error) {
              appInsights.trackException(
                error,
                'PNHandler::_handleForegroundNotification: _ok action',
                {},
                {},
                SeverityLevel.Error
              );
            }
          };
          actions['_close'] = () => {};
        }
        if (props.message !== null) {
          props.popMessage();
        }
        props.pushMessage(snackBarMessage, info.snackBarStyle, actions);
      }

      props.setPushNotificationRead(fgNotification);
      if (navigator.notification && navigator.notification.beep) {
        navigator.notification.beep(1);
      }
    } catch (error) {
      appInsights.trackException(
        error,
        'PNHandler::_handleForegroundNotification',
        { pushNotifications: 'foreground' },
        {},
        SeverityLevel.Error
      );
    }
  };

  _handleBackgroundNotification = async props => {
    props.loadingStart();
    const bgNotification = props.lastNotification;
    const addAwardNotification = props.addAwardNotification;
    const info = this._getNotificationRelevantInfo(bgNotification, props.location.pathname);
    if (info.isGamification) {
      addAwardNotification(info);
    }
    try {
      switch (info.itemType) {
        case 'mission':
        case 'emergencyCommunication':
          await props.updateIREACTFeatures(true);
          // ft will be null if deleted
          // const feat = {
          //   properties: { ...info },
          //   geometry: { coordinates: [] }
          // };

          // const ft = props.selectFeature(feat);
          // if (ft) {
          props.history[info.redirect.action](info.redirect.path);
          // }
          break;
        default:
          props.history[info.redirect.action](info.redirect.path);
          break;
      }
      props.loadingStop();

      props.setPushNotificationRead(bgNotification);
    } catch (error) {
      props.loadingStop();
      appInsights.trackException(
        error,
        'PNHandler::_handleBackgroundNotification',
        { pushNotifications: 'background' },
        {},
        SeverityLevel.Error
      );
    }
  };

  _initPushNotifications = async props => {
    try {
      const pnInitResult = await props.initializePushNotifications();
      if (pnInitResult.registered) {
        console.log('%cPN INIT Success', 'color: lightgreen;', pnInitResult);
      }
    } catch (error) {
      console.error('%cPN INIT Failure', 'color: whitesmoke; background: darkred', error);
      appInsights.trackException(
        error,
        'PNHandler::_initPushNotifications',
        { pushNotifications: 'initializePushNotifications' },
        {},
        SeverityLevel.Error
      );
    }
  };

  componentDidMount() {
    if (this.props.loggedIn) {
      //login
      this._initPushNotifications(this.props);
    } else {
      this.props.unsubscribePushNotifications();
    }
  }

  componentDidUpdate(prevProps) {
    const isLoggedIn = this.props.loggedIn;
    if (!prevProps.loggedIn && isLoggedIn) {
      //login
      this._initPushNotifications(this.props);
    }
    if (prevProps.loggedIn && !isLoggedIn) {
      // logout
      this.props.unsubscribePushNotifications();
    }

    if (isLoggedIn && this.props.hasPushNotifications) {
      const lastNotification = prevProps.lastNotification;
      if (
        !lastNotification ||
        (lastNotification &&
          this.props.lastNotification.receivedTimeStamp !== lastNotification.receivedTimeStamp)
      ) {
        const isForeground = this.props.lastNotification.additionalData.foreground;

        console.log(
          `A new ${isForeground ? 'FOREGROUND' : 'BACKGROUND'} notification`,
          this.props.lastNotification
        );
        isForeground
          ? this._handleForegroundNotification(this.props)
          : this._handleBackgroundNotification(this.props);
      }
    }
  }

  render() {
    return <div className="pnHandler" style={{ display: 'none' }} />;
  }
}

export default withRouter(enhance(PNHandler));
