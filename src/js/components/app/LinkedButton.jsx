import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class LinkedButton extends Component {
  static defaultProps = {
    to: '/',
    replace: true
  };

  render() {
    const { to, replace, children, disabled, ...props } = this.props;
    const InnerComponent = disabled ? 'span' : Link;
    const linkProps = disabled ? {} : { to, replace };
    return (
      <button disabled={disabled} {...props}>
        <InnerComponent {...linkProps}>{children}</InnerComponent>
      </button>
    );
  }
}
