import React, { Component } from 'react';
import { compose } from 'redux';
// import { routesConfig } from 'js/startup';
// import { default as RouteRedirector } from 'js/startup/RouteRedirector';
import { Navigator } from 'js/components/Navigator';
import { Router, Route } from 'react-router-dom';

import { Drawer } from './drawer';
import Modal from './Modal';
import SnackBar from './SnackBar';
import LoadingOverlay from './LoadingOverlay';
import { withMessages } from 'js/modules/ui';
import { withAppStatus } from 'js/modules/appStatus';
import { withMap, withMapZoom } from 'js/modules/map';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import PNHandler from './PNHandler';
import AuthorityLocationUpdater from './AuthorityLocationUpdater';
import { FeatureSelectionContext } from 'js/components/app/FeatureSelectionContext';
import styled from 'styled-components';
import { isIREACTPointFeature } from 'js/utils/ireactFeatureComparison';

const enhance = compose(
  withMessages,
  withMapZoom,
  withMap,
  withAppStatus
);

const LayoutContainer = styled.div`
  /* width: 100vw;
  height: 100vh; */
  position: absolute;
  margin: 0;
  top: 0;
  left: 0;
  color: ${props => props.theme.palette.textColor};
  background-color: ${props => props.theme.palette.canvasColor};
`;

const windowStyle = {
  width: window.innerWidth,
  height: window.innerHeight,
  maxWidth: window.innerWidth,
  maxHeight: window.innerHeight
};

// const navigatorStyle = {
//   width: '100%',
//   height: '100%',
//   maxWidth: '100%',
//   maxHeight: '100%',
//   overflow: 'hidden'
// };

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      featureDetailsLoading: 0,
      clickedPoint: null, // Array[lng, lat]
      // only id: number and itemType: string wrapped in properties: {} for GeoJSON compatibility
      // plus coordinates for displaying on maps and so on
      // hoveredFeature: null,
      selectedFeature: null,
      // Mission task Management
      // hoveredMissionTask: null,
      selectedMissionTask: null,
      // State updaters
      setClickedPoint: this.setClickedPoint,
      selectFeature: this.selectFeature,
      selectMissionTask: this.selectMissionTask,
      deselectFeature: this.deselectFeature,
      deselectMissionTask: this.deselectMissionTask,
      setFeatureDetailsLoading: this.setFeatureDetailsLoading
    };
  }

  _determineClickedPointFromPreviousState = (point, previousState) =>
    previousState.clickedPoint ? null : Array.isArray(point) && point.length === 2 ? point : null;

  setClickedPoint = point => {
    this.setState(previousState => {
      const clickedPoint = this._determineClickedPointFromPreviousState(point, previousState);
      return { ...previousState, clickedPoint };
    });
  };

  setFeatureDetailsLoading = loading => {
    this.setState(prevState => ({
      featureDetailsLoading: loading
        ? prevState.featureDetailsLoading + 1
        : Math.max(0, prevState.featureDetailsLoading - 1)
    }));
  };

  selectFeature = feature => {
    this.setState(previousState => {
      const { id, itemType } = feature.properties;
      const { coordinates } = feature.geometry;
      let selectedMissionTask = null;
      // let hoveredMissionTask = null;

      if (itemType === 'mission') {
        if (
          previousState.selectedMissionTask &&
          previousState.selectedMissionTask.properties.missionId === id
        ) {
          selectedMissionTask = previousState.selectedMissionTask;
        }
        // if (
        //   previousState.hoveredMissionTask &&
        //   previousState.hoveredMissionTask.properties.missionId === id
        // ) {
        //   hoveredMissionTask = previousState.hoveredMissionTask;
        // }
      }
      if (isIREACTPointFeature(itemType)) {
        this.props.zoomToFeature(coordinates);
      } else {
        this.props.updateMapSettings({ center: coordinates });
      }
      return {
        selectedFeature: { properties: { id, itemType }, coordinates },
        // hoveredFeature: null,
        selectedMissionTask,
        // hoveredMissionTask,
        clickedPoint: null
      };
    });
    return feature;
  };

  deselectFeature = point => {
    this.setState(previousState => {
      if (previousState.selectedFeature == null) {
        const clickedPoint = this._determineClickedPointFromPreviousState(point, previousState);
        return { ...previousState, clickedPoint };
      } else {
        return { ...previousState, selectedFeature: null, selectedMissionTask: null };
      }
    });
  };

  selectMissionTask = taskFeature => {
    if (this.state.selectedFeature) {
      const { /* id, */ itemType, missionId } = taskFeature.properties;
      if (itemType === 'missionTask' && missionId === this.state.selectedFeature.properties.id) {
        const { coordinates } = taskFeature.geometry;
        this.setState({
          selectedMissionTask: { properties: taskFeature.properties, coordinates }
        });
      }
    }
  };

  deselectMissionTask = () => {
    this.setState({ selectedMissionTask: null });
  };

  // Read https://reactjs.org/blog/2017/07/26/error-handling-in-react-16.html
  componentDidCatch(error, info) {
    console.error('Top Level Component error', error, info);
    // Here all exception are critical because it means unhandled by other views
    appInsights.trackException(
      error,
      'Layout::componentDidCatch',
      info,
      {},
      SeverityLevel.Critical
    );
    this.props.pushError(error);
  }

  render() {
    const { isAppForeground, onRouterError, history } = this.props;
    const { selectedFeature, selectFeature, deselectFeature } = this.state;
    const featureSelection = { selectedFeature, selectFeature, deselectFeature };
    const cordovaChildren =
      IS_CORDOVA || !IS_PRODUCTION ? <PNHandler {...featureSelection} /> : null;

    // const rc = routesConfig(IS_CORDOVA ? cordova.file.applicationDirectory : null);
    return (
      <Router onRouterError={onRouterError} history={history}>
        <LayoutContainer className="layout" style={windowStyle}>
          <FeatureSelectionContext.Provider value={this.state}>
            <Navigator fileAppDirectory={IS_CORDOVA ? cordova.file.applicationDirectory : null} />
          </FeatureSelectionContext.Provider>
          {isAppForeground && <Route render={props => <Drawer {...props} />} />}
          {isAppForeground && <Route render={props => <Modal {...props} />} />}
          {isAppForeground && <SnackBar />}
          {isAppForeground && <LoadingOverlay />}
          <AuthorityLocationUpdater />
          {cordovaChildren}
        </LayoutContainer>
      </Router>
      // <AuthorityLocationUpdater key={1} />,
      // <RouteRedirector key={2} routesConfig={rc} />,
      // ...cordovaChildren
    );
  }
}

export default enhance(Layout);
