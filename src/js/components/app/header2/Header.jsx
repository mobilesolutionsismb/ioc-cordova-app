import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {
  MenuButton,
  CloseButton as CloseButtonInner,
  BackButton as BackButtonInner
} from '../HeaderButton';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { withDrawer } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import { getAsset } from 'js/utils/getAssets';
import styled from 'styled-components';
import { DEFAULT_PAGE_AUTHENTICATED } from 'js/startup/routesConfig';

const IREACT_LOGO = getAsset('logoTextClear', true);

const LOGO_SIZE_H = 24;
const LOGO_SIZE_W = 92;
const HEADER_HEIGHT = 64;

const STYLES = {
  header: {
    width: '100%',
    height: HEADER_HEIGHT,
    minHeight: HEADER_HEIGHT,
    maxHeight: HEADER_HEIGHT,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    transition: 'height, max-height, opacity 350ms ease-in-out'
  },
  content: {
    position: 'relative',
    width: '100%',
    height: `calc(100% - ${HEADER_HEIGHT}px)`
  },
  logo: {
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: IREACT_LOGO,
    width: `${LOGO_SIZE_W}px`,
    minWidth: `${LOGO_SIZE_W / 2}px`,
    height: `${LOGO_SIZE_H}px`,
    backgroundSize: 'contain',
    display: 'inline-block',
    marginLeft: 0,
    marginRight: 'auto',
    padding: 12,
    position: 'relative'
  }
};

const DrawerButton = withDrawer(({ appDrawerIsOpen, closeDrawer, openDrawer }) => (
  <MenuButton
    onClick={() => {
      appDrawerIsOpen ? closeDrawer() : openDrawer();
    }}
  />
));

const CloseButton = withRouter(({ history }) => (
  <CloseButtonInner
    onClick={() => {
      history.goBack();
    }}
  />
));

const BackButton = withRouter(({ history }) => (
  <BackButtonInner
    onClick={() => {
      history.goBack();
    }}
  />
));

/**
 *
 * @param {String} buttonType (drawer|back or close|clear)
 * @param {Function} onHeaderButtonClick (optional custom handler instead of default (for baclkbutton))
 */
function getButtonType(buttonType, onHeaderButtonClick = null) {
  let button = null;
  switch (buttonType) {
    case 'drawer':
      button = <DrawerButton />;
      break;
    case 'back':
      button =
        onHeaderButtonClick === null ? (
          <BackButton />
        ) : (
          <BackButtonInner onClick={onHeaderButtonClick} />
        );
      break;
    case 'close':
    case 'clear':
      button =
        onHeaderButtonClick === null ? (
          <CloseButton />
        ) : (
          <CloseButtonInner onClick={onHeaderButtonClick} />
        );
      break;
    default:
      break;
  }
  return button;
}

const Header = ({
  showLogo, //boolean -> show app logo
  leftButtonType, // see getButtonType
  rightButtonType, // see getButtonType
  leftButton, // left button or element, takes precedence over leftButtonType
  rightButton, // right button or element, takes precedence over rightButtonType
  title, // string or element
  muiTheme, // from muiThemeable -> default styling
  dictionary, // from withDictionary
  style, // custom style
  onLeftHeaderButtonClick, // see getButtonType
  onRightHeaderButtonClick, // see getButtonType
  primary,
  secondary
}) => (
  <div
    className="top-header"
    style={{
      ...STYLES.header,
      color: muiTheme.palette.textColor,
      backgroundColor:
        primary === true
          ? muiTheme.palette.primary2Color
          : secondary === true
          ? muiTheme.palette.accent2Color
          : muiTheme.palette.backgroundColor,
      ...style
    }}
  >
    {React.isValidElement(leftButton)
      ? leftButton
      : typeof leftButtonType !== 'undefined' &&
        getButtonType(leftButtonType, onLeftHeaderButtonClick)}
    {showLogo === true && !React.isValidElement(title) && (
      <Link
        to={DEFAULT_PAGE_AUTHENTICATED}
        replace={true}
        className="title-logo"
        style={STYLES.logo}
      />
    )}
    {React.isValidElement(title)
      ? title
      : typeof title === 'string' && (
          <span style={{ marginRight: 'auto' }}>{dictionary(title)}</span>
        )}
    {React.isValidElement(rightButton)
      ? rightButton
      : typeof rightButtonType !== 'undefined' &&
        getButtonType(rightButtonType, onRightHeaderButtonClick)}
  </div>
);

export default withDictionary(muiThemeable()(Header));

export const Content = styled.div`
  position: relative;
  width: 100%;
  height: calc(100% - ${HEADER_HEIGHT}px);
`;
// export const Content = ({ children, style, ...props }) => (
//   <div style={{ ...STYLES.content, ...style }} {...props}>
//     {children}
//   </div>
// );
