import React from 'react';
import styled from 'styled-components';

export const Emoji = styled.span.attrs(props => ({
  role: 'img'
}))`
  font-size: ${props => 0.75 * props.size}px;
  background-color: rgb(255, 235, 0);
  width: ${props => props.size}px;
  height: ${props => props.size}px;
  text-align: center;
  border-radius: 100%;
`;

const ComingSoonContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  height: auto;
  max-height: 100%;
  flex-grow: 1;
`;

export function ComingSoon({ size = 128 }) {
  return (
    <ComingSoonContainer>
      <Emoji aria-label="Pick" children="⛏️" size={size} />
      <p>This I-REACT feature is coming soon!</p>
    </ComingSoonContainer>
  );
}
