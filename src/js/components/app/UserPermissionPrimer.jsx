import React from 'react';
import { switchToSettings } from 'js/modules/devicePermissions';
import { FontIcon, FlatButton } from 'material-ui';
import styled from 'styled-components';

const PRIMER_TEXT_TYPES = {
  location: '_geol_permission_primer_message',
  camera: '_camera_permission_primer_message'
};

const PrimerBodyContainer = styled.div.attrs(props => ({ className: 'primer-container' }))`
  width: 100%;
  .text {
    width: 100%;
    margin-bottom: 8px;
  }
`;

const settingsLabel = '_permissions_settings';

export function UserPermissionPrimer({ type, denied = false, dictionary }) {
  const text = PRIMER_TEXT_TYPES[type];
  return (
    <PrimerBodyContainer>
      <div className="text">{dictionary(text)}</div>
      {denied && (
        <FlatButton
          style={{ padding: 0 }}
          icon={<FontIcon className="material-icons">settings_applications</FontIcon>}
          primary={true}
          className="settings-button"
          label={dictionary(settingsLabel)}
          onClick={switchToSettings}
        />
      )}
    </PrimerBodyContainer>
  );
}

export function UserGeolocationTurnedOff({ dictionary }) {
  return (
    <PrimerBodyContainer>
      <div className="text">{dictionary._geol_turned_off_text}</div>
    </PrimerBodyContainer>
  );
}
