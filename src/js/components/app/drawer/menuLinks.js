/**
 * MenuLinks config: Array of POJOs
 * mandatory attribues: link
 * link === null --> Divider
 * link === string --> NavLink
 * link === false --> ListItem disabled
 *
 * needsLocation: true => disabled if liveAOI = null
 */

export const menuLinks = [
  {
    label: '_home',
    link: '/home', //'/learnermenu',
    icon: 'home',
    needsLocation: false
  },
  {
    label: '_new_report',
    link: '/startCapture',
    icon: 'newreport',
    needsLocation: true
  },
  // {
  //   label: '_drawer_label_dashboard',
  //   link: '/dashboard',
  //   icon: 'dashboard',
  //   needsLocation: true
  // },
  {
    label: '_drawer_label_social',
    link: '/tabs/social',
    icon: 'social',
    needsLocation: true
  },
  {
    link: null
  },
  {
    label: '_map',
    link: '/tabs/map', //'/learnermenu',
    icon: 'map',
    needsLocation: true
  },
  {
    label: '_layers',
    link: '/settings/map',
    icon: 'layers',
    needsLocation: true
  },
  // {
  //   label: '_layers_legend',
  //   link: '/layerslegend',
  //   icon: 'legend'
  // },
  {
    link: null
  },
  {
    label: '_drawer_label_mission',
    link: '/missions',
    icon: 'missions',
    userType: ['authority'], // add others if any
    needsLocation: true
  },
  {
    label: '_drawer_label_notifications',
    link: '/tabs/emergencyCommunications',
    icon: 'notifications',
    needsLocation: true
  },
  // {
  //     label: '_drawer_label_notifications',
  //     link: '/tabs/notifications',
  //     icon: 'notifications'
  // },
  // {
  //     label: '_drawer_label_events',
  //     link: '/tabs/events',
  //     icon: 'events'
  // },
  {
    label: '_drawer_label_reports',
    link: '/tabs/reports',
    icon: 'reports',
    needsLocation: true
  },
  {
    link: null,
    userType: 'citizen'
  },
  {
    label: '_achievements',
    link: '/achievements',
    icon: 'achievements',
    userType: 'citizen',
    needsLocation: false
  },
  {
    label: '_tips_and_quizzes',
    link: '/learner',
    icon: 'learner',
    userType: 'citizen',
    needsLocation: false
  },
  {
    link: null
  },
  // {
  //   label: '_invite',
  //   link: false, //'/social',
  //   icon: 'invite'
  // },
  {
    label: '_settings',
    link: '/settings',
    icon: 'settings',
    needsLocation: false
  },
  {
    link: null
  },
  {
    label: '_about',
    link: '/about',
    icon: 'about',
    needsLocation: false
  },
  {
    label: '_help',
    link: '/help',
    icon: 'help',
    needsLocation: false
  },
  {
    link: null
  },
  {
    label: '_signout',
    link: '/logout',
    icon: 'logout',
    needsLocation: false
  }
];
