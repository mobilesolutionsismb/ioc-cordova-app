import React, { Component } from 'react';
import { List, ListItem, Divider, FontIcon } from 'material-ui';
import { NavLink } from 'react-router-dom';
import { menuLinks } from './menuLinks';
import { iReactTheme } from 'js/startup/iReactTheme';
import { withLogin, withAPISettings } from 'ioc-api-interface';
import { withDictionary } from 'ioc-localization';
import { compose } from 'redux';

function checkUserType(userType, menuListUserType) {
  if (Array.isArray(menuListUserType)) {
    return menuListUserType.indexOf(userType) > -1;
  } else {
    return menuListUserType === userType;
  }
}

const STYLES = {
  leftFontIcon: {
    position: 'absolute',
    top: 0,
    margin: 12,
    left: 4
  },
  activeLink: {
    color: iReactTheme.palette.textColor,
    backgroundColor: iReactTheme.palette.primary1Color
  },
  disabled: {
    color: 'rgba(255,255,255,0.37)'
  }
};

/**
 * Factory function that returns a FontIcon with optional disabled style (prop)
 * @param {String} iconText - material design icon text (see https://material.io/icons/)
 */
export const MenuIcon = (iconText, type = 'material') => ({ disabled, style, color }) => (
  <FontIcon
    color={color}
    style={
      disabled
        ? {
            ...STYLES.leftFontIcon,
            ...STYLES.disabled,
            ...style
          }
        : { ...STYLES.leftFontIcon, ...style, color: color || iReactTheme.palette.textColor }
    }
    className={
      type === 'mdi' ? `mdi mdi-${iconText}` : type === 'material' ? 'material-icons' : type
    }
  >
    {type === 'mdi' ? '' : iconText}
  </FontIcon>
);

// const ImageIcon = styled.span.attrs(props => ({ className: 'image-icon' }))`
//   background-image: ${props => props.src};
//   background-size: contain;
//   width: 30px;
//   height: 30px;
//   background-repeat: no-repeat;
//   position: absolute;
//   font-size: 24px;
//   display: inline-block;
//   user-select: none;
//   transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
//   top: 0px;
//   margin: 6px;
//   left: 4px;
// `;

const ICONS = {
  home: MenuIcon('home'),

  newreport: MenuIcon('photo_camera'),

  // dashboard: MenuIcon('dashboard'),
  social: MenuIcon('twitter', 'mdi'),

  map: MenuIcon('room'),
  layers: MenuIcon('layers'),

  // legend: MenuIcon('list'),

  missions: MenuIcon('\ue908', 'ireact-pinpoint-icons'),
  notifications: MenuIcon('notifications'),
  // events: MenuIcon('date_range'),
  reports: MenuIcon('view_list'),

  achievements: MenuIcon('account-star', 'mdi'),
  learner: MenuIcon('lightbulb-on', 'mdi'),

  // invite: MenuIcon('group_add'),
  settings: MenuIcon('settings'),

  about: MenuIcon('\ue918', 'ireact-icons'), //() => <ImageIcon src={getAsset('logo', true)} />,
  // about: MenuIcon('info'),
  help: MenuIcon('help'),

  logout: MenuIcon('exit_to_app')
};

class DrawerLink extends Component {
  render() {
    const { style, to, children, ...rest } = this.props;
    return (
      <NavLink
        style={style}
        to={to}
        activeClassName={'current'}
        activeStyle={STYLES.activeLink}
        strict={true}
        exact={true}
        {...rest}
      >
        {children}
      </NavLink>
    );
  }
}

class DrawerBody extends Component {
  _getNavLinkFor = (el, ...rest) => <DrawerLink to={el.link} {...rest} />;

  _getMenuLinks = (el, i) => {
    const { liveAOI, closeDrawer, dictionary } = this.props;
    const linkDisabled = el.needsLocation === true && liveAOI === null;
    if (el.link === null) {
      return <Divider key={i} inset={false} />;
    } else if (el.link === false) {
      const Icon = ICONS[el.icon];
      return (
        <ListItem
          key={i}
          onClick={closeDrawer}
          primaryText={<div className="app-drawer__body-label">{dictionary(el.label)}</div>}
          innerDivStyle={STYLES.disabled}
          disabled={true}
          leftIcon={<Icon disabled={true} />}
        />
      );
    } else {
      const Icon = ICONS[el.icon];
      return (
        <ListItem
          key={i}
          onClick={closeDrawer}
          primaryText={<div className="app-drawer__body-label">{dictionary(el.label)}</div>}
          disabled={linkDisabled}
          innerDivStyle={linkDisabled ? STYLES.disabled : {}}
          containerElement={linkDisabled ? 'div' : this._getNavLinkFor(el)}
          leftIcon={<Icon disabled={linkDisabled} color={iReactTheme.palette.textColor} />}
        />
      );
    }
  };

  render() {
    const userType = this.props.user ? this.props.user.userType : null;
    return (
      <div className="app-drawer__body">
        {userType && (
          <List>
            {menuLinks
              // if no userType or if userType is current user type
              .filter(ml => !ml.hasOwnProperty('userType') || checkUserType(userType, ml.userType))
              .map(this._getMenuLinks)}
          </List>
        )}
      </div>
    );
  }
}

const enhance = compose(
  withLogin,
  withAPISettings,
  withDictionary
);

export default enhance(DrawerBody);
