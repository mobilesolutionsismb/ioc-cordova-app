import './styles.scss';
import React, { Component } from 'react';

import { Drawer } from 'material-ui';
import { AchievementsHeader } from 'js/components';
import DrawerBody from './DrawerBody';

import { withDrawer } from 'js/modules/ui';
import { withLogin } from 'ioc-api-interface';

const BASE_CLASS_NAME = 'app-drawer';

class AppDrawer extends Component {
  onRequestChange = open => {
    open ? this.props.openDrawer() : this.props.closeDrawer();
  };

  render() {
    // console.log('AppDrawer',this.props.location);
    return this.props.loggedIn ? (
      <Drawer
        disableSwipeToOpen={true}
        className={BASE_CLASS_NAME}
        containerStyle={{ overflow: 'hidden' }}
        docked={false}
        open={this.props.appDrawerIsOpen}
        onRequestChange={this.onRequestChange}
      >
        <AchievementsHeader
          baseClassName={BASE_CLASS_NAME}
          open={this.props.appDrawerIsOpen}
          closeDrawer={this.props.closeDrawer}
          history={this.props.history}
          avatarIsLink={true}
        />
        <DrawerBody
          search={this.props.location.search}
          path={this.props.location.pathname}
          closeDrawer={this.props.closeDrawer}
        />
      </Drawer>
    ) : null;
  }
}

export default withLogin(withDrawer(AppDrawer));
