import React from 'react';
import { IconButton, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { withDictionary } from 'ioc-localization';

export const HeaderButton = (iconText, tooltip) =>
  muiThemeable()(
    withDictionary(({ muiTheme, onClick, dictionary, style, iconStyle }) => (
      <IconButton
        className="menu-button"
        onClick={onClick}
        tooltip={dictionary(tooltip)}
        style={style}
        iconStyle={iconStyle}
      >
        <FontIcon className="material-icons" color={muiTheme.palette.textColor}>
          {iconText}
        </FontIcon>
      </IconButton>
    ))
  );

export const BackButton = HeaderButton('arrow_back', '_back');
export const CloseButton = HeaderButton('clear', '_close');
export const MenuButton = HeaderButton('menu', '_menu');
