import React, { Component } from 'react';
import { NavigatorBody } from 'js/components/app/commons';
import { Route, Switch, Redirect } from 'react-router-dom';
import {
  Login,
  Logout,
  Registration,
  TermsAndConditions,
  SMSValidation,
  EmailValidation,
  Home,
  MapTabs,
  // Dashboard,
  Social,
  ReportDetails,
  EmergencyCommunicationDetails,
  Achievements,
  CongratsPage,
  Learner,
  ProgressPage,
  CardsPlayer,
  UserProfile,
  PasswordChange,
  About,
  Help,
  Settings,
  TimeWindowSetting,
  LanguageSettings,
  MapSettings,
  PasswordForgot,
  NewReportCapture,
  WebcamCapture,
  NewReport,
  HazardSelect,
  PickPositionFromMap,
  ContentSelect,
  CategorySelect,
  ContentValue,
  Missions,
  LayersLegend,
  Wearable
} from 'js/components';
import { IS_ANDROID, IS_IOS } from 'js/utils/getAppInfo';
import HardwareBackButtonHandler from 'js/components/app/HardwareBackButtonHandler';

export const DEFAULT_PAGE_AUTHENTICATED = '/home';
export const DEFAULT_PAGE_UNAUTHENTICATED = '/';

export class Navigator extends Component {
  render() {
    const { fileAppDirectory } = this.props;
    return (
      <NavigatorBody>
        <Route
          render={({ location }) => (
            <Switch location={location}>
              <Route path={DEFAULT_PAGE_AUTHENTICATED} exact={true}>
                <Home />
              </Route>
              {/* <Route path="/dashboard" exact={true}>
                <Dashboard />
              </Route> */}
              <Route path="/social/:id" exact={true}>
                <Social />
              </Route>
              <Redirect exact={true} to="/tabs/map" from="/tabs" strict={true} />
              <Redirect exact={true} to="/tabs/map" from="/tabs/" strict={true} />
              <Route path="/tabs/:tabname" exact={true}>
                <MapTabs />
              </Route>
              <Route path="/reportdetails/:id" exact={true}>
                <ReportDetails />
              </Route>
              <Route path="/emcommdetails/:id" exact={true}>
                <EmergencyCommunicationDetails />
              </Route>
              <Route path="/congratulations" exact={true}>
                <CongratsPage />
              </Route>
              <Redirect
                exact={true}
                to="/achievements/personal/skills"
                from="/achievements"
                strict={false}
              />
              <Route path="/achievements/:tabname/:cardname?" exact={true}>
                <Achievements />
              </Route>

              <Route
                path="/learner"
                exact={false}
                render={({ location }) => (
                  <Switch location={location}>
                    <Route path="/learner/:type" exact={true}>
                      <ProgressPage />
                    </Route>
                    <Route path="/learner/:type/cards" exact={true}>
                      <CardsPlayer />
                    </Route>
                    <Route path="/" exact={false}>
                      <Learner />
                    </Route>
                  </Switch>
                )}
              />

              <Route path="/editprofile" exact={true}>
                <UserProfile />
              </Route>

              <Route
                path="/editpassword"
                exact={true}
                render={({ history }) => <PasswordChange history={history} />}
              />

              <Route
                path="/settings"
                exact={false}
                render={({ location }) => (
                  <Switch location={location}>
                    <Route path="/settings/timewindow" exact={true}>
                      <TimeWindowSetting />
                    </Route>
                    <Route path="/settings/language" exact={true}>
                      <LanguageSettings />
                    </Route>
                    <Route path="/settings/map" exact={true}>
                      <MapSettings />
                    </Route>
                    <Route path="/settings/wearable" exact={true}>
                      <Wearable />
                    </Route>
                    <Route path="/" exact={false}>
                      <Settings />
                    </Route>
                  </Switch>
                )}
              />

              <Route path="/webcamCapture" exact={true}>
                <WebcamCapture />
              </Route>

              <Route path="/startCapture" exact={true}>
                <NewReportCapture />
              </Route>

              <Route
                path="/newreport"
                exact={false}
                render={({ location }) => (
                  <Switch location={location}>
                    <Route path="/newreport/hazardSelect/contentSelect" exact={true}>
                      <ContentSelect />
                    </Route>
                    <Route path="/newreport/hazardSelect" exact={true}>
                      <HazardSelect />
                    </Route>
                    <Route path="/newreport/categorySelect/:contentType" exact={true}>
                      <CategorySelect />
                    </Route>
                    <Route path="/newreport/contentValue/:contentType" exact={true}>
                      <ContentValue />
                    </Route>
                    <Route path="/newreport/pickReportPosition" exact={true}>
                      <PickPositionFromMap />
                    </Route>
                    <Route path="/newreport" exact={true}>
                      <NewReport />
                    </Route>
                  </Switch>
                )}
              />

              <Route path="/missions" exact={true}>
                <Missions />
              </Route>
              <Route path="/missiondetails/:id" exact={true} strict={true}>
                <Missions />
              </Route>
              <Route path="/missiondetails/:id/:taskId" exact={true} strict={true}>
                <Missions />
              </Route>

              <Route path="/layerslegend" exact={true}>
                <LayersLegend />
              </Route>

              <Route path="/help" exact={true}>
                <Help />
              </Route>

              <Route path="/about" exact={true}>
                <About />
              </Route>

              <Route path="/logout" exact={true}>
                <Logout />
              </Route>

              {IS_ANDROID && (
                <Redirect from="/android_asset/www/index.html" to={DEFAULT_PAGE_UNAUTHENTICATED} />
              )}

              {IS_IOS && fileAppDirectory && (
                <Redirect
                  from={`${fileAppDirectory.replace(/^file:\/\//, '')}www/index.html`}
                  to={DEFAULT_PAGE_UNAUTHENTICATED}
                />
              )}

              <Route
                path={DEFAULT_PAGE_UNAUTHENTICATED}
                render={({ location }) => (
                  <Switch location={location}>
                    <Redirect to={DEFAULT_PAGE_UNAUTHENTICATED} from="/login" strict={true} />
                    <Route path="/registration" exact={true} component={Registration} />
                    <Route path="/smsvalidation" exact={true} component={SMSValidation} />
                    <Route path="/emailvalidation" exact={true} component={EmailValidation} />
                    <Route path="/passwordForgot" exact={true} component={PasswordForgot} />
                    <Route path="/terms" exact={true} component={TermsAndConditions} />
                    <Route path={DEFAULT_PAGE_UNAUTHENTICATED} exact={true} component={Login} />
                  </Switch>
                )}
              />
              <Route render={() => <h1>Not Found :( </h1>} />
            </Switch>
          )}
        />
        {IS_ANDROID && <HardwareBackButtonHandler />}
      </NavigatorBody>
    );
  }
}
