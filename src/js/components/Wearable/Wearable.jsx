import React, { Component } from 'react';
import { withBluetooth } from 'js/modules/bluetooth/';
import WearableBTEnable from './WearableBTEnable';
import WearableDetails from './WearableDetails';
import WearableSelectionList from './WearableSelectionList';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';

class Wearable extends Component {
  static defaultProps = {
    devices: [],
    selectedDevice: null
  };

  componentDidMount() {
    this.props.checkBTEnabled();
  }

  componentDidCatch(err, info) {
    appInsights.trackException(err, 'Wearable::_updateAreaIfNeeded', info, {}, SeverityLevel.Error);
    // Clear BT State, a drastic decision
    this.props.resetBTAll();
  }

  render() {
    const {
      // BT State
      isScanning,
      isConnected,
      isConnecting,
      isBtEnabled,
      isDeviceAvailable,
      selectedDevice,
      devices,
      isNotifying,
      rssiValue,
      // BT ACs
      checkBTEnabled,
      checkBTIsConnected,
      startScan,
      stopScan,
      connectDevice,
      disconnectDevice,
      bleMessages,
      lastBleMessage,
      startBTMessageListening,
      stopBTMessageListening,
      clearMessages
    } = this.props;

    const btProps = {
      // BT State
      isScanning,
      isConnecting,
      isConnected,
      isBtEnabled,
      isDeviceAvailable,
      selectedDevice,
      devices,
      isNotifying,
      rssiValue,
      // BT ACs
      checkBTEnabled,
      checkBTIsConnected,
      startScan,
      stopScan,
      connectDevice,
      disconnectDevice,
      bleMessages,
      lastBleMessage,
      startBTMessageListening,
      stopBTMessageListening,
      clearMessages
    };
    if (isDeviceAvailable) {
      return <WearableDetails {...btProps} />;
    } else {
      if (!isBtEnabled) {
        return <WearableBTEnable {...btProps} />;
      } else {
        if (selectedDevice === null) {
          return <WearableSelectionList {...btProps} />;
        } else {
          // TODO better handling of this
          return <div>Oops, something is wrong here</div>;
        }
      }
    }
  }
}

export default withBluetooth(Wearable);
