import React, { Component } from 'react';
import { Page, ColumnPage } from 'js/components/app/commons';
import { BTHSettingHeader, BT_TEXT_COLOR, BT_HEADER_COLOR } from './commons';
import { RaisedButton, IconButton } from 'material-ui';
import ClearAll from 'material-ui/svg-icons/communication/clear-all';
import PlayArrow from 'material-ui/svg-icons/av/play-arrow';
import Pause from 'material-ui/svg-icons/av/pause';
import { colorMap, closestIndex } from 'js/utils/arrayUtils';
import { logMain, logSevereWarning } from 'js/utils/log';
import styled from 'styled-components';
import BatteryUnknown from 'material-ui/svg-icons/device/battery-unknown';

import BatteryAlert from 'material-ui/svg-icons/device/battery-alert';
import Battery20 from 'material-ui/svg-icons/device/battery-20';
import Battery30 from 'material-ui/svg-icons/device/battery-30';
import Battery50 from 'material-ui/svg-icons/device/battery-50';
import Battery60 from 'material-ui/svg-icons/device/battery-60';
import Battery80 from 'material-ui/svg-icons/device/battery-80';
import Battery90 from 'material-ui/svg-icons/device/battery-90';
import BatteryFull from 'material-ui/svg-icons/device/battery-full';
import FiberManual from 'material-ui/svg-icons/av/fiber-manual-record';

const btnStyle = {
  margin: 12
};

const devStatusStyle = {
  ...btnStyle,
  width: 'calc(100% - 24px)'
};

const CHECK_CONN_INTERVAL = 2 * 60 * 1000;

const MessageArea = styled.div`
  width: calc(100% - 24px);
  height: 40%;
  max-height: 40%;
  line-break: normal;
  white-space: pre-wrap;
  word-break: break-all;
  overflow-x: hidden;
  overflow-y: auto;
  outline: 1px dotted ${BT_TEXT_COLOR};
  background-color: rgba(255, 255, 255, 0.45);
`;

const MessageItem = styled.div`
  width: 100%;
  margin: 4px 0;
  font-family: 'Courier New', Courier, monospace;
  color: ${BT_HEADER_COLOR};
  border-bottom: 1px solid ${BT_HEADER_COLOR};
  &:last-child {
    border-bottom-color: transparent;
  }
  box-sizing: border-box;
`;

const Row = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: ${props => (props.centerd ? 'center' : 'flex-start')};
  flex-direction: row;
`;

const Column = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  flex-direction: column;
`;

const batteryStatusColorMap = colorMap(10).reverse();

/*
21 percent Normal outside air
17 percent Impaired judgment and coordination
12 percent Headache, dizziness, nausea, fatigue
9 percent Unconsciousness
6 percent Respiratory arrest, cardiac arrest, death
 */
const OXYGEN_THRESHOLDS = [6, 9, 12, 17, 21];
// See http://www.geography.hunter.cuny.edu/tbw/wc.notes/1.atmosphere/oxygen_and_human_requirements.htm
// const OXYGEN_THRESHOLDS = [6, 8, 10, 12, 15, 19.5];
const oxygenColorMap = colorMap(OXYGEN_THRESHOLDS.length).reverse();

function getBatteryColor(value) {
  const index = isNaN(value)
    ? 5
    : Math.min(Math.floor(value / 10), batteryStatusColorMap.length - 1);
  return batteryStatusColorMap[index];
}

function getBatteryIconComponent(value) {
  if (isNaN(value)) {
    return BatteryUnknown;
  } else {
    return value > 90
      ? BatteryFull
      : value > 80
      ? Battery90
      : value > 60
      ? Battery80
      : value > 50
      ? Battery60
      : value > 30
      ? Battery50
      : value > 20
      ? Battery30
      : value > 0
      ? Battery20
      : BatteryAlert;
  }
}

function getOxygenColor(value) {
  return typeof value === 'number'
    ? oxygenColorMap[closestIndex(OXYGEN_THRESHOLDS, value)]
    : '#fff';
}

const DeviceStatus = ({ message }) => {
  if (!message) {
    return <Column />;
  } else {
    const { devStatus, envir, navData } = message;
    const batt = devStatus.batt;
    const BatteryIcon = getBatteryIconComponent(batt);
    const batteryIconColor = getBatteryColor(batt);
    // console.log('batteryIconColor', batt, batteryIconColor);
    const { H, O2, P, T } = envir;
    const { alt, lat, lon, valid } = navData;
    const oxygenColor = getOxygenColor(O2);
    return (
      <Column style={devStatusStyle}>
        <Row>
          Battery: {batt}% <BatteryIcon color={batteryIconColor} />
        </Row>
        <Row>Temperature: {T.toFixed(1)}°C</Row>
        <Row>Humidity: {H.toFixed(1)}%</Row>
        <Row>Pressure: {P.toFixed(2)} mBar</Row>
        {O2 && (
          <Row>
            Oxygen: {O2.toFixed(2)}% <FiberManual color={oxygenColor} />
          </Row>
        )}
        {valid === 1 && (
          <Column>
            <Row>Altitude: {alt.toFixed(1)} m</Row>
            <Row>
              Position: {lat.toFixed(5)}, {lon.toFixed(5)}
            </Row>
          </Column>
        )}
      </Column>
    );
  }
};

class WereableDetails extends Component {
  _connectionCheckInterval = null;
  _autoRef = null;

  _checkConnected = async () => {
    try {
      const result = await this.props.checkBTIsConnected();
      logMain('IS CONNECTED', result);
    } catch (err) {
      logSevereWarning(err);
    }
  };

  _checkConnectedPeriodically = () => {
    if (this._autoRef) {
      this._checkConnected();
      this._connectionCheckInterval = setInterval(this._checkConnected, CHECK_CONN_INTERVAL);
    }
  };

  _clearInterval = () => {
    clearInterval(this._connectionCheckInterval);
    this._connectionCheckInterval = null;
  };

  componentDidMount() {
    this._checkConnectedPeriodically();
  }

  componentWillUnmount() {
    this._clearInterval();
  }

  _setRef = e => {
    this._autoRef = e;
  };

  render() {
    const {
      selectedDevice,
      bleMessages,
      isNotifying,
      startBTMessageListening,
      stopBTMessageListening,
      clearMessages,
      lastBleMessage
    } = this.props;
    return (
      <Page ref={this._setRef} className="wereable-page">
        <BTHSettingHeader title={'_bt_settings'} rightButton={null} />
        <ColumnPage style={{ position: 'relative', height: 'calc(100% - 64px)' }}>
          <p style={btnStyle}>
            Connected to {selectedDevice.name} ({selectedDevice.id})
          </p>
          <DeviceStatus message={lastBleMessage} />
          <Row centerd={true}>
            <RaisedButton
              label="Disconnect"
              secondary={true}
              style={btnStyle}
              onClick={this.props.disconnectDevice}
            />
          </Row>
          <Row style={devStatusStyle}>
            <h4>Raw Device Data</h4>
            <IconButton onClick={isNotifying ? stopBTMessageListening : startBTMessageListening}>
              {isNotifying ? <Pause /> : <PlayArrow />}
            </IconButton>
            <IconButton onClick={clearMessages}>
              <ClearAll />
            </IconButton>
          </Row>
          <MessageArea className="wereable-message-area">
            {bleMessages.map(m => (
              <MessageItem key={m.time}>{JSON.stringify(m)}</MessageItem>
            ))}
          </MessageArea>
        </ColumnPage>
      </Page>
    );
  }
}

export default WereableDetails;
