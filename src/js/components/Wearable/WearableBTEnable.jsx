import React, { Component } from 'react';
import { Page, ColumnPage } from 'js/components/app/commons';
import { BTHSettingHeader } from './commons';
import { RaisedButton } from 'material-ui';
// import { logMain } from 'js/utils/log';
import { IS_ANDROID, IS_IOS } from 'js/utils/getAppInfo';
import { withMessages } from 'js/modules/ui';

/*
 Enable BT or open bluetooth setting is not possible on iOS
 SEE https://github.com/don/cordova-plugin-ble-central#showbluetoothsettings 
 SEE https://github.com/don/cordova-plugin-ble-central#enable
*/

const btnStyle = {
  margin: 12
};

class WerableBTEnable extends Component {
  _enableBluetooth = () => {
    return new Promise((success, failure) => {
      if (IS_ANDROID && window.ble) {
        window.ble.enable(success, failure);
      } else {
        failure(new Error('not supported'));
      }
    });
  };

  _openSettings = async () => {
    try {
      if (IS_ANDROID) {
        await this._enableBluetooth();
        await this.props.checkBTEnabled();
      } else {
        this.props.pushMessage('Operation not supported on this platform');
      }
    } catch (err) {
      this.props.pushError(err);
    }
  };

  render() {
    const { isBTEnabled } = this.props;

    return (
      <Page className="wereable-page">
        <BTHSettingHeader title={'_bt_settings'} rightButton={null} />
        <ColumnPage style={{ position: 'relative', height: 'calc(100% - 64px)' }}>
          {isBTEnabled && <p>Bluetooth Enabled</p>}
          {!isBTEnabled && (
            <div>
              <p>Bluetooth NOT ENABLED</p>
              {IS_ANDROID && (
                <RaisedButton
                  label="Open Bluetooth Settings"
                  primary={true}
                  style={btnStyle}
                  onClick={this._openSettings}
                />
              )}
              {IS_IOS && <p>Please enable Bluetooth in your device settings</p>}
            </div>
          )}
        </ColumnPage>
      </Page>
    );
  }
}

export default withMessages(WerableBTEnable);
