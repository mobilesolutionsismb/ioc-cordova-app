import React, { Component } from 'react';
import { Page, ColumnPage } from 'js/components/app/commons';
import { logMain, logError } from 'js/utils/log';
import { List } from 'react-virtualized';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { CircularProgress, ListItem, Avatar, IconButton } from 'material-ui';
import { getAsset } from 'js/utils/getAssets';
import Bluetooth from 'material-ui/svg-icons/device/bluetooth';
import Refresh from 'material-ui/svg-icons/navigation/refresh';
import Cancel from 'material-ui/svg-icons/navigation/cancel';
import styled from 'styled-components';
import { grey600 } from 'material-ui/styles/colors';
import { BTHSettingHeader } from './commons';
import { withMessages } from 'js/modules/ui';

const themeable = muiThemeable();

const Overlay = styled(ColumnPage)`
  position: absolute;
  background-color: rgba(0, 0, 0, 0.5);
`;

const innerItemStyle = {
  maxHeight: '100%',
  boxSizing: 'border-box'
};

const DeviceAvatar = styled.div.attrs(props => ({ className: 'device-avatar' }))`
  user-select: none;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  font-size: 20px;
  border-radius: 50%;
  height: 40px;
  width: 40px;
  position: absolute;
  top: 16px;
  left: 16px;
  background-color: ${props => props.theme.palette.primary2Color};
  background-image: ${getAsset('logoR', true)};
  background-repeat: no-repeat;
  background-size: 65%;
  background-position: center;
`;

const ROW_HEIGHT = 100;

class WereableSelectionList extends Component {
  static defaultProps = {
    devices: [],
    selectedDevice: null
  };

  state = {
    width: window.innerWidth,
    height: window.innerHeight - 64
  };

  constructor(props) {
    super(props);
    this._pageRef = React.createRef();
    this._listRef = React.createRef();
  }

  _select = async index => {
    const device = this.props.devices[index];
    if (device.isIreact === true) {
      logMain('Select device', index, device);
      try {
        await this.props.connectDevice(device);
      } catch (err) {
        logError(err);
        this.props.pushError(err);
      }
    }
  };

  _rowRenderer = ({
    key, // Unique key within array of rows
    index, // Index of row within collection
    isScrolling, // The List is currently being scrolled
    isVisible, // This row is visible within the List (eg it is not an overscanned row)
    style // Style object to be applied to row (to position it)
  }) => {
    const { devices, muiTheme } = this.props;
    const device = devices[index];
    const { canvasColor, textColor /* , primary1Color */ } = muiTheme.palette;
    const listItemStyle = {
      maxHeight: style.height - 4,
      height: style.height - 4,
      backgroundColor: canvasColor
    };

    return (
      <div
        key={key}
        style={style}
        className="bt-devices-list-item"
        onClick={this._select.bind(this, index)}
      >
        <ListItem
          disabled={!device.isIreact}
          innerDivStyle={innerItemStyle}
          style={listItemStyle}
          containerElement="span"
          className="ireact-device-item"
          leftAvatar={
            device.isIreact ? (
              <DeviceAvatar />
            ) : (
              <Avatar icon={<Bluetooth />} backgroundColor={grey600} />
            )
          }
          primaryText={device.name}
          secondaryText={
            <p>
              <span style={{ color: textColor }}>RSSI {device.rssi}</span> {device.id}
            </p>
          }
          secondaryTextLines={2}
        />
      </div>
    );
  };

  _scan = async (doScan = true) => {
    try {
      const promise = doScan ? this.props.startScan() : this.props.stopScan();
      await promise;
    } catch (err) {
      logError(err);
      this.props.pushError(err);
    }
  };

  _onHeaderButtonClick = () => {
    this._scan(!this.props.isScanning);
  };

  _updateSize = () => {
    this.setState({
      width:
        this._pageRef && this._pageRef.current
          ? this._pageRef.current.clientWidth
          : window.innerWidth,
      height:
        this._pageRef && this._pageRef.current
          ? this._pageRef.current.clientHeight
          : window.innerHeight - 64
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.width === 0 || prevState.height === 0) {
      this._updateSize();
    }
  }

  componentDidMount() {
    this._updateSize();
    window.addEventListener('resize', this._updateSize);
    this._scan();
  }

  componentWillUnmount() {
    this.props.stopScan();
  }

  render() {
    const { muiTheme, style, isScanning, devices, isConnecting } = this.props;
    const Icon = isScanning ? <Cancel /> : <Refresh />;
    const ireactDevicesCount = devices.filter(d => d.isIreact === true).length;

    return (
      <Page className="wereable-page">
        <BTHSettingHeader
          title={'_bt_settings'}
          rightButton={<IconButton onClick={this._onHeaderButtonClick}>{Icon}</IconButton>}
        />
        <ColumnPage
          style={{ position: 'relative', height: 'calc(100% - 64px)' }}
          ref={this._pageRef}
        >
          <List
            className="bt-devices-list"
            ref={this._listRef}
            width={this.state.width | window.innerWidth}
            height={this.state.height | (window.innerHeight - 64)}
            key="device-list"
            style={{ background: muiTheme.palette.backgroundColor, ...style }}
            rowCount={devices.length}
            rowHeight={ROW_HEIGHT}
            rowRenderer={this._rowRenderer}
            scrollToAlignment="auto"
          />
          {isScanning === false && ireactDevicesCount === 0 && <h4>No compatible devices found</h4>}
          {(isScanning || isConnecting) && (
            <Overlay className="overlay">
              <CircularProgress size={80} thickness={5} />
              {isScanning && <h5>Scanning for I-REACT wereable devices...</h5>}
              {isConnecting && <h5>Connecting to device...</h5>}
            </Overlay>
          )}
        </ColumnPage>
      </Page>
    );
  }
}

export default themeable(withMessages(WereableSelectionList));
