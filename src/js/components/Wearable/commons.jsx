import React from 'react';
import { Header } from 'js/components/app';

import { indigo900, indigo400 } from 'material-ui/styles/colors';

export const BT_HEADER_COLOR = indigo900;
export const BT_TEXT_COLOR = indigo400;

const BT_HEADER_STYLE = {
  backgroundColor: BT_HEADER_COLOR
};

export const BTHSettingHeader = ({ title, rightButton }) => (
  <Header title={title} leftButtonType="back" style={BT_HEADER_STYLE} rightButton={rightButton} />
);
