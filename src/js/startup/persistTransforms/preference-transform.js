import { createTransform } from 'redux-persist';
// import EmergencyEventTimeSpan from 'js/modules/preferences/EmergencyEventTimeSpan';

// We only want to save the user preferences

// Called lately (when saving state) several times
function serialize(inboundState, key) {
    // console.log('%c serialize PREF', 'color: yellow; background: darkgreen', inboundState, key);
    return inboundState;
}

// Called first (when loading state) at startup
function deserialize(outboundState, key) {
    // console.log('%c DEserialize PREF', 'color: orange; background: black', outboundState, key);
    // const newState = { ...outboundState, eventTimeSpan: new EmergencyEventTimeSpan()/* , eventBoundsWKT: null, eventBoundsFeature: null */ };
    return outboundState;
}

const preferenceTransform = createTransform(
    // transform state coming from redux on its way to being serialized and stored
    (inboundState, key) => serialize(inboundState, key),
    // transform state coming from storage, on its way to be rehydrated into redux
    (outboundState, key) => deserialize(outboundState, key),
    // configuration options - a subset of the store whitelist
    { whitelist: ['preferences'] }
);

export default preferenceTransform;
export { preferenceTransform };