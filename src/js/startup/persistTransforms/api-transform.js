import { createTransform } from 'redux-persist';
import { TimeWindow, DEFAULT_PADDING_IN_FUTURE } from 'ioc-api-interface';

function serializeAPI(inboundApiState, key) {
    // console.log('%c serializeAPI', 'color: blue; background: white', inboundApiState.receivedNotifications, key);
    // inboundApiState.receivedNotifications = { //reset
    //     items: [],
    //     totalCount: 0,
    //     unreadCount: 0
    // };
    // const now = Date.now();
    // if (now - inboundApiState.reports.lastUpdated > TIMEOUT) {
    //     let emptyFC = featureCollection([]);
    //     emptyFC.lastUpdated = now - 1000;
    //     inboundApiState.reports = emptyFC;
    // }
    return inboundApiState;
}

function deserializeAPI(outboundApiState, key) {
    // Selection

    outboundApiState.selectedFeature = null;
    outboundApiState.selectedFeatureBounds = null;
    // Hover
    outboundApiState.hoveredFeature = null;
    outboundApiState.hoveredFeatureBounds = null;

    outboundApiState.selectedEvent = null;
    outboundApiState.selectedEventId = null;

    outboundApiState.selectedMissionTask = null;
    outboundApiState.hoveredMissionTask = null;

    // Update TW
    const prefs = outboundApiState.liveTWPreferences;
    const { quantity, unit } = prefs;
    const nextTW = (new TimeWindow(null, null, {
        quantity,
        unit
    }, false, DEFAULT_PADDING_IN_FUTURE)).toPojo();

    outboundApiState.liveTW = nextTW;
    outboundApiState.appLastSelectedItems = [];
    console.log('%c DEserializeAPI', 'color: rebeccapurle; background: white', outboundApiState, key);

    return outboundApiState;
}


const apiTransform = createTransform(
    // transform state coming from redux on its way to being serialized and stored
    (inboundState, key) => serializeAPI(inboundState, key),
    // transform state coming from storage, on its way to be rehydrated into redux
    (outboundState, key) => deserializeAPI(outboundState, key),
    // configuration options - a subset of the store whitelist
    { whitelist: ['api'] }
);

export default apiTransform;
export { apiTransform };