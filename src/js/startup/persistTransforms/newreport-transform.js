import { createTransform } from 'redux-persist';

// Called lately (when saving state) several times
function serializeNewReports(inboundStateNewReport, key) {
    // console.log('%c serialize NEW REPORT', 'color: green; background: white', inboundStateNewReport, key);
    return inboundStateNewReport;
}

// Called first (when loading state) at startup
function deserializeNewReports(outboundStateNewReport, key) {
    console.log('%c DEserialize NEW REPORT', 'color: orangered; background: white', outboundStateNewReport, key);
    // Delete draft when resuming the app
    outboundStateNewReport.newReportDraft = null;
    outboundStateNewReport.isSending = false;

    return outboundStateNewReport;
}


const newReportTransform = createTransform(
    // transform state coming from redux on its way to being serialized and stored
    (inboundState, key) => serializeNewReports(inboundState, key),
    // transform state coming from storage, on its way to be rehydrated into redux
    (outboundState, key) => deserializeNewReports(outboundState, key),
    // configuration options - a subset of the store whitelist
    { whitelist: ['newReport'] }
);

export default newReportTransform;
export { newReportTransform };