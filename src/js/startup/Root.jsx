import React, { Component } from 'react';
import { MuiThemeProvider, getMuiTheme } from 'material-ui/styles';
import { iReactTheme } from './iReactTheme';
import { Layout } from 'js/components/app';
import { ThemeProvider } from 'styled-components';
// import { Router } from 'react-router-dom';

const StateContext = {
  store: {}
};

export const StateContextContext = React.createContext(StateContext);

export const theme = getMuiTheme(iReactTheme);

// The app root component
class Root extends Component {
  state = {
    store: {}
  };

  componentDidMount() {
    this.setState({ store: this.props.store });
    document.querySelector('body').style.backgroundColor = theme.palette.canvasColor;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.store !== this.props.store) {
      this.setState({ store: this.props.store });
    }
    if (prevProps.appError !== this.props.appError) {
      console.error('APP ERROR', this.props.appError);
    }
  }

  onRouterError = err => {
    console.error('Router error', err);
  };

  render() {
    const { onRouterError, history } = this.props;
    return (
      <MuiThemeProvider muiTheme={theme}>
        <ThemeProvider theme={theme}>
          <StateContextContext.Provider value={this.state}>
            <Layout onRouterError={onRouterError} history={history} />
          </StateContextContext.Provider>
        </ThemeProvider>
      </MuiThemeProvider>
    );
  }
}

export default Root;
