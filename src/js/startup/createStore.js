import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/**
 * Create store, appending redux devtools extension in debug mode
 */
const finalCreateStore = persistedReducer =>
  createStore(persistedReducer, composeEnhancers(applyMiddleware(thunk)));

export default finalCreateStore;
