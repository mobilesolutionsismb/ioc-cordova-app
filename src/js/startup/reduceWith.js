/**
 * Plain Reducer factory
 * returns a reducer given a mutators and the defaultState
 * @param Function|Object mutators the default reducer or an Object whose keys are the Actions and values are its relative mutated states
 * @param Object defaultState
 *
 * The reducer function will get two arguments: (state, action) if mutators is a function
 * or (action, state) if mutators is a config object
 * that's because in the latter case you more often refer to the action only
 */
function reduceWith(mutators, defaultState) {
  return function(state = defaultState, action) {
    const mutator = mutators[action.type];

    if (!mutator) {
      return state;
    }

    if (mutator instanceof Function) {
      return mutator.call(null, state, action);
    }

    const mutations = Object.keys(mutator).reduce((r, n) => {
      r[n] = mutator[n] instanceof Function ? mutator[n].call(null, action, state) : mutator[n];

      return r;
    }, {});

    return {
      ...state,
      ...mutations
    };
  };
}

export default reduceWith;
