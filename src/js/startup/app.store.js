import React from 'react';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { Reducer as DeviceOrientationReducer } from 'ioc-device-orientation';
import { Reducer as GeolocationReducer } from 'ioc-geolocation';
import { Reducer as LocaleReducer } from 'ioc-localization';
import { Reducer as MapViewReducer } from 'js/modules/map';
import { mobileAppModules } from 'ioc-api-interface';
import * as modules from 'js/modules';
import {
  //   apiTransform,
  newReportTransform,
  /*  preferenceTransform, */ localeTransform
} from 'js/startup/persistTransforms';
// import { CLEAR_USER_DATA } from '@state/auth/auth.actions';
import { persistStore, persistReducer } from 'redux-persist';
import { makePersistConfig } from 'js/utils/storageUtils';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const localModules = Object.entries(modules)
  .map(e => ({ [e[0]]: e[1].Reducer }))
  .reduce((ob, n) => {
    ob = { ...ob, ...n };
    return ob;
  }, {});

const reducersMap = {
  deviceOrientation: DeviceOrientationReducer,
  geolocation: GeolocationReducer,
  locale: LocaleReducer,
  map: MapViewReducer,
  ...mobileAppModules,
  ...localModules
};

console.log('reducersMap', reducersMap);

const appReducer = combineReducers(reducersMap);

// https://stackoverflow.com/questions/35622588/how-to-reset-the-state-of-a-redux-store
const rootReducer = (state, action) => {
  if (action.type === 'api:authentication@LOGOUT_SUCCESS') {
    // Preserve some pieces of state from reset (passing undefined would reset completely)
    // For the following modules it does not make sense to perform a reset
    const { appStatus, devicePermissions, geolocation, locale, networkInformation } = state;
    state = { appStatus, devicePermissions, geolocation, locale, networkInformation };
  }

  return appReducer(state, action);
};

const transforms = [
  /* appVersionCheckTransform, */ newReportTransform,
  localeTransform /* , preferenceTransform */
];

const persistingAPIModules = [
  //   'api_agentLocations',
  'api_authentication',
  'api_mobileApp',
  'api_categories',
  'api_gamification',
  //   'api_emergencyCommunications',
  //   'api_emergencyEvents',
  'api_enums',
  //   'api_ireactFeatures',
  'api_liveParams',
  'api_layers',
  //   'api_mapRequests',
  //   'api_missions',
  'api_organizationUnits',
  //   'api_reports',
  // 'api_roles',
  'api_mobileApp_tweets'
  // 'api_settings'
]; // for now everything, then we'll remove unnecessary things

const whitelist = [
  'locale',
  'geolocation',
  'map',
  'bluetooth',
  'devicePermissions',
  'partialRegistration',
  'preferences',
  'layersAndSettings',
  'gamificationAwards',
  ...persistingAPIModules
];

const persistConfig = makePersistConfig(whitelist, transforms);

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer, composeEnhancers(applyMiddleware(thunk)));

export const StoreContext = React.createContext({ store });

export const getPersistor = onRehydrate => persistStore(store, null, onRehydrate);

export const StoreProvider = ({ store, children }) => (
  <StoreContext.Provider value={store}>{children}</StoreContext.Provider>
);
