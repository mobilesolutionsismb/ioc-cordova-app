import React, { Component } from 'react';
import { withRouter, matchPath } from 'react-router';
import { withLogin } from 'ioc-api-interface';
import { DEFAULT_PAGE_AUTHENTICATED, DEFAULT_PAGE_UNAUTHENTICATED } from 'js/startup/routesConfig';

/**
 * Match user role to required roles, case insensitive
 * @param {String} role
 * @param {Array} roles (Array of strings)
 */
function matchUserRole(role, roles) {
  const rolesRx = new RegExp(roles.join('|'), 'i');
  return rolesRx.test(role);
}

function matchUserRoles(requiredRoles, userRoles) {
  if (!Array.isArray(requiredRoles) || requiredRoles.length === 0) {
    return true;
  } else {
    return userRoles.reduce((match, nextUserRole) => {
      match = match || matchUserRole(nextUserRole, requiredRoles);
      return match;
    }, false);
  }
}

class RouteRedirector extends Component {
  _checkUserRoles(pathname, history, requiredRoles, userRoles) {
    if (!matchUserRoles(requiredRoles, userRoles)) {
      history.goBack();
    }
  }

  _checkRouteLoginRequirements(pathname, history, routeRequirements, loggedIn, user) {
    if (routeRequirements.loggedIn === true) {
      if (!loggedIn) {
        // cannot see this page, go to login page
        history.replace(DEFAULT_PAGE_UNAUTHENTICATED, { from: pathname });
      } else {
        this._checkUserRoles(pathname, history, routeRequirements.roles, user.roles);
      }
    } else if (routeRequirements.loggedIn === false) {
      if (loggedIn) {
        // cannot see this page, go to default page
        history.replace(DEFAULT_PAGE_AUTHENTICATED, { from: pathname });
      }
    } else {
      // don't care, just check clearance if the user is logged in (otherwise no user, no roles)
      if (loggedIn) {
        this._checkUserRoles(pathname, history, routeRequirements.roles, user.roles);
      }
    }
  }

  _checkRoutesRequirements(props) {
    const { location, history, loggedIn, user, routesConfig } = props;

    const currentRoute = routesConfig.find(
      r => matchPath(location.pathname, { path: r.path, exact: true, strict: true }) !== null
    );
    if (!currentRoute || !currentRoute.component) {
      return;
    } else {
      this._checkRouteLoginRequirements(
        location.pathname,
        history,
        currentRoute.requirements,
        loggedIn,
        user
      );
    }
  }

  componentDidMount() {
    this._checkRoutesRequirements(this.props);
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.loggedIn !== prevProps.loggedIn ||
      prevProps.location.pathname !== this.props.location.pathname
    ) {
      this._checkRoutesRequirements(this.props);
    }
  }

  render() {
    return <div />;
  }
}

export default withRouter(withLogin(RouteRedirector));
