import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { openDrawer, closeDrawer } from '../dataSources/ActionCreators';

const drawerMapState = state => state.ui.appDrawerOpen;

export const drawerSelector = createStructuredSelector({
  appDrawerIsOpen: drawerMapState
});

export const drawerActionCreators = { openDrawer, closeDrawer };

export default connect(
  drawerSelector,
  drawerActionCreators
);
