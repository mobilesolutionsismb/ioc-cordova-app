import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import {
  pushModalMessage,
  popModalMessage,
  pushMessage,
  popMessage,
  pushError,
  popError
} from '../dataSources/ActionCreators';

const messagesMapState = state => {
  const hasErrors = state.ui.errors.length > 0; //errors have priority
  return state.ui.messages.length > 0 && !hasErrors ? state.ui.messages[0] : null;
};

const modalsMapState = state => {
  return state.ui.modals.length > 0 ? state.ui.modals[0] : null;
};

const hasModalsOpenMapState = state => {
  return state.ui.modals.length > 0;
};

const errorSelector = state => {
  return state.ui.errors.length > 0 ? state.ui.errors[0] : null;
};

export const messagesSelector = createStructuredSelector({
  hasModalsOpen: hasModalsOpenMapState,
  message: messagesMapState,
  modal: modalsMapState,
  error: errorSelector
});

export const messagesActionCreators = {
  pushModalMessage,
  pushMessage,
  popMessage,
  popModalMessage,
  pushError,
  popError
};

export default connect(
  messagesSelector,
  messagesActionCreators
);
