import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { pushMessage } from '../dataSources/ActionCreators';

const select = createStructuredSelector({

});

export default connect(select, {
    pushMessage
});