import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { loadingStart, loadingStop, loadingStopAll } from '../dataSources/ActionCreators';

const loadingMapState = state => ({
  status: state.ui.loading > 0,
  count: state.ui.loading
});

export const loadingSelector = createStructuredSelector({
  loading: loadingMapState
});

export const loadingActionCreators = { loadingStart, loadingStop, loadingStopAll };

export default connect(
  loadingSelector,
  loadingActionCreators
);
