import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { checkStartupState } from '../dataSources/ActionCreators';

const select = createStructuredSelector({
});

export default connect(select, { checkStartupState });