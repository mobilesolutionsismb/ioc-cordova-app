import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

// import { setAppBackgroundState, setAppForegroundState } from '../dataSources/ActionCreators';

const isAppForegroundSelector = state => state.ui.isAppForeground;

const select = createStructuredSelector({
  isAppForeground: isAppForegroundSelector
});

export default connect(select /* , { setAppBackgroundState, setAppForegroundState } */);
