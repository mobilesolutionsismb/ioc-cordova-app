import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import {
  LOADING_START,
  LOADING_STOP,
  LOADING_CLEAR,
  PUSH_ERROR,
  POP_ERROR,
  OPEN_DRAWER,
  CLOSE_DRAWER,
  PUSH_MESSAGE,
  POP_MESSAGE,
  PUSH_MODAL_MESSAGE,
  POP_MODAL_MESSAGE,
  RESTORE_DEFAULT
  // APP_BACKGROUND,
  // APP_FOREGROUND
} from './Actions';

const mutators = {
  [RESTORE_DEFAULT]: {
    ...DefaultState
  },
  [LOADING_START]: {
    loading: (action, state) => state.loading + 1
  },
  [LOADING_STOP]: {
    loading: (action, state) => Math.max(state.loading - 1, 0)
  },
  [LOADING_CLEAR]: {
    loading: 0
  },
  [PUSH_ERROR]: {
    errors: (action, state) => {
      let errors = [...state.errors];
      errors.push(action.error);
      return errors;
    }
  },
  [POP_ERROR]: {
    //errors is a FIFO queue
    errors: (action, state) => {
      let errors = [...state.errors];
      errors.shift();
      return errors;
    }
  },
  [OPEN_DRAWER]: {
    appDrawerOpen: true
  },
  [CLOSE_DRAWER]: {
    appDrawerOpen: false
  },
  [PUSH_MESSAGE]: {
    messages: (action, state) => {
      let messages = state.messages;
      messages.push(action.message);
      return messages;
    }
  },
  [POP_MESSAGE]: {
    //messages is a FIFO queue
    messages: (action, state) => {
      let messages = state.messages;
      messages.shift();
      return messages;
    }
  },
  [PUSH_MODAL_MESSAGE]: {
    modals: (action, state) => {
      let modals = state.modals;
      modals.push(action.message);
      return modals;
    }
  },
  [POP_MODAL_MESSAGE]: {
    //messages is a FIFO queue
    modals: (action, state) => {
      let modals = state.modals;
      modals.shift();
      return modals;
    }
  }
};

export default reduceWith(mutators, DefaultState);
