import isPojo from 'is-pojo';

import {
  LOADING_START,
  LOADING_STOP,
  LOADING_CLEAR,
  PUSH_ERROR,
  POP_ERROR,
  OPEN_DRAWER,
  CLOSE_DRAWER,
  PUSH_MESSAGE,
  POP_MESSAGE,
  PUSH_MODAL_MESSAGE,
  POP_MODAL_MESSAGE
  // APP_BACKGROUND,
  // APP_FOREGROUND
} from './Actions';
import { logError } from 'js/utils/log';

const MESSAGE_TYPES = {
  default: 'default',
  success: 'success',
  warning: 'warning',
  modal: 'modal' //should only be handled by modal
};

// export function setAppForegroundState() {
// 	return {
// 		type: APP_FOREGROUND
// 	};
// }

// export function setAppBackgroundState() {
// 	return {
// 		type: APP_BACKGROUND
// 	};
// }

//Control loader
export function loadingStart() {
  return {
    type: LOADING_START
  };
}

export function loadingStop() {
  return {
    type: LOADING_STOP
  };
}

export function loadingStopAll() {
  return {
    type: LOADING_CLEAR
  };
}

function errorify(error, actionName = null) {
  if (error instanceof Error) {
    error.actionName = actionName;
    return error;
  } else if (typeof error === 'string') {
    let e = new Error(error);
    e.actionName = actionName;
    return e;
  } else if (isPojo(error) && typeof error.error !== undefined) {
    const e = { ...error.error };
    e.actionName = actionName;
    return e;
  } else if (isPojo(error) && typeof error.message === 'string') {
    error.actionName = actionName;
    return error;
  } else if (typeof error === 'undefined' || error === null) {
    return {
      actionName: actionName,
      stack: '',
      message: 'An error has occurred'
    };
  } else {
    try {
      let errorMessage = JSON.stringify(error);
      return {
        actionName: actionName,
        stack: error.stack || '',
        message: errorMessage
      };
    } catch (jsonException) {
      console.error('JSON Exception converting error', error);
      return {
        actionName: actionName,
        stack: error.stack || '',
        message: 'An error has occurred'
      };
    }
  }
}

function areErrorsEqual(err1, err2) {
  return (
    err1.message === err2.message &&
    err1.stack === err2.stack &&
    err1.actionName === err2.actionName
  );
}

/**
 * Create a message
 * @param {String} title
 * @param {String|React.ReactNode|React.ReactElement} text
 * @param {Enum|String} type
 * @param {String|null|object} actionName
 * @param {Array} params
 */
function messagify(title, text, type = MESSAGE_TYPES.default, actionName = null, params = []) {
  //let messageText = typeof text === 'string' ? text : '' + text;
  return {
    id: Symbol(),
    title,
    text,
    type,
    actionName,
    params
  };
}

export function pushErrorAction(error) {
  logError(error);
  return {
    type: PUSH_ERROR,
    error
  };
}

export function pushError(error, actionName = null) {
  return (dispatch, getState) => {
    let nextError = errorify(error, actionName);
    let errors = getState().ui.errors;
    if (errors.length > 0) {
      //Check if last error wasn't the same. Avoid flooding error queue with useless logs
      let lastError = errors[errors.length - 1];
      if (!areErrorsEqual(nextError, lastError)) {
        dispatch(pushErrorAction(nextError));
      }
    } else {
      dispatch(pushErrorAction(nextError));
    }
  };
}

export function popError() {
  return {
    type: POP_ERROR
  };
}

export function openDrawer() {
  return {
    type: OPEN_DRAWER
  };
}

export function closeDrawer() {
  return {
    type: CLOSE_DRAWER
  };
}

//Message destined to the snackbar
//By default it closes after 4s, but if actionName is valid (currently only '_close') it will
//wait the user to tap on the action
export function pushMessage(content, type = MESSAGE_TYPES.default, actionName = null, ...params) {
  return dispatch => {
    if (type !== MESSAGE_TYPES.modal) {
      const message = messagify('', content, type, actionName, params);
      dispatch({
        type: PUSH_MESSAGE,
        message
      });
      return message.id;
    } else {
      return null;
    }
  };
}

export function popMessage() {
  return {
    type: POP_MESSAGE
  };
}

//Message destined to the modal
//usage (for connected components)
// this.props.pushModalMessage('Modal Window', 'Close this modal');
// this will display a modal with title 'Modal Window' and body 'Close this modal'
// or let actions = {'_some_action_name': actionCallback, '_some_other_action': action2Callback}
// this.props.pushModalMessage('Modal Window', 'Close this modal', actions);
// will add 2 more buttons with (translated) labels '_some_action_name' and '_some_other_action'
export function pushModalMessage(title, content, actionName = null, ...params) {
  return dispatch => {
    const message = messagify(title, content, MESSAGE_TYPES.modal, actionName, params);
    dispatch({
      type: PUSH_MODAL_MESSAGE,
      message
    });
    return message.id;
  };
}

export function popModalMessage() {
  return {
    type: POP_MODAL_MESSAGE
  };
}
