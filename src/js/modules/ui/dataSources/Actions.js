const NS = 'UI';

export const APP_FOREGROUND = `${NS}@APP_FOREGROUND`;
export const APP_BACKGROUND = `${NS}@APP_BACKGROUND`;

export const LOADING_START = `${NS}@LOADING_START`;
export const LOADING_STOP = `${NS}@LOADING_STOP`;
export const LOADING_CLEAR = `${NS}@LOADING_CLEAR`;

export const PUSH_ERROR = `${NS}@PUSH_ERROR`;
export const POP_ERROR = `${NS}@POP_ERROR`;

export const OPEN_DRAWER = `${NS}@OPEN_DRAWER`;
export const CLOSE_DRAWER = `${NS}@CLOSE_DRAWER`;

export const PUSH_MESSAGE = `${NS}@PUSH_MESSAGE`;
export const POP_MESSAGE = `${NS}@POP_MESSAGE`;

export const PUSH_MODAL_MESSAGE = `${NS}@PUSH_MODAL_MESSAGE`;
export const POP_MODAL_MESSAGE = `${NS}@POP_MODAL_MESSAGE`;

export const RESTORE_DEFAULT = `${NS}@RESTORE_DEFAULT`;
