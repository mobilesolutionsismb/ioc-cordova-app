import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import {
  RESET_ALL,
  /* ADD_FILTER, REMOVE_FILTER, */ MISSION_FILTERS_CHANGE,
  MISSION_SORTER_CHANGE
} from './Actions';
import { FILTERS, SORTERS } from './filters';

function isValidCategory(filterCategory) {
  return Array.isArray(FILTERS[filterCategory]);
}

const mutators = {
  [RESET_ALL]: {
    ...DefaultState
  },
  [MISSION_SORTER_CHANGE]: {
    sorter: (action, state) =>
      SORTERS.hasOwnProperty(action.sorterName) ? action.sorterName : state.sorter
  },
  [MISSION_FILTERS_CHANGE]: {
    filters: (action, state) => {
      const { filterCategory, filterNames } = action;
      if (isValidCategory(filterCategory)) {
        const newFiltersSet = new Set(filterNames);
        return { ...state.filters, [filterCategory]: Array.from(newFiltersSet) };
      } else {
        return state.filters;
      }
    }
  }
  // [ADD_FILTER]: {
  //     filters: (action, state) => {
  //         const { filterCategory, filterName } = action;
  //         if (isValidFilter(filterCategory, filterName)) {
  //             // Convert array to set
  //             const currentFilterSet = new Set(state.filters[filterCategory]);
  //             currentFilterSet.add(filterName);
  //             // Convert set to array
  //             return { ...state.filters, [filterCategory]: Array.from(currentFilterSet) };
  //         }
  //         else {
  //             return state.filters;
  //         }
  //     }
  // },
  // [REMOVE_FILTER]: {
  //     filters: (action, state) => {
  //         const { filterCategory, filterName } = action;
  //         if (isValidFilter(filterCategory, filterName)) {
  //             // Convert array to set
  //             const currentFilterSet = new Set(state.filters[filterCategory]);
  //             if (currentFilterSet.delete(filterName)) {
  //                 // Convert set to array
  //                 return { ...state.filters, [filterCategory]: Array.from(currentFilterSet) };
  //             }
  //             else {
  //                 return state.filters;
  //             }
  //         }
  //         else {
  //             return state.filters;
  //         }
  //     }
  // }
};

export default reduceWith(mutators, DefaultState);
