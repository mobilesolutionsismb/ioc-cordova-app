// import { FILTERS } from '../filters';

const STATE = {
    filters: {
        temporalStatus: []
    },
    sorter: 'lastModified_asc'
};

export default STATE;