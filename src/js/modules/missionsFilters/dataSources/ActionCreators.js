import { MISSION_FILTERS_CHANGE, MISSION_SORTER_CHANGE, RESET_ALL } from './Actions';

// const DESELECT_REPORT = 'api::ireact_features@DESELECT_FEATURE';

export function resetAllMissionFiltersAndSorters() {
  return {
    type: RESET_ALL
  };
}

// function _deselectIfMission(dispatch, getState) {
//     const apiState = getState().api;
//     if (apiState.selectedFeatureItemType === 'mission') {
//         dispatch({
//             type: DESELECT_REPORT
//         });
//     }
// }

export function setMissionsFilters(filterCategory, filterNames) {
  return (dispatch, getState) => {
    // _deselectIfMission(dispatch, getState);
    dispatch({
      type: MISSION_FILTERS_CHANGE,
      filterCategory,
      filterNames
    });
  };
}

export function setMissionsSorter(sorterName) {
  return {
    type: MISSION_SORTER_CHANGE,
    sorterName
  };
}
