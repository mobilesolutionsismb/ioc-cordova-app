import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FILTERS, temporalStatus } from '../dataSources/filters';

import * as ActionCreators from '../dataSources/ActionCreators';
import { getActiveFiltersDefinitions } from 'js/utils/getActiveFiltersDefinitions';

export const FilterMap = {
  temporalStatus
};

const select = createStructuredSelector({
  missionFiltersDefinition: state =>
    getActiveFiltersDefinitions(state.missionFilters.filters, FilterMap, FILTERS),
  missionFilters: state => state.missionFilters.filters,
  missionsSorter: state => state.missionFilters.sorter
});

export default connect(select, ActionCreators);
