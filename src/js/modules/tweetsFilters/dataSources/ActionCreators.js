import { RESET_ALL, ADD_FILTER, REMOVE_FILTER, SORTER_CHANGE } from './Actions';

export function resetAllTweetsFiltersAndSorters() {
  return {
    type: RESET_ALL
  };
}

function _addTweetsFilter(filterCategory, filterName) {
  return {
    type: ADD_FILTER,
    filterCategory,
    filterName
  };
}

export function addTweetsFilter(filterCategory, filterName) {
  return (dispatch, getState) => {
    dispatch(_addTweetsFilter(filterCategory, filterName));
  };
}

function _removeFilter(filterCategory, filterName) {
  return {
    type: REMOVE_FILTER,
    filterCategory,
    filterName
  };
}

export function removeTweetsFilter(filterCategory, filterName) {
  return (dispatch, getState) => {
    dispatch(_removeFilter(filterCategory, filterName));
  };
}

export function setTweetsSorter(sorterName) {
  return {
    type: SORTER_CHANGE,
    sorterName
  };
}
