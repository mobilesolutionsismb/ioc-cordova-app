// import { FILTERS } from './filters';

const STATE = {
  // But filter checkboxes are initially unchecked
  // filters: { ...FILTERS },
  filters: {
    hasGeolocation: ['_has_geolocation'],
    isValid: [],
    // isRetweet: [],
    hasPicture: []
  },
  sorter: '_date_time_desc' //'_date_time_desc'
};

export default STATE;
