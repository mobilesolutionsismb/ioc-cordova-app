import { RESET_ALL, ADD_FILTER, REMOVE_FILTER, SORTER_CHANGE } from './Actions';

// const DESELECT_ECOMM = 'api::ireact_features@DESELECT_FEATURE';

export function resetAllReportFiltersAndSorters() {
  return {
    type: RESET_ALL
  };
}

function _addEmcommFilter(filterCategory, filterName) {
  return {
    type: ADD_FILTER,
    filterCategory,
    filterName
  };
}

// function _deselectIfEmcomm(dispatch, getState) {
//     const apiState = getState().api;
//     if (apiState.selectedFeatureItemType === 'emergencyCommunication') {
//         dispatch({
//             type: DESELECT_ECOMM
//         });
//     }
// }

export function addEmcommFilter(filterCategory, filterName) {
  return (dispatch, getState) => {
    // ensure no report is currently selected
    // _deselectIfEmcomm(dispatch, getState);
    dispatch(_addEmcommFilter(filterCategory, filterName));
  };
}

function _removeFilter(filterCategory, filterName) {
  return {
    type: REMOVE_FILTER,
    filterCategory,
    filterName
  };
}

export function removeEmcommFilter(filterCategory, filterName) {
  return (dispatch, getState) => {
    // ensure no report is currently selected
    // _deselectIfEmcomm(dispatch, getState);
    dispatch(_removeFilter(filterCategory, filterName));
  };
}

export function setEmcommSorter(sorterName) {
  return {
    type: SORTER_CHANGE,
    sorterName
  };
}
