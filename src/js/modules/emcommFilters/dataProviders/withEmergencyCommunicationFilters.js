import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FILTERS, emcommType } from '../dataSources/filters';

import * as ActionCreators from '../dataSources/ActionCreators';
import { getActiveFiltersDefinitions } from 'js/utils/getActiveFiltersDefinitions';

export const FilterMap = {
  emcommType
};

const select = createStructuredSelector({
  emcommFiltersDefinition: state =>
    getActiveFiltersDefinitions(state.emcommFilters.filters, FilterMap, FILTERS),
  emcommFilters: state => state.emcommFilters.filters,
  emcommSorter: state => state.emcommFilters.sorter
});

export default connect(select, ActionCreators);
