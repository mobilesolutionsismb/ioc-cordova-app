// TODO check if this has to be moved into API
import Reducer from './dataSources/Reducer';
import withLayersAndSettings from './dataProviders/withLayersAndSettings';

export {
  computeLayersTree,
  computeSettingsTree,
  findCopernicusByName,
  findInSettingsTree,
  findLayerSettingBySettingName,
  findLayerSettingByTaskId,
  findNodeByName,
  findMaxTreeDepth
} from './settingsTreeUtils';

export { withLayersAndSettings, Reducer };

export default Reducer;
