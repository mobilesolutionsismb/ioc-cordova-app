import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import {
  removeAllLayers,
  activateLayer,
  loadSettings,
  resetDefaultLayerSettings
} from '../dataSources/ActionCreators';
// import { findLayerSettingByTaskId } from '../settingsTreeUtils';
import { findLayerSettingBySettingName, findCopernicusByName } from '../settingsTreeUtils';

function leadTimeSort(lcfg1, lcfg2) {
  const l1 = new Date(lcfg1.leadTime);
  const l2 = new Date(lcfg2.leadTime);
  return l1 - l2;
}

const findActiveIreactTaskSettings = state => {
  if (
    Array.isArray(state.layersAndSettings.remoteSettings.children) &&
    state.layersAndSettings.activeIReactSettingNames.length
  ) {
    return state.layersAndSettings.activeIReactSettingNames.map(settingName =>
      findLayerSettingBySettingName(state.layersAndSettings.remoteSettings, settingName)
    );
  } else return [];
};

const findActiveCopernicusLayers = state => {
  if (
    Array.isArray(state.api_layers.mapLayers.copernicusLayers) &&
    state.layersAndSettings.activeCopernicusNames.length
  ) {
    return state.layersAndSettings.activeCopernicusNames.map(copernicusName =>
      findCopernicusByName(state.api_layers.mapLayers.copernicusLayers, copernicusName, [])
    );
  } else return [];
};

// Return layer config of the active layer set
const findActiveLayersSet = state => {
  let activeLayerConfigurations = [];
  const activeTaskSettings = findActiveIreactTaskSettings(state);
  if (activeTaskSettings.length > 0) {
    const activeSetting = activeTaskSettings[0];
    const activeTaskIDs = Array.isArray(
      activeSetting.setting.settingDefinition.customData.relatedIreactTasks
    )
      ? activeSetting.setting.settingDefinition.customData.relatedIreactTasks
      : [activeSetting.setting.settingDefinition.customData.relatedIreactTasks];

    const layers = state.api_layers.mapLayers.layers;
    activeLayerConfigurations = layers
      .filter(lc => activeTaskIDs.indexOf(lc.taskId) > -1)
      .sort(leadTimeSort);
  } else {
    const activeCopernicusLayer = findActiveCopernicusLayers(state);
    if (activeCopernicusLayer.length > 0) {
      activeLayerConfigurations = activeCopernicusLayer[0].sort(leadTimeSort); // ?
    }
  }
  return activeLayerConfigurations;
};

const getActiveSettingName = state => {
  let activeSettingName = null;
  const activeIReactSettingNames = state.layersAndSettings.activeIReactSettingNames;
  if (activeIReactSettingNames.length > 0) {
    activeSettingName = activeIReactSettingNames[0];
  } else {
    const activeCopernicusNames = state.layersAndSettings.activeCopernicusNames;
    if (activeCopernicusNames.length > 0) {
      activeSettingName = activeCopernicusNames[0];
    }
  }

  return activeSettingName;
};

const getActiveSetting = state => {
  const activeTaskSettings = findActiveIreactTaskSettings(state);
  let activeSetting = null;
  if (activeTaskSettings.length > 0) {
    const actSett = activeTaskSettings[0];
    const { grandParentGroup, parentGroup, setting } = actSett;
    activeSetting = {
      id: setting.name,
      hazard: grandParentGroup ? actSett.grandParentGroup.displayName : 'Unknown Hazard', // See for Copernicus
      layerType: parentGroup.displayName,
      name: setting.settingDefinition.displayName,
      isCopernicus: false
    };
  } else {
    // TODO Check it!
    const activeCopernicusLayer = findActiveCopernicusLayers(state);
    if (activeCopernicusLayer.length > 0) {
      const copSett = activeCopernicusLayer[0];
      const { name, code, hazard, layerType } = copSett;
      activeSetting = {
        id: name,
        code,
        hazard,
        layerType,
        isCopernicus: true
      };
    }
  }
  return activeSetting;
};

/* const select = createStructuredSelector({
  remoteSettings: state => state.layersAndSettings.remoteSettings,
  remoteLayers: state => state.layersAndSettings.remoteLayers,
  remoteSettingsUpdating: state => state.layersAndSettings.remoteSettingsUpdating,
  activeIReactSettingNames: state => state.layersAndSettings.activeIReactSettingNames,
  // activeIReactTasks: state => state.layersAndSettings.activeIReactTasks,
  activeIReactTaskSettings: state => {
    if (
      Array.isArray(state.layersAndSettings.remoteSettings.children) &&
      state.layersAndSettings.activeIReactSettingNames.length
    ) {
      return state.layersAndSettings.activeIReactSettingNames.map(settingName =>
        findLayerSettingBySettingName(state.layersAndSettings.remoteSettings, settingName)
      );
    } else return [];
  },
  activeCopernicusNames: state => state.layersAndSettings.activeCopernicusNames,
  activeCopernicusLayers: state => {
    if (
      Array.isArray(state.api_layers.mapLayers.copernicusLayers) &&
      state.layersAndSettings.activeCopernicusNames.length
    ) {
      return state.layersAndSettings.activeCopernicusNames.map(copernicusName =>
        findCopernicusByName(state.api_layers.mapLayers.copernicusLayers, copernicusName, [])
      );
    } else return [];
  },
  itemEditedList: state => state.layersAndSettings.itemEditedList,
  activeLayersFromMapRequest: state => state.layersAndSettings.activeLayersFromMapRequest
}); */

const select = createStructuredSelector({
  // Status of settings and layers
  remoteTaskIDsAvailable: state =>
    state.api_layers.mapLayers
      ? Array.isArray(state.api_layers.mapLayers.availableTaskIds)
        ? state.api_layers.mapLayers.availableTaskIds
        : []
      : [],
  remoteSettings: state => state.layersAndSettings.remoteSettings,
  remoteLayers: state => state.layersAndSettings.remoteLayers,
  remoteSettingsLastUpdate: state => state.layersAndSettings.settingLastUpdated,
  remoteSettingsUpdating: state => state.layersAndSettings.remoteSettingsUpdating,
  activeLayersSet: findActiveLayersSet,
  activeSettingName: getActiveSettingName,
  activeSetting: getActiveSetting
});

export default connect(select, {
  removeAllLayers,
  activateLayer,
  loadSettings,
  resetDefaultLayerSettings
});
