const isLayerKey = /^App.UserSettings.Layers/;

// A preset for finding Setting by taskId
const _findLayerSettingsByTaskId = (taskId, s) =>
  typeof s.settingDefinition !== 'undefined' &&
  Array.isArray(s.settingDefinition.customData.relatedIreactTasks) &&
  isLayerKey.test(s.settingDefinition.name) &&
  s.settingDefinition.customData.relatedIreactTasks.indexOf(taskId) > -1;

// A preset for finding Setting by taskId
const _findLayerSettingsBySettingName = (settingName, s) =>
  typeof s.settingDefinition !== 'undefined' &&
  Array.isArray(s.settingDefinition.customData.relatedIreactTasks) &&
  // isLayerOptionKey.test(s.settingDefinition.name) &&
  s.settingDefinition.name === settingName;

// GEt any node by name
const _findNodeByName = (name, node) => node.name === name;

/**
 * @param {Object} treeChildren
 * @param {Function} findCb function(settings: Setting): boolean
 * @param {Object} previousNode? - optional previous node, useful for grandpas
 * @param {boolean} returnGroupNode? - optional if true, return whole group for type: 'SettingsGroup'
 * @yields {Object} result - type: string (Setting|SettingsGroup), name: string - abp name, displayName: string: label
 * if it's a Setting you get also: grandParentGroup: null or {displayName: name} and parentGroup {displayName: name} for each Setting
 */
function* _findInTree(treeChildren, findCb, previousNode = null, returnGroupNode = false) {
  if (!Array.isArray(treeChildren)) {
    const err = new Error('Unexpected data structure for settingsGroupTree');
    console.error(err, treeChildren);
    throw err;
  }
  if (typeof findCb !== 'function') {
    const err = new Error('findCb must be a function');
    console.error(err, findCb);
    throw err;
  }

  for (const node of treeChildren) {
    const { children, settings } = node;
    if (Array.isArray(settings)) {
      // it's a setting
      const index = settings.findIndex(findCb);
      if (index > -1) {
        let grandParentGroup = null;
        if (previousNode) {
          grandParentGroup = {
            displayName: previousNode.displayName,
            name: previousNode.displayName
          };
        }
        const _branch = previousNode ? [previousNode, node] : [node];
        const branch = Array.isArray(node.branch) ? [...node.branch, ..._branch] : _branch;
        // Parent
        const { displayName, name } = node;
        yield {
          type: 'Setting',
          grandParentGroup,
          parentGroup: { displayName, name },
          setting: settings[index],
          branch
        };
      } else {
        yield null;
      }
    }
    if (Array.isArray(children)) {
      // it's a group: search in children
      const index = children.findIndex(findCb);
      if (index > -1) {
        const { displayName, name } = children[index];
        yield {
          type: 'SettingsGroup',
          group: returnGroupNode === true ? children[index] : { displayName, name },
          branch: Array.isArray(node.branch)
            ? [...node.branch, node]
            : previousNode
            ? [previousNode, node]
            : [node]
        };
      } else {
        // continue search
        yield* _findInTree(children, findCb, node, returnGroupNode);
      }
    }
  }
}

/**
 * Recursively compute the max depth of a setting tree
 * @param {SettingsTree} tree
 * @param {Number} max - do not set for initial search
 */
export function findMaxTreeDepth(tree, max = 0) {
  let _max = max;

  const hasChildren = Array.isArray(tree.children);
  if (hasChildren && tree.children.length > 0) {
    _max += 1;
    const maximums = tree.children.map(child => findMaxTreeDepth(child, _max));
    _max = Math.max.apply(null, [_max, ...maximums]);
  }

  return _max;
}

/**
 * Find Setting by using the findCB function on each setting of the
 * settings array of each node (settings group)
 * @param {Function} findCb function(settings: Setting): boolean
 */
export function findInSettingsTree(tree, findCb, returnGroupNode = false) {
  const it = _findInTree(tree.children, findCb, null, returnGroupNode);

  let res = it.next();
  while (!res.done) {
    if (res.value !== null) {
      break;
    }
    res = it.next();
  }
  return res.value || null;
}

/**
 * Find by task Id
 * @param {Object} tree - the complete setting tree
 * @param {Number} taskId - the required task id
 */
export function findLayerSettingByTaskId(tree, taskId, returnGroupNode = false) {
  return findInSettingsTree(tree, _findLayerSettingsByTaskId.bind(null, taskId), returnGroupNode);
}

/**
 * @param {Object} tree - the complete setting tree
 * @param {String} settingName - the required unique setting name
 */
export function findLayerSettingBySettingName(tree, settingName, returnGroupNode = false) {
  return findInSettingsTree(
    tree,
    _findLayerSettingsBySettingName.bind(null, settingName),
    returnGroupNode
  );
}

/**
 *
 * @param {Object} tree - the complete setting tree
 * @param {String} name - the node unique name
 */
export function findNodeByName(tree, name) {
  return findInSettingsTree(tree, _findNodeByName.bind(null, name), true);
}

// Credits: https://derickbailey.com/2015/07/19/using-es6-generators-to-recursively-traverse-a-nested-data-structure/
function* _processTree(treeChildren, leaves) {
  if (!Array.isArray(treeChildren)) {
    const err = new Error('Unexpected data structure for settingsGroupTree');
    console.error(err, treeChildren);
    throw err;
  }
  // for (let i = 0; i < tree.length; i++) {
  //     const settingsGroup = tree[i];
  for (const settingsGroup of treeChildren) {
    const { name, displayName, children } = settingsGroup;
    yield { name, displayName };

    if (Array.isArray(children)) {
      // it's a group
      yield* _processTree(children, leaves);
    } else {
      const setting = leaves[name];
      settingsGroup['settings'] = setting;
    }
  }
}

export function computeSettingsTree(settingsGroupTree, settingsDefinitions) {
  let settingsGroupTreeProcessed = JSON.parse(JSON.stringify(settingsGroupTree));
  const it = _processTree(settingsGroupTreeProcessed.children, settingsDefinitions);
  let res = it.next();
  while (!res.done) {
    // TODO should we put a max iteration safety here^
    res = it.next();
  }
  return settingsGroupTreeProcessed;
}

function* _processLayersTree(tree, leaves) {
  if (!Array.isArray(tree)) {
    const err = new Error('Unexpected data structure for settingsGroupTree');
    console.error(err, tree);
    throw err;
  }
  for (const layersGroup of tree) {
    const { name, displayName, children } = layersGroup;
    yield { name, displayName };
    if (Array.isArray(children)) {
      // it's a group
      yield* _processLayersTree(layersGroup.children, leaves);
    } else {
      const setting = leaves[name];
      layersGroup['children'] = setting;
    }
  }
}

function filterChild(child) {
  if (Array.isArray(child.children)) {
    child.children = child.children.map(filterChild).filter(c => c !== null);
    return child.children.length > 0 ? child : null;
  } else {
    return Array.isArray(child.iReactTasks) && child.iReactTasks.length > 0 ? child : null;
  }
}

function pruneEmptyChildren(children) {
  return children.reduce((filteredChildren, nextChild) => {
    const filteredChild = filterChild(nextChild);
    if (filteredChild) {
      filteredChildren = [...filteredChildren, filteredChild];
    }
    return filteredChildren;
  }, []);
}

// function _getGroupedLeaves(settingsDefinitions) {
//   let groups = {};

//   Object.keys(settingsDefinitions).forEach(a =>
//     settingsDefinitions[a].forEach((item, key) => {
//       let val = item.settingDefinition.name.replace('.LeadTime', '').replace('.ShowHide', '');
//       val = `${val}.ShowHide`;
//       groups[a] = groups[a] || [];

//       const index = groups[a].findIndex(b => b.name === val);
//       if (index !== -1) {
//         groups[a][index][item.settingDefinition.customData.settingDefinitionType] = item.value;
//       } else {
//         let content = {
//           iReactTasks: item.settingDefinition.customData.relatedIreactTasks,
//           name: val,
//           displayName: item.settingDefinition.displayName
//         };
//         content[item.settingDefinition.customData.settingDefinitionType] = item.value;
//         groups[a].push(content);
//       }
//     })
//   );
//   return groups;
// }

function _getGroupedLeaves2(settingsDefinitions, availableTaskIds) {
  let groups = Object.keys(settingsDefinitions).reduce((prev, nextKey) => {
    const nextKeyValues = settingsDefinitions[nextKey].reduce((grouped, item) => {
      let val = item.settingDefinition.name.replace('.LeadTime', '').replace('.ShowHide', '');
      val = `${val}.ShowHide`;
      const index = grouped.findIndex(b => b.name === val);
      if (index !== -1) {
        grouped[index][item.settingDefinition.customData.settingDefinitionType] = item.value;
      } else {
        const iReactTasks = item.settingDefinition.customData.relatedIreactTasks.filter(task =>
          availableTaskIds.includes(task)
        );
        if (iReactTasks.length) {
          let content = {
            iReactTasks: iReactTasks,
            name: val,
            displayName: item.settingDefinition.displayName
          };
          content[item.settingDefinition.customData.settingDefinitionType] = item.value;
          grouped = [...grouped, content];
        }
      }
      return grouped;
    }, []);
    if (nextKeyValues.length) {
      prev[nextKey] = nextKeyValues;
    }
    return prev;
  }, {});

  return groups;
}

export function computeLayersTree(settingsGroupTree, settingsDefinitions, availableTaskIds) {
  // Poor man deep clone
  let copySettingsGroupTree = JSON.parse(JSON.stringify(settingsGroupTree));
  if (!copySettingsGroupTree || !Array.isArray(copySettingsGroupTree.children)) {
    const err = new Error('Unexpected data structure for settingsGroupTree');
    console.error(err, copySettingsGroupTree);
    throw err;
  }
  // let groups = _getGroupedLeaves(settingsDefinitions);
  let groups = _getGroupedLeaves2(settingsDefinitions, availableTaskIds);

  const it2 = _processLayersTree(copySettingsGroupTree.children, groups);
  let res = it2.next();
  while (!res.done) {
    // TODO should we put a max iteration safety here^
    res = it2.next();
  }
  copySettingsGroupTree.children = pruneEmptyChildren(copySettingsGroupTree.children);

  return copySettingsGroupTree;
}

export function findCopernicusByName(copernicusLayers, copernicusName, layers) {
  if (Array.isArray(copernicusLayers)) {
    let total = copernicusLayers.reduce((a, item) => {
      if (item.children) {
        return findCopernicusByName(item.children, copernicusName, a);
      } else {
        if (item.id === copernicusName) {
          return item;
        } else {
          return a;
        }
      }
    }, layers);
    return total;
  } else {
    let total = Object.values(copernicusLayers).reduce((a, item) => {
      if (item.children) {
        return findCopernicusByName(item.children, copernicusName, a);
      } else {
        if (item.id === copernicusName) {
          return item;
        } else {
          return a;
        }
      }
    }, layers);
    return total;
  }
}
