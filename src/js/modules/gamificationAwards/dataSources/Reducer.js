import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import { ADD_NOTIFICATION, REMOVE_NOTIFICATION, CLEAR_ALL } from './Actions';

const mutators = {
  [CLEAR_ALL]: {
    ...DefaultState
  },
  [ADD_NOTIFICATION]: {
    list: (action, state) => {
      const notification = action.notification;
      const list = state.list;
      return [...list, notification];
    }
  },
  [REMOVE_NOTIFICATION]: {
    list: (action, state) => {
      const notificationId = action.notificationId;
      let list = [...state.list];
      if (list.length > 0) {
        const index = list.findIndex(n => n.id === notificationId);
        if (index > -1) {
          list.splice(index, 1);
        }
      }
      return list;
    }
  }
};

export default reduceWith(mutators, DefaultState);
