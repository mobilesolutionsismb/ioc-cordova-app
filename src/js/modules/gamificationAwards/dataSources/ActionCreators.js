import { ADD_NOTIFICATION, REMOVE_NOTIFICATION, CLEAR_ALL } from './Actions';

export function clearAll() {
  return {
    type: CLEAR_ALL
  };
}

export function addAwardNotification(notification) {
  return {
    type: ADD_NOTIFICATION,
    notification
  };
}

export function removeAwardNotification(notificationId) {
  return {
    type: REMOVE_NOTIFICATION,
    notificationId
  };
}

export function removeOldestNotification() {
  return (dispatch, getState) => {
    const state = getState().gamificationAwards;
    const list = state.list;
    if (list.length > 0) {
      dispatch(removeAwardNotification(list[0].id));
    }
  };
}
