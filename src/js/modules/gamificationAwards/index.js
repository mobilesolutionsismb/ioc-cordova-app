import Reducer from './dataSources/Reducer';
import withGamificationAwards from './dataProviders/withGamificationAwards';
export default Reducer;
export { Reducer, withGamificationAwards };
