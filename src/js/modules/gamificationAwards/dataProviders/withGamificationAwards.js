import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as ActionCreators from '../dataSources/ActionCreators';

const select = createStructuredSelector({
  gamificationAwards: state => state.gamificationAwards.list
});

export default connect(
  select,
  ActionCreators
);
