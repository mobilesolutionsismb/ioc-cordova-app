import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

const select = createStructuredSelector({
    isAppForeground: state => state.appStatus.status === 'ready' || state.appStatus.status === 'resumed',
    cordovaStatus: state => state.appStatus.status
});

export default connect(select);