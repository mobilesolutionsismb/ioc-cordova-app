const NS = 'app';

export const PAUSE = `${NS}@PAUSE`;
export const RESUME = `${NS}@RESUME`;
export const READY = `${NS}@READY`;