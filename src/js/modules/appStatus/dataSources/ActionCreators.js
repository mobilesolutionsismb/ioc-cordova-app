import {READY, RESUME, PAUSE} from './Actions';
 
export function ready(){
    return {
        type: READY
    };
}

export function pause(){
    return {
        type: PAUSE
    };
}

export function resume(){
    return {
        type: RESUME
    };
}