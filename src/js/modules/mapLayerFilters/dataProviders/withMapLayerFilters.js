import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FILTERS, layersVisibility } from '../dataSources/filters';

import * as ActionCreators from '../dataSources/ActionCreators';
import { getActiveFiltersDefinitions } from 'js/utils/getActiveFiltersDefinitions';

export const FilterMap = {
  layersVisibility
};

const select = createStructuredSelector({
  mapLayerFiltersDefinition: state =>
    getActiveFiltersDefinitions(state.mapLayerFilters.filters, FilterMap, FILTERS),
  mapLayerFilters: state => state.mapLayerFilters.filters
});

export default connect(select, ActionCreators);
