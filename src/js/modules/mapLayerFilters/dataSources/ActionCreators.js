import { RESET_ALL, ADD_FILTER, REMOVE_FILTER } from './Actions';
const DESELECT_FEATURE = 'api::ireact_features@DESELECT_FEATURE';

export function resetAllMapLayerFilters() {
    return {
        type: RESET_ALL
    };
}

function _addMapLayerFilter(filterCategory, filterName) {
    return {
        type: ADD_FILTER,
        filterCategory,
        filterName
    };
}

export function addMapLayerFilter(filterCategory, filterName) {
    return dispatch => {
        dispatch(_addMapLayerFilter(filterCategory, filterName));
    };
}

function _removeMapLayerFilter(filterCategory, filterName) {
    return {
        type: REMOVE_FILTER,
        filterCategory,
        filterName
    };
}

export function removeMapLayerFilter(filterCategory, filterName) {
    return dispatch => {
        // ensure no Ireact feature is currently selected
        dispatch({ type: DESELECT_FEATURE });
        dispatch(_removeMapLayerFilter(filterCategory, filterName));
    };
}
