/* ENTITY TYPE FILTERS */
const _is_layer_report_visible = ['==', 'itemType', 'report'];
const _is_layer_rr_visible = ['==', 'itemType', 'reportRequest'];
const _is_layer_emcomm_visible = [
  'any',
  ['in', 'type', 'alert', 'warning'],
  ['==', 'itemType', 'emergencyCommunicationAOI']
];
const _is_layer_mission_visible = ['in', 'itemType', 'mission', 'missionTask', 'missionAOI'];
const _is_layer_agloc_visible = ['==', 'itemType', 'agentLocation'];
const _is_layer_maprequest_visible = ['==', 'itemType', 'mapRequest'];
const _no_layer_visible = ['==', 'itemType', 'undefined'];

export const layersVisibility = {
  _is_layer_report_visible,
  _is_layer_rr_visible,
  _is_layer_emcomm_visible,
  _is_layer_mission_visible,
  _is_layer_agloc_visible,
  _is_layer_maprequest_visible,
  _no_layer_visible
};

export const FILTERS = {
  layersVisibility: Object.keys(layersVisibility)
};

export const availableFilters = Object.keys(FILTERS);
