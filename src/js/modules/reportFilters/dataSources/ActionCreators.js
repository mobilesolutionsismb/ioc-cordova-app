import { RESET_ALL, ADD_FILTER, REMOVE_FILTER, SORTER_CHANGE } from './Actions';

//const DESELECT_REPORT = 'api::ireact_features@DESELECT_FEATURE';

export function resetAllReportFiltersAndSorters() {
  return {
    type: RESET_ALL
  };
}

function _addReportFilter(filterCategory, filterName) {
  return {
    type: ADD_FILTER,
    filterCategory,
    filterName
  };
}

// function _deselectIfReport(dispatch, getState) {
//     const apiState = getState().api;
//     if (apiState.selectedFeatureItemType === 'report') {
//         dispatch({
//             type: DESELECT_REPORT
//         });
//     }
// }

export function addReportFilter(filterCategory, filterName) {
  return (dispatch, getState) => {
    // ensure no report is currently selected
    // _deselectIfReport(dispatch, getState);
    dispatch(_addReportFilter(filterCategory, filterName));
  };
}

function _removeFilter(filterCategory, filterName) {
  return {
    type: REMOVE_FILTER,
    filterCategory,
    filterName
  };
}

export function removeReportFilter(filterCategory, filterName) {
  return (dispatch, getState) => {
    // ensure no report is currently selected
    // _deselectIfReport(dispatch, getState);
    dispatch(_removeFilter(filterCategory, filterName));
  };
}

export function setReportSorter(sorterName) {
  return {
    type: SORTER_CHANGE,
    sorterName
  };
}
