import { FILTERS } from './filters';

const STATE = {
  // By setting userType to empty we have to same result of selecting both userType filters
  // But filter checkboxes are initially unchecked
  filters: { ...FILTERS, userType: [] },
  sorter: '_date_time_desc'
};

export default STATE;
