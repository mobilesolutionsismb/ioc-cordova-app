import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getActiveFiltersDefinitions } from 'js/utils/getActiveFiltersDefinitions';

import { FILTERS as REPORT_FILTERS } from 'js/modules/reportFilters/dataSources/filters';
import { FilterMap as Report_FilterMap } from 'js/modules/reportFilters/dataProviders/withReportFilters';

import { FILTERS as EMCOMM_FILTERS } from 'js/modules/emcommFilters/dataSources/filters';
import { FilterMap as Emcomms_FilterMap } from 'js/modules/emcommFilters/dataProviders/withEmergencyCommunicationFilters';

import { FILTERS as TWEET_FILTERS } from 'js/modules/tweetsFilters/dataSources/filters';
import { FilterMap as Tweet_FilterMap } from 'js/modules/tweetsFilters/dataProviders/withTweetsFilters';

// import { FILTERS as REPORT_REQUEST_FILTERS } from 'js/modules/reportRequestFilters/dataSources/filters';
// import { FilterMap as ReportRequest_FilterMap } from 'js/modules/reportRequestFilters/dataProviders/withReportRequestFilters';

import { FILTERS as MISSION_FILTERS } from 'js/modules/missionsFilters/dataSources/filters';
import { FilterMap as Mission_FilterMap } from 'js/modules/missionsFilters/dataProviders/withMissionsFilters';

// import { FILTERS as MAPREQUEST_FILTERS } from 'js/modules/mapRequestFilters/dataSources/filters';
// import { FilterMap as MapRequest_FilterMap } from 'js/modules/mapRequestFilters/dataProviders/withMapRequestFilters';

import { FILTERS as LAYER_FILTERS } from 'js/modules/mapLayerFilters/dataSources/filters';
import { FilterMap as Layer_FilterMap } from 'js/modules/mapLayerFilters/dataProviders/withMapLayerFilters';

function isOfType(itemType, isEqual = true) {
  return [isEqual ? '==' : '!=', 'itemType', itemType];
}

const select = createStructuredSelector({
  allFeatureFiltersDefinitions: state => {
    let defs = ['all'];
    const reportFilters = getActiveFiltersDefinitions(
      state.reportFilters.filters,
      Report_FilterMap,
      REPORT_FILTERS
    );
    if (reportFilters) {
      defs.push(['any', isOfType('report', false), ['all', isOfType('report'), reportFilters]]);
    }

    const emcommFilters = getActiveFiltersDefinitions(
      state.emcommFilters.filters,
      Emcomms_FilterMap,
      EMCOMM_FILTERS
    );
    if (emcommFilters) {
      defs.push([
        'any',
        isOfType('reportRequest', false),
        isOfType('emergencyCommunication', false),
        ['all', isOfType('emergencyCommunication'), emcommFilters]
      ]);
    }

    // const reportRequestFilters = getActiveFiltersDefinitions(
    //   state.reportRequestFilters.filters,
    //   ReportRequest_FilterMap,
    //   REPORT_REQUEST_FILTERS
    // );
    // if (reportRequestFilters) {
    //   defs.push([
    //     'any',
    //     isOfType('reportRequest', false),
    //     ['all', isOfType('reportRequest'), reportRequestFilters]
    //   ]);
    // }

    const tweetFilters = getActiveFiltersDefinitions(
      state.tweetFilters.filters,
      Tweet_FilterMap,
      TWEET_FILTERS
    );
    if (tweetFilters) {
      defs.push(['any', isOfType('tweet', false), ['all', isOfType('tweet'), tweetFilters]]);
    }

    const missionFilters = getActiveFiltersDefinitions(
      state.missionFilters.filters,
      Mission_FilterMap,
      MISSION_FILTERS
    );
    if (missionFilters) {
      defs.push(['any', isOfType('mission', false), ['all', isOfType('mission'), missionFilters]]);
    }

    // const mapRequestFilters = getActiveFiltersDefinitions(
    //   state.mapRequestFilters.filters,
    //   MapRequest_FilterMap,
    //   MAPREQUEST_FILTERS
    // );
    // if (mapRequestFilters) {
    //   defs.push([
    //     'any',
    //     isOfType('mapRequest', false),
    //     ['all', isOfType('mapRequest'), mapRequestFilters]
    //   ]);
    // }

    const mapLayerFilters = getActiveFiltersDefinitions(
      state.mapLayerFilters.filters,
      Layer_FilterMap,
      LAYER_FILTERS
    );
    if (mapLayerFilters) {
      defs.push(mapLayerFilters);
    }

    return defs.length === 1 ? null : defs;
  }
});
export default connect(select);
