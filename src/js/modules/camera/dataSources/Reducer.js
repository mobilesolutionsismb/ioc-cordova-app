import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import {
  TAKE_PICTURE,
  CANCEL_PICTURE,
  SET_STATUS_CAPTURING,
  CAPTURE_ERROR,
  SET_SAVE_TO_GALLERY
} from './Actions';

const mutators = {
  [TAKE_PICTURE]: {
    cameraImage: action => action.imageURL,
    status: 'ready'
  },
  [CANCEL_PICTURE]: {
    cameraImage: '',
    status: 'ready'
  },
  [SET_STATUS_CAPTURING]: {
    status: 'busy'
  },
  [CAPTURE_ERROR]: {
    status: 'error',
    error: action => action.error
  },
  [SET_SAVE_TO_GALLERY]: {
    saveToGallery: action => !!action.saveToGallery
  }
};

export default reduceWith(mutators, DefaultState);
