const STATE = {
  error: null,
  cameraImage: null,
  status: 'ready',
  saveToGallery: true
};

export default STATE;
