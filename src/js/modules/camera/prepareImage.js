export async function prepareImageForUpload(imageUrlOrBlob, asBlob = false) {
    return new Promise((success, fail) => {
        console.debug('prepareImageForUpload', imageUrlOrBlob);
        const canvas = document.createElement('canvas'),
            canvasContext = canvas.getContext('2d');
        const img = new Image();

        if (IS_CORDOVA && cordova.platformId === 'ios') { //its a blob
            const fd = asBlob ? null : new FormData();
            if (asBlob) {
                success(imageUrlOrBlob);
            }
            else {
                fd.append('image', imageUrlOrBlob, 'image/jpeg');
                console.log('%c Blob added to formdata', 'color:orange', imageUrlOrBlob);
                success(fd);
            }
        }
        else { //its an URL to a file
            img.onload = () => {
                canvas.width = img.width;
                canvas.height = img.height;
                canvasContext.drawImage(img, 0, 0, img.width, img.height);
                const fd = asBlob ? null : new FormData();
                canvas.toBlob(function (blob) {
                    if (!asBlob) {
                        fd.append('image', blob, 'image/jpeg');
                    }
                    else {
                        success(blob);
                    }
                    console.log('%c Blob loaded', 'color:gold', blob);
                }, 'image/jpeg', 1);
            };
            img.onabort = fail;
            img.onerror = fail;
            img.src = imageUrlOrBlob;
        }
    });
}