import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { setSaveToGallery } from '../dataSources/ActionCreators';

const select = createStructuredSelector({
  saveToGallery: state => state.camera.saveToGallery
});

export default connect(
  select,
  { setSaveToGallery }
);
