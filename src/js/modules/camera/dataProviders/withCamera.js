import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import {takePicture, clearCamera} from '../dataSources/ActionCreators';

const select = createStructuredSelector({
    cameraImage: state => state.camera.cameraImage,
    cameraStatus: state => state.camera.status,
    cameraError: state => state.camera.error
});

export default connect(select, {takePicture, clearCamera});