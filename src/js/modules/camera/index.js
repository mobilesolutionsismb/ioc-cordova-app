import Reducer from './dataSources/Reducer';
import withCamera from './dataProviders/withCamera';
import withCameraPreferences from './dataProviders/withCameraPreferences';

export { takePicture, clearCamera } from './dataSources/ActionCreators';

export default Reducer;
export { Reducer, withCamera, withCameraPreferences };
