import { API } from 'ioc-api-interface';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { logMain } from 'js/utils/log';

const api = API.getInstance();
/**
REQUEST
interface IPartialUser {
    userId: null || number
    name: string
    surname: string
    userName: string
    password: string
    emailAddress: string
    phoneNumber: string
    country?: string
    province?: string // region used so far only for selecting province...
    city?: string
    captchaResponse: ''
    bypassCaptchaToken: 'bypass'
}

REQUEST
IPhoneNumberValidation {
  emailAddress: string // usare userId: number se rinominiamo
  confirmationCode: string
}

REQUEST
IAbortRegistration {
   userId: number
}

RESPONSE
interface IRegistrationResult {
   isPhoneConfirmed: boolean
   isEmailConfirmed: boolean,
   canLogin: true,
   userId: number
}
 */

const userDefaults = {
  // Values sent to backend
  userId: null,
  name: '',
  surname: '',
  userName: '',
  password: '',
  emailAddress: '',
  phoneNumber: '',
  country: '',
  // internal usage only
  prefix: '',
  phoneDigits: '',
  fullCountry: null,
  fullPrefix: null,
  isEmailConfirmed: false,
  isPhoneConfirmed: false,
  canLogin: false
};

// Following fields are NOT to be sent to the server
// either because read-only or either because internal variables
// such as prefix
const keyNameBlacklist = [
  'isEmailConfirmed',
  'isPhoneConfirmed',
  'prefix',
  'phoneDigits',
  'fullPrefix',
  'fullCountry',
  'canLogin'
];

export class PartialUser {
  constructor(config = {}) {
    const params = {
      ...userDefaults,
      ...config
    };

    const propsConfig = Object.keys(params).reduce((cfg, paramName) => {
      if (typeof userDefaults[paramName] !== 'undefined') {
        cfg[paramName] = {
          enumerable: !keyNameBlacklist.includes(paramName),
          configurable: false,
          get: () => params[paramName],
          set: value => {
            params[paramName] = value;
          }
        };
      }
      return cfg;
    }, {});

    Object.defineProperties(this, propsConfig);
  }

  static fromObject(partialUserObj) {
    return new PartialUser(partialUserObj || {});
  }

  // Used for sending the data to server
  toPlainObject() {
    const userPojo = Object.keys(userDefaults).reduce((pojo, keyName) => {
      const key = keyName;
      pojo[key] = this[key];
      return pojo;
    }, {});

    return {
      captchaResponse: '',
      bypassCaptchaToken: 'bypass',
      ...userPojo
    };
  }

  /**
   * Convert the PartialUser and returns a JSON string
   * used by serializer
   * @return String
   */
  toString = () => {
    return JSON.stringify(this);
  };

  _trackRegistrationError(method, error) {
    const partUserPojo = this.toPlainObject();
    appInsights.trackException(
      error,
      `PartialUser:${method}`,
      partUserPojo,
      {},
      SeverityLevel.Critical
    );
  }

  // REMOTE METHODS
  registerOrUpdateRegistration = async params => {
    try {
      for (const p of Object.keys(params)) {
        if (typeof userDefaults[p] !== 'undefined') {
          this[p] = params[p];
        }
      }
      const partUserPojo = this.toPlainObject(); // IPartialUser

      // if (IS_PRODUCTION) {
      const response = await api.account.register(partUserPojo);
      const result = response.result;
      const { isEmailConfirmed, isPhoneConfirmed, userId, canLogin } = result;
      this.isEmailConfirmed = isEmailConfirmed;
      this.isPhoneConfirmed = isPhoneConfirmed;
      this.userId = userId;
      this.canLogin = canLogin === true;
      return result; // IRegistrationResult
      // } else {
      //   this.userId = 1244;
      //   this.isEmailConfirmed = false;
      //   this.isPhoneConfirmed = false;
      //   return {
      //     isPhoneConfirmed: false,
      //     isEmailConfirmed: false
      //   }; // IRegistrationResult
      // }
    } catch (err) {
      this._trackRegistrationError('registerOrUpdateRegistration', err);
      return Promise.reject(err);
    }
  };

  confirmPhoneNumberAndFinalizeRegistration = async confirmationCode => {
    if (!this.emailAddress || !confirmationCode) {
      const errorMessage = !this.emailAddress
        ? 'Must have a valid email address'
        : 'Missing SMS confirmation code';
      const err = new Error(errorMessage);
      this._trackRegistrationError('checkIsFullyRegistered', err);
      return Promise.reject(err);
    } else {
      try {
        // if (IS_PRODUCTION) {
        const response = await api.account.finalizeRegistration(
          this.emailAddress,
          confirmationCode
        );
        const result = response.result;
        const { isEmailConfirmed, isPhoneConfirmed, userId, canLogin } = result;
        this.isEmailConfirmed = isEmailConfirmed;
        this.isPhoneConfirmed = isPhoneConfirmed;
        this.userId = userId;
        this.canLogin = canLogin === true;
        return result; // IRegistrationResult
        // } else {
        //   this.isEmailConfirmed = false;
        //   this.isPhoneConfirmed = true;
        //   return {
        //     isPhoneConfirmed: true,
        //     isEmailConfirmed: false
        //   }; // IRegistrationResult
        // }
      } catch (err) {
        this._trackRegistrationError('confirmPhoneNumberAndFinalizeRegistration', err);
        return Promise.reject(err);
      }
    }
  };

  checkIsFullyRegistered = async () => {
    if (this.userId === null) {
      const err = new Error('User must have a valid ID');
      this._trackRegistrationError('checkIsFullyRegistered', err);
      return Promise.reject(err);
    } else {
      try {
        // if (IS_PRODUCTION) {
        const response = await api.account.isUserFullyRegistered(this.userId);
        const result = response.result;
        const { isEmailConfirmed, isPhoneConfirmed /*, userId canLogin */ } = result;
        this.isEmailConfirmed = isEmailConfirmed;
        this.isPhoneConfirmed = isPhoneConfirmed;
        // this.userId = userId;
        // } else {
        //   this.isEmailConfirmed = Math.random() >= 0.5; // emulate random checking
        //   this.isPhoneConfirmed = true;
        // }

        return Promise.resolve(this.isPhoneConfirmed && this.isEmailConfirmed); // IRegistrationResult
      } catch (err) {
        this._trackRegistrationError('checkIsFullyRegistered', err);
        return Promise.reject(err);
      }
    }
  };

  resendPhoneActivationSMS = async () => {
    if (this.userId === null || !this.emailAddress || !this.phoneNumber) {
      const missingFieldName =
        this.userId === null ? 'ID' : !this.phoneNumber ? 'phone number' : 'email address';
      const err = new Error(`User must have a valid ${missingFieldName}`);
      this._trackRegistrationError('resendPhoneActivationSMS', err);
      return Promise.reject(err);
    } else {
      try {
        const response = await api.account.sendPhoneActivationSMS(
          this.emailAddress,
          this.phoneNumber
        );
        logMain('resendPhoneActivationSMS', response.success);
        return Promise.resolve(true);
      } catch (err) {
        this._trackRegistrationError('sendPhoneActivationSMS', err);
        return Promise.reject(err);
      }
    }
  };

  resendEmailActivationLink = async (redirectURL = '') => {
    if (this.userId === null || !this.emailAddress || !this.phoneNumber) {
      const missingFieldName =
        this.userId === null ? 'ID' : !this.phoneNumber ? 'phone number' : 'email address';
      const err = new Error(`User must have a valid ${missingFieldName}`);
      this._trackRegistrationError('resendEmailActivationLink', err);
      return Promise.reject(err);
    } else {
      try {
        const response = await api.account.sendEmailActivationLink(this.emailAddress);
        logMain('resendEmailActivationLink', response.success);
        return Promise.resolve(true);
      } catch (err) {
        this._trackRegistrationError('sendEmailActivationLink', err);
        return Promise.reject(err);
      }
    }
  };

  abortRegistration = async () => {
    if (!this.userId) {
      const err = new Error('User must have a valid ID');
      this._trackRegistrationError('abortRegistration', err);
      return Promise.reject(err);
    } else {
      try {
        // if (IS_PRODUCTION) {
        const response = await api.account.abortRegistration(this.userId);
        logMain('Registration aborted', response.result);
        // }
        return Promise.resolve(true);
      } catch (err) {
        this._trackRegistrationError('abortRegistration', err);
        return Promise.reject(err);
      }
    }
  };
}

export default PartialUser;
