import userRegistrationReducer from './dataSources/Reducer';
export { userRegistrationReducer as Reducer };
export { default as withPartialRegistration } from './dataProviders/withPartialRegistration';

export default userRegistrationReducer;
