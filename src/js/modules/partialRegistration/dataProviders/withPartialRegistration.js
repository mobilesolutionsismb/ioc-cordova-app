import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import {
  clearUser,
  saveOrUpdateUser,
  abortRegistration,
  confirmPhoneNumber,
  checkRegistrationIsComplete,
  resendPhoneActivationSMS,
  resendEmailActivationLink
} from '../dataSources/ActionCreators';

const select = createStructuredSelector({
  hasPendingRegistration: state => state.partialRegistration.partialUser !== null,
  partialUser: state => state.partialRegistration.partialUser,
  partialUserLastUpdate: state => state.partialRegistration.lastUpdate,
  partialUserPhoneNumberLastUpdate: state => state.partialRegistration.lastPhoneUpdate,
  partialUserEmailAddressLastUpdate: state => state.partialRegistration.lastEmailUpdate
});

export default connect(
  select,
  {
    clearUser,
    saveOrUpdateUser,
    abortRegistration,
    confirmPhoneNumber,
    checkRegistrationIsComplete,
    resendPhoneActivationSMS,
    resendEmailActivationLink
  }
);
