import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import { SAVE_USER, CLEAR_USER } from './Actions';

const mutators = {
  [CLEAR_USER]: {
    ...DefaultState
  },
  [SAVE_USER]: {
    partialUser: action => action.partialUserObject,
    lastUpdate: action => action.lastUpdate,
    lastPhoneUpdate: (action, state) =>
      action.lastPhoneUpdate === null ? state.lastPhoneUpdate : action.lastPhoneUpdate,
    lastEmailUpdate: (action, state) =>
      action.lastEmailUpdate === null ? state.lastEmailUpdate : action.lastEmailUpdate
  }
};

export default reduceWith(mutators, DefaultState);
