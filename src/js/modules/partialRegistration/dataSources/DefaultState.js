const state = {
  partialUser: null,
  lastUpdate: null,
  lastPhoneUpdate: null,
  lastEmailUpdate: null
};

export default state;
