import { SAVE_USER, CLEAR_USER } from './Actions';
import { PartialUser } from '../PartialUser';
function _saveUser(partialUserObject, phoneUpdated = false, emailUpdated = false) {
  const now = new Date().toISOString();
  return {
    type: SAVE_USER,
    partialUserObject,
    lastUpdate: now,
    lastPhoneUpdate: phoneUpdated ? now : null,
    lastEmailUpdate: emailUpdated ? now : null
  };
}

export function clearUser() {
  return {
    type: CLEAR_USER
  };
}

export function saveOrUpdateUser({ ...params }) {
  return async (dispatch, getState) => {
    const state = getState();
    const partialUserState = state.partialRegistration.partialUser;
    const phoneUpdated = partialUserState
      ? params.phoneNumber !== partialUserState.phoneNumber
      : true;
    const emailUpdated = partialUserState
      ? params.emailAddress !== partialUserState.emailAddress
      : true;
    const partial = PartialUser.fromObject(partialUserState);
    const result = await partial.registerOrUpdateRegistration(params);
    dispatch(_saveUser(partial.toPlainObject(), phoneUpdated, emailUpdated));
    return result;
  };
}

export function abortRegistration() {
  return async (dispatch, getState) => {
    const state = getState();
    let result = {};
    if (state.partialRegistration.partialUser !== null) {
      const partial = new PartialUser(state.partialRegistration.partialUser);
      result = await partial.abortRegistration();
      dispatch(clearUser());
    }
    return result;
  };
}

export function confirmPhoneNumber(pinCode) {
  return async (dispatch, getState) => {
    const state = getState();
    let result = {};
    if (state.partialRegistration.partialUser !== null) {
      const partial = new PartialUser(state.partialRegistration.partialUser);
      result = await partial.confirmPhoneNumberAndFinalizeRegistration(pinCode);
      dispatch(_saveUser(partial.toPlainObject(), false, true));
    }
    return result;
  };
}

export function checkRegistrationIsComplete() {
  return async (dispatch, getState) => {
    const state = getState();
    let result = {};
    if (state.partialRegistration.partialUser !== null) {
      const partial = new PartialUser(state.partialRegistration.partialUser);
      result = await partial.checkIsFullyRegistered();
      if (result === true) {
        dispatch(_saveUser(partial.toPlainObject(), false, false));
      }
    }
    return result;
  };
}

export function resendPhoneActivationSMS() {
  return async (dispatch, getState) => {
    const state = getState();
    let result = {};
    if (state.partialRegistration.partialUser !== null) {
      const partial = new PartialUser(state.partialRegistration.partialUser);
      result = await partial.resendPhoneActivationSMS();
      if (result === true) {
        dispatch(_saveUser(partial.toPlainObject(), true, false));
      }
    }
    return result;
  };
}

export function resendEmailActivationLink() {
  return async (dispatch, getState) => {
    const state = getState();
    let result = {};
    if (state.partialRegistration.partialUser !== null) {
      const partial = new PartialUser(state.partialRegistration.partialUser);
      result = await partial.resendEmailActivationLink();
      if (result === true) {
        dispatch(_saveUser(partial.toPlainObject(), false, true));
      }
    }
    return result;
  };
}
