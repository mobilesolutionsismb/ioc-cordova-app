const NS = 'PARTIAL_REG';

// export const SET_PARAMS = `${NS}@SET_PARAMS`;
// export const CLEAR_PARAMS = `${NS}@CLEAR_PARAMS`;
export const SAVE_USER = `${NS}@SAVE_USER`;
export const CLEAR_USER = `${NS}@CLEAR_USER`;
