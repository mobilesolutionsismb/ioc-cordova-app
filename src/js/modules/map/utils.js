// A collection of utilities for manipulating mapboxgl.Map
// See also https://github.com/mapbox/mapbox-gl-js/issues/4029

// See https://www.mapbox.com/mapbox-gl-js/api/#user interaction handlers
const mapInteractionHandlers = [
    'scrollZoom', 'dragPan', 'touchZoomRotate', 'dragRotate', 'keyboard', 'boxZoom', 'doubleClickZoom'
];

const rotationHandlers = [
    'touchZoomRotate', 'dragRotate'
];

/**
 * Returns true at least one of the above interaction handlers is enabled
 * @see options.interactive in https://www.mapbox.com/mapbox-gl-js/api/#map
 * that property has no setter nor getter
 * 
 * @param {mapboxgl.Map} map 
 * @param {String} rotationType ('auto'|'compass')
 * @returns {boolean} true if at least one handler is enabled
 */
export function isInteractionEnabled(map, rotationType = 'auto') {
    let handlers = rotationType === 'auto' ? mapInteractionHandlers : mapInteractionHandlers.filter(handlerName => rotationHandlers.indexOf(handlerName) === 1);
    return handlers.reduce((isEnabled, nextHandler) => {
        return map[nextHandler].isEnabled() && isEnabled;
    }, true);
}

/**
 * Enable interaction for all user interaction handlers
 * 
 * @param {mapboxgl.Map} map 
 * @param {String} rotationType ('auto'|'compass')
 */
export function enableInteraction(map, rotationType = 'auto') {
    let handlers = rotationType === 'auto' ? mapInteractionHandlers : mapInteractionHandlers.filter(handlerName => rotationHandlers.indexOf(handlerName) === 1);
    for (let handler of handlers) {
        map[handler].enable();
    }
}

/**
 * Disable interaction for all user interaction handlers
 * 
 * @param {mapboxgl.Map} map 
 * @param {String} rotationType ('auto'|'compass')
 */
export function disableInteraction(map, rotationType = 'auto') {
    let handlers = rotationType === 'auto' ? mapInteractionHandlers : mapInteractionHandlers.filter(handlerName => rotationHandlers.indexOf(handlerName) === 1);
    for (let handler of handlers) {
        map[handler].disable();
    }
}

/**
 * Disable interaction for all user interaction handlers
 * 
 * @param {mapboxgl.Map} map 
 * @returns {boolean} true if maxBounds has been set
 */
export function hasMaxBounds(map) {
    //Could be totally unreliable
    return (!Array.isArray(map.transform.latRange) || map.transform.latRange.length === 0) &&
        (!Array.isArray(map.transform.lngRange) || map.transform.lngRange.length === 0);
}

/**
 * Returns maxBounds as Array<Array> if they are set
 * This property lacks of getters in the Mapbox constructor
 * 
 * @param {mapboxgl.Map} map 
 * @returns {Array<Array>|null} bounds as [NE, SW], null if no maxBounds are set
 */
export function getMaxBounds(map) {
    //Could be totally unreliable
    if (!hasMaxBounds(map)) {
        return null;
    } else {
        let transform = map.transform;
        let { latRange, lngRange } = transform;
        let ne = [lngRange[1], latRange[1]];
        let sw = [lngRange[0], latRange[0]];
        return [ne, sw];
    }
}

/**
 * Detect if rotation handlers are enabled
 * 
 * @param {mapboxgl.Map} map 
 * @returns {string} 'auto' if user interaction for rotation is enabled, 'compass' if rotation can be set only via API
 */
export function detectRotationType(map) {
    return (map.dragRotate.isEnabled() || map.touchZoomRotate.isEnabled()) ? 'auto' : 'compass';
}

export function setRotationType(map, rotationType, prevRotationType) {
    switch (rotationType) {
        case 'auto':
            map.dragRotate.enable();
            map.touchZoomRotate.enable();
            if (prevRotationType === 'compass') {
                map.setBearing(0);
            }
            break;
        case 'compass':
            map.dragRotate.disable();
            map.touchZoomRotate.disable();
            break;
        default:
            break;
    }
}

/**
 * 
 * @param {Array|undefined} arrayA array of 2 number [lng, lat]
 * @param {Array|undefined} arrayB array of 2 number [lng, lat]
 * @returns {bool} if the two arrays are equal 
 */
export function compareLngLatLike(arrayA, arrayB) {
    if (Array.isArray(arrayA) && arrayA.length === 2 && Array.isArray(arrayB) && arrayB.length === 2) {
        return arrayA[0] === arrayB[0] && arrayA[1] === arrayB[1];
    } else {
        return (typeof arrayA === 'undefined' && typeof arrayB === 'undefined') || (arrayA === null && arrayB === null);
    }
}

/**
 * @param {Array<Array>|undefined} boundsA array of 2 lngLatLike
 * @param {Array<Array>|undefined} boundsB array of 2 lngLatLike
 * @returns {bool} true if bounds are equal
 */
export function compareBounds(boundsA, boundsB) {
    if (Array.isArray(boundsA) && boundsA.length === 2 && Array.isArray(boundsB) && boundsB.length === 2) {
        return compareLngLatLike(boundsA[0], boundsB[0]) && compareLngLatLike(boundsA[1], boundsB[1]);
    } else {
        return (typeof boundsA === 'undefined' && typeof boundsB === 'undefined') || (boundsA === null && boundsB === null);
    }
}

/**
 * Print debug only if environment === debug
 */
export function debug() {
    if (ENVIRONMENT === 'debug') {
        console.debug.apply(console, arguments);
    }
}