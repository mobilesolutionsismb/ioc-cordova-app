import DefaultState from './DefaultState';
import { UPDATE_MAP, RESET_MAP } from './Actions';

const VALID_SETTINGS_NAME = Object.keys(DefaultState);

export function resetMap() {
  return {
    type: RESET_MAP
  };
}

function updateMap(mapSettings) {
  return {
    type: UPDATE_MAP,
    ...mapSettings
  };
}

// Does not validate types or values
function filterSettings(candidateSettings) {
  return Object.keys(candidateSettings)
    .filter(setting => VALID_SETTINGS_NAME.indexOf(setting) > -1)
    .reduce((settings, nextSettingName) => {
      settings[nextSettingName] = candidateSettings[nextSettingName];
      return settings;
    }, {});
}

export function updateMapSettings(newSettings) {
  return (dispatch, getState) => {
    let mapState = getState().map;
    dispatch(
      updateMap(
        filterSettings({
          ...mapState,
          ...newSettings
        })
      )
    );
    //dispatch(updateMap({ ...mapState, ...newSettings }));
  };
}

export function increaseZoom() {
  return (dispatch, getState) => {
    let mapState = getState().map;
    let { zoom, maxZoom } = mapState;
    if (zoom + 1 <= maxZoom) {
      dispatch(
        updateMap({
          ...mapState,
          zoom: zoom + 1
        })
      );
    }
  };
}

export function decreaseZoom() {
  return (dispatch, getState) => {
    let mapState = getState().map;
    let { zoom, minZoom } = mapState;
    if (zoom - 1 >= minZoom) {
      dispatch(
        updateMap({
          ...mapState,
          zoom: zoom - 1
        })
      );
    }
  };
}

const FEATURE_LEVEL_ZOOM = 16;

export function zoomToFeature(featureCenter) {
  return (dispatch, getState) => {
    let mapState = getState().map;
    let nextCenter = featureCenter;
    if (
      (nextCenter[0] !== mapState.center[0] && nextCenter[1] !== mapState.center[1]) ||
      mapState.zoom < FEATURE_LEVEL_ZOOM
    ) {
      //should update
      dispatch(
        updateMap({
          ...mapState,
          center: nextCenter,
          zoom: Math.max(mapState.zoom, FEATURE_LEVEL_ZOOM)
        })
      );
    }
  };
}

export function disableMapCompassRotation() {
  return (dispatch, getState) => {
    let mapState = getState().map;
    dispatch(
      updateMap({
        ...mapState,
        rotation: 'auto'
      })
    );
  };
}

export function enableMapCompassRotation() {
  return (dispatch, getState) => {
    let mapState = getState().map;
    dispatch(
      updateMap({
        ...mapState,
        rotation: 'compass'
      })
    );
  };
}

export function toggleMapRotation() {
  return (dispatch, getState) => {
    let mapState = getState().map;
    dispatch(
      updateMap({
        ...mapState,
        rotation: mapState.rotation === 'auto' ? 'compass' : 'auto'
      })
    );
  };
}

export function setMaxBounds(maxBounds) {
  return (dispatch, getState) => {
    let mapState = getState().map;
    dispatch(
      updateMap({
        ...mapState,
        maxBounds
      })
    );
  };
}
