import { DEFAULT_ZOOM, MAX_ZOOM, MIN_ZOOM } from '../defaultMapSettings';

const STATE = {
  // Map constructor params
  minZoom: MIN_ZOOM,
  maxZoom: MAX_ZOOM,
  center: [0, 0],
  zoom: DEFAULT_ZOOM,
  bearing: 0,
  pitch: 0,
  // Map derived PARAMS
  bounds: undefined, //current bounds (from getBounds())
  interactive: true, //all handlers on|off (dragRotate, dragPan, boxZoom, scrollZoom, keyboard, doubleClickZoom, touchZoomRotate)
  maxBounds: null, //constraint
  rotation: 'auto' //map.dragRotate.isEnabled() && map.touchZoomRotate.isEnabled() -> 'compass' -> !map.dragRotate.isEnabled() && !map.touchZoomRotate.isEnabled()
};

export default STATE;
