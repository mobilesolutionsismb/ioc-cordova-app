import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { enableMapCompassRotation, disableMapCompassRotation, toggleMapRotation } from '../dataSources/ActionCreators';

const select = createStructuredSelector({
    rotation: state => state.map.rotation
});


export default connect(select, {
    enableMapCompassRotation, disableMapCompassRotation, toggleMapRotation
});