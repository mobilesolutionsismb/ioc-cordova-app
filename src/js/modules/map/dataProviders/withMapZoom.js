import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { increaseZoom, decreaseZoom, zoomToFeature } from '../dataSources/ActionCreators';

const select = createStructuredSelector({
    zoom: state => state.map.zoom,
    minZoom: state => state.map.minZoom,
    maxZoom: state => state.map.maxZoom
});


export default connect(select, {
    increaseZoom,
    decreaseZoom,
    zoomToFeature
});