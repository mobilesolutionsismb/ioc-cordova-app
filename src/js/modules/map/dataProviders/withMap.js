import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { updateMapSettings, resetMap } from '../dataSources/ActionCreators';

const select = createStructuredSelector({
    minZoom: state => state.map.minZoom,
    maxZoom: state => state.map.maxZoom,
    center: state => state.map.center,
    zoom: state => state.map.zoom,
    bearing: state => state.map.bearing,
    pitch: state => state.map.pitch,
    bounds: state => state.map.bounds,
    interactive: state => state.map.interactive,
    maxBounds: state => state.map.maxBounds,
    rotation: state => state.map.rotation
});


export default connect(select, {
    updateMapSettings,
    resetMap
});