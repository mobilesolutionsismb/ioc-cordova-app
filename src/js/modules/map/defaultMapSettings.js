export const DEFAULT_ZOOM = 9;
export const MAX_ZOOM = 19;
export const MIN_ZOOM = 1;

export const CONTAINER_STYLE = {
  position: 'absolute',
  top: 0,
  left: 0,
  height: '100%',
  width: '100%',
  textAlign: 'center',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center'
};

export const CONTROLS_STYLE = {
  position: 'absolute',
  display: 'block',
  zIndex: 99
};

export const CONTROLS_ROW_STYLE = {
  display: 'inline-block'
};
