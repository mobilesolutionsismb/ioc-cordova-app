import * as utils from './utils';
import Reducer from './dataSources/Reducer';
export * from './defaultMapSettings';
export { utils };

export {
  metersToPixelsAtMaxZoom,
  makeDefaultCircle,
  variableCircleInMeters,
  DEFAULT_POSITION_CIRCLE_PAINT
} from './mapFeaturesStyles';

export { default as withMap } from './dataProviders/withMap';
export { default as withMapRotation } from './dataProviders/withMapRotation';
export { default as withMapZoom } from './dataProviders/withMapZoom';
export { default as withMapBounds } from './dataProviders/withMapBounds';

export default {
  name: 'map',
  reducer: Reducer
};

export { Reducer };
