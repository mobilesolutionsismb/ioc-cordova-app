import { connect } from 'react-redux';
import { createStructuredSelector /* , defaultMemoize, createSelectorCreator */ } from 'reselect';
import * as ActionCreators from '../dataSources/ActionCreators';
import { OutputReport } from '../OutputReport';

const select = createStructuredSelector({
  newReportDraft: state =>
    state.newReport.newReportDraft ? OutputReport.fromObject(state.newReport.newReportDraft) : null,
  hasDraft: state => state.newReport.newReportDraft !== null,
  outgoingReportsQueueLength: state => state.newReport.outputReports.length,
  lastSentReportId: state => state.newReport.lastSentReportId
});

export default connect(
  select,
  ActionCreators
);
