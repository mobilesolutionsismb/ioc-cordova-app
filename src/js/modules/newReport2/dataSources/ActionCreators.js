import { apiCallActionCreatorFactory } from 'ioc-api-interface';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';

import { _updateReportsInner } from 'ioc-api-interface';
import nop from 'nop';
import {
  RESET_ALL,
  SEND_REPORT,
  ADD_DRAFT_TO_QUEUE,
  CREATE_REPORT_DRAFT,
  UPDATE_REPORT_DRAFT,
  DELETE_REPORT_DRAFT,
  FLAG_SENDING,
  SET_LAST_SENT_REPORT_ID,
  INIT_QUEUE
} from './Actions';
import { OutputReport } from '../OutputReport';
import { getNewReportStore, DRAFT_NAME_KEY } from '../commons';
import { pushMessage, pushModalMessage } from 'js/modules/ui/dataSources/ActionCreators';
import { logMain, logError } from 'js/utils/log';

function _setLastSentReportId(lastSentReportId = null) {
  return {
    type: SET_LAST_SENT_REPORT_ID,
    lastSentReportId
  };
}

export function clearLastSentReportId() {
  return _setLastSentReportId(null);
}

// TODO add serialize/deserialize functions
export function createNewReportDraft(picture, position) {
  return async (dispatch, getState) => {
    const state = getState();
    if (state.api_authentication.logged_in) {
      try {
        const userType = state.api_authentication.user.userType;
        const draft = new OutputReport();
        draft.userPosition = position;
        draft.pictureFile = picture;
        draft.visibility = userType === 'citizen' ? 'public' : 'private';
        const draftPojo = draft.toPlainObject();
        await getNewReportStore().setItem(DRAFT_NAME_KEY, draftPojo);
        dispatch({
          type: CREATE_REPORT_DRAFT,
          newReportDraft: draftPojo
        });
        return draft;
      } catch (ex) {
        logError('Error when creating the new report', ex);
        throw ex;
      }
    }
  };
}

export function updateReportDraft(update) {
  return async (dispatch, getState) => {
    const state = getState().newReport;
    const newReportDraft = state.newReportDraft;
    if (!newReportDraft) {
      return;
    } else {
      try {
        const newReportStore = getNewReportStore();
        const storedDraft = await newReportStore.getItem(DRAFT_NAME_KEY);
        if (storedDraft) {
          const draft = OutputReport.fromObject(storedDraft);
          for (let field in update) {
            draft[field] = update[field];
          }
          const draftPojo = draft.toPlainObject();
          await newReportStore.setItem(DRAFT_NAME_KEY, draftPojo);
          dispatch({
            type: UPDATE_REPORT_DRAFT,
            newReportDraft: draftPojo
          });
          return draft;
        }
      } catch (ex) {
        logError('Error when creating the new report', ex);
        throw ex;
      }
    }
  };
}

/**
 * Delete the current report draft
 */
export function deleteReportDraft() {
  return async dispatch => {
    try {
      await getNewReportStore().removeItem(DRAFT_NAME_KEY); // does not return anything
      const nrd = await getNewReportStore().getItem(DRAFT_NAME_KEY); // so better check
      if (nrd === null) {
        dispatch({ type: DELETE_REPORT_DRAFT });
      }
    } catch (ex) {
      logError('Error when deleting the new report draft', ex);
      throw ex;
    }
  };
}

/**
 * Clear the new report store and resets the db
 */
export function resetReportsDraftState() {
  return async dispatch => {
    try {
      await getNewReportStore().clear();
      dispatch({ type: RESET_ALL });
    } catch (ex) {
      logError('Error when deleting the new report storage', ex);
      throw ex;
    }
  };
}

function _flagSending(isSending) {
  return {
    type: FLAG_SENDING,
    isSending
  };
}

function _isValidationError(err) {
  let isValidationError = false;
  let error = err;
  if (
    err.details &&
    err.details.serverDetails &&
    err.details.serverDetails.error &&
    err.details.serverDetails.error.validationErrors &&
    err.details.serverDetails.error.validationErrors.length > 0
  ) {
    isValidationError = true;
    error = err.details.serverDetails.error.validationErrors;
  }

  return {
    error,
    isValidationError
  };
}

async function _sendQueuedReports(dispatch, getState) {
  //TODO pop report from queue
  const state = getState();
  const locale = state.locale.locale;
  const nrState = state.newReport;
  const queue = [...nrState.outputReports];
  console.debug('sendQueuedReports: queue', queue, queue.length);
  if (queue.length > 0 && !nrState.isSending) {
    dispatch(_flagSending(true));
    const nextItemKey = queue.shift();

    const newReportStore = getNewReportStore();

    const nextReportPojo = await newReportStore.getItem(nextItemKey);
    if (nextReportPojo) {
      const outgoingReport = OutputReport.fromObject(nextReportPojo);
      if (!outgoingReport.isValid() || !outgoingReport.sentByUser) {
        // REMOVE FROM QUEUE Pending drafts
        await newReportStore.removeItem(nextItemKey);
        const keys = await newReportStore.keys();
        dispatch({
          type: SEND_REPORT,
          outputReports: keys
        });
        dispatch(_flagSending(false));
        _sendQueuedReports(dispatch, getState);
        return null;
      } else {
        try {
          const { id, points, /*  blurred, */ explicit } = await apiCallActionCreatorFactory(
            null,
            null,
            'report',
            'uploadReport',
            '_report_submission_error',
            async response => {
              // Hocus pocus with the queue
              console.debug('REPORT SUBMITTED SUCCESSFULLY', response.result);
              try {
                await newReportStore.removeItem(nextItemKey);
                const keys = await newReportStore.keys();
                return {
                  type: SEND_REPORT,
                  outputReports: keys
                };
              } catch (e) {
                logError('Error when removing ', nextItemKey, 'from db');
                throw e;
              }
            },
            true,
            outgoingReport.toFormData(locale)
          )(dispatch, getState);
          dispatch(_flagSending(false));
          dispatch(_setLastSentReportId(id));
          if (explicit === true) {
            pushModalMessage(
              '_new_report_explicit_warning_title',
              '_new_report_explicit_warning_message',
              { _close: nop }
            );
          }
          if (points > 0) {
            pushMessage('_new_report_thanks_points', 'success', null, points)(dispatch);
          } else {
            pushMessage('_new_report_thanks')(dispatch);
          }
          try {
            await _updateReportsInner(dispatch, getState, false);
          } catch (err) {
            logMain('Error reloading reports list after successfull send', err);
            appInsights.trackException(
              err,
              'newReport2::_updateReportsInner',
              {},
              {},
              SeverityLevel.Error
            );
          }
          logMain('sendQueuedReports', id, 'remaining', queue.length);
          // Send recursively
          _sendQueuedReports(dispatch, getState);
          return id;
        } catch (err) {
          appInsights.trackException(
            err,
            'newReport2::_sendQueuedReports',
            {},
            {},
            SeverityLevel.Error
          );
          const errorResult = _isValidationError(err);
          logError('ERR sending report', err, 'IS VALIDATION ERROR', errorResult.isValidationError);
          appInsights.trackException(
            err,
            'newReport2::_updateReportsInner (sending)',
            {},
            {},
            SeverityLevel.Error
          );
          if (errorResult.isValidationError) {
            logError('Validation error from server!', errorResult.error);
            await newReportStore.removeItem(nextItemKey);
          }
          const keys = await newReportStore.keys();

          dispatch({
            type: SEND_REPORT,
            outputReports: keys
          });
          dispatch(_flagSending(false));
          return null;
        }
      }
    } else {
      return null;
    }
  }
}

export function startSendingEnqueuedReports() {
  return async (dispatch, getState) => {
    return _sendQueuedReports(dispatch, getState);
  };
}

// Persist DOES not support async transforms
export function initializeReportQueueAtStartup() {
  return async dispatch => {
    const newReportStore = getNewReportStore();
    try {
      const keys = await newReportStore.keys();
      dispatch({
        type: INIT_QUEUE,
        outputReports: keys
      });
    } catch (err) {
      throw err;
    }
  };
}

const INVALID_DRAFT_ERROR_MESSAGE =
  'The report draft is not valid and cannot be added to the queue';

export function enqueueReportDraft() {
  return async (dispatch, getState) => {
    const state = getState().newReport;
    const draft = state.newReportDraft;
    const outgoingReport = OutputReport.fromObject(draft);
    if (outgoingReport) {
      outgoingReport.sentByUser = true;
      if (outgoingReport.isValid()) {
        const storageKey = `or-${outgoingReport.usercreationguid}`;
        const newReportStore = getNewReportStore();
        const pojo = outgoingReport.toPlainObject();
        // Store with key = storagekey and save the key to redux state
        await newReportStore.setItem(storageKey, pojo);
        await newReportStore.removeItem(DRAFT_NAME_KEY); // does not return anything
        dispatch({
          type: ADD_DRAFT_TO_QUEUE,
          reportKey: storageKey
        });
      } else {
        throw new Error(INVALID_DRAFT_ERROR_MESSAGE);
      }
    } else {
      throw new Error(INVALID_DRAFT_ERROR_MESSAGE);
    }
  };
}
