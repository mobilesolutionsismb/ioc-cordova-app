import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import {
  RESET_ALL,
  SEND_REPORT,
  ADD_DRAFT_TO_QUEUE,
  CREATE_REPORT_DRAFT,
  UPDATE_REPORT_DRAFT,
  DELETE_REPORT_DRAFT,
  FLAG_SENDING,
  SET_LAST_SENT_REPORT_ID,
  INIT_QUEUE
} from './Actions';

const mutators = {
  [RESET_ALL]: {
    ...DefaultState
  },
  [SEND_REPORT]: {
    outputReports: action => action.outputReports
  },
  [FLAG_SENDING]: {
    isSending: action => action.isSending
  },
  [INIT_QUEUE]: {
    outputReports: (action, state) => [...state.outputReports, ...action.outputReports]
  },
  [ADD_DRAFT_TO_QUEUE]: {
    newReportDraft: null,
    outputReports: (action, state) => [...state.outputReports, action.reportKey]
  },
  [CREATE_REPORT_DRAFT]: {
    newReportDraft: action => action.newReportDraft
  },
  [UPDATE_REPORT_DRAFT]: {
    newReportDraft: action => action.newReportDraft
  },
  [DELETE_REPORT_DRAFT]: {
    newReportDraft: null
  },
  [SET_LAST_SENT_REPORT_ID]: {
    lastSentReportId: action => action.lastSentReportId
  }
};

export default reduceWith(mutators, DefaultState);
