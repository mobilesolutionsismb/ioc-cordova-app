const NS = 'NEW_REPORT';

export const CREATE_REPORT_DRAFT = `${NS}@CREATE_REPORT_DRAFT`;
export const UPDATE_REPORT_DRAFT = `${NS}@UPDATE_REPORT_DRAFT`;
export const DELETE_REPORT_DRAFT = `${NS}@DELETE_REPORT_DRAFT`;

export const ADD_DRAFT_TO_QUEUE = `${NS}@ADD_DRAFT_TO_QUEUE`; // push to reports queue
export const SEND_REPORT = `${NS}@SEND_REPORT`; // send report and remove from queue

export const FLAG_SENDING = `${NS}@FLAG_SENDING`; // signal report is being sent or not

export const RESET_ALL = `${NS}@RESET_ALL`; // send report and remove from queue

export const SET_LAST_SENT_REPORT_ID = `${NS}@SET_LAST_SENT_REPORT_ID`;
export const INIT_QUEUE = `${NS}@INIT_QUEUE`;
