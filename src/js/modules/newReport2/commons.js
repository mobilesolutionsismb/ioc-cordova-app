import localForage from 'localforage';

export const NEW_REPORT_STORAGE_NAME = 'newReportsDb';
export const DRAFT_NAME_KEY = 'new-report-draft';

const newReportStore = localForage.createInstance({
    name: NEW_REPORT_STORAGE_NAME,
    storeName: NEW_REPORT_STORAGE_NAME,
    description: 'storage for new reports'
});

// window.newReportStore = newReportStore;
// window.localForage = localForage;

export function getNewReportStore() {
    return newReportStore;
}