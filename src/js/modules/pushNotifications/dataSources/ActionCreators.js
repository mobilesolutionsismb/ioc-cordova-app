import {
    AVAILABLE,
    ENABLED,
    SET_READY_STATUS,
    RECEIVED,
    READ,
    ERROR,
    CLEAR_PUSH_ERRORS,
    UNSUBSCRIBE_AND_RESET
} from './Actions';
import { getPnManager } from '../PushNotificationsManager';

const pnManager = getPnManager();

function _setPushNotificationsReady(ready = false) {
    return {
        type: SET_READY_STATUS,
        ready
    };
}

function _setPushNotificationsEnabled(hasPermissions = false) {
    return {
        type: ENABLED,
        hasPermissions
    };
}

export function checkPushNotificationsAvailable() {
    console.log('%c checkPushNotificationsAvailable','color: palegoldenrod', pnManager.isAvailableForPlatform);
    return {
        type: AVAILABLE,
        isAvailableForPlatform: pnManager.isAvailableForPlatform
    };
}

function _receivePushNotification(notification) {
    return {
        type: RECEIVED,
        notification
    };
}

function _getPushNotificationsError(error) {
    return {
        type: ERROR,
        error
    };
}

export function clearPushNotificationsError() {
    return {
        type: CLEAR_PUSH_ERRORS
    };
}

export function initializePushNotifications() {
    return async dispatch => {
        try {
            const notificationHandler = notification => { dispatch(_receivePushNotification(notification)); };
            const notificationErrorHandler = err => { dispatch(_getPushNotificationsError(err)); };
            const result = await pnManager.initialize(notificationHandler, notificationErrorHandler);
            dispatch(_setPushNotificationsReady(pnManager.ready));
            const hasUserPermissions = await pnManager.checkPermissions();
            dispatch(_setPushNotificationsEnabled(hasUserPermissions));
            return result;
        }
        catch (err) {
            console.error(err);
            throw err;
        }
    };
}

export function checkUserPermissionsForPushNotifications() {
    return async dispatch => {
        const hasUserPermissions = await pnManager.checkPermissions();
        dispatch(_setPushNotificationsEnabled(hasUserPermissions));
        return hasUserPermissions;
    };
}

export function setPushNotificationRead(notification) {
    console.warn('setPushNotificationRead', notification);
    return {
        type: READ,
        notification
    };
}

export function unsubscribePushNotifications() {
    return async dispatch => {
        try {
            await pnManager.unsubscribeAll();           
            dispatch({
                type: UNSUBSCRIBE_AND_RESET
            });
        }
        catch (err) {
            console.error(err);
            throw err;
        }
    };
}