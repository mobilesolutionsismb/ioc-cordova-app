import { RECEIVED } from './dataSources/Actions';

/**
 * Return a function that allows to dispatch mocked-up push notification
 * to be used in App Developmenet and testing
 * @param {Function} dispatch - redux dispatch
 */
export function pnMock(dispatch) {
  return ({
    foreground = true,
    title = 'Mock-up PN',
    message = 'A mocked up Push Notification',
    itemType = 'Warning',
    itemId = -1,
    ...rest
  }) => {
    dispatch({
      type: RECEIVED,
      notification: {
        title,
        message,
        additionalData: {
          foreground,
          customProp: {
            Type: itemType,
            Id: itemId,
            ...rest
          }
        }
      }
    });
  };
}
