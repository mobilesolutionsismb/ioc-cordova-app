import { IREACT_ITEM_TYPES } from './mapStyles';
/* ENTITY TYPE FILTERS */
const _is_layer_report_visible = ['==', 'itemType', 'report'];
const _is_layer_rr_visible = ['==', 'itemType', 'reportRequest'];
const _is_layer_emcomm_visible = [
  'any',
  ['in', 'type', 'alert', 'warning'],
  ['==', 'itemType', 'emergencyCommunicationAOI']
];
const _is_layer_mission_visible = ['in', 'itemType', 'mission', 'missionTask', 'missionAOI'];
const _is_layer_agloc_visible = ['==', 'itemType', 'agentLocation'];
const _is_layer_tweets_visible = ['==', 'itemType', 'tweet'];
// const _is_layer_maprequest_visible = ['==', 'itemType', 'mapRequest'];
export const _no_layer_visible = ['==', 'itemType', 'undefined'];

export const layersVisibility = {
  _is_layer_report_visible,
  _is_layer_rr_visible,
  _is_layer_emcomm_visible,
  _is_layer_mission_visible,
  _is_layer_agloc_visible,
  _is_layer_tweets_visible,
  //   _is_layer_maprequest_visible,
  _no_layer_visible
};

export const FILTERS = {
  layersVisibility: Object.keys(layersVisibility)
};

export const availableFilters = Object.keys(FILTERS);

export function isValidFilter(filterCategory, filterName) {
  return FILTERS.hasOwnProperty(filterCategory) && FILTERS[filterCategory].indexOf(filterName) > -1;
}

const ITEMTYPES2FILTER = {
  report: _is_layer_report_visible,
  reportRequest: _is_layer_rr_visible,
  alertAndWarning: _is_layer_emcomm_visible,
  mission: _is_layer_mission_visible,
  agentLocation: _is_layer_agloc_visible,
  tweet: _is_layer_tweets_visible
};

export function itemType2Filter(itemType) {
  const validItemTypeFilter = ITEMTYPES2FILTER[itemType];
  return validItemTypeFilter ? validItemTypeFilter : null;
}

const initialFilters = IREACT_ITEM_TYPES.map(itemType => itemType2Filter);

export const INITIAL_FILTERS = ['all', initialFilters];
