import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';

import {
  SET_MAP_STYLE,
  SET_DEFAULT_MAP_STYLE,
  RESTORE_DEFAULT,
  SET_IREACT_FEATURES_ON_MAP,
  RESET_IREACT_FEATURES_ON_MAP,
  UPDATE_CONTENTS_RADIUS
} from './Actions';

const mutators = {
  [RESTORE_DEFAULT]: { ...DefaultState },
  [SET_DEFAULT_MAP_STYLE]: {
    mapStyleName: DefaultState.mapStyleName
  },
  [SET_MAP_STYLE]: {
    mapStyleName: action => action.mapStyleName
  },
  [RESET_IREACT_FEATURES_ON_MAP]: {
    itemTypesOnMap: DefaultState.itemTypesOnMap
  },
  [SET_IREACT_FEATURES_ON_MAP]: {
    itemTypesOnMap: action => action.itemTypesOnMap
  },
  [UPDATE_CONTENTS_RADIUS]: {
    contentsRadius: action => action.contentsRadius
  }
};

export default reduceWith(mutators, DefaultState);
