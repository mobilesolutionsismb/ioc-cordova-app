const NS = 'SETTINGS';

export const RESTORE_DEFAULT = `${NS}@RESTORE_DEFAULT`;
export const SET_DEFAULT_MAP_STYLE = `${NS}@SET_DEFAULT_MAP_STYLE`;
export const SET_MAP_STYLE = `${NS}@SET_MAP_STYLE`;

export const SET_IREACT_FEATURES_ON_MAP = `${NS}@SET_IREACT_FEATURES_ON_MAP`;
export const RESET_IREACT_FEATURES_ON_MAP = `${NS}@RESET_IREACT_FEATURES_ON_MAP`;

export const UPDATE_CONTENTS_RADIUS = `${NS}@UPDATE_CONTENTS_RADIUS`;
