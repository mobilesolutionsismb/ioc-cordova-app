import { AVAILABLE_STYLES, IREACT_ITEM_TYPES } from '../mapStyles';

export const DEFAULT_RADIUS = 50; //km // SAME AS api modules but independent

const state = {
  mapStyleName: AVAILABLE_STYLES[0],
  itemTypesOnMap: IREACT_ITEM_TYPES,
  contentsRadius: DEFAULT_RADIUS
};

export default state;
