import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { MAP_STYLES } from '../mapStyles';
import { getUserType } from 'js/utils/userPermissions';
import { IREACT_ITEM_TYPES, AUTH_ONLY_FEATURES } from '../mapStyles';
import { itemType2Filter, _no_layer_visible } from '../filters';

import {
  setMapStyleName,
  changeMapStyle,
  restoreDefaultMapPreferences,
  restoreIreactFeaturesOnMap,
  setIreactFeaturesOnMap,
  updateContentsRadius
} from '../dataSources/ActionCreators';

export const allFeatureFiltersSelector = state => {
  const itemTypesOnMap = state.preferences.itemTypesOnMap;
  const nextFiltersDef = itemTypesOnMap.map(itemType2Filter).filter(fdef => fdef !== null);
  return nextFiltersDef.length === 0 ? _no_layer_visible : ['any', ...nextFiltersDef];
};

const mapSettingsSelector = state => ({
  mapStyleName: state.preferences.mapStyleName,
  mapStyle: MAP_STYLES[state.preferences.mapStyleName]
});

const ireactFeaturesSelector = state => {
  const user = state.api_authentication.user;
  const itemTypesOnMap = state.preferences.itemTypesOnMap;
  const userType = getUserType(user);
  return user
    ? userType === 'citizen'
      ? itemTypesOnMap.filter(f => AUTH_ONLY_FEATURES.indexOf(f) === -1)
      : itemTypesOnMap
    : [];
};

const allIreactFeaturesSelector = state => {
  const user = state.api_authentication.user;
  const allValues = IREACT_ITEM_TYPES;
  const userType = getUserType(user);
  return user
    ? userType === 'citizen'
      ? allValues.filter(f => AUTH_ONLY_FEATURES.indexOf(f) === -1)
      : allValues
    : [];
};

const select = createStructuredSelector({
  preferences: mapSettingsSelector,
  ireactFeaturesOnMap: ireactFeaturesSelector,
  allSelectableItemTypes: allIreactFeaturesSelector,
  allFeatureFiltersDefinitions: allFeatureFiltersSelector,
  contentsRadius: state => state.preferences.contentsRadius
});

export default connect(
  select,
  {
    setMapStyleName,
    changeMapStyle,
    restoreDefaultMapPreferences,
    restoreIreactFeaturesOnMap,
    setIreactFeaturesOnMap,
    updateContentsRadius
  }
);
