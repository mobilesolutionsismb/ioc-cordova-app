export const AVAILABLE_STYLES = [
  'basic-gl',
  'basic-raster',
  'dark-matter-gl',
  'fiord-color-gl',
  'osm-bright-gl',
  // 'osm-raster',
  'positron-gl'
];

export const MAP_STYLES = AVAILABLE_STYLES.reduce((stylesMap, name) => {
  stylesMap[name] = `${MAP_STYLES_URL}${name}.json`;
  return stylesMap;
}, {});

export const MAP_STYLES_NAMES = Object.keys(MAP_STYLES);

export const IREACT_ITEM_TYPES = [
  'report',
  'reportRequest',
  'alertAndWarning',
  'mission',
  'agentLocation',
  'tweet'
];

export const AUTH_ONLY_FEATURES = ['mission', 'agentLocation'];
