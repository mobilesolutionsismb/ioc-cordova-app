import Reducer from './dataSources/Reducer';
import withDevicePermissions from './dataProviders/withDevicePermissions';
export { switchToSettings } from './dataSources/helpers';
export default Reducer;
export { Reducer, withDevicePermissions };
