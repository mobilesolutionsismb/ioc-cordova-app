import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { GRANTED /* , NOT_REQUESTED, DENIED, DENIED_ALWAYS */ } from '../dataSources/helpers';
import DefaultState from '../dataSources/DefaultState';
import * as ActionCreators from '../dataSources/ActionCreators';

const selectorsMap = Object.keys(DefaultState).reduce((selectorsMap, nextKey) => {
  selectorsMap[`${nextKey}Permission`] = state => state.devicePermissions[nextKey];
  selectorsMap[`${nextKey}IsGranted`] = state => state.devicePermissions[nextKey] === GRANTED;
  return selectorsMap;
}, {});

const select = createStructuredSelector(selectorsMap);

export default connect(
  select,
  ActionCreators
);
