import { NOT_REQUESTED } from './helpers';

const STATE = {
  location: NOT_REQUESTED,
  camera: NOT_REQUESTED,
  gallery: NOT_REQUESTED,
  notifications: NOT_REQUESTED
};

export default STATE;
