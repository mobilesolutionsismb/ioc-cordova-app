const NS = 'BLE';

export const RESET_ALL = `${NS}@RESET_ALL`;
export const START_SCAN = `${NS}@START_SCAN`;
export const DEVICE_FOUND = `${NS}@DEVICE_FOUND`;
export const STOP_SCAN = `${NS}@STOP_SCAN`;

export const SET_ENABLED = `${NS}@SET_ENABLED`;
export const CONNECTING = `${NS}@CONNECTING`;
export const CONNECT = `${NS}@CONNECT`;
export const DISCONNECT = `${NS}@DISCONNECT`;
export const NOTIFICATION_START = `${NS}@NOTIFICATION_START`;
export const NOTIFICATION_RECEIVED = `${NS}@NOTIFICATION_RECEIVED`;
export const NOTIFICATION_STOP = `${NS}@NOTIFICATION_STOP`;
export const CLEAR_MESSAGES = `${NS}@CLEAR_MESSAGES`;
