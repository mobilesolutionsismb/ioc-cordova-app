import {
  RESET_ALL,
  START_SCAN,
  STOP_SCAN,
  DEVICE_FOUND,
  SET_ENABLED,
  CONNECTING,
  CONNECT,
  DISCONNECT,
  NOTIFICATION_START,
  NOTIFICATION_STOP,
  NOTIFICATION_RECEIVED,
  CLEAR_MESSAGES
} from './Actions';
import { MockupDevice } from './MockupDevice';
import { BTDevice } from './bleOperations';
import { pushMessage } from 'js/modules/ui/dataSources/ActionCreators';
import { logMain, logWarning } from 'js/utils/log';

const TIMEOUT = 3 * 60; //seconds
const IREACT_DEVICE_NAME_RX = /^iREACT*/i; // e.g. 'iREACT-BLE-5';

const passAny = () => true;

function isIreactDeviceName(device) {
  return device.name && IREACT_DEVICE_NAME_RX.test(device.name);
}

function _scanning(isScanning = false) {
  return {
    type: isScanning ? START_SCAN : STOP_SCAN
  };
}

function _deviceFound(device) {
  return {
    type: DEVICE_FOUND,
    device
  };
}

function _setBTEnabled(isBtEnabled) {
  return {
    type: SET_ENABLED,
    isBtEnabled
  };
}

function _connecting() {
  return {
    type: CONNECTING
  };
}

function _connected(device) {
  return {
    type: CONNECT,
    device
  };
}

function _disconnected() {
  return {
    type: DISCONNECT
  };
}

function _resetAll() {
  return {
    type: RESET_ALL
  };
}

function _onNotificationStart() {
  return {
    type: NOTIFICATION_START
  };
}

function _onNotificationStop() {
  return {
    type: NOTIFICATION_STOP
  };
}

function _onNotificationReceived(message) {
  return {
    type: NOTIFICATION_RECEIVED,
    message
  };
}

export function clearMessages() {
  return {
    type: CLEAR_MESSAGES
  };
}

export function checkBTEnabled() {
  return async (dispatch, getState) => {
    try {
      if (IS_CORDOVA && window.device && !window.device.isVirtual) {
        if (BTDevice.ble) {
          const isEnabled = await BTDevice.isEnabled();
          logMain('Check BT Enabled...', isEnabled);
          dispatch(_setBTEnabled(isEnabled));
        }
      } else {
        const isEnabled = await MockupDevice.isEnabled();
        dispatch(_setBTEnabled(isEnabled));
      }
    } catch (err) {
      throw err;
    }
  };
}

export function startScan() {
  return async (dispatch, getState) => {
    const btState = getState().bluetooth;
    if (!btState.isScanning) {
      try {
        const onDeviceFound = device => {
          device['isIreact'] = isIreactDeviceName(device);
          logMain('Device found!', device);
          dispatch(_deviceFound(device));
        };
        if (IS_CORDOVA && window.device && !window.device.isVirtual) {
          dispatch(_scanning(true));
          if (BTDevice.ble) {
            await BTDevice.startScan(TIMEOUT, passAny, onDeviceFound);
            dispatch(_scanning(false));
          } else {
            pushMessage('Missing Bluetooth Plugin!', 'warning', '_close')(dispatch);
            dispatch(_scanning(false));
          }
        } else {
          dispatch(_scanning(true));
          pushMessage('Web UI with mocked up BT device', 'warning')(dispatch);

          await MockupDevice.startScan(TIMEOUT, passAny, onDeviceFound);
          dispatch(_scanning(false));
        }
      } catch (err) {
        // if (err.message === 'Already Scanning') {
        //   return;
        // }
        const btState = getState().bluetooth;
        if (btState.isScanning) {
          dispatch(_scanning(false));
          throw err;
        }
        // else it means stopScan() has been called
      }
    }
  };
}

export function stopScan() {
  return async (dispatch, getState) => {
    const btState = getState().bluetooth;
    if (btState.isScanning) {
      try {
        if (BTDevice.ble) {
          await BTDevice.stopScan();
          logMain('BT scan stop');
        } else {
          await MockupDevice.stopScan();
        }
        dispatch(_scanning(false));
      } catch (err) {
        dispatch(_scanning(false));
        throw err;
      }
    }
  };
}

export function connectDevice(deviceObject) {
  return async (dispatch, getState) => {
    const btState = getState().bluetooth;
    if (!btState.isScanning && !btState.isConnecting && !btState.isConnected) {
      dispatch(_connecting());
      logMain('Connecting to', deviceObject.id);
      let device = null;
      try {
        if (BTDevice.ble) {
          device = await BTDevice.connect(
            deviceObject,
            err => {
              dispatch(_disconnected(err));
              logWarning('Self disconnected', err);
            }
          );
        } else {
          const currentPosition = getState().geolocation.currentPosition;
          device = await MockupDevice.connect(
            deviceObject,
            currentPosition
          );
        }
        device['isIreact'] = true;
        logMain('BT device connected', deviceObject.id, device);
        dispatch(_connected(device));
        await startBTMessageListening()(dispatch, getState);
      } catch (err) {
        dispatch(_disconnected());
        throw err;
      }
    }
  };
}

const MAX_AGE = 5; // minutes from last message
function timeDiff(date) {
  const diff = new Date() - date;
  return isNaN(diff) ? -1 : Math.round(diff / 60000);
}

export function checkBTIsConnected() {
  return async (dispatch, getState) => {
    const btState = getState().bluetooth;
    let deviceConnected = false;
    if (
      !btState.isScanning &&
      !btState.isConnecting &&
      btState.isConnected &&
      btState.selectedDevice !== null
    ) {
      try {
        if (
          btState.messages.length === 0 ||
          (btState.messages.length > 0 && timeDiff(new Date(btState.messages[0].time)) >= MAX_AGE)
        ) {
          const deviceId = btState.selectedDevice.id;
          if (BTDevice.ble) {
            deviceConnected = await BTDevice.isConnected(deviceId);
            if (!deviceConnected) {
              dispatch(_disconnected());
            }
          } else {
            const currentPosition = getState().geolocation.currentPosition;
            await MockupDevice.connect(
              btState.selectedDevice,
              currentPosition
            );
            const device = await MockupDevice.getDeviceInstance(
              btState.selectedDevice,
              currentPosition
            );
            await connectDevice(device)(dispatch, getState);
            deviceConnected = getState().bluetooth.isConnected;
          }
        } else if (!BTDevice.ble) {
          // page reload
          const currentPosition = getState().geolocation.currentPosition;
          await MockupDevice.connect(
            btState.selectedDevice,
            currentPosition
          );
          const device = await MockupDevice.getDeviceInstance(
            btState.selectedDevice,
            currentPosition
          );
          await connectDevice(device)(dispatch, getState);
          deviceConnected = getState().bluetooth.isConnected;
          if (getState().bluetooth.isNotifying) {
            await startBTMessageListening()(dispatch, getState);
          }
        }
      } catch (err) {
        dispatch(_scanning(false));
        dispatch(_disconnected());
        throw err;
      }
    }
    return deviceConnected;
  };
}

export function disconnectDevice() {
  return async (dispatch, getState) => {
    const btState = getState().bluetooth;
    if (!btState.isScanning && btState.selectedDevice) {
      try {
        if (btState.isNotifying) {
          await stopBTMessageListening()(dispatch, getState);
        }
        // dispatch(_connecting());
        if (BTDevice.ble) {
          await BTDevice.disconnect(btState.selectedDevice.id);
          logMain('BT device disconnected');
        } else {
          await MockupDevice.disconnect(btState.selectedDevice.id);
        }
        dispatch(_disconnected());
      } catch (err) {
        dispatch(_disconnected());
        throw err;
      }
    }
  };
}

export function resetBTAll() {
  return async (dispatch, getState) => {
    const btState = getState().bluetooth;
    if (btState.selectedDevice) {
      if (BTDevice.ble) {
        await BTDevice.disconnect(btState.selectedDevice.id);
      } else {
        await MockupDevice.disconnect(btState.selectedDevice.id);
      }
    }
    dispatch(_resetAll());
  };
}

// CHECK IS ACTUALLY ASYNC AWAIT
export function startBTMessageListening() {
  return async (dispatch, getState) => {
    const btState = getState().bluetooth;
    if (btState.selectedDevice) {
      dispatch(_onNotificationStart());
      const onNotification = message => dispatch(_onNotificationReceived(message));
      if (BTDevice.ble) {
        const btDevice = await BTDevice.getDeviceInstance(btState.selectedDevice.id);
        if (btDevice) {
          await btDevice.startNotification(onNotification);
        }
        // await bleStartNotification(btState.selectedDevice.id);
      } else {
        const mockupDevice = await MockupDevice.getDeviceInstance(btState.selectedDevice);
        await mockupDevice.startNotification(onNotification);
      }
    }
  };
}

export function stopBTMessageListening() {
  return async (dispatch, getState) => {
    const btState = getState().bluetooth;
    if (btState.isNotifying) {
      if (BTDevice.ble) {
        const btDevice = await BTDevice.getDeviceInstance(btState.selectedDevice.id);
        await btDevice.stopNotification();
      } else {
        const mockupDevice = await MockupDevice.getDeviceInstance(btState.selectedDevice);
        await mockupDevice.stopNotification();
      }
      dispatch(_onNotificationStop());
    }
  };
}
