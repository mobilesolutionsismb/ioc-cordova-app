const IREACT_TRANSPARENT_SERVICE_ID = '49535343-fe7d-4ae5-8fa9-9fafd205e455';
const IREACT_TRANSPARENT_CHARACTERISTIC_ID = '49535343-1e4d-4bd9-ba61-23c647249616';

const SCAN_SERVICES = []; //[IREACT_TRANSPARENT_SERVICE_ID];

const SCAN_OPTIONS = {
  reportDuplicates: false
};

const leadingRegexp = /^{"time"/;
const trailingRegexp = /\r\n$/;

class BTMessageBuilder {
  constructor(onMessageBuild) {
    this._onSuccess =
      typeof onMessageBuild === 'function' ? onMessageBuild : m => console.log('Message Built', m);
    this._messages = []; //{key: Symbol, messageString: string}
    this._keys = []; // Array<Symbol>
  }

  _deleteKey(key) {
    this._keys.splice(this._keys.indexOf(key), 1);
    this._messages = [...this._messages.filter(mo => mo.key !== key)];
  }

  /**
   * Try to compute a message from an array of strings
   * @param {Array<string>} messages
   */
  _tryBuildMessages(key) {
    try {
      const messages = this._messages.filter(mo => mo.key === key).map(mo => mo.messageString);
      if (Array.isArray(messages)) {
        const message = JSON.parse(messages.join('').trim());
        this._onSuccess(message);
      }
      this._deleteKey(key);
    } catch (err) {
      console.warn('Throwing everything away', err);
      this._deleteKey(key);
    }
  }

  add(messageString) {
    if (leadingRegexp.test(messageString)) {
      // first message string of a sequence
      const key = Symbol();
      this._keys.push(key);
      this._messages.push({
        key,
        messageString
      });
    } else {
      const currentKey = this._keys[this._keys.length - 1];
      this._messages.push({
        key: currentKey,
        messageString
      });
      if (trailingRegexp.test(messageString)) {
        // last message string of a sequence
        this._tryBuildMessages(currentKey);
      }
    }
  }
}

function clearScanTimeout() {
  if (typeof BTDevice.clearTimeoutHandler === 'function') {
    BTDevice.clearTimeoutHandler();
    BTDevice.clearTimeoutHandler = null;
  }
  if (BTDevice.scanTimeout !== null) {
    clearTimeout(BTDevice.scanTimeout);
    BTDevice.canTimeout = null;
  }
}

async function _scanAndConnect(deviceId) {
  let device = null;
  try {
    const findFn = device => device.id === deviceId;
    const devices = await BTDevice.startScan(20, findFn, () => {});
    let device = devices.length ? devices.find(findFn) : null;
    if (device) {
      device = await BTDevice.connect(deviceId);
    } else {
      throw new Error(`Connection lost with ${deviceId}`);
    }
  } catch (err) {
    throw err;
  }
  return device;
}

function _getDeviceId(device) {
  return device === null || typeof device === 'undefined'
    ? null
    : typeof device === 'string'
      ? device
      : typeof device.id === 'string'
        ? device.id
        : null;
}

export class BTDevice {
  static scanTimeout = null;
  static clearTimeoutHandler = null;
  static scanCanceled = false;

  // The connected device if any
  static selectedDeviceInstance = null;
  // The list of results of scan
  static devices = [];
  static decoder = new TextDecoder('utf-8');

  // see https://github.com/don/cordova-plugin-ble-central/blob/master/www/ble.js#L188
  static ble =
    IS_CORDOVA && window.device && !window.device.isVirtual && window.ble ? window.ble : null;
  static blePromises =
    IS_CORDOVA && window.device && !window.device.isVirtual && window.ble
      ? window.ble.withPromises
      : {};

  /**
   * Get selectedDeviceInstance checking its connection status and trying a connect if lost
   * @param {Device|DevicObject|string} device
   */
  static async getDeviceInstance(device) {
    const deviceId = _getDeviceId(device);
    try {
      if (BTDevice.selectedDeviceInstance === null) {
        const peripheralObj = await _scanAndConnect(deviceId);
        BTDevice.selectedDeviceInstance = new BTDevice(peripheralObj);
      } else {
        const connected = await BTDevice.isConnected(deviceId);
        if (!connected) {
          const peripheralObj = await _scanAndConnect(deviceId);
          BTDevice.selectedDeviceInstance = new BTDevice(peripheralObj);
        }
        return BTDevice.selectedDeviceInstance;
      }
    } catch (err) {
      BTDevice.selectedDeviceInstance = null;
      try {
        await BTDevice.disconnect(deviceId);
      } catch (err) {
        console.warn(err);
      }
      throw new Error('Cannot select device');
    }
    return BTDevice.selectedDeviceInstance;
  }

  // https://github.com/don/cordova-plugin-ble-central#isenabled
  static async isEnabled() {
    let enabled = false;
    try {
      await BTDevice.blePromises.isEnabled();
      enabled = true;
    } catch (e) {}
    return enabled;
  }

  // https://github.com/don/cordova-plugin-ble-central#isconnected
  static async isConnected(deviceId) {
    let connected = false;
    try {
      await BTDevice.blePromises.isConnected(deviceId);
      connected = true;
    } catch (e) {}
    return connected;
  }

  /**
   * @see https://github.com/don/cordova-plugin-ble-central#startscan
   * @param {number} timeout in seconds
   * @param {function} filterFn callback for filtering devices
   * @param {function} onDeviceFound callaback for found devices
   */
  static startScan(timeout, filterFn = () => true, onDeviceFound) {
    return new Promise((success, failure) => {
      if (BTDevice.scanTimeout !== null) {
        clearScanTimeout();
        // failure(new Error('Already scanning'));
      }
      const TIMEOUT = timeout * 1000;
      BTDevice.devices = [];
      BTDevice.scanCanceled = false;

      BTDevice.clearTimeoutHandler = () => {
        BTDevice.scanCanceled = true;
        success(BTDevice.devices);
      };

      function onScan(device) {
        // Duplicate removal seems not to work properly
        if (filterFn(device) && BTDevice.devices.findIndex(d => d.id === device.id) === -1) {
          device['name'] = device.name ? device.name : 'Unknown Device';
          BTDevice.devices.push(device);
          onDeviceFound(device);
        }
      }

      BTDevice.ble.startScanWithOptions(SCAN_SERVICES, SCAN_OPTIONS, onScan, failure);

      BTDevice.scanTimeout = setTimeout(clearScanTimeout, TIMEOUT);
    });
  }

  /**
   * @see https://github.com/don/cordova-plugin-ble-central#connect
   * @param {Device|DevicObject|string} device or deviceId
   */
  static connect(device, onSelfDisconnect = () => {}) {
    return new Promise((success, failure) => {
      const deviceId = _getDeviceId(device);
      if (deviceId) {
        // NOTE: the connect failure callback will be called if the peripheral disconnects.
        const handlePotentialDisconnect = err => {
          const errstring =
            typeof err === 'string'
              ? err
              : typeof err.errorMessage === 'string'
                ? err.errorMessage
                : typeof err.errorString === 'string'
                  ? err.errorString
                  : typeof err.message === 'string'
                    ? err.message
                    : '';
          if (err.length === 0) {
            console.warn('Unknown disconnection error', err);
          }
          if (errstring.startsWith('Peripheral Disconnected')) {
            onSelfDisconnect(err);
          } else {
            failure();
          }
        };

        BTDevice.ble.connect(
          deviceId,
          device => {
            BTDevice.selectedDeviceInstance = new BTDevice(device);
            success(BTDevice.selectedDeviceInstance);
          },
          handlePotentialDisconnect
        );
      } else {
        failure(new Error('Missing device id'));
      }
    });
  }

  // https://github.com/don/cordova-plugin-ble-central#peripheral-data
  constructor(peripheralData) {
    for (const k of Object.keys(peripheralData)) {
      this[k] = peripheralData[k];
    }
    this.messageBuilder = null;
  }

  _onNotificationReceived = buffer => {
    if (this.messageBuilder) {
      this.messageBuilder.add(BTDevice.decoder.decode(buffer));
    }
  };

  _onNotificationStop = null;

  /**
   * @see https://github.com/don/cordova-plugin-ble-central#startnotification
   * @param {Function} onNotification
   * @param {Device|DevicObject|string} device or deviceId
   */
  startNotification = onNotification => {
    return new Promise((success, failure) => {
      this.messageBuilder = new BTMessageBuilder(onNotification);
      this._onNotificationStop = () => {
        success();
      };

      BTDevice.ble.startNotification(
        this.id,
        IREACT_TRANSPARENT_SERVICE_ID,
        IREACT_TRANSPARENT_CHARACTERISTIC_ID,
        this._onNotificationReceived,
        failure
      );
    });
  };

  /**
   * @see https://github.com/don/cordova-plugin-ble-central#stopscan
   */
  static async stopScan() {
    try {
      await BTDevice.blePromises.stopScan();
      BTDevice.scanCanceled = true;
      clearScanTimeout();
    } catch (e) {
      throw e;
    }
  }

  static disconnect(deviceId) {
    return deviceId ? BTDevice.blePromises.disconnect(deviceId) : Promise.resolve();
  }

  stopNotification = () => {
    this.messageBuilder = null;
    if (typeof this._onNotificationStop === 'function') {
      this._onNotificationStop();
      this._onNotificationStop = null;
    }
    return BTDevice.blePromises.stopNotification(
      this.id,
      IREACT_TRANSPARENT_SERVICE_ID,
      IREACT_TRANSPARENT_CHARACTERISTIC_ID
    );
  };
}
