import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import {
  RESET_ALL,
  START_SCAN,
  DEVICE_FOUND,
  STOP_SCAN,
  SET_ENABLED,
  CONNECTING,
  CONNECT,
  DISCONNECT,
  NOTIFICATION_START,
  NOTIFICATION_RECEIVED,
  NOTIFICATION_STOP,
  CLEAR_MESSAGES
  // CHECK_STATUS,
  // ENABLE,
  // READ_RSSI,
  // RESET_ALL
} from './Actions';

const MAX_MESSAGES = 5;

const mutators = {
  [RESET_ALL]: {
    ...DefaultState
  },
  [SET_ENABLED]: {
    isBtEnabled: action => action.isBtEnabled
  },
  [START_SCAN]: {
    isScanning: true,
    availableDevices: []
  },
  [STOP_SCAN]: {
    isConnecting: false,
    isScanning: false
  },
  [DEVICE_FOUND]: {
    availableDevices: (action, state) => [...state.availableDevices, action.device]
  },
  [CONNECTING]: {
    isConnecting: true,
    isConnected: false,
    isScanning: false
  },
  [CONNECT]: {
    isConnecting: false,
    isConnected: true,
    isScanning: false,
    selectedDevice: action => action.device
  },
  [DISCONNECT]: {
    isConnecting: false,
    isConnected: false,
    isNotifying: false,
    selectedDevice: null,
    messages: []
  },
  [NOTIFICATION_START]: {
    isNotifying: true
    // messages: []
  },
  [NOTIFICATION_RECEIVED]: {
    messages: (action, state) => [action.message, ...state.messages.slice(0, MAX_MESSAGES - 1)]
  },
  [NOTIFICATION_STOP]: {
    isNotifying: false
    // messages: []
  },
  [CLEAR_MESSAGES]: {
    messages: []
  }
};

export default reduceWith(mutators, DefaultState);
