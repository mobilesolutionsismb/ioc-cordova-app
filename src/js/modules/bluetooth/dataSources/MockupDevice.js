const MOCKUP_NOTIFICATION_INTERVAL = 1000; // 1s

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Credits
function genMAC() {
  let hexDigits = '0123456789ABCDEF';
  let macAddress = '';
  for (let i = 0; i < 6; i++) {
    macAddress += hexDigits.charAt(Math.round(Math.random() * 15));
    macAddress += hexDigits.charAt(Math.round(Math.random() * 15));
    if (i !== 5) macAddress += ':';
  }

  return macAddress;
}

const NON_IREACT_NAMES = ['Funky Device ', 'Headset ', 'iBlio i4678', 'Unknown Device '];

function getRandomName() {
  return NON_IREACT_NAMES[Math.floor(Math.random() * NON_IREACT_NAMES.length)];
}

function perturbate(value, percent, positive = false) {
  const amount = (Math.random() < 0.5 ? -1 : 1) * percent;
  return positive ? Math.max(value * (1 - amount), 0) : value * (1 - amount);
}

function randomDevice(forceIreact = false) {
  const randomInt = Math.round(Math.random() * 100);
  const IS_IREACT = forceIreact || randomInt % 3 === 0;
  return {
    name: `${IS_IREACT ? 'iREACT-BLE-' : getRandomName()}${randomInt}`,
    id: genMAC(),
    advertising: {},
    rssi: -Math.round(Math.random() * 180)
  };
}

function clearScanTimeout() {
  if (typeof MockupDevice.clearTimeoutHandler === 'function') {
    MockupDevice.clearTimeoutHandler();
    MockupDevice.clearTimeoutHandler = null;
  }
  if (MockupDevice.scanTimeout !== null) {
    clearTimeout(MockupDevice.scanTimeout);
    MockupDevice.scanTimeout = null;
  }
}

export class MockupDevice {
  static selectedDeviceInstance = null;

  static devices = [];

  static scanTimeout = null;
  static scanCanceled = false;

  static clearTimeoutHandler = null;

  static startScan(timeout, filterFn, onDeviceFound) {
    return new Promise((success, fail) => {
      if (MockupDevice.scanTimeout !== null) {
        fail(new Error('Already scanning'));
      }

      MockupDevice.devices = [];
      const TIMEOUT = timeout * 1000;
      const INTERVAL = TIMEOUT / 100;
      MockupDevice.scanCanceled = false;
      let interval = setInterval(() => {
        if (MockupDevice.scanCanceled) {
          clearInterval(interval);
          return;
        }
        if ((Math.round(Math.random() * 100) % 7) % 2 === 0) {
          const device = randomDevice();
          device['isIreact'] = filterFn(device);
          MockupDevice.devices.push(device);
          onDeviceFound(device);
        }
      }, INTERVAL);

      MockupDevice.clearTimeoutHandler = () => {
        MockupDevice.scanCanceled = true;
        clearInterval(interval);
        success(MockupDevice.devices);
      };

      MockupDevice.scanTimeout = setTimeout(clearScanTimeout, TIMEOUT);
    });
  }

  static stopScan() {
    return new Promise(success => {
      clearScanTimeout();
      MockupDevice.scanCanceled = true;
      setTimeout(() => {
        success(MockupDevice.devices);
      }, 150);
      // if (MockupDevice.scanTimeout !== null) {
      //   clearTimeout(MockupDevice.scanTimeout);
      //   MockupDevice.scanTimeout = null;
      // }
      // setTimeout(() => success(MockupDevice.devices), 150); //150ms delay
    });
  }

  static isEnabled() {
    return new Promise(success => {
      setTimeout(() => success(true), 100); //100ms delay
    });
  }

  static connect(device, geolocation = null) {
    return new Promise(success => {
      MockupDevice.getDeviceInstance(device, geolocation).then(device => {
        setTimeout(() => success(MockupDevice.selectedDeviceInstance), 400); //400ms delay
      });
    });
  }

  static disconnect(deviceId) {
    if (MockupDevice.selectedDeviceInstance.id === deviceId) {
      MockupDevice.selectedDeviceInstance = null;
    }
    return Promise.resolve();
  }

  // Used by, for instance, notification methods
  static getDeviceInstance(device, geolocation = null) {
    if (MockupDevice.selectedDeviceInstance === null) {
      MockupDevice.selectedDeviceInstance = new MockupDevice(device, geolocation);
    }
    return Promise.resolve(MockupDevice.selectedDeviceInstance);
  }

  constructor(deviceObject = null, geolocation = null) {
    const deviceExists = MockupDevice.selectedDeviceInstance !== null;
    if (!deviceExists) {
      let _device = deviceObject ? deviceObject : randomDevice(true);
      const keys = Object.keys(_device);
      for (let devKey of keys) {
        this[devKey] = _device[devKey];
      }

      if (geolocation) {
        // make new from geolcation
        const { latitude, longitude, altitude } = geolocation.coords;
        this._valid = 1;
        this._latitude = latitude;
        this._longitude = longitude;
        this._altitude = altitude || getRandomInt(0, 51);
      } else {
        this._valid = 0;
        this._latitude = 0;
        this._longitude = 0;
        this._altitude = 0;
      }
      this._batteryStatus = getRandomInt(30, 100); // %
      this._temperature = getRandomArbitrary(-5, 42); // celsius
      this._pressure = getRandomArbitrary(900, 1050); // mBar
      this._humidity = getRandomInt(30, 70); //%
      this._oxigen = Math.round(Math.random() * 10) < 7 ? getRandomArbitrary(19.5, 23.5) : null; // null = no sensor
    } else {
      throw new Error('Device already exists: ' + MockupDevice.selectedDeviceInstance.id);
    }
  }

  _notificationInterval = null;

  _clearNotificationIntervalHandler = null;

  _updateEnvir = () => {
    this._temperature = perturbate(this._temperature, 0.02);
    this._pressure = this._pressure > 0 ? perturbate(this._pressure, 0.005, true) : 0;
    this._humidity = this._humidity > 0 ? perturbate(this._humidity, 0.02, true) : 0;
    const envir = { T: this._temperature, P: this._pressure, H: this._humidity };
    if (this._oxigen !== null) {
      this._oxigen = perturbate(this._oxigen, 0.00001, true);
      envir['O2'] = this._oxigen;
    }
    return envir;
  };

  _updateNavData = () => {
    this._latitude = this._latitude > 0 ? perturbate(this._latitude, 0.00001) : 0;
    this._longitude = this._longitude > 0 ? perturbate(this._longitude, 0.00001) : 0;
    this._altitude = this._altitude > 0 ? perturbate(this._altitude, 0.01, true) : 0;
    return {
      valid: this._valid,
      lat: this._latitude,
      lon: this._longitude,
      alt: this._altitude
    };
  };

  _genMessage = () => {
    this._batteryStatus = this._batteryStatus > 0 ? this._batteryStatus * 0.999 : 0; // simulate a 0.1%/message battery depletion rate
    const navData = this._updateNavData();
    const envir = this._updateEnvir();

    return {
      time: new Date().toISOString(),
      minSetGNSSRxData: {
        svid: [],
        psRng: [],
        resid: [],
        cPhase: [],
        dopp: [],
        azEl: [],
        cNo: [],
        nSvs: 0
      },
      /*
        Value of number “valid” is 0 or 1. If device doesn’t have valid GNSS position value is 0, or If device have valid GNSS position value is 1.
        Value of number “lat” is latitude [deg].
        Value of number “lon” is longitude [deg].
        Value of number “alt” is altitude [m].
       */
      navData,
      devStatus: { batt: Math.round(this._batteryStatus) },
      envir,
      CS: genMAC().slice(0, 2)
    };
  };

  startNotification = onNotification => {
    return new Promise(success => {
      if (this._notificationInterval !== null) {
        clearInterval(this._notificationInterval);
        this._notificationInterval = null;
      }
      this._notificationInterval = setInterval(() => {
        if (this._notificationInterval) {
          onNotification(this._genMessage());
        }
      }, MOCKUP_NOTIFICATION_INTERVAL);

      this._clearNotificationIntervalHandler = () => {
        clearInterval(this._notificationInterval);
        this._notificationInterval = null;
        success();
      };
    });
  };

  stopNotification = () => {
    return new Promise(success => {
      if (typeof this._clearNotificationIntervalHandler === 'function') {
        this._clearNotificationIntervalHandler();
      }
      success();
    });
  };
}

export default MockupDevice;
