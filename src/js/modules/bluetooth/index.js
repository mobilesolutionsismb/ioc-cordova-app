import Reducer from './dataSources/Reducer';
import withBluetooth from './dataProviders/withBluetooth';

export { withBluetooth, Reducer };

export default Reducer;
