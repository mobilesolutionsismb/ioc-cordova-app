import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const SORT_IREACT_DEVICE_FIRST = (d1, d2) =>
  d1.isIreact === d2.isIreact ? 0 : d1.isIreact ? -1 : 1;

const isDeviceAvailable = state =>
  state.bluetooth.selectedDevice !== null &&
  state.bluetooth.isBtEnabled &&
  state.bluetooth.isConnected;
const isScanning = state => state.bluetooth.isScanning;
const selectedDevice = state => state.bluetooth.selectedDevice;
const devices = state => state.bluetooth.availableDevices.sort(SORT_IREACT_DEVICE_FIRST);
const isConnected = state => state.bluetooth.isConnected;
const isConnecting = state => state.bluetooth.isConnecting;
const isBtEnabled = state => state.bluetooth.isBtEnabled;
const isNotifying = state => state.bluetooth.isNotifying;
const bleMessages = state => state.bluetooth.messages;
const lastBleMessage = state =>
  state.bluetooth.messages.length > 0 ? state.bluetooth.messages[0] : null;
// const rssiValue = state => state.bluetooth.rssiValue;

const select = createStructuredSelector({
  isDeviceAvailable,
  isScanning,
  isConnected,
  isConnecting,
  isBtEnabled,
  selectedDevice,
  devices,
  isNotifying,
  bleMessages,
  lastBleMessage
});

export default connect(
  select,
  ActionCreators
);
