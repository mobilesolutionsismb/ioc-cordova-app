import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import { CONNECTION_TYPE_CHANGED, STATUS_CHANGED } from './Actions';

const mutators = {
    [CONNECTION_TYPE_CHANGED]: {
        connectionType: action => action.connectionType,
        // offline: action => action.connectionType === 'none'
    },
    [STATUS_CHANGED]: {
        offline: action => action.offline
    }
};

export default reduceWith(mutators, DefaultState);