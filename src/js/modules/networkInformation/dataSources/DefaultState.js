export const HAS_NAVIGATOR_ONLINE = typeof navigator.onLine === 'boolean';

const initialNetworkStatus = HAS_NAVIGATOR_ONLINE ? !navigator.onLine: (navigator.connection && navigator.connection.type === 'none' );

const STATE = {
    offline: initialNetworkStatus,
    connectionType: 'unknown'
};

export default STATE;