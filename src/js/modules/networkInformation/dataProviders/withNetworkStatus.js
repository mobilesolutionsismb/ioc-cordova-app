import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const offlineSelector = state => state.networkInformation.offline;
const connectionTypeSelector = state => state.networkInformation.connectionType;

const select = createStructuredSelector({
    isOffline: offlineSelector,
    connectionType: connectionTypeSelector
});

export default connect(select, ActionCreators);