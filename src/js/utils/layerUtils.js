import geoServerParams from './geoserverParams';
import axios from 'axios';

/**
 * Compose Layer URL params
 * @param {Object} params
 * @return {String} url params
 */
function composeParams(params) {
  return Object.keys(params)
    .reduce((par, key) => {
      par = par.concat([`${key}=${params[key]}`]);
      return par;
    }, [])
    .join('&');
}

/**
 * Compose a layer request URL
 * @param {String|Array<String>} layerNames
 * @param {String} geoserverURL
 * @param {String} format
 */
export function makeLayerURL(layerNames, geoserverURL, format = 'wmts') {
  const settings = geoServerParams.params[format];
  const { suffix, params } = settings;
  const layerName = Array.isArray(layerNames) ? layerNames.join(',') : layerNames;
  let url;
  switch (format) {
    case 'wms':
      url = `${geoserverURL}/${suffix}?${composeParams(params)}&layers=${layerName}`;
      break;
    case 'vector-tiles':
      url = `${geoserverURL}/gwc/service/${suffix}?${composeParams(params)}&layers=${layerName}`;
      break;
    case 'wmts':
      url = `${geoserverURL}/gwc/service/${suffix}?${composeParams(params)}&layer=${layerName}`;
      break;
    default:
      break;
  }
  return url;
}

/**
 * Create a TileJSON
 * @param {String} name
 * @param {String} geoserverURL
 * @param {String} format
 * @param {String} scheme
 * @param {String} type
 * @param {String} minzoom
 * @param {String} maxzoom
 * @param {String} bounds
 * @param {String} center
 */
export function tileJSONIfy(
  name,
  geoserverURL,
  format = 'wmts',
  scheme = 'tms',
  bounds = [-26.19140625, 33.87041555094183, 42.01171875, 71.52490903732816],
  center = [7.91015625, 52.69766229413499],
  minzoom = 0,
  maxzoom = 24,
  type = 'raster'
) {
  return {
    type: type,
    tilejson: '2.2.0',
    name: name,
    description: 'layer description...',
    version: '1.0.0',
    attribution: "<a href='http://ireact.eu'>I-REACT</a>",
    scheme: scheme, //xyz or tms
    tiles: [makeLayerURL(name, geoserverURL, format)],
    data: [],
    minzoom: minzoom,
    maxzoom: maxzoom,
    bounds: bounds,
    center: center
  };
}

export function getFeatureInfoUrl(
  point, // feature point, as mapboxgl.Point
  width, // width in pixel of the map
  height, // height in pixel of the map
  bounds, // map bounds, as mapboxgl.LngLatBounds
  geoserverUrl, // string: geoserver URL
  layers, // string or Array<string> layers to be queryied about
  version = '1.1.0',
  headers = {}
) {
  const isMultiple = Array.isArray(layers);
  const _layers = isMultiple ? layers.join(',') : layers;
  const feature_count = isMultiple ? layers.length : 1;
  const params = {
    request: 'GetFeatureInfo',
    service: 'WMS',
    srs: 'EPSG:4326',
    styles: '',
    transparent: true,
    version: version,
    format: 'image/png',
    bbox: toBBoxString(bounds),
    height,
    width,
    layers: _layers,
    query_layers: _layers,
    feature_count,
    info_format: 'application/json',
    exceptions: 'application/json'
  };
  params[params.version === '1.3.0' ? 'i' : 'x'] = Math.round(point.x);
  params[params.version === '1.3.0' ? 'j' : 'y'] = Math.round(point.y);

  return axios({
    headers,
    method: 'get',
    url: `${geoserverUrl.replace(/\/$/, '')}/gwc/service`, //:GEOSERVER_URL,// 'https://geoserver.ireact.cloud/geoserver/gwc/service/',
    params
  });
}

function toBBoxString(lngLatBound) {
  const _southWest = lngLatBound.getSouthWest();
  const _northEast = lngLatBound.getNorthEast();
  return [_southWest.lng, _southWest.lat, _northEast.lng, _northEast.lat].join(',');
}
