/**
 * Flatten a structure ready for form data
 * @param {Object} data 
 * @param {String} optional locale
 */
export function flattenObject(data, locale) {
    const result = {};
    function recurse(cur, prop) {
        // TODO add other instanceofs
        if (cur instanceof Date) {
            result[prop] = cur.toISOString();
        }
        else if (typeof cur === 'number') {
            result[prop] = cur.toLocaleString(locale);
        }
        else if (cur instanceof Blob || Object(cur) !== cur) {
            result[prop] = cur;
        } else if (Array.isArray(cur)) {
            const l = cur.length;
            for (let i = 0; i < l; i++)
                recurse(cur[i], `${prop}[${i}]`);
            if (l === 0)
                result[prop] = [];
        } else {
            let isEmpty = true;
            for (const p in cur) {
                isEmpty = false;
                recurse(cur[p], prop ? `${prop}.${p}` : p);
            }
            if (isEmpty && prop)
                result[prop] = {};
        }
    }
    recurse(data, '');
    return result;
}

export default flattenObject;