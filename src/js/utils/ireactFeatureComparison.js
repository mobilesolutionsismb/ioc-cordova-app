import booleanEqual from '@turf/boolean-equal';
import { point } from '@turf/helpers';

function _hasNaNs(pointFeat) {
  const coordinates = Array.isArray(pointFeat.coordinates)
    ? pointFeat.coordinates
    : pointFeat.geometry.coordinates;
  const lon = coordinates[0];
  const lat = coordinates[1];
  return coordinates.length === 2 && (isNaN(lat) || lat === null || isNaN(lon) || lon === null);
}
/**
 *
 * @param {GeoJSON} ftA - ireact feature with unique(id, itemType)
 * @param {GeoJSON} ftB - ireact feature with unique(id, itemType)
 */
export function areFeaturesEqual(ftA = null, ftB = null) {
  if (ftA && ftB) {
    const sameProps =
      ftA.properties.id === ftB.properties.id &&
      ftA.properties.itemType === ftB.properties.itemType;

    // Since we have tweets now, they may have NaNs for coordinates
    const A_hasNaNs = _hasNaNs(ftA);
    const B_hasNaNs = _hasNaNs(ftB);
    const _bothNaNs = A_hasNaNs && B_hasNaNs;
    const _anyNaNs = A_hasNaNs || B_hasNaNs;

    return sameProps === false
      ? sameProps
      : _bothNaNs
        ? true
        : _anyNaNs //CHECK
          ? true
          : booleanEqual(
              Array.isArray(ftA.coordinates) ? point(ftA.coordinates).geometry : ftA.geometry,
              Array.isArray(ftB.coordinates) ? point(ftB.coordinates).geometry : ftB.geometry
            );
  } else if (ftA === null && ftB === null) {
    return true;
  } else return false;
}

/**
 *
 * @param {LngLatLike} ptA
 * @param {LngLatLike} ptB
 */
export function arePointsEqual(ptA = null, ptB = null) {
  if (ptA === null && ptB === null) {
    return true;
  } else if (ptA !== null && ptB !== null) {
    const pt1 = mapboxgl.LngLat.convert(ptA).toArray();
    const pt2 = mapboxgl.LngLat.convert(ptB).toArray();
    return pt1[0] === pt2[0] && pt1[1] === pt2[1];
  } else return false;
}

export const IREACT_POINT_FEATURES_TYPES = ['report', 'missionTask', 'agentLocation', 'tweet'];

export function isIREACTPointFeature(itemType) {
  return IREACT_POINT_FEATURES_TYPES.indexOf(itemType) !== -1;
}

function _isNullOrUndefined(x) {
  return x === null || typeof x === 'undefined';
}

/**
 *
 * @param {LngLatBoundsLike} lngLatBoundsLike1
 * @param {LngLatBoundsLike} lngLatBoundsLike2
 */
export function areBoundsEqual(lngLatBoundsLike1, lngLatBoundsLike2) {
  const isN1 = _isNullOrUndefined(lngLatBoundsLike1);
  const isN2 = _isNullOrUndefined(lngLatBoundsLike2);
  if (isN1 === isN2 && isN1 === true) {
    return true;
  } else if (isN1 !== isN2 && (isN1 === true || isN2 === true)) {
    return false;
  } else {
    const b1 = mapboxgl.LngLatBounds.convert(lngLatBoundsLike1).toArray();
    const b2 = mapboxgl.LngLatBounds.convert(lngLatBoundsLike2).toArray();
    return (
      b1[0][0] === b2[0][0] &&
      b1[0][1] === b2[0][1] &&
      b1[1][0] === b2[1][0] &&
      b1[1][1] === b2[1][1]
    );
  }
}
