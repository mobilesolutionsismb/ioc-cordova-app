function normalizeRotation(nR) {
    let aR = this.rot % 360;
    if (aR < 0) {
        aR += 360;
    }
    if (aR < 180 && (nR - aR > 180)) {
        this.rot -= 360;
    }
    if (aR >= 180 && (nR <= (aR - 180))) {
        this.rot += 360;
    }
    this.rot += (nR - aR);
    return this.rot;
}
 export default normalizeRotation;