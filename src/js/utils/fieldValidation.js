import { isEmail, normalizeEmail, isEmpty, trim, isLength /* , isMobilePhone */ } from 'validator';

const MIN_PASSWORD_LENGTH = 6;
const MAX_PASSWORD_LENGTH = 32;

const MIN_USERNAME_LENGTH = 2;
const MAX_USERNAME_LENGTH = 32;

const MIN_NAME_LENGTH = 2;
const MAX_NAME_LENGTH = 32;

const VALID_MOBILE_NUMBER = /\d{4,15}/;
const IS_INVALID_USERNAME = /(^\d|\W+)/;

const PIN_LENGTH = 6;

const onlyLetters = str => !IS_INVALID_USERNAME.test(str);
/**
 * Enhanced version of Attire.validate
 * Main difference: validator must return an empty string if valid, a meaningful validation error message if not valid
 * validator: (d: string) => string
 * Credits https://github.com/gianmarcotoso/react-attire/blob/master/src/validate.ts
 * @param {Object} rules - validation rules {key : validator}
 * @param {Object} data - data to be validated
 */
export function validate(rules, data) {
  const closure = data => {
    return new Promise((success, fail) => {
      if (!rules || Object.keys(rules).length === 0) {
        return success(data);
      }

      let validationStatus = {};
      let hasErrors = false;
      validationStatus = Object.keys(rules).reduce((validationStatus, nextKey) => {
        const validationFn = rules[nextKey];
        const value = data[nextKey];
        const validationError = validationFn.call(null, value, data);
        if (hasErrors === false && validationError.length > 0) {
          hasErrors = true;
        }
        validationStatus[nextKey] = validationError;

        return validationStatus;
      }, validationStatus);

      hasErrors === false ? success(data) : fail(validationStatus);
    });
  };
  return data ? closure(data) : closure;
}

/**
 * Reduce output from several error rules
 * @param {Array<IValidationRule>} rules
 * @param {any} value - this field value
 * @param {any} state - form state
 */
export function reduceValidationErrors(rules, value, state) {
  return rules.reduce((errors, rule) => {
    const err = rule(value, state);
    if (err && err.text && err.text.length) {
      errors = [...errors, err];
    }
    return errors;
  }, []);
}

/**
 * Generate a validation rule against another fieldname
 * @param {string} fieldName - sttus field to be matched
 * @param {string} errorMessage? - error string to be displayed
 * @returns {Function} - a validation rule against fieldName (text field must match)
 */
export const confirmRule = (fieldName, errorMessage = null) => (
  passwordConfirmValue,
  validationState
) => {
  let errStr = null;
  if (passwordConfirmValue !== validationState[fieldName]) {
    errStr = { text: '_form_field_not_matching' }; //errorMessage || `${fieldName} does not match`;
  }
  return errStr;
};

export function emailRule(emailStr) {
  let errStr = null;
  if (isEmpty(emailStr)) {
    errStr = { text: '_form_field_not_empty' }; //'Email cannot be empty';
  } else {
    let candidateEmail = normalizeEmail(trim(emailStr), { all_lowercase: true });
    if (!isEmail(candidateEmail)) {
      errStr = { text: '_form_field_not_valid' }; //'You must insert a valid email';
    }
  }
  return errStr;
}

export function passwordRule(passwordStr) {
  let errStr = null;
  const min = MIN_PASSWORD_LENGTH,
    max = MAX_PASSWORD_LENGTH;
  let candidatePassword = trim(passwordStr);
  if (isEmpty(candidatePassword)) {
    errStr = { text: '_form_field_not_empty' };
  } else if (!isLength(candidatePassword, { min, max })) {
    errStr = { text: '_form_field_limits', min, max }; //`Password must be at least ${MIN_PASSWORD_LENGTH} and max ${MAX_PASSWORD_LENGTH} characters long`;
  }
  // TODO add more complexity if needed by the BE
  return errStr;
}

export function usernameRule(usernameStr) {
  let errStr = null;
  const min = MIN_USERNAME_LENGTH,
    max = MAX_USERNAME_LENGTH;
  let candidateUsername = trim(usernameStr);
  if (isEmpty(candidateUsername)) {
    errStr = { text: '_form_field_not_empty' }; //'Username cannot be empty';
  } else if (!isLength(candidateUsername, { min, max })) {
    errStr = { text: '_form_field_limits', min, max }; //`Username must be at least ${MIN_USERNAME_LENGTH} and max ${MAX_USERNAME_LENGTH} characters long`;
  } else if (!onlyLetters(candidateUsername)) {
    errStr = { text: '_form_field_only_letters' };
  }
  return errStr;
}

export function firstnameRule(firstnameStr) {
  let errStr = null;
  const min = MIN_NAME_LENGTH,
    max = MAX_NAME_LENGTH;
  let candidateValue = trim(firstnameStr);
  if (isEmpty(candidateValue)) {
    errStr = { text: '_form_field_not_empty' };
  } else if (!isLength(candidateValue, { min, max })) {
    errStr = { text: '_form_field_limits', min, max }; //`First name must be at least ${MIN_NAME_LENGTH} and max ${MAX_NAME_LENGTH} characters long`;
  }
  // TODO add more complexity if needed by the BE
  return errStr;
}

export function lastnameRule(lastnameStr) {
  let errStr = null;
  const min = MIN_NAME_LENGTH,
    max = MAX_NAME_LENGTH;
  let candidateValue = trim(lastnameStr);
  if (isEmpty(candidateValue)) {
    errStr = { text: '_form_field_not_empty' };
  } else if (!isLength(candidateValue, { min, max })) {
    errStr = { text: '_form_field_limits', min, max }; //`Last name must be at least ${MIN_NAME_LENGTH} and max ${MAX_NAME_LENGTH} characters long`;
  }
  // TODO add more complexity if needed by the BE
  return errStr;
}

export function mobilePhoneRule(phoneStr) {
  let errStr = null;
  if (isEmpty(phoneStr)) {
    errStr = { text: '_form_field_not_empty' };
  } else if (!VALID_MOBILE_NUMBER.test(phoneStr)) {
    errStr = { text: '_form_field_not_valid' };
  }
  return errStr;
}

export function pinDigitsRule(pincodeStr) {
  let errStr = null;
  const len = PIN_LENGTH;
  let candidateValue = trim(pincodeStr);
  if (isEmpty(candidateValue)) {
    errStr = { text: '_form_field_not_empty' };
  } else if (candidateValue.length !== len) {
    errStr = { text: '_form_field_exact_length', len }; //`Last name must be at least ${MIN_NAME_LENGTH} and max ${MAX_NAME_LENGTH} characters long`;
  }
  return errStr;
}
