import loadScripts from 'load-scripts';
import axios from 'axios';
import remark from 'remark';
import reactRenderer from 'remark-react';
import { /* IS_ANDROID, */ IS_IOS } from 'js/utils/getAppInfo';

const rootDirectoryProvider = {};

Object.defineProperty(rootDirectoryProvider, 'rootDirectory', {
  get: () =>
    IS_CORDOVA && cordova.file && cordova.file.applicationDirectory
      ? `${cordova.file.applicationDirectory}www/`
      : PUBLIC_PATH
});

const IS_B64 = /^data:image/i;

const staticAssets = {
  logoR: require('assets/logo/ireact_R.svg'),
  logo: require('assets/logo/logo_pin.svg'),
  logBg: require('assets/logo/login_screen_bg.jpg'),
  logoTextClear: require('assets/logo/logo_text_clear.svg')
};

export function getAsset(assetName, asURL = false) {
  if (!assetName) {
    return '';
  }
  const isStatic = typeof staticAssets[assetName] !== 'undefined';
  const assetPath = isStatic ? staticAssets[assetName] : assetName.replace(/^\//, '');

  const _asset =
    IS_B64.test(assetPath) || (!IS_CORDOVA && isStatic)
      ? assetPath
      : `${rootDirectoryProvider.rootDirectory}${assetPath}`;
  return asURL ? `url(${_asset})` : _asset;
}

export function openExternalLink(link) {
  const target = IS_CORDOVA ? '_system' : '_blank';
  const open = IS_CORDOVA ? cordova.InAppBrowser.open : window.open;
  open(link, target);
}

const MAP_APP_BASE_URL =
  (IS_CORDOVA && IS_IOS) || navigator.userAgent.includes('Macintosh; Intel Mac OS X 10_14')
    ? 'http://maps.apple.com/?q='
    : 'https://www.google.com/maps/search/?api=1&query=';

export function openMapAppLink(coordinates) {
  return () => openExternalLink(`${MAP_APP_BASE_URL}${coordinates[1]},${coordinates[0]}`);
}

let localeIsLoading = false;

export function getNumeralLocaleName(langCode) {
  let lc = langCode;
  switch (langCode) {
    case 'da':
      lc = 'da-dk';
      break;
    case 'en':
      lc = 'en-gb';
      break;
    default:
      break;
  }

  return lc;
}

export function loadNumeralLocale(locale) {
  const loc = getNumeralLocaleName(locale);
  return window.numeral.locales[loc] === undefined
    ? loadScripts(`${rootDirectoryProvider.rootDirectory}numeralLocales/${loc}.js`)
    : Promise.resolve();
}

export async function loadDictionary(locale) {
  if (localeIsLoading === false) {
    localeIsLoading = true;
    await loadNumeralLocale(locale);
    const translationResponse = await axios.get(
      `${rootDirectoryProvider.rootDirectory}locales/lang_${locale}/translation.json`
    );
    localeIsLoading = false;
    return {
      translationLoader: fn => {
        fn(translationResponse.data);
      },
      locale,
      loading: false
    }; // for backward comp with ioc-localization
  } else {
    return {
      loading: true
    };
  }
}

/**
 * Transform a markdown text into a React Component
 * @see https://github.com/mapbox/remark-react
 * @param {string} filename
 * @param {string} locale
 * @requires React.Component
 */
export async function getMarkdownTextAsComponent(filename, locale = 'en') {
  const pathname = `${rootDirectoryProvider.rootDirectory}locales/lang_${locale}/${filename}.md`;
  const markdownResponse = await axios.get(pathname);
  const text = markdownResponse.data;
  return remark()
    .use(reactRenderer)
    .processSync(text).contents;
}
