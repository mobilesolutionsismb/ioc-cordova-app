import axios from 'axios';
import nop from 'nop';
import { getTaskAttributesConfig } from './geodata-config';

const TEST_ENDPOINT = 'https://geodata.ireact.cloud/test/apigeoquery';
const DEV_ENDPOINT = 'https://geodata.ireact.cloud/test/apigeoquery';
const SERVICE_URL = IREACT_BACKEND_TYPE === 'DEV' ? DEV_ENDPOINT : TEST_ENDPOINT;

const geodataAPI = axios.create({
  baseURL: SERVICE_URL
});

/**
 * @typedef {Object} GetGeodataParams
 * @property {[Number, Number]} geom
 * @property {Array<Number>} tasks
 * @property {Array<Number>} metadatas
 */

/**
 * Get data from the geodata services
 * @param {GetGeodataParams} params
 * @param {Function} onData - callback for data
 * @param {Function} onError - callback for errors
 * @param {Function} onLoad - callback for loading state
 * @param {Function} onCancel - callback for cancelling request
 */
export async function getGeodata(
  params = {},
  onData = nop,
  onError = nop,
  onLoad = nop,
  onCancel = nop
) {
  onLoad(true);
  try {
    const attributes = params.tasks.map(getTaskAttributesConfig);
    const data = { mode: 'list', attributes, ...params };
    const response = await geodataAPI.post('/getfeatures', data, {
      // `cancelToken` specifies a cancel token that can be used to cancel the request
      cancelToken: new axios.CancelToken(onCancel)
    });
    onData(response.data);
  } catch (err) {
    onError(err);
  } finally {
    onLoad(false);
  }
}

/*
{
"mode": "aggregation",
"geom": [[10, 66, 25, 72]],
"tasks": [4310, 4110],
"metadatas" : [190605, 140335],
"attributes": [["metadatafile_id"], ["metadatafile_id"]],
"time_aggregation": false,
"detail": true,
"aggregates": ["sum"]
}
*/
// END POINTS: https://geodata.ireact.cloud/test/apigeoquery/getfeatures
// dev: https://geodata.ireact.cloud/dev/apigeoquery/getfeatures
