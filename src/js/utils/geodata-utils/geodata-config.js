/**
 * Get parameters for geodata api depending on task ID
 * @param {Number|String} taskId
 * @returns {Array<string>} attrs
 */
export function getTaskAttributesConfig(taskId) {
  let attrs = [];
  let _taskID =
    typeof taskId === 'string' ? parseInt(taskId, 10) : typeof taskId === 'number' ? taskId : NaN;
  if (isNaN(_taskID)) {
    throw new Error(`Invalid task id: ${taskId}`);
  }
  switch (_taskID) {
    // Extreme Weather
    case 4101:
    case 4102:
    case 4103:
    case 4104:
    case 4105:
    case 4106:
    case 4107:
    case 4108:
    case 4109:
    case 4115:
    case 4116:
    case 4110:
    case 4111:
    case 4112:
    case 4113:
    case 4114:
    case 4117:
    case 4118:
    case 4119:
      attrs = ['mean'];
      break;
    // Weather
    case 4121:
    case 4122:
    case 4123:
    case 4124:
      attrs = ['median', 'ProbBin_0', 'ProbBin_0-0.1', 'ProbBin_0.1-10', 'ProbBin_>10'];
      break;
    // Put other task definitions here

    default:
      break;
  }
  return attrs;
}
