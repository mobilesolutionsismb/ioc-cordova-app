export function isLocationEnabled() {
  if (IS_CORDOVA && cordova.plugins.diagnostic) {
    return new Promise((success, failure) => {
      cordova.plugins.diagnostic.isLocationEnabled(success, failure);
    });
  } else {
    return Promise.resolve(true);
  }
}
