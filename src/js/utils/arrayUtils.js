/**
 * var arr = [1,2,3] --> [null, 1, null, 2, null, 3, null]
 * @param {Array} arr
 */
export function alternateWithNullItems(arr) {
  return new Array(arr.length > 0 ? 2 * arr.length + 1 : 0)
    .fill(null)
    .map((e, i) => (i % 2 === 1 ? arr[(i + 1) / 2 - 1] : e));
}

export function heatMap(index, size) {
  if (size > 1) {
    const k = 120 / (size - 1);
    const v = index * k;
    return `hsl(${v},60%,50%)`;
  } else {
    return 'hsl(120,60%,40%)';
  }
}

export function colorMap(size) {
  return [...Array(size)].map((u, i) => heatMap(i, size)).reverse();
}

// Credits https://gist.github.com/vipickering/6552366
export function closest(array, num) {
  let i = 0;
  let minDiff = 1000;
  let ans;
  for (i in array) {
    const m = Math.abs(num - array[i]);
    if (m < minDiff) {
      minDiff = m;
      ans = array[i];
    }
  }
  return ans;
}

export function closestIndex(array, num) {
  return array.indexOf(closest(array, num));
}
