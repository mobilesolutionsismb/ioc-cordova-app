import React from 'react';

/**
 * Format the report content
 * @param {Object} reportProperties 
 * @param {Proxy} dictionary 
 */
export function formatReportContent(reportProperties, dictionary) {
    const reportContent = reportProperties[reportProperties.type]; // e.g. measure
    if (reportContent) {
        let contentString = '';
        try {
            switch (reportProperties.type) {
                case 'damage':
                    contentString = `${dictionary(reportContent.label)} - ${dictionary('_damage_degree')}: ${dictionary('_' + reportContent.degree)}`
                    break;
                case 'resource':
                    contentString = dictionary(reportContent);
                    break;
                case 'measure':
                    const reportContentNameLabel = dictionary(`_${reportContent.name}`);
                    const isQualitative = typeof reportContent.qualitative !== 'undefined' && reportContent.qualitative !== null;
                    const reportContentValue = isQualitative ? dictionary(`${reportContent.qualitative.label}`) : `${reportContent.quantitative.value} ${reportContent.quantitative.measureUnit}`;
                    contentString = `${reportContentNameLabel}: ${reportContentValue}`;
                    break;
                default:
                    contentString = dictionary('_unknown');
                    break;
            }
            return contentString;
        }
        catch (ex) {
            console.error('Exception', ex, reportContent);
            return dictionary('_no_report_content_available');
        }
    }
    else {
        return dictionary('_no_report_content_available');
    }
}

export default formatReportContent;

/**
 * Convert validation statuses to icons
 * This version uses emojis while waiting for final font set
 * Known values: "unknown", "inaccurate", "validated", "inappropriate", "submitted"
 * @param {Object} reportProperties 
 */
export function reportValidationStatus2Icon(reportProperties) {
    const reportStatus = reportProperties.status; // e.g. measure
    let icon = '❓';
    switch (reportStatus) {
        case 'inaccurate':
            icon = '⚠️';
            break;
        case 'validated':
            icon = '✅';
            break;
        case 'inappropriate':
            icon = '❌';
            break;
        case 'submitted':
            // icon = '📩';
            icon = '';
            break;
        case 'undefined':
        default:
            break;
    }
    return icon;
}

export function getValidationStatus(reportProperties/* , appUserType */, validationButton = null) {
    if(!reportProperties){
        return null;
    }
    // if (appUserType === 'authority') {
    if (reportProperties.voteEnabled && reportProperties.status === 'submitted') //TODO should we have a validationEnabled?
    {
        return validationButton;
    }
    else {
        return (<span className="validation-status">{reportValidationStatus2Icon(reportProperties)}</span>);
    }
    // }
    // else {
    //     return (<span className="validation-status">{reportValidationStatus2Icon(reportProperties)}</span>);
    // }
}