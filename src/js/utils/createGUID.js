//Generate a GUID using https://stackoverflow.com/a/2117523/1679665
const crypto = window.crypto || window.msCrypto;

export function createGUID() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        /* eslint-disable */
        // Would warn about mixed operators ^, & and >>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        /* eslint-enable */
    );
};
