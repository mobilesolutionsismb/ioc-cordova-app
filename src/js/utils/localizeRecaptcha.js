const HL_RX = /&hl=[a-z]{2}/;

window.wigetId = null;

function addRecaptchaScript(locale, id = 'recaptcha-script', done) {
    let nse = document.createElement('script');
    nse.id = 'recaptcha-script';
    nse.setAttribute('async', true);
    nse.setAttribute('defer', true);
    nse.src = `https://www.google.com/recaptcha/api.js?render=explicit&hl=${locale}`;
    document.body.appendChild(nse);
    done();
}

function cleanInjectedScripts() {
    return new Promise((success) => {
        let injectedScripts = Array. from (document.querySelectorAll('script[async]')).filter((s) => {
            return s.src.indexOf('https://www.gstatic.com/recaptcha/api2/') > -1;
        });
        if (injectedScripts.length > 1) {
            for (let i = 0; i < injectedScripts.length; i++) {
                injectedScripts[i].remove();
                if (i === injectedScripts.length - 1) {
                    success();
                }
            }
        } else {
            success();
        }
    });
}

function updateRecaptchaScript(locale, gscriptElement, done) {
    let id = gscriptElement.id;
    gscriptElement.remove();
    cleanInjectedScripts().then(() => {
        addRecaptchaScript(locale, id, done);
        console.log('Recaptcha updated', locale);
    });
}

function localizeRecaptcha(locale, id = 'recaptcha-script') {
    return new Promise((success) => {
        let gscriptElement = document.getElementById(id);
        if (gscriptElement) {
            let match = gscriptElement.src.match(HL_RX);
            let currentGscriptLanguage = match ? match[0].replace('&hl=', '') : null;
            if (currentGscriptLanguage === locale) {
                success();
            } else if (!currentGscriptLanguage || currentGscriptLanguage !== locale) {
                updateRecaptchaScript(locale, gscriptElement, success);
            }
        } else {
            addRecaptchaScript(locale, id, success);
            console.log('Recaptcha added', locale);
        }
    });
}

function renderRecaptcha(element, verifyCallback = undefined, theme = 'light', type = 'image', size = 'size', tabindex = '0', expiredCallback = undefined) {
    window.wigetId = window.grecaptcha.render(element.id, {
        sitekey: element.dataset.sitekey,
        callback: verifyCallback,
        theme: element.dataset.theme,
        type: element.dataset.type,
        size: element.dataset.size,
        tabindex: element.dataset.tabindex,
        'expired-callback': expiredCallback
    });
}

function resetRecaptcha() {
    if (window.widgetId) {
        window.grecaptcha.reset(window.widgetId);
        window.widgetId = null;
    }
}

export { localizeRecaptcha, cleanInjectedScripts, renderRecaptcha, resetRecaptcha };
export default localizeRecaptcha;