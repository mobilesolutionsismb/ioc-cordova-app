export const IS_IOS = IS_CORDOVA && cordova.platformId === 'ios';
export const IS_ANDROID = IS_CORDOVA && cordova.platformId === 'android';

const rxstart = /(\w|:|\/)+ireactbe-/i;
const rxend = /.azurewebsites.net$/i;

function getBackendEnv(beString) {
  if (rxstart.test(beString)) {
    return beString.replace(rxstart, '').replace(rxend, '');
  } else {
    return beString;
  }
}

/**
 * Information about this app as available from webpack and cordova
 */
const appInfo = {
  title: TITLE,
  packageName: PKG_NAME,
  description: DESCRIPTION,
  version: VERSION,
  buildDate: BUILD_DATE,
  environment: ENVIRONMENT,
  backendEnvironment: getBackendEnv(IREACT_BACKEND),
  geoServerURL: GEOSERVER_URL ? GEOSERVER_URL : 'from API',
  rasterLayersMode: USE_WMTS ? 'WMTS' : 'WMS',
  ...CORDOVA_APP_ATTRIBUTES
};
if (!IS_IOS) {
  delete appInfo['iosBundleVersion'];
}
if (!IS_ANDROID) {
  delete appInfo['androidApkVersion'];
}

export { appInfo };
