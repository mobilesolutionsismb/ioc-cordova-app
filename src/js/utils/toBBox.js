export function toBBox(latLngBounds) {
  //return [westLng, southLat, eastLng, northLat];
  return [
    latLngBounds.getWest(),
    latLngBounds.getSouth(),
    latLngBounds.getEast(),
    latLngBounds.getNorth()
  ];
}
