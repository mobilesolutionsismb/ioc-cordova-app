// type ConsoleType = 'log' | 'warn' | 'error' | 'debug';
const ConsoleTypes = ['log', 'warn', 'error', 'debug'];

/**
 *
 * @param {String} initialString
 * @param {String} style
 * @param {String} type value must be in ConsoleTypes
 */
function _logStyle(initialString, style, type = 'log') {
  if (ConsoleTypes.indexOf(type) === -1) {
    return;
  }
  if (IS_PRODUCTION) {
    return () => {};
  } else {
    // either lose log style or line no. - we choose the latter
    return (function() {
      return Function.prototype.bind.call(console[type], console, initialString, style);
    })();
  }
}

const mainStyle = 'color: #7FD5E0;';
const clusterWorkerStyle = 'color: #961279;';

const warnStyle = 'background: gold; color: black; font-weight: bold';
const severeWarningStyle = 'background: orangered; color: gold; font-weight: bold';
const fatalErrorStyle = 'background: darkred; color: white; font-weight: bold';

const errorStyle = 'background: #a50000; color: white; font-weight: bold';

export const logMain = _logStyle('%c I-REACT [🚒 Main]:', mainStyle);
export const logWarning = _logStyle('%c I-REACT [🚒 Main] Warning:', warnStyle, 'warn');
export const logSevereWarning = _logStyle(
  '%c I-REACT [🚒 Main] ⚠️ Warning ⚠️:',
  severeWarningStyle,
  'warn'
);
export const logError = _logStyle('%c I-REACT [🚒 Main] 😵 Error:', errorStyle, 'error');

export const logFatal = _logStyle(
  '%c I-REACT [🚒 Main] ☠️ Fatal Error ☠️:',
  fatalErrorStyle,
  'error'
);

export const logClusterWorker = _logStyle('%c I-REACT [🗺️ Cl. Worker]:', clusterWorkerStyle);
export const logClusterWorkerError = _logStyle(
  '%c I-REACT [🗺️ Cl. Worker] Error:',
  errorStyle,
  'error'
);
