export function b64ImageToBlob(b64ImageData) {
    const match = b64ImageData.match(/image\/(jpeg|png)/);
    if (match) {
        const data = b64ImageData.replace(/^data:image\/(jpeg|png);base64,/, '');
        const contentType = match[0];
        return b64toBlob(data, contentType);
    }
    else {
        return null;
    }
}

export function b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
};

export function blobToB64(blob) {
    return new Promise((succ, fail) => {
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onerror = function () {
            fail(reader.error);
        };
        reader.onloadend = function () {
            succ(reader.result);
        };
    });
}