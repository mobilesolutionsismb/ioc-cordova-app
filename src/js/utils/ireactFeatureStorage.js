import localForage from 'localforage';

// Same as API worker
const FEATURES_STORAGE_NAME = 'ireact-features';

export function getFeatureStore() {
  const featureStore = localForage.createInstance({
    name: FEATURES_STORAGE_NAME,
    storeName: FEATURES_STORAGE_NAME,
    description: 'storage for ireact GeoJSON features'
  });
  return featureStore;
}
