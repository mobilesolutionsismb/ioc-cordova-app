Hvis du ikke accepterer og accepterer følgende betingelser, skal du ikke bruge I-REACT MOBILE APP.

# I-REACT Vilkår og betingelser

Ved at vælge "Jeg accepterer" anerkender du, at du har **læst**, **forstået** og **accepteret** både _[Privacy Policy](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=da)_ og _[Servicevilkår](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=da)_.

_I-REACT sigter mod at minimere virkningerne af katastrofer ved at give borgerne mulighed for at dele aktive oplysninger i realtid for at holde dit lokalsamfund informeret om nødsituationer og samtidig modtage katastroferelateret information fra eksisterende systemer, herunder sociale medier (Twitter). I-REACT fremmer borgernes opmærksomhed og engagement i nødhåndtering ved at implementere en gamified approach._  
_Vi håndterer dine personlige data i overensstemmelse med den generelle databeskyttelsesforordning (GDPR) (EU) 2016/679 ("GDPR"). Bemærk at du vil bruge I-REACT appen på eget ansvar._  
_For at blive medlem af familien I-REACT skal du læse og acceptere vores privatlivspolitik og vores servicevilkår. Klik på nedenstående links, hvis du vil vide mere._

- Læs vores [fulde servicevilkår her](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=da).
- Læs vores [fulde privatlivspolitik her](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=da).
