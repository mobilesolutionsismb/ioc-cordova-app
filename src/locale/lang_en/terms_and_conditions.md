IF YOU DO NOT ACCEPT AND AGREE TO THE FOLLOWING TERMS THEN YOU MUST NOT USE THE I-REACT MOBILE APP.

# I-REACT Terms And Conditions

By selecting "I Agree", you acknowledge that you have **read**, **understood** and **accepted** both the _[Privacy Policy](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=en)_ and _[Terms of Service](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=en)_.

_I-REACT aims to minimize the impact of disasters by allowing citizens to share actionable information in real-time to keep your local community informed about emergencies, and at the same time to receive disaster related information from existing systems, including social media (Twitter). I-REACT promotes citizen awareness and engagement on emergency management by implementing a gamified approach._  
_We handle your personal data in compliance with the General Data Protection Regulation (GDPR) (EU) 2016/679 ("GDPR"). Note that you will use the I-REACT app under your own responsibility._  
_In order to become a member of the I-REACT family you need to read and accept our Privacy Policy and our Terms of Services. Click on the links below if you want to know more._

- Read our [full Terms of Service here](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=en).
- Read our [full Privacy Policy here](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=en).
