WENN SIE DIE GENANNTEN BEDINGUNGEN NICHT AKZEPTIEREN UND NICHT EINVERSTANDEN SIND, DÜRFEN SIE DIE I-REACT MOBILE APP NICHT BENUTZEN.

# I-REACT Allgemeine Geschäftsbedingungen

Wenn Sie "Ich stimme zu" auswählen, bestätigen Sie, dass Sie sowohl die _[Datenschutzerklärung](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=de)_ als auch die _[Nutzungsbedingungen](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=de)_ **gelesen**, **verstanden** und **akzeptiert** haben.

_I-REACT zielt darauf ab, die Auswirkungen von Katastrophen zu minimieren, indem die Bürger in Echtzeit umsetzbare Informationen austauschen können, um Ihre lokale Gemeinschaft über Notfälle zu informieren und gleichzeitig katastrophenbezogene Informationen von bestehenden Systemen zu erhalten, einschließlich sozialer Medien (Twitter). I-REACT fördert das Bewusstsein der Bürger und das Engagement für das Notfallmanagement durch die Implementierung eines gamifizierten Ansatzes._  
_Wir behandeln Ihre personenbezogenen Daten in Übereinstimmung mit der Datenschutz-Grundverordnung (DSGVO) (EU) 2016/679 ("DSGVO"). Beachten Sie, dass Sie die I-REACT App in Ihrer eigenen Verantwortung verwenden._  
_Um Mitglied der I-REACT-Familie zu werden, müssen Sie unsere Datenschutzrichtlinie und unsere Nutzungsbedingungen lesen und akzeptieren. Klicken Sie auf die folgenden Links, wenn Sie mehr erfahren möchten._

- Lesen Sie unsere vollständigen [Nutzungsbedingungen](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=de).
- Lesen Sie unsere vollständige [Datenschutzrichtlinie hier](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=de).
