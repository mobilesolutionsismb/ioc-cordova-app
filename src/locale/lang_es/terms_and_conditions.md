SI NO ACEPTA LOS TÉRMINOS SIGUIENTES, NO DEBE UTILIZAR LA APLICACIÓN MÓVIL I-REACT.

# I-REACT Términos y Condiciones

Al seleccionar "Aceptar", usted reconoce que ha **leído**, **entendido** y **aceptado** tanto la _[Política de privacidad](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=es)_ como los _[Términos de servicio](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=es)_.

_I-REACT tiene como objetivo minimizar el impacto de los desastres al permitir que los ciudadanos compartan información procesable en tiempo real para mantener a su comunidad local informada sobre las emergencias y al mismo tiempo recibir información relacionada con el desastre de los sistemas existentes, incluidas las redes sociales (Twitter). I-REACT promueve la concienciación y participación ciudadana en la gestión de emergencias mediante la implementación de un enfoque gamificado._  
_Tratamos sus datos personales de conformidad con el Reglamento General de Protección de Datos (GDPR) (UE) 2016/679 ("GDPR"). Tenga en cuenta que utilizará la aplicación I-REACT bajo su propia responsabilidad._  
_Para ser miembro de la familia I-REACT, debe leer y aceptar nuestra Política de privacidad y nuestros Términos de servicio. Haga clic en los enlaces de abajo si desea saber más._

- Lea nuestros [Términos de servicio completos aquí](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=es).
- Lea nuestra [Política de privacidad completa aquí](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=es).
