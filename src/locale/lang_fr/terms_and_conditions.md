SI VOUS N'ACCEPTEZ PAS LES CONDITIONS SUIVANTES, VOUS NE DEVEZ PAS UTILISER L'APPLICATION MOBILE I-REACT.

# I-REACT Termes et conditions

En sélectionnant "J'accepte", vous reconnaissez que vous avez **lu**, **compris** et **accepté** à la fois la _[Politique de confidentialité](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=fr)_ et les _[Conditions de service](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=fr)_.

_I-REACT vise à minimiser l'impact des catastrophes en permettant aux citoyens de partager des informations exploitables en temps réel afin de tenir votre communauté locale informée des urgences et, simultanément, de recevoir des informations relatives aux catastrophes des systèmes existants, y compris les médias sociaux (Twitter) . I-REACT encourage la sensibilisation des citoyens et leur engagement en matière de gestion des urgences en mettant en œuvre une approche ludique._  
_Nous traitons vos données personnelles conformément au règlement général sur la protection des données (RGPD) (UE) 2016/679 ("RGPD"). Notez que vous utiliserez l'application I-REACT sous votre propre responsabilité._  
_Pour devenir membre de la famille I-REACT, vous devez lire et accepter notre politique de confidentialité et nos conditions d'utilisation. Cliquez sur les liens ci-dessous si vous voulez en savoir plus._

- Lisez l'intégralité de nos [Conditions d'utilisation ici](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=fr).
- Lisez l'intégralité de notre [politique de confidentialité ici](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=fr).
