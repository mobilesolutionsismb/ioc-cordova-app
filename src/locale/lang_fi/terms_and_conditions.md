JOS ET HYVÄKSYT JA HYVÄKSYT SEURAAVILLE EHTEILLE, SEN EI KÄYTÄ I-REACT MOBILE APP: ää.

# I-REACT Ehdot ja ehdot

Valitsemalla "Hyväksyn", että olet **lukenut**, **ymmärretty** ja **hyväksynyt** sekä _[tietosuojakäytännön](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=fi)_ että _[valintapalvelun](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=fi)_.

_I-REACT pyrkii minimoimaan katastrofien vaikutukset antamalla kansalaisille mahdollisuuden jakaa toimivaa tietoa reaaliaikaisesti paikallisväestön tiedottamiseksi hätätilanteista ja samalla saada katastrofeihin liittyviä tietoja olemassa olevista järjestelmistä, mukaan lukien sosiaalinen media (Twitter) . I-REACT edistää kansalaisten tietoisuutta ja sitoutumista hätätilanteiden hallintaan panemalla täytäntöön pelottua lähestymistapaa._  
_Käsittelemme henkilötietojasi yleisen tietosuojasäädöksen (GDPR) (EU) 2016/679 ("GDPR") mukaisesti. Huomaa, että käytät I-REACT -sovellusta omalla vastuullasi._  
_Jotta voisimme liittyä I-REACT-perheeseen sinun täytyy lukea ja hyväksyä tietosuojakäytäntö ja käyttöehdot. Klikkaa alla olevia linkkejä, jos haluat tietää lisää._

- Lue koko [Käyttöehdot täältä](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=fi).
- Lue koko [Tietosuojakäytäntö](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=fi).
