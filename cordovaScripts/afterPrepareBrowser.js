const fs = require('fs-extra');
const path = require('path');
//Copy logo for browser platform splash
module.exports = function (context) {
    if (context.opts.platforms.length > 0 && context.opts.platforms.indexOf('browser') < 0) {
        return;
    }
    const ROOT = context.opts.projectRoot;
    const LOGO_SRC = path.join(ROOT, 'src', 'assets', 'logo', 'logo.png');
    const LOGO_DEST = path.join(ROOT, 'platforms', 'browser', 'www', 'img', 'logo.png');
    return fs.pathExists(LOGO_DEST).then(exists => {
        if (exists) {
            return Promise.resolve();
        }
        else {
            return fs.copy(LOGO_SRC, LOGO_DEST);
        }
    });
};