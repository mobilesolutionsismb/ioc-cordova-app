const fs = require('fs-extra');
const path = require('path');
//Copy styles.xml for android bar customization
// And copy push notifications icons
module.exports = function(context) {
  if (context.opts.platforms.length > 0 && context.opts.platforms.indexOf('android') < 0) {
    return;
  }
  const ROOT = context.opts.projectRoot;

  const STYLES_XML_SRC = path.join(ROOT, 'assets', 'android', 'styles.xml');
  const STYLES_XML_DEST = path.join(
    ROOT,
    'platforms',
    'android',
    'app',
    'src',
    'main',
    'res',
    'values',
    'styles.xml'
  );
  const copyStyles = fs.pathExists(STYLES_XML_DEST).then(exists => {
    if (exists) {
      return Promise.resolve();
    } else {
      return fs.copy(STYLES_XML_SRC, STYLES_XML_DEST);
    }
  });

  // const GRADLE_SETTINGS_SRC = path.join(ROOT, 'assets', 'android', 'build-extras.gradle');
  // const GRADLE_SETTINGS_DEST = path.join(ROOT, 'platforms', 'android', 'build-extras.gradle');
  // const copyGradleExt = fs.copy(GRADLE_SETTINGS_SRC, GRADLE_SETTINGS_DEST);

  const PN_ICONS_SRC = path.join(ROOT, 'assets', 'android', 'push_notifications_icons');
  const PN_ICONS_FORMATS = ['', '-hdpi', '-mdpi', '-xhdpi', '-xxhdpi', '-xxxhdpi'];
  const PN_ICON_NAME = 'ireact_pn.png';
  // const PN_ICONS_DEST = path.join(ROOT, 'platforms', 'android', 'res');
  const PN_ICONS_DEST = path.join(ROOT, 'platforms', 'android', 'app', 'src', 'main', 'res');

  const copyPNIcons = Promise.all(
    PN_ICONS_FORMATS.map(format => {
      const dest = path.join(PN_ICONS_DEST, `drawable${format}`, PN_ICON_NAME);
      return fs.pathExists(dest).then(exists => {
        if (exists) {
          return Promise.resolve();
        } else {
          const src = path.join(PN_ICONS_SRC, `drawable${format}`, PN_ICON_NAME);
          return fs.copy(src, dest);
        }
      });
    })
  );

  return Promise.all([copyStyles /* , copyGradleExt */, copyPNIcons]);
};
