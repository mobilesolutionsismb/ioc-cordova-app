const fs = require('fs-extra');
const path = require('path');

//Copy styles.xml for android bar customization
// And copy push notifications icons
module.exports = async function(context) {
  try {
    if (context.opts.platforms.length > 0 && context.opts.platforms.indexOf('ios') < 0) {
      console.log('Skipping ios script', context.opts.platforms);
      return;
    }
    const ROOT = context.opts.projectRoot;

    const XC_ASSETS_SRC = path.join(ROOT, 'assets', 'ios', 'Images.xcassets');
    const XC_ASSETS_DEST_CHECK = path.join(
      ROOT,
      'platforms',
      'ios',
      'IROC',
      'Images.xcassets',
      'ToolbarNavigation.imageset'
    );
    const XC_ASSETS_DEST = path.join(ROOT, 'platforms', 'ios', 'IROC', 'Images.xcassets');
    const exists = await fs.pathExists(XC_ASSETS_DEST_CHECK);
    if (exists) {
      console.log('Skipping ios files since they exist', XC_ASSETS_DEST_CHECK);
    } else {
      console.log('Copying ios files...');
    }
    const result = await fs.copy(XC_ASSETS_SRC, XC_ASSETS_DEST);
    return result;
  } catch (exc) {
    console.error('Exception caught', exc);
  } finally {
    console.log('Finished iOS After Prepare script');
  }
};
