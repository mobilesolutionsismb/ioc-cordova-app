process.traceDeprecation = true;
require('dotenv').config();

const webpack = require('webpack');
const eslintFormatter = require('react-dev-utils/eslintFormatter');
const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware');

const TerserPlugin = require('terser-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

const path = require('path');
const pkg = require('./package.json');

const fs = require('fs-extra');
const appDirectory = fs.realpathSync(process.cwd());

const getEnvConfiguration = require('./config/env').getEnvConfiguration;
const cordovaConfiguration = require('./config/cordovaConfiguration');
const webConfiguration = require('./config/webConfiguration');

const vendorChunkName = 'vendor';

async function createConfig() {
  try {
    const APP_MOUNT_ID = 'app';
    //See https://content-security-policy.com/
    //Validate through http://www.cspplayground.com/csp_validator
    const contentSecurityPolicy = await fs.readFile(
      path.join(appDirectory, 'content-security-policy.txt')
    );

    const envConfig = await getEnvConfiguration(
      appDirectory,
      pkg,
      process.env,
      APP_MOUNT_ID,
      require('./src/locale/languages.json')
    );

    let config = {};
    if (envConfig.IS_CORDOVA) {
      config = await cordovaConfiguration.getConfiguration(
        appDirectory,
        envConfig,
        contentSecurityPolicy,
        APP_MOUNT_ID
      );
    } else {
      config = await webConfiguration.getConfiguration(
        appDirectory,
        envConfig,
        contentSecurityPolicy,
        APP_MOUNT_ID
      );
    }

    const optimization = {
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]/,
            name: vendorChunkName,
            chunks: 'all'
          }
        }
      }
    };

    // Webpack plugins
    const plugins = [
      envConfig.DEFINITIONS,
      config.htmlPlugin,
      // CHUNKS_PLUGIN,
      new webpack.NamedModulesPlugin(),
      ...config.copyPlugins
    ];

    if (envConfig.IS_PRODUCTION) {
      plugins.push(
        new TerserPlugin({
          sourceMap: true,
          terserOptions: { ecma: 8 }
        })
      );
    } else {
      plugins.push(new CaseSensitivePathsPlugin());
    }

    // Webpack entry
    const entry = {
      app: ['blueimp-canvas-to-blob', 'babel-polyfill', path.join(appDirectory, 'src', 'main.js')]
    };

    if (!(envConfig.IS_PRODUCTION || envConfig.IS_CORDOVA)) {
      entry.app.splice(entry.length - 1, 0, require.resolve('react-dev-utils/webpackHotDevClient'));
    }

    const esLoaders = [
      {
        test: /\.json$/i,
        type: 'javascript/auto',
        loader: 'json-loader'
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto'
      }
    ];

    // Webpack loaders
    const eslintLoader = {
      test: /\.(js|jsx|mjs)$/,
      enforce: 'pre',
      use: [
        {
          options: {
            formatter: eslintFormatter,
            eslintPath: require.resolve('eslint')
          },
          loader: require.resolve('eslint-loader')
        }
      ],
      include: /(src)/
    };

    const sourceMapLoader = {
      test: /\.(js|jsx|mjs)$/,
      use: ['source-map-loader'],
      exclude: /(src)/,
      enforce: 'pre'
    };

    const getJSLoaders = require('./config/commons').getJSLoaders;
    const jsLoaders = {
      test: /\.(js|jsx|mjs)$/,
      exclude: /(node_modules)/,
      use: getJSLoaders(appDirectory, envConfig)
    };

    const assetsLoaders = [
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader?importLoaders=true', 'postcss-loader', 'sass-loader']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader?importLoaders=true', 'postcss-loader']
      },
      {
        test: /\.woff?(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.woff2?(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader'
      },
      {
        test: /\.ico(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader'
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
      },
      {
        test: /\.jpg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=25000&mimetype=image/jpeg'
      },
      {
        test: /\.png(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=25000&mimetype=image/png'
      }
    ];

    const module = {
      rules: [...esLoaders, eslintLoader, sourceMapLoader, jsLoaders, ...assetsLoaders]
    };

    // Webpack context
    const context = path.join(appDirectory, 'src');

    // Dev server config
    const devServer = {
      host: '0.0.0.0', //Any network interface,
      historyApiFallback: {
        rewrites: [{ from: /^\/*$/, to: '/index.html' }]
      },
      compress: true,
      disableHostCheck: true,
      // clientLogLevel: 'none',
      hot: true,
      // overlay: false,
      // quiet: true,
      before(app) {
        // This lets us open files from the runtime error overlay.
        app.use(errorOverlayMiddleware());
      }
    };

    // Libraries which are excluded from bundling
    const externals = {
      'mapbox-gl': 'window["mapboxgl"]',
      numeral: 'window["numeral"]'
    };

    const output = {
      // Add /* filename */ comments to generated require()s in the output.
      pathinfo: !envConfig.IS_PRODUCTION,
      path: config.outputPath,
      filename: '[name].js',
      publicPath: envConfig.IS_CORDOVA ? undefined : envConfig.PUBLIC_PATH,
      sourceMapFilename: '[name].map',
      globalObject: 'this'
    };

    // Resolve
    const resolve = {
      extensions: [
        '.js', // automatically in webpack 2
        '.jsx',
        '.json', // automatically in webpack 2
        '.scss',
        '.css'
      ],
      alias: {
        'ioc-api-interface': envConfig.IS_PRODUCTION
          ? 'ioc-api-interface/dist/index.min.js'
          : 'ioc-api-interface/dist/index.js'
      },
      modules: ['node_modules', path.resolve(appDirectory, 'node_modules'), 'src'],
      plugins: [
        // Prevents users from importing files from outside of src/ (or node_modules/).
        // This often causes confusion because we only process files within src/ with babel.
        // To fix this, we prevent you from importing files out of src/ -- if you'd like to,
        // please link the files into your node_modules/ and let module-resolution kick in.
        // Make sure your source files are compiled, as they will not be processed in any way.
        // new ModuleScopePlugin(path.resolve(appDirectory, 'src'), [
        //   path.resolve(appDirectory, 'package.json')
        // ])
      ]
    };

    // DevTool (source maps)
    // TODO use 'nosources-source-map' in production when stable
    const devtool = envConfig.IS_PRODUCTION ? 'source-map' : 'inline-source-map';

    const mode = envConfig.IS_PRODUCTION ? 'production' : 'development';

    // Final config
    const webpackConfig = {
      context,
      entry,
      optimization,
      mode,
      devServer,
      output,
      externals,
      resolve,
      plugins,
      devtool,
      module
    };
    return webpackConfig;
  } catch (err) {
    console.error('Configuration error', err);
    process.exit(1);
  }
}
module.exports = createConfig();
